export interface TaskErrors {
  title?: string;
  assignee?: string;
  cluster?: string;
  project?: string;
  dates?: string;
}

export interface LinkErrors {
  cluster?: string;
  project?: string;
  task?: string;
}

export interface MeetingErrors {
  title?: string;
  description?: string;
  endTime?: string;
  startTime?: string;
  secretary?: string;
}
