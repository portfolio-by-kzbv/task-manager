export interface ChatRequest {
  channel_id: string;
  channel_name: string;
  command: string;
  response_url: string;
  team_domain: string;
  team_id: string;
  text: string;
  token: string;
  trigger_id: string;
  user_id: string;
  user_mentions: string;
  user_mentions_ids: string;
  user_name: string;
}

// Full list https://developers.mattermost.com/integrate/slash-commands/
export interface ChatResponse {
  text: string;
  response_type?: 'ephemeral' | 'in_channel';
  username?: string;
  channel_id?: string;
  icon_url?: string;
  goto_location?: string;
  attachments?: string;
  type?: string;
  extra_responses?: ChatResponse[];
  skip_slack_parsing?: boolean;
  props?: any;
}
