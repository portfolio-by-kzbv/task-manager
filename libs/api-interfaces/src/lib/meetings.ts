export interface Meeting {
  id: string;
  title: string;
  description: string;
  startTime: string;
  endTime: string;
  createdAt: string;
  updatedAt: string;
  owner: string;
  secretary?: string;
  participants: string[];
  externalParticipants: string[];
  ownerEmail: string;
  location: string;
  videoConfUrl: string;
  recurrence: string;
  recurringEventId: string;
  status: string;
}

export interface MeetingDTO {
  id: null;
  title: string;
  description: string;
  startTime: Date;
  endTime: Date;
  participants: string[];
  secretary: string;
  owner: string;
  ownerEmail: string;
  externalParticipants: string[];
  recurrence: string | null;
}

export interface MeetingState {
  state: string;
  timeToNowLabel: string;
  hours: string;
  date: string;
}

export interface MoMDTO {
  content: string;
  meeting_id: string;
  author_id: string;
}

export interface MoM {
  id?: number;
  content: string;
  meeting_id: string;
  author_id: string;
  updatedAt?: Date;
  createdAt?: Date;
}

export interface MoMQueryParams {
  meeting_id: string;
  author_id?: string;
}

export interface NotesDTO {
  content: string;
  meeting_id: string;
  author_id: string;
}

export interface Notes {
  id?: number;
  content: string;
  meeting_id: string;
  author_id: string;
  updatedAt?: Date;
  createdAt?: Date;
}

export interface NotesQueryParams {
  meeting_id: string;
  author_id: string;
}

export interface MeetingSettings {
  meeting_id: string;
  secretary_id: string;
}

export interface MeetingSettingsDTO {
  meeting_id: string;
  secretary_id: string;
}
