export interface User {
  id: string;
  displayName: string;
  firstName: string;
  lastName: string;
  jobTitle: string;
  workEmail: string;
  photoUrl: string | null;
}

export interface UserResponse {
  total: number;
  entities: User[];
}

export interface Profile {
  avatar: {
    original: string;
    normal: string;
    thumbnail: string;
  };
  bucket_id: string;
  city: string;
  country: string;
  date_of_birth: string | null;
  email: string;
  email_verified: false;
  first_name: string;
  gender: string;
  id: string;
  last_name: string;
  mfa_type: string;
  nickname: string;
  phone_number: string;
  phone_number_verified: boolean;
  pin: string;
  time_zone: string;
  user_identities: {
    bucket_id: string;
    connection_type: string;
    provider: string;
    type: string;
    user_id: string;
  };
}

export interface UserMock {
  id: string;
  avatar: string;
  email: string;
  firstName: string;
  fullName: string;
  lastName: string;
  logName: string;
  phoneNumber: string;
  role: string;
  company?: string;
}
