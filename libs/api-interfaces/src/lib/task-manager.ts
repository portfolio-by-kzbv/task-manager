import { User } from '@dar/api-interfaces';

export interface Task {
  id?: number;
  group_id?: string;
  project_id?: string;
  company_id?: number;
  creator_id: string;
  reporter_id?: string;
  assignee_id: string;
  title: string;
  description?: string;
  status?: string;
  status_id?: number;
  priority?: string;
  start_time: string;
  end_time: string;
  cluster?: Cluster;
  project?: Project;
  menu?: any;
  assignee?: User;
}

export interface TasksQueryParams {
  assignee_id?: string;
  creator_id?: string;
  status_id?: number | number[];
  group_id?: string;
  project_id?: string;
  query?: string;
  operator_or?: boolean;
  user_id?: string;
  start_time_min?: string;
  end_time_max?: string;
}

export interface TaskDTO {
  creator_id: string;
  assignee_id: string;
  title: string;
  description: string;
  status: string;
  group_id?: string;
  project_id?: string;
  status_id: number;
  priority: string;
  start_time: string;
  end_time: string;
}

export interface StatusDTO {
  status_id: number;
}

export interface TaskActionStatus {
  status: number;
  error: number;
}

export enum TaskStatusTypes {
  INITIAL = 'Initial',
  NEW = 'New',
  IN_PROGRESS = 'In progress',
  IN_REVIEW = 'In review',
  COMPLETED = 'Completed',
  CANCELLED = 'Cancelled',
  HOLD = 'Hold',
}
export interface TaskStatus {
  id: number;
  name: TaskStatusTypes;
  color: string;
  transitions: string;
}

export interface Todo {
  id?: number;
  task_id: number;
  title: string;
  is_done: boolean;
  due_time?: Date;
}

export interface TodoDTO {
  id?: number;
  task_id: number;
  title: string;
  is_done: boolean;
}

export interface TodoQueryParams {
  task_id?: number;
}

export interface Project {
  id: string;
  entityType: 'PROJECT';
  name: string;
  status: string;
}

export interface Cluster {
  id: string;
  entityType: 'CLUSTER';
  name: string;
  nameRu: string;
  description: string;
  status: string;
}

export interface LinksRequest {
  task_id: number;
  dependent_task_id: number;
}

export interface LinksResponse {
  affects: number[];
  depends: number[];
}

export interface Comment {
  id?: number;
  task_id: number;
  parent_id?: number;
  user_id: string;
  user_name: string;
  text: string;
  created_at?: Date;
}

export interface CommentsQueryParams {
  task_id: number;
}

export interface Attachment {
  id: number;
  task_id: number;
  s3_bucket: string;
  bucket_id: string;
  user_id: string;
  name: string;
  extension: string;
  content_type: string;
  size: number;
  comment_id?: number;
  link?: string;
  error?: string;
  isLoading?: boolean;
  loadingProgress?: number;
}

export interface AttachmentDTO {
  attachment: File;
}

export interface AttachmentURL {
  link: string;
}

export interface AttachmentsQueryParams {
  task_id: number;
}
