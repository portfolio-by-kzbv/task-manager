FROM node:12-alpine as build-stage
ARG ENV
ARG NPM_TOKEN
ENV PROJECT_ENV=${ENV}
WORKDIR /app

RUN npm config set '//registry.npmjs.org/:_authToken' ${NPM_TOKEN}
RUN npm config set scope dartech

COPY package*.json ./
RUN npm ci

COPY . .

RUN npm run-script build:api:${PROJECT_ENV}
RUN npm run-script build:front:${PROJECT_ENV}
RUN npm set unsafe-perm true
RUN npm install pm2@latest -g

EXPOSE 8080
CMD ["pm2-runtime", "ecosystem.config.js"]
