import React, { useEffect, useState, useMemo, Suspense } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { Box, Grid } from '@material-ui/core';
import ErrorOutlineIcon from '@material-ui/icons/ErrorOutline';

import PageHeader from '../components/PageHeader';
import Spinner from '../components/Spinner/Spinner';
import useMeetingById from '../hooks/useMeetingById';
import { getMeeting } from '../store/actions/meetings.action';
import NoData from '@dar/components/NoData/NoData';
import { Dispatch as CustomDispatch } from '@dar/api-interfaces';

const JitsiMeeting = React.lazy(() => import('../components/Jitsi/JitsiMeeting'));

function MeetingPage() {
  const [loading, setLoading] = useState(false);
  const { id, type } = useParams<{ id: string; type: string }>();
  const dispatch: CustomDispatch = useDispatch();
  const meeting = useMeetingById(id);

  useEffect(() => {
    setLoading(true);
    dispatch(getMeeting(id))
      .finally(() => {
        setLoading(false);
      });
  }, [id, dispatch]);

  const renderCall = () => {
    if (type === 'jitsi') {
      return <JitsiMeeting meeting={meeting} />;
    } else {
      return <div>Something went wrong</div>;
    }
  };

  return loading ? (
    <Spinner fullscreen />
  ) : meeting ? (
    <Grid container spacing={3}>
      <Grid item xs={12}>
        <PageHeader title={meeting.title} />
      </Grid>
      <Grid item xs={12}>
        <Suspense fallback={<Spinner fullscreen />}>{renderCall()}</Suspense>
      </Grid>
    </Grid>
  ) : (
    <Box height="100%" display="flex" justifyContent="center">
      <NoData text={`There is no meeting with ID: ${id}`} Icon={ErrorOutlineIcon} />
    </Box>
  );
}

export default MeetingPage;
