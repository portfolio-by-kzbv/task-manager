import React, { useState, useContext } from 'react';
import { useDispatch } from 'react-redux';
import { useHistory, useParams } from 'react-router-dom';

import { Grid, Typography, ButtonGroup, Button, Box, makeStyles } from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import ListIcon from '@material-ui/icons/List';
import AppsIcon from '@material-ui/icons/Apps';
import HistoryIcon from '@material-ui/icons/History';
import TasksView from '../components/TaskManager/TasksView/TasksView';
import Spinner from '../components/Spinner/Spinner';
import CreateTask from '../components/TaskManager/CreateTask/CreateTask';

import PromptModal from '@dar/components/Modal/PromptModal/PromptModal';
import TaskManagerModalsContext from '../contexts/TaskManagerModalsContext';
import { deleteTask } from '../store/actions/tasks.action';
import { useTasksState } from '../selectors/tasks.selector';
import { Dispatch } from '@dar/api-interfaces';

const TASKS_PATH = '/productivity/tasks';

const useStyles = makeStyles((theme) => ({
  button: {
    padding: '0',
    height: '40px',
    backgroundColor: theme.palette.grey[50],
    borderColor: theme.palette.grey[300],
  },
  activeButton: {
    backgroundColor: theme.palette.grey[200],
    borderColor: theme.palette.grey[400],
  },
}));

function TaskManagerPage() {
  const { taskID, deleteModalOpen, deleteModalToggle, detailsModalOpen, handleDetailsModalClose } = useContext(
    TaskManagerModalsContext
  );
  const [openCreateTaskDrawer, setOpenCreateTaskDrawer] = useState(false);
  const [view, setView] = useState<'list' | 'kanban' | 'calendar'>('list');
  const dispatch: Dispatch = useDispatch();
  const history = useHistory();
  const classes = useStyles();
  const { loading } = useTasksState();

  const handleDelete = () => {
    if (detailsModalOpen) {
      history.push(TASKS_PATH);
      handleDetailsModalClose();
    }
    deleteModalToggle();
    dispatch(deleteTask(taskID));
  };

  return (
    <React.Fragment>
      {loading && <Spinner absolute />}
      <CreateTask openDrawer={openCreateTaskDrawer} onClose={() => setOpenCreateTaskDrawer(false)} />

      <Box
        display="flex"
        flexWrap="nowrap"
        alignItems="center"
        flexDirection="row"
        style={{ width: '100%', marginBottom: '16px' }}
      >
        <Grid item container lg={8} xs={12}>
          <Typography variant="h1">Tasks</Typography>
        </Grid>

        <Grid item container lg={4} xs={12}>
          <Box
            display="flex"
            flexWrap="nowrap"
            alignItems="center"
            justifyContent="flex-end"
            width="100%"
            marginTop={0}
          >
            <Box marginRight={4}>
              <ButtonGroup variant="outlined" orientation="horizontal" color="secondary">
                <Button
                  className={`${classes.button} ${view === 'list' ? classes.activeButton : ''}`}
                  aria-label="list-view"
                  onClick={() => setView('list')}
                >
                  <ListIcon fontSize="small" />
                </Button>
                <Button
                  className={`${classes.button} ${view === 'kanban' ? classes.activeButton : ''}`}
                  aria-label="kanban-view"
                  onClick={() => setView('kanban')}
                >
                  <AppsIcon fontSize="small" />
                </Button>
              </ButtonGroup>
            </Box>
            <Button
              variant="contained"
              color="primary"
              size="large"
              startIcon={<AddIcon />}
              onClick={() => setOpenCreateTaskDrawer(true)}
            >
              New Task
            </Button>
          </Box>
        </Grid>
      </Box>
      <TasksView view={view} />
      <PromptModal
        open={deleteModalOpen}
        onClose={deleteModalToggle}
        onCancel={deleteModalToggle}
        onConfirm={handleDelete}
        propmtType="danger"
        title="Delete Task?"
        text="A deleted task cannot be recovered."
        confirmText="Delete"
        confirmColor="secondary"
      />
    </React.Fragment>
  );
}

export default TaskManagerPage;
