import React, { useEffect, useState, useMemo } from 'react';
import { Grid } from '@material-ui/core';

import CreateMeetingCard from '../components/Meetings/CreateMeeting/CreateMeetingCard';
import MeetingsList from '../components/Meetings/MeetingsList/MeetingsList';
import SearchMeetings from '../components/Meetings/SearchMeetings/SearchMeetings';
import MeetingDetails from '../components/Meetings/MeetingDetails/MeetingDetails';

import queryString from 'query-string';
import { useDispatch } from 'react-redux';
import { useHistory, useLocation } from 'react-router-dom';
import useMeetingById from '../hooks/useMeetingById';
import { useMeetingsState, useMeetingsMeta } from '../selectors/meetings.selector';
import { listMeetings } from '../store/actions/meetings.action';
import useInterval from '../hooks/useInterval';

import { Meeting, Dispatch as CustomDispatch } from '@dar/api-interfaces';

const MEETINGS_PATH = '/productivity/meetings';
const MEETINGS_POLLING_INTERVAL = 60000;

function MeetingsPage() {
  const dispatch: CustomDispatch = useDispatch();
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  const location = useLocation();
  const meetings = useMeetingsState();
  const { q: query } = useMeetingsMeta();
  const modalId = useMemo(() => {
    const query = queryString.parse(location.search);
    return query.id ? query.id : null;
  }, [location]);
  const modalMeeting = useMeetingById(modalId);
  const modalOpen = useMemo(() => Boolean(modalId && modalMeeting), [modalId, modalMeeting]);
  const [filterMeetings, setFilterMeetings] = useState<Meeting[]>(meetings);
  useEffect(() => {
    setLoading(true);
    const sortedMeetingsByQuery = query
      ? meetings.filter(
          (meeting) =>
            meeting.title.toLowerCase().includes(query.toLowerCase()) ||
            meeting.description?.toLowerCase().includes(query.toLowerCase())
        )
      : meetings;
    setFilterMeetings(sortedMeetingsByQuery);
    setLoading(false);
  }, [query, meetings]);

  useEffect(() => {
    setLoading(true);
    dispatch(listMeetings()).then(() => setLoading(false));
  }, [dispatch]);

  useInterval(() => dispatch(listMeetings()), MEETINGS_POLLING_INTERVAL);

  const onModalClose = () => history.push(MEETINGS_PATH);
  return (
    <div>
      <Grid container spacing={2}>
        <Grid item xs={12}>
          <h1>Meetings</h1>
        </Grid>
      </Grid>
      <Grid container spacing={2}>
        <Grid item container lg={8} xs={12} spacing={2}>
          <Grid item xs={12}>
            <SearchMeetings />
          </Grid>
          <Grid item xs={12}>
            <MeetingsList loading={loading} old={false} meetings={filterMeetings} />
          </Grid>
          <Grid item xs={12}>
            <MeetingsList loading={loading} old={true} meetings={filterMeetings} />
          </Grid>
        </Grid>
        <Grid item lg={4} xs={12}>
          <CreateMeetingCard />
        </Grid>
      </Grid>
      {modalOpen && <MeetingDetails open={modalOpen} onClose={onModalClose} meeting={modalMeeting} onDelete={null} />}
    </div>
  );
}

export default MeetingsPage;
