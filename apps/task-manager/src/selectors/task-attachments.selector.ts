import { AppState } from '../store/reducers';
import { useSelector } from 'react-redux';

export const selectAttachments = (state: AppState) => state.attachments;

export const selectAttachment = (id: number) => (state: AppState) => state.attachments.attachments.find(attachment => attachment.id === id);

export const useAttachmentsState = () => useSelector(selectAttachments);

export const useAttachmentState = (id: number) => useSelector(selectAttachment(id));
