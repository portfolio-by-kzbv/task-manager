import { useSelector } from 'react-redux';
import { AppState } from '../store/reducers';

export const selectMeetingNotes = (state: AppState) => state.meetingNotes;

export const useMeetingNotesState = () => useSelector(selectMeetingNotes);
