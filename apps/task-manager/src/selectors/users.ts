import { useSelector } from 'react-redux';
import { AppState } from '../store/reducers';
import { User, Profile } from '@dar/api-interfaces';

export const selectUserSession = (state: AppState) => {
  const profile: Profile = JSON.parse(window.localStorage.getItem('profile'));
  return state.users.users.find((user: User) => user.id === profile.id);
};

export const selectUsers = (state: AppState) => state.users.users;

export const selectUser = (id: string) => (state: AppState) => state.users.users.find((user: User) => user.id === id);

export const useUsersState = () => useSelector(selectUsers);

export const useUserState = (id: string) => useSelector(selectUser(id));

export const useUserSessionState = () => useSelector(selectUserSession);
