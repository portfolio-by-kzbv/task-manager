import { useSelector } from 'react-redux';
import { AppState } from '../store/reducers';

export const selectChecklist = (state: AppState) => state.checklist;

export const selectTaskChecklist = (id: number) => (state: AppState) =>
  state.checklist.checklist.find(todo => todo.id === id);

export const useChecklistState = () => useSelector(selectChecklist);

export const useTaskChecklist = (id: number) => useSelector(selectTaskChecklist(id));
