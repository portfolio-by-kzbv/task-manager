import { useSelector } from 'react-redux';

/**
 * @param {AppState} state
 * @return {Meeting[]}
 */
export const selectMeetings = state => {
  return state.meetings.meetings;
};

export const selectMeeting = id => state =>
  state.meetings.meetings.find(meeting => meeting.id === id);

export const selectMeetingsMeta = state => ({
  q: state.meetings.q,
  total: state.meetings.total
});

export const selectNewMeetingsCount = state => state.meetings.newMeetingsCount;

/**
 * React custom hook
 * @return {Meeting[]}
 */
export const useMeetingsState = () => useSelector(selectMeetings);

export const useMeeting = id => useSelector(selectMeeting(id));

export const useMeetingsMeta = () => useSelector(selectMeetingsMeta);

export const useNewMeetingsCount = () => useSelector(selectNewMeetingsCount);
