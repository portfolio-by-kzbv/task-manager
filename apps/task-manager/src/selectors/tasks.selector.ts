import { useSelector } from 'react-redux';
import { AppState } from '../store/reducers';

const selectTasks = (state: AppState) => state.tasks;

export const selectTask = (id: number) => (state: AppState) => state.tasks.tasks.find(task => task.id === id);

export const useTasksState = () => useSelector(selectTasks);

export const useTask = (id: number) => useSelector(selectTask(id));
