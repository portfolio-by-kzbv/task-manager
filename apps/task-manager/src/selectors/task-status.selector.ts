import { useSelector } from 'react-redux';
import { AppState } from '../store/reducers';

const selectStatuses = (state: AppState) => state.statuses;

export const useStatusesState = () => useSelector(selectStatuses);
