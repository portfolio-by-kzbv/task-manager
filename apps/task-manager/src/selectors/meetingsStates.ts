import { useSelector } from 'react-redux';

/**
 * @param {AppState} state
 * @return {object}
 */
export const selectMeetingsStates = state => state.meetingsStates;

export const selectMeetingStateById = id => state => selectMeetingsStates(state)[id] || null;

export const useMeetingsStates = () => useSelector(selectMeetingsStates);
/**
 * @param {string} id
 * @return {{state: string, timeToNowLabel: string} | null}
 */
export const useMeetingState = id => useSelector(selectMeetingStateById(id));
