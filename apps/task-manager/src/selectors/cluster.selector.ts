import { useSelector } from 'react-redux';
import { AppState } from '../store/reducers';

export const selectClusters = (state: AppState) => state.clusters;

export const useClusters = () => useSelector(selectClusters);
