import { useSelector } from 'react-redux';
import { AppState } from '../store/reducers';

export const selectMeetingMom = (state: AppState) => state.meetingMom;

export const useMeetingMomState = () => useSelector(selectMeetingMom);
