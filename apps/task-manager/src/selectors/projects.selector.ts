import { useSelector } from 'react-redux';
import { AppState } from '../store/reducers';

export const selectProjects = (state: AppState) => state.projects;

export const useProjects = () => useSelector(selectProjects);
