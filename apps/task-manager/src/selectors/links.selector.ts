import { AppState } from '../store/reducers';
import { useSelector } from 'react-redux';

export const selectLinks = (state: AppState) => state.links;

export const useLinks = () => useSelector(selectLinks);
