import { AppState } from '../store/reducers';
import { useSelector } from 'react-redux';

export const selectComments = (state: AppState) => state.comments;

export const selectComment = (id: number) => (state: AppState) => state.comments.comments.find(comment => comment.id === id);

export const useCommentsState = () => useSelector(selectComments);

export const useCommentState = (id: number) => useSelector(selectComment(id));
