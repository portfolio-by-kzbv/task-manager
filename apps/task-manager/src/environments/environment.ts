// This file can be replaced during build by using the `fileReplacements` array.
// When building for production, this file is replaced with `environment.prod.ts`.

export const environment = {
  production: false,
  envName: 'local',
  authBffRoot: 'http://localhost:3330',
  griffonRedirectUrl: 'http://localhost:3330/api/auth/griffon/callback',
  griffonApiRoot: 'https://griffon.dar-dev.zone/api/v1',
  griffonClientId: 'bc087072-9341-480a-a225-7c6d29f9bc05',
  baseApiRoot: 'http://localhost:3333/api',
  assetsBase: 'http://localhost:4201/assets',
  AWSBase: 'https://s3.eu-west-1.amazonaws.com',
  dmsCommsBucket: 'dev-dms-communications-task-api-bucket'
};
