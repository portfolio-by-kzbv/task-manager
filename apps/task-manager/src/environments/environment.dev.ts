// This file can be replaced during build by using the `fileReplacements` array.
// When building for production, this file is replaced with `environment.prod.ts`.

export const environment = {
  production: true,
  envName: 'development',
  authBffRoot: 'https://dms-layout.dar-dev.zone',
  griffonRedirectUrl:
    'http://dms-layout.dar-dev.zone/api/auth/griffon/callback',
  griffonApiRoot: 'https://griffon.dar-dev.zone/api/v1',
  griffonClientId: 'bc087072-9341-480a-a225-7c6d29f9bc05',
  baseApiRoot: 'https://comms.dar-dev.zone/api',
  assetsBase: 'https://comms.dar-dev.zone/assets',
  AWSBase: 'https://s3.eu-west-1.amazonaws.com',
  dmsCommsBucket: 'dev-dms-communications-task-api-bucket'
};
