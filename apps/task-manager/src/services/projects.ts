import { Project } from '@dar/api-interfaces';
import { environment } from '../environments/environment';
import { taskManagerApiClient } from './clients';

const getAllProjects = () => {
  return taskManagerApiClient
    .get<Project[]>(`${environment.baseApiRoot}/task-manager/projects`)
    .then((res) => res.data);
};

const getProject = (project_id: string) => {
  return taskManagerApiClient
    .get<Project>(`${environment.baseApiRoot}/task-manager/projects/${project_id}`)
    .then((res) => res.data);
};

export default {
  getAllProjects,
  getProject,
};
