import { Attachment, AttachmentsQueryParams } from '@dar/api-interfaces';
import { environment } from '../environments/environment';
import { taskManagerApiClient } from './clients';
import { saveAs } from 'file-saver';
const getAttachments = (params: AttachmentsQueryParams) => {
  return taskManagerApiClient
    .get<Attachment[]>(`${environment.baseApiRoot}/task-manager/tasks/attachments`, { params })
    .then((res) => res.data);
};

const getAttachment = (attachment_id: number) => {
  return taskManagerApiClient
    .get<Attachment>(`${environment.baseApiRoot}/task-manager/tasks/attachments/${attachment_id}`)
    .then((res) => res.data);
};

const uploadCommentAttachment = (task_ID: number, comment_id: number, data) => {
  return taskManagerApiClient
    .post<Attachment>(`${environment.baseApiRoot}/task-manager/tasks/${task_ID}/attachments/${comment_id}`, data)
    .then((res) => res.data);
};

const uploadAttachment = (task_ID: number, data) => {
  return taskManagerApiClient
    .post<Attachment>(`${environment.baseApiRoot}/task-manager/tasks/${task_ID}/attachments/`, data)
    .then((res) => res.data);
};

const deleteAttachment = (attachment_id: number) => {
  return taskManagerApiClient
    .delete<Attachment>(`${environment.baseApiRoot}/task-manager/tasks/attachments/${attachment_id}`)
    .then((res) => res.data);
};

const downloadAttachment = (link: string, filename: string) => {
  saveAs(link, filename);
};

export default {
  getAttachments,
  getAttachment,
  uploadAttachment,
  uploadCommentAttachment,
  deleteAttachment,
  downloadAttachment,
};
