import {
  Meeting,
  MeetingDTO,
  MeetingSettings,
  MeetingSettingsDTO,
  MoM,
  MoMDTO,
  MoMQueryParams,
  Notes,
  NotesDTO,
  NotesQueryParams,
} from '@dar/api-interfaces';
import { environment } from '../environments/environment';
import { getMeetingIdWithoutTimestamp } from '../utils/meetings';
import { resetMeetingsInStorage } from '../utils/storage';
import { meetingApiClient } from './clients';
export const ENDPOINT_KEY = '/meetings';
export const NOTE_KEY = 'notes';
export const MOM_KEY = 'mom';
export const RECORDINGS_KEY = 'recordings';

const getMeeting = (id: string) => {
  return meetingApiClient.get(`${environment.baseApiRoot}/meetings/${id}`).then((res) => res.data);
};

const list = () => {
  return meetingApiClient
    .request({
      method: 'GET',
      url: `${environment.baseApiRoot}/meetings`,
    })
    .then((res) => res.data);
};

const create = (data: MeetingDTO) => {
  return meetingApiClient
    .request({
      method: 'POST',
      url: `${environment.baseApiRoot}/meetings`,
      data,
    })
    .then((res) => {
      resetMeetingsInStorage();
      return res.data;
    });
};

const update = (data: Meeting) => {
  return meetingApiClient
    .request({
      method: 'PUT',
      url: `${environment.baseApiRoot}/meetings`,
      data,
    })
    .then((res) => res.data);
};

const remove = (id: string) => {
  return meetingApiClient
    .request({
      method: 'DELETE',
      url: `${environment.baseApiRoot}/meetings/${id}`,
    })
    .then((res) => res.data);
};

const getNoteList = async (queryParams: NotesQueryParams) => {
  const params = { ...queryParams, meeting_id: getMeetingIdWithoutTimestamp(queryParams.meeting_id) };

  return meetingApiClient
    .get<Notes[]>(`${environment.baseApiRoot}/meeting-manager/notes`, { params })
    .then((res) => res.data);
};

const createNote = async (dataObj: NotesDTO) => {
  const data = { ...dataObj, meeting_id: getMeetingIdWithoutTimestamp(dataObj.meeting_id) };

  return meetingApiClient.post(`${environment.baseApiRoot}/meeting-manager/notes`, data).then((res) => res.data);
};

const updateNote = async (noteId: number, dataObj: NotesDTO) => {
  const data = { ...dataObj, meeting_id: getMeetingIdWithoutTimestamp(dataObj.meeting_id) };

  return meetingApiClient
    .put(`${environment.baseApiRoot}/meeting-manager/notes/${noteId}`, data)
    .then((res) => res.data);
};

const removeNote = async (noteId: number) => {
  return meetingApiClient.delete(`${environment.baseApiRoot}/meeting-manager/notes/${noteId}`).then((res) => res.data);
};

const getMoM = async (queryParams: MoMQueryParams) => {
  const params = { ...queryParams, meeting_id: getMeetingIdWithoutTimestamp(queryParams.meeting_id) };

  return meetingApiClient
    .get<MoM>(`${environment.baseApiRoot}/meeting-manager/mom`, { params })
    .then((res) => res.data);
};

const createMoM = async (dataObj: MoMDTO) => {
  const data = { ...dataObj, meeting_id: getMeetingIdWithoutTimestamp(dataObj.meeting_id) };

  return meetingApiClient.post(`${environment.baseApiRoot}/meeting-manager/mom`, data).then((res) => res.data);
};

const updateMoM = async (momId: number, dataObj: MoMDTO) => {
  const data = { ...dataObj, meeting_id: getMeetingIdWithoutTimestamp(dataObj.meeting_id) };

  return meetingApiClient.put(`${environment.baseApiRoot}/meeting-manager/mom/${momId}`, data).then((res) => res.data);
};

const removeMoM = async (momId: number) => {
  return meetingApiClient.delete(`${environment.baseApiRoot}/meeting-manager/mom/${momId}`).then((res) => res.data);
};

const getSettings = async (id: string) => {
  return meetingApiClient
    .get<MeetingSettings>(`${environment.baseApiRoot}/meeting-manager/settings/${getMeetingIdWithoutTimestamp(id)}`, {
      bypassAlertOnErrorCodes: [`404`],
    })
    .then((res) => res.data);
};

const updateSettings = async (id: string, data: MeetingSettingsDTO) => {
  return meetingApiClient.post(`${environment.baseApiRoot}/meeting-manager/settings/`, data).then((res) => res.data);
};

/**
 * @param {string} meetingId
 * @returns {Promise<object>}
 */
const getRecordings = async (meetingId: string) => {
  return meetingApiClient
    .request({
      method: 'GET',
      url: `${environment.baseApiRoot}/meetings/${meetingId}/user-recordings`,
    })
    .then((res) => res.data);
};

// /**
//  * @param {string} meetingId
//  * @param {string} recordingId
//  * @returns {Promise<boolean>}
//  */
// const removeRecording = async (meetingId, recordingId) => {
//   try {
//     await apiClient.request({
//       method: 'DELETE',
//       url: [ENDPOINT_KEY, meetingId, RECORDINGS_KEY, recordingId].join('/')
//     });
//   } catch (e) {
//     commonExceptionHandler(e);
//     throw e;
//   }
// };

export default {
  getMeeting,
  list,
  create,
  update,
  remove,
  getNoteList,
  createNote,
  updateNote,
  removeNote,
  getMoM,
  createMoM,
  updateMoM,
  removeMoM,
  getRecordings,
  getSettings,
  updateSettings,
  // removeRecording,
};
