import { Task, TasksQueryParams, TaskDTO, TaskActionStatus, StatusDTO } from '@dar/api-interfaces';
import { environment } from '../environments/environment';
import { taskManagerApiClient } from './clients';

const getAllTasks = (params: TasksQueryParams = {}) => {
  return taskManagerApiClient
    .get<Task[]>(`${environment.baseApiRoot}/task-manager/tasks`, { params })
    .then((res) => res.data);
};

const getTasksBatch = (data: number[] = []) => {
  return taskManagerApiClient
    .post<Task[]>(`${environment.baseApiRoot}/task-manager/tasks/batch`, data)
    .then((res) => res.data);
};

const getTask = (id: number) => {
  return taskManagerApiClient.get<Task>(`${environment.baseApiRoot}/task-manager/tasks/${id}`).then((res) => res.data);
};

const searchTask = (params: TasksQueryParams = {}) => {
  return taskManagerApiClient
    .get<Task[]>(`${environment.baseApiRoot}/task-manager/tasks/search`, { params })
    .then((res) => res.data);
};

const createTask = (data: TaskDTO) => {
  return taskManagerApiClient
    .post<TaskActionStatus>(`${environment.baseApiRoot}/task-manager/tasks`, data)
    .then((res) => res.data);
};

const deleteTask = (id: number) => {
  return taskManagerApiClient
    .delete<TaskActionStatus>(`${environment.baseApiRoot}/task-manager/tasks/${id}`)
    .then((res) => res.data);
};

const editTask = (id: number, data: Task) => {
  return taskManagerApiClient
    .put<Task>(`${environment.baseApiRoot}/task-manager/tasks/${id}`, data)
    .then((res) => res.data);
};

const updateTaskStatus = (id: number, status: StatusDTO) => {
  return taskManagerApiClient
    .put<TaskActionStatus>(`${environment.baseApiRoot}/task-manager/tasks/status/${id}`, status)
    .then((res) => res.data);
};

export default {
  getAllTasks,
  getTask,
  searchTask,
  createTask,
  deleteTask,
  editTask,
  updateTaskStatus,
  getTasksBatch,
};
