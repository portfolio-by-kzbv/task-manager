import { LinksRequest, LinksResponse } from '@dar/api-interfaces';
import { environment } from '../environments/environment';
import { taskManagerApiClient } from './clients';

const getLinks = (task_id: number) => {
  return taskManagerApiClient
    .get<LinksResponse>(`${environment.baseApiRoot}/task-manager/tasks/${task_id}/links`)
    .then((res) => res.data);
};

const createLink = (data: LinksRequest) => {
  return taskManagerApiClient
    .post<LinksRequest>(`${environment.baseApiRoot}/task-manager/tasks/links`, data)
    .then((res) => res.data);
};

const deleteLink = (task_id: number, link_id: number) => {
  return taskManagerApiClient
    .delete(`${environment.baseApiRoot}/task-manager/tasks/${task_id}/links/${link_id}`)
    .then((res) => res.data);
};

export default {
  getLinks,
  createLink,
  deleteLink,
};
