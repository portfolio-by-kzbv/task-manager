import axios, { AxiosResponse } from 'axios';
import { commonExceptionHandler } from './handlers';

declare module 'axios' {
  export interface AxiosRequestConfig {
    bypassAlertOnErrorCodes?: string[];
  }
}

const griffonBearerAuthorization = (config) => {
  try {
    const token = JSON.parse(localStorage.getItem('dms-auth')).id_token;
    if (token) {
      config.headers.common['Authorization'] = `Bearer ${token}`;
    } else {
      delete config.headers.common['Authorization'];
    }
  } catch (e) {
    console.error(e);
  }

  return config;
};

export const meetingApiClient = axios.create();
meetingApiClient.interceptors.request.use(griffonBearerAuthorization);
meetingApiClient.interceptors.response.use((response: AxiosResponse) => response, commonExceptionHandler);

export const taskManagerApiClient = axios.create();
taskManagerApiClient.interceptors.request.use(griffonBearerAuthorization);
taskManagerApiClient.interceptors.response.use((response: AxiosResponse) => response, commonExceptionHandler);
