import { Comment, CommentsQueryParams } from '@dar/api-interfaces';
import { environment } from '../environments/environment';
import { taskManagerApiClient } from './clients';

const getComments = (params: CommentsQueryParams) => {
  return taskManagerApiClient
    .get<Comment[]>(`${environment.baseApiRoot}/task-manager/tasks/comments`, { params })
    .then((res) => res.data);
};

const createComment = (data: Comment) => {
  return taskManagerApiClient
    .post<Comment>(`${environment.baseApiRoot}/task-manager/tasks/comments`, data)
    .then((res) => res.data);
};

const deleteComment = (task_id: number) => {
  return taskManagerApiClient
    .delete(`${environment.baseApiRoot}/task-manager/tasks/${task_id}/comments`)
    .then((res) => res.data);
};

const editComment = (id: number, data: Comment) => {
  return taskManagerApiClient
    .put<Comment>(`${environment.baseApiRoot}/task-manager/tasks/comments/${id}`, data)
    .then((res) => res.data);
};

export default {
    getComments,
    createComment,
    deleteComment,
    editComment
}
