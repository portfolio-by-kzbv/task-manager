import { Cluster } from '@dar/api-interfaces';
import { environment } from '../environments/environment';
import { taskManagerApiClient } from './clients';

const getAllClusters = () => {
  return taskManagerApiClient
    .get<Cluster[]>(`${environment.baseApiRoot}/task-manager/clusters`)
    .then((res) => res.data);
};

const getCluster = (cluster_id: string) => {
  return taskManagerApiClient
    .get<Cluster>(`${environment.baseApiRoot}/task-manager/clusters/${cluster_id}`)
    .then((res) => res.data);
};

export default {
  getAllClusters,
  getCluster,
};
