import { toast } from '@dartech/dms-ui';
import { AxiosError } from 'axios';

export const commonExceptionHandler = (error: AxiosError) => {
  const { bypassAlertOnErrorCodes } = error.config;
  if (!bypassAlertOnErrorCodes || !bypassAlertOnErrorCodes.includes(`${error.response.status}`)) {
    toast.error(error.message, { duration: 3000 });
  }
  return Promise.reject(error);
};
