import { Todo, TodoQueryParams } from '@dar/api-interfaces';
import { environment } from '../environments/environment';
import { taskManagerApiClient } from './clients';

const getAllTodos = (params: TodoQueryParams = {}) => {
  return taskManagerApiClient
    .get<Todo[]>(`${environment.baseApiRoot}/task-manager/checklist`, { params })
    .then((res) => res.data);
};

const createTodo = (data: Todo) => {
  return taskManagerApiClient
    .post<Todo>(`${environment.baseApiRoot}/task-manager/checklist`, data)
    .then((res) => res.data);
};

const deleteTodo = (id: number) => {
  return taskManagerApiClient
    .delete<any>(`${environment.baseApiRoot}/task-manager/checklist/${id}`)
    .then((res) => res.data);
};

const editTodo = (data: Todo) => {
  return taskManagerApiClient
    .put<Todo>(`${environment.baseApiRoot}/task-manager/checklist/${data.id}`, data)
    .then((res) => res.data);
};

export default {
  getAllTodos,
  createTodo,
  deleteTodo,
  editTodo,
};
