import { TaskStatus } from '@dar/api-interfaces';
import { environment } from '../environments/environment';
import { taskManagerApiClient } from './clients';

const list = () => {
  return taskManagerApiClient.get<TaskStatus[]>(`${environment.baseApiRoot}/task-manager/statuses`)
    .then(res => res.data);
};

export default {
    list
}
