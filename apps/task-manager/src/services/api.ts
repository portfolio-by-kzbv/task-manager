import tasks from './tasks';
import taskStatuses from './task-statuses';
import checklist from './checklist';
import users from './users';
import clusters from './clusters';
import projects from './projects';
import meetings from './meetings';
import links from './links';
import comments from './task-comments';
import attachments from './task-attachments';

export default {
  tasks,
  taskStatuses,
  checklist,
  users,
  clusters,
  projects,
  meetings,
  links,
  comments,
  attachments
};
