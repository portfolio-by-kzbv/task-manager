import { environment } from '../environments/environment';
import { User } from '@dar/api-interfaces';
import { taskManagerApiClient } from './clients';

const list = () => {
  return taskManagerApiClient.get<User[]>(`${environment.baseApiRoot}/users`).then((res) => res.data);
};

const getUser = (user_id: number) => {
  return taskManagerApiClient.get<User>(`${environment.baseApiRoot}/users/${user_id}`).then((res) => res.data);
};

export default {
  list,
  getUser,
};
