import { format } from 'date-fns';
import moment from 'moment';
import { MEETING_STATES } from '../constants/meeting-states';

/**
 * @param {Meeting} meeting
 * @returns {boolean}
 */
export const isLive = (meeting) => {
  const currentMoment = moment();
  const startMoment = moment(meeting.startTime);
  const endMoment = moment(meeting.endTime);
  return (
    currentMoment.isBetween(startMoment, endMoment, 'minutes') ||
    currentMoment.isSame(startMoment, 'minute') ||
    currentMoment.isSame(endMoment, 'minute')
  );
};

/**
 * @param {Meeting} meeting
 * @returns {boolean}
 */
export const isOld = (meeting) => {
  return moment().isAfter(moment(meeting.endTime), 'minutes');
};

/**
 * @param {Meeting} meeting
 * @returns {boolean}
 */
export const isTodayCreated = (meeting) => {
  return moment().isSame(moment(meeting.createdAt), 'day');
};

/**
 * @param {Meeting} meeting
 * @returns {"OLD"|"LIVE"|"NEW"|"FUTURE"}
 */
export const getMeetingState = (meeting) => {
  if (isLive(meeting)) {
    return MEETING_STATES.LIVE;
  } else if (isOld(meeting)) {
    return MEETING_STATES.OLD;
  } else if (
    isTodayCreated(meeting) &&
    Math.abs(moment().diff(meeting.createdAt, 'hours')) <= 3
  ) {
    return MEETING_STATES.NEW;
  } else {
    return MEETING_STATES.FUTURE;
  }
};

/**
 * HOF to create filter handle function to filter Meeting Collection
 * @param {string} text
 * @returns {function(*): boolean}
 */
export const filterMeetingsByText = (text) => (meeting) =>
  meeting.title.toLowerCase().includes(text) ||
  meeting.description.toLowerCase().includes(text);

/**
 *
 * @param {Meeting[]} meetings
 * @param {Date} date
 * @return {Meeting[]}
 */
export const filterMeetingByStartTime = (meetings, date) => {
  return meetings.filter((item) =>
    moment(date).isSame(moment(item.startTime), 'day')
  );
};

/**
 * HOF to sort Meeting collections
 * @param {Meeting} a
 * @param {Meeting} b
 * @returns {number}
 */
export const sortMeetingByStartTime = (a, b) =>
  +new Date(a.startTime) >= +new Date(b.startTime) ? 1 : -1;

/**
 * @param {Date|string} startTime
 * @returns {string}
 */
export const generateStartMeetingStrictMomentString = (startTime) =>
  moment(startTime)
    .startOf('minutes')
    .toISOString();

export const generateRecurringMeetingEndDateString = (startTime) => 
  format(startTime, 'yyyyMMdd');

/**
 * @param {Date|string} endTime
 * @returns {string}
 */
export const generateEndMeetingStrictMomentString = (endTime) =>
  moment(endTime).endOf('minutes').subtract(1, 'minute').toISOString();

/**
 * 14:59 -> 15:00, 14:58 -> 15:00, 15:58 -> 16:00 ...
 * @param {Date|string} endTime
 * @returns {Date}
 */
export const roundEndMeetingDate = (endTime) => {
  const endMinutes = moment(endTime).format('mm');
  const minutesOffset = ['29', '59'].includes(endMinutes)
    ? 1
    : ['28', '58'].includes(endMinutes)
    ? 2
    : ['27', '57'].includes(endMinutes)
    ? 3
    : 0;
  return moment(endTime).add(minutesOffset, 'minutes').toDate();
};

/**
 * Human time to now
 * @param {Meeting} meeting
 * @param {moment.Moment} [compareMoment]
 * @return string
 */
export const composeLeaveToMeetingTimeLabel = (meeting) => {
  const diffInDays = Math.abs(moment().diff(meeting.startTime, 'days'));

  if (isOld(meeting)) {
    if (diffInDays > 4) {
      return moment(meeting.endTime).format('ddd DD MMM');
    } else {
      return moment(meeting.endTime).fromNow();
    }
  }

  if (isLive(meeting)) {
    return 'Ends ' + moment(meeting.endTime).fromNow();
  }

  if (diffInDays > 4) {
    return moment(meeting.startTime).format('ddd DD MMM');
  }

  if (
    diffInDays === 0 &&
    new Date(meeting.startTime).getDate() - new Date().getDate() === 1
  ) {
    return 'tomorrow';
  }

  return moment(meeting.startTime).fromNow();
};

export const composeMeetingHours = (meeting) => {
  return (
    moment(meeting.startTime).format('HH:mm') +
    ' — ' +
    moment(meeting.endTime).format('HH:mm')
  );
};

export const getMeetingDate = (meeting) => {
  return moment(meeting.startTime).format('DD.MM.YY');
};

/**
 * @typedef {object} MeetingUIState
 * @property {"OLD"|"LIVE"|"NEW"|"FUTURE"} state
 * @property {string} timeToNowLabel
 *
 * @param {Meeting} meeting
 * @return MeetingUIState
 */
export const composeMeetingUIState = (meeting) => ({
  state: getMeetingState(meeting),
  timeToNowLabel: composeLeaveToMeetingTimeLabel(meeting),
  hours: composeMeetingHours(meeting),
  date: getMeetingDate(meeting),
});

/**
 * @param {Meeting} meeting
 * @return {string}
 */
export const composeMeetingFeatureTimeEventMessage = (meeting) => {
  switch (getMeetingState(meeting)) {
    case MEETING_STATES.NEW:
    case MEETING_STATES.FUTURE:
      return 'will start ' + composeLeaveToMeetingTimeLabel(meeting);
    case MEETING_STATES.LIVE:
      return 'will finish ' + composeLeaveToMeetingTimeLabel(meeting);
    case MEETING_STATES.OLD:
    default:
      return 'finished ' + composeLeaveToMeetingTimeLabel(meeting);
  }
};

export const getMeetingIdWithoutTimestamp = (meetingId: string) => {
  if (meetingId.includes('_')) {
    meetingId = meetingId.split('_')[0];
  }

  return meetingId;
}
