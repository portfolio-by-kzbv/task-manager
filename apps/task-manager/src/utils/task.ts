import { TaskStatus, TaskStatusTypes } from '@dar/api-interfaces';

export const addTaskStatusColor = (statuses: TaskStatus[]) => {
  statuses.map((status: TaskStatus) => {
      switch (status.name) {
        case TaskStatusTypes.NEW:
          status.color = '#3781CB';
          break;
        case TaskStatusTypes.IN_PROGRESS:
          status.color = '#976AD1';
          break;
        case TaskStatusTypes.IN_REVIEW:
          status.color = '#FFA530';
          break;
        case TaskStatusTypes.COMPLETED:
          status.color = '#2DB77B';
          break;
        case TaskStatusTypes.CANCELLED:
          status.color = '#ED4C36';
          break;
        case TaskStatusTypes.HOLD:
          status.color = '#99A5B1';
          break;
        default:
          break;
      }
      return statuses;
  });
  return statuses;
};
