import JITSI from '../constants/jitsi';
import { environment } from '../environments/environment';

declare global {
  interface Window {
    JitsiMeetExternalAPI: any;
    System: any;
  }
}

const isJitsiConferenceFrame = () => !!document.getElementById(JITSI.CONFERENCE_FRAME_ID);

export const isJitsiScriptConnect = () => !!document.getElementById(JITSI.SCRIPT_ID);

export const connectJitstApi = (cb) => {
  // eslint-disable-next-line no-restricted-properties
  window.System.import(`${environment.assetsBase}/external_api.js`)
    .then((m) => {
      window.JitsiMeetExternalAPI = m.default;
    })
    .then(() => (!isJitsiConferenceFrame() ? cb() : null));
};

export const removeJitsiApi = (jitsiAPI) => {
  const jitsiIFrame = document.getElementById(JITSI.CONFERENCE_FRAME_ID);

  if (isJitsiConferenceFrame()) {
    jitsiIFrame.remove();
    jitsiAPI.dispose();
    jitsiAPI = {};
  }
};

export const createJitsiMeeting = (meetingId) =>
  window.JitsiMeetExternalAPI
    ? new window.JitsiMeetExternalAPI(JITSI.DOMAIN, {
        roomName: meetingId,
        height: 700,
        parentNode: document.getElementById(JITSI.CONTAINER_ID),
        interfaceConfigOverwrite: {
          filmStripOnly: false,
          SHOW_JITSI_WATERMARK: false,
          NATIVE_APP_NAME: 'dms',
          TOOLBAR_BUTTONS: [
            'microphone',
            'camera',
            // 'closedcaptions',
            'desktop',
            // 'embedmeeting',
            // 'fodeviceselection',
            'hangup',
            // 'profile',
            'chat',
            // 'recording',
            // 'livestreaming',
            // 'etherpad',
            // 'sharedvideo',
            'settings',
            'raisehand',
            'videoquality',
            'filmstrip',
            'invite',
            // 'feedback',
            // 'stats',
            // 'shortcuts',
            'tileview',
            'videobackgroundblur',
            // 'download',
            // 'help',
            // 'mute-everyone',
            // 'security',
          ],
        },
        configOverwrite: {
          disableSimulcast: false,
          disableAudioLevels: true, // Should reduce CPU usage
          enableNoisyMicDetection: false,
        },
      })
    : {};
