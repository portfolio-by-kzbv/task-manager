import moment from 'moment';

import { MAX_DATE, MIN_DATE } from '../constants/date';

const today = moment();
const yesterday = moment().add(-1, 'days');

export const isMaxPossibleDate = date =>
  !(date > new Date(MAX_DATE)) && !(date < new Date(MAX_DATE));

export const isMinPossibleDate = date =>
  !(date > new Date(MIN_DATE)) && !(date < new Date(MIN_DATE));

export const isValidDate = date => !isNaN(new Date(date).getTime());

export const toValidDate = date => {
  const dateObj = new Date(date);

  if (!isValidDate(date) || date === null) {
    throw new Error('');
  }

  return dateObj;
};

export const getFormatDateWithTime = yourDate => {
  const month = getValidDateValue(yourDate.getMonth() + 1);
  const day = getValidDateValue(yourDate.getDate());

  const hours = getValidDateValue(yourDate.getHours());
  const minutes = getValidDateValue(yourDate.getMinutes());
  const seconds = getValidDateValue(yourDate.getSeconds());

  const date = `${yourDate.getFullYear()}-${month}-${day}`;
  const time = `T${hours}:${minutes}:${seconds}Z`;

  return date + time;
};

const getValidDateValue = value => (value < 10 ? `0${value}` : value);

export const getNextDay = () => {
  const date = new Date();
  date.setDate(date.getDate() + 1);
  return date;
};

export const nameOfDates = (date) => {
  if(moment(date).isSame(today, 'day')) {
    return 'Today';
  } else if(moment(date).isSame(yesterday, 'day')) {
    return 'Yesterday';
  } else {
    return moment(date).format('MMMM, DD');
  }
}