const minute = 1000 * 60;

export const addHalfHour = (date: Date) => {
  return new Date(Math.ceil(date.getTime() + minute * 30));
};

export const roundTime = (date: Date) => {
  return new Date(Math.ceil(date.getTime() / (minute * 15)) * minute * 15);
};

export const getHoursFromInput = (inputValue) => inputValue.substr(0, 2);
export const getMinutesFromInput = (inputValue) => inputValue.substr(2, 2);

export const isRemoveTimeSeparator = (currentValueLength, previousValueLength) =>
  currentValueLength === 3 && previousValueLength === 4;

export const isAddTimeSeparator = (currentValueLength, previousValueLength) =>
  (currentValueLength === 3 && previousValueLength === 2) ||
  (currentValueLength === 2 && previousValueLength === 1);
