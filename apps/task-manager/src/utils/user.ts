import { User } from '@dar/api-interfaces';

export const extractLetters = (user: User) => {
  const { firstName = '', lastName = '', displayName = '' } = user || {};
  let letters = [firstName[0], lastName[0]].filter(Boolean).join('').toUpperCase();

  if (letters.length < 1) {
    letters = displayName
      .split(' ')
      .filter(Boolean)
      .map((name) => name[0])
      .join('')
      .toUpperCase();
  }

  return letters;
};

export const compareUsers = (a: User, b: User) => {
  const userA = a.displayName.toUpperCase();
  const userB = b.displayName.toUpperCase();

  let comparison = 0;
  if (userA > userB) {
    comparison = 1;
  } else if (userA < userB) {
    comparison = -1;
  }
  return comparison;
};
