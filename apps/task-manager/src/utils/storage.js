export const STORE = {
  MEETINGS: '@store/meetings',
  TASKS: '@store/tasks',
  TASKS_RESET: '@store/reset_tasks',
  MEETINGS_RESET: '@store/reset_meetings',
};

/**
 * @param {string} key
 * @param {object, Array} defaultValue
 * @returns {object|{}|[]}
 */
export const getObjectFromStorageSync = (key, defaultValue = {}) => {
  try {
    return JSON.parse(window.localStorage.getItem(key)) || defaultValue;
  } catch (e) {
    return defaultValue;
  }
};

/**
 * @param {string} key
 * @param {object} data
 */
export const setObjectToStorageAsync = (key, data) => {
  window.setImmediate(() => {
    try {
      window.localStorage.setItem(key, JSON.stringify(data));
    } catch (e) {
      console.error(e);
    }
  });
};

/**
 * @returns {object}
 */
export function getMeetingsFromStorage() {
  return JSON.parse(localStorage.getItem(STORE.MEETINGS));
}

/**
 * @returns {object}
 */
export function getTasksFromStorage() {
  return JSON.parse(localStorage.getItem(STORE.TASKS));
}

/**
 * @param {array} meetings
 * @param {number} newMeetingsCount
 */
export function setMeetingsToStorage(meetings, newMeetingsCount) {
  localStorage.setItem(
    STORE.MEETINGS,
    JSON.stringify({ meetings, newMeetingsCount })
  );
}

/**
 * @param {array} issues
 * @param {number} newIssuesCount
 */
export function setTasksToStorage(issues, newIssuesCount) {
  localStorage.setItem(STORE.TASKS, JSON.stringify({ issues, newIssuesCount }));
}

/**
 * @returns {}
 */
export function resetTasksInStorage() {
  const checker = localStorage.getItem(STORE.TASKS_RESET)
    ? !JSON.parse(localStorage.getItem(STORE.TASKS_RESET))
    : true;

  localStorage.setItem(STORE.TASKS_RESET, JSON.stringify(checker));
  localStorage.removeItem(STORE.TASKS);
}

/**
 * @returns {}
 */
export function resetMeetingsInStorage() {
  const checker = localStorage.getItem(STORE.MEETINGS_RESET)
    ? !JSON.parse(localStorage.getItem(STORE.MEETINGS_RESET))
    : true;

  localStorage.setItem(STORE.MEETINGS_RESET, JSON.stringify(checker));
  localStorage.removeItem(STORE.MEETINGS);
}
