import _get from 'lodash/get';
import { MIME_TYPES } from '../constants/attachments';

export const isObjectEmpty = (value: Record<string, unknown> | unknown) => !value || Object.keys(value).length === 0;

export const isStringHtmlCode = (str) => /<([A-Za-z][A-Za-z0-9]*)\b[^>]*>(.*?)<\/\1>/.test(str);

export const getMimeTypes = (allowedTypes: string[]) =>
  allowedTypes
    .map((type) => _get(MIME_TYPES, type, ''))
    .filter((type) => !!type)
    .join(',');
