import { TaskStatus, TaskStatusTypes } from "@dar/api-interfaces";

export const getStatusTransitions = 
(currentStatus: string, statusList: TaskStatus[]) => {
    const currentStatusModel = statusList.find((status: TaskStatus) => status.name === currentStatus);
    return currentStatus
      ? statusList.filter((status: TaskStatus) => {
          if (currentStatus === TaskStatusTypes.INITIAL) {
            return status.name === TaskStatusTypes.NEW;
          }
          if (currentStatusModel) {
            try {
              const currentStatuses = JSON.parse(
                currentStatusModel.transitions
              );
              return (
                currentStatuses.includes(status.id) ||
                currentStatusModel.id === status.id
              );
            } catch (error) {
              console.log(error);
            }
          }
          return status;
        })
      : statusList;
  }