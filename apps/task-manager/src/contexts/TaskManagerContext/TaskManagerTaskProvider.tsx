import { getStatuses } from '@dar/actions/task-status.action';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import TaskManagerContext from './index';
import useTasks from './useTasks';

export const TaskManagerContextProvider = ({ children }) => {
  const taskTimeData = useTasks();

  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(getStatuses());
  }, [dispatch]);

  return <TaskManagerContext.Provider value={taskTimeData}>{children}</TaskManagerContext.Provider>;
};
