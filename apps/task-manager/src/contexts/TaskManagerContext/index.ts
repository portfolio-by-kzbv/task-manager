import { createContext } from 'react';

const TaskManagerContext = createContext(null);
export default TaskManagerContext;
