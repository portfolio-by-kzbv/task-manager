import { Comment } from '@dar/api-interfaces';
import { useState } from 'react';

export enum COMMENT_ACTION {
  REPLY = 'COMMENT_REPLY',
  EDIT = 'COMMENT_EDIT',
}
export interface CommentAction {
  comment: Comment;
  action: COMMENT_ACTION;
}
export interface UseTasksContext {
  commentAction: CommentAction;
  changeCommentAction: (comment: Comment, action: COMMENT_ACTION) => void;
}

const useTasks = (): UseTasksContext => {
  const [commentAction, setCommentAction] = useState<CommentAction>({
    comment: null,
    action: null,
  });

  const changeCommentAction = (comment: Comment, action: COMMENT_ACTION) => {
    setCommentAction({
      comment: comment,
      action: action,
    });
  };

  return {
    commentAction,
    changeCommentAction,
  };
};

export default useTasks;
