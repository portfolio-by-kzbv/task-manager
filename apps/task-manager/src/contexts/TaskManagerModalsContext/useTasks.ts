import { useState, useMemo, useEffect } from 'react';
import { LinkForm } from '@dar/components/TaskManager/CreateLink/useLinkForm';
import { useUserSessionState } from '@dar/selectors/users';
import { Dispatch, Task } from '@dar/api-interfaces';
import { getCluster } from '@dar/actions/clusters.action';
import { getProject } from '@dar/actions/projects.actions';
import { getTask } from '@dar/actions/tasks.action';
import { useDispatch } from 'react-redux';

export interface UseTasksContext {
  taskID: number;
  task: Task;
  loading: boolean;
  detailsModalOpen: boolean;
  handleDetailsModalClose: () => void;
  handleDetailsModalOpen: (id: number) => void;
  deleteModalOpen: boolean;
  deleteModalToggle: () => void;
  isCreateLinkModalOpened: boolean;
  openCreateLinkModal: () => void;
  closeCreateLinkModal: () => void;
  linkType: LinkTypes;
  changeLinkType: (newLinkType: LinkTypes) => void;
  linkForm: LinkForm;
  changeLinkForm: (newForm: LinkForm) => void;
  linkActionType: LinkActionTypes;
  changeLinkActionType: (newLinkActionType: LinkActionTypes) => void;
  canEdit: boolean;
  isAssignee: boolean;
  changeTaskId: (task_id: number) => void;
}

export enum LinkTypes {
  AFFECTS = 'Affects',
  DEPENDS = 'Depends',
}

export enum LinkActionTypes {
  CREATE = 'Create',
  EDIT = 'Edit',
}

const useTasks = (): UseTasksContext => {
  const dispatch: Dispatch = useDispatch();
  const session = useUserSessionState();
  const [detailsModalOpen, setOpenDetailsModal] = useState<boolean>(false);
  const [taskID, setTaskID] = useState<number>(null);
  const [task, setTask] = useState<Task>(null);
  const [linkType, setlinkType] = useState<LinkTypes>(null);
  const [loading, setLoading] = useState<boolean>(false);
  const [linkActionType, setLinkActionType] = useState<LinkActionTypes>(LinkActionTypes.CREATE);

  const [linkForm, setLinkForm] = useState<LinkForm>(null);

  const [deleteModalOpen, setDeleteModalOpen] = useState<boolean>(false);

  const [openCreateLinkDrawer, setOpenCreateLinkDrawer] = useState<boolean>(false);

  const canEdit = useMemo(() => Boolean(task?.creator_id === session?.id), [task, session]);

  const isAssignee = useMemo(() => Boolean(task?.assignee_id === session?.id), [task, session]);

  const openCreateLinkModal = () => {
    setOpenCreateLinkDrawer(true);
  };

  const changeTaskId = (task_id: number) => {
    setTaskID(task_id);
  };

  const closeCreateLinkModal = () => {
    setOpenCreateLinkDrawer(false);
  };

  const changeLinkType = (newLinkType: LinkTypes) => {
    setlinkType(newLinkType);
  };

  const changeLinkActionType = (newLinkActionType: LinkActionTypes) => {
    setLinkActionType(newLinkActionType);
  };

  const changeLinkForm = (newLinkForm: LinkForm) => {
    setLinkForm((prev) => ({ ...prev, ...newLinkForm }));
  };

  const handleDetailsModalClose = () => {
    setOpenDetailsModal(false);
    setTaskID(null);
    setTask(null);
  };

  const handleDetailsModalOpen = (id: number) => {
    setTaskID(+id);
    setOpenDetailsModal(true);
  };

  const deleteModalToggle = () => {
    setDeleteModalOpen(!deleteModalOpen);
  };

  useEffect(() => {
    if (detailsModalOpen && taskID && taskID !== task?.id) {
      setLoading(true);
      dispatch(getTask(taskID))
        .then((result) => {
          setTask(result.value);
          const taskResult = { ...result.value };
          if (result.value.group_id) {
            return dispatch(getCluster(result.value.group_id))
              .then((clusterResult) => {
                return { ...taskResult, cluster: clusterResult.value };
              })
              .catch((err) => {
                console.log(err);
                return taskResult;
              });
          }
          return taskResult;
        })
        .then((taskResult) => {
          if (taskResult.project_id) {
            return dispatch(getProject(taskResult.project_id))
              .then((projectResult) => {
                return { ...taskResult, project: projectResult.value };
              })
              .catch((err) => {
                console.log(err);
                return taskResult;
              });
          }
          return taskResult;
        })
        .then((taskResult) => {
          setTask(taskResult);
        })
        .finally(() => {
          setLoading(false);
        });
    }
  }, [taskID, detailsModalOpen, dispatch, task]);

  return {
    taskID,
    task,
    detailsModalOpen,
    loading,
    handleDetailsModalClose,
    handleDetailsModalOpen,
    deleteModalOpen,
    deleteModalToggle,
    isCreateLinkModalOpened: openCreateLinkDrawer,
    openCreateLinkModal,
    closeCreateLinkModal,
    linkType,
    changeLinkType,
    linkForm,
    changeLinkForm,
    linkActionType,
    changeLinkActionType,
    canEdit,
    changeTaskId,
    isAssignee,
  };
};

export default useTasks;
