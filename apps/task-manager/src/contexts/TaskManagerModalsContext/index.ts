import { createContext } from 'react';
import { UseTasksContext } from './useTasks';

const TaskManagerModalsContext = createContext<UseTasksContext>(null);
export default TaskManagerModalsContext;
