import React from 'react';
import TaskManagerModalsContext from './index';
import useTasks from './useTasks';

export const TaskManagerModalsProvider = ({ children }) => {
  const toggleModals = useTasks();
  return (
    <TaskManagerModalsContext.Provider value={toggleModals}>
      {children}
    </TaskManagerModalsContext.Provider>
  );
};
