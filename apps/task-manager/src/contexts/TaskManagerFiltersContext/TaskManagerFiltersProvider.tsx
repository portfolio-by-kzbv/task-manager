import React from 'react';
import TaskManagerFiltersContext from './index';
import useTasks from './useTasks';

export const TaskManagerFiltersProvider = ({ children }) => {
  const filterData = useTasks();
  return (
    <TaskManagerFiltersContext.Provider value={filterData}>
      {children}
    </TaskManagerFiltersContext.Provider>
  );
};
