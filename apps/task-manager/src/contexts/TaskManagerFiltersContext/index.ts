import { createContext } from 'react';

const TaskManagerFiltersContext = createContext(null);
export default TaskManagerFiltersContext;
