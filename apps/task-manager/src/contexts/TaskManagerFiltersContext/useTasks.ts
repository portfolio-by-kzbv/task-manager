import { useCallback, useMemo, useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { getAllTasks } from '../../store/actions/tasks.action';
import { Task } from '@dar/api-interfaces';
import { User } from '@dar/api-interfaces';
import { TasksQueryParams } from '@dar/api-interfaces';
import { Dispatch } from '@dar/api-interfaces';
import { useUserSessionState } from '@dar/selectors/users';

interface Filter {
  status: number[];
  assignee?: User;
  assignedToMe?: User;
  assignedByMe?: User;
  archive?: boolean;
  start_time?: string;
  end_time?: string;
}

const initialFilters: Filter = {
  status: [],
};

type Order = 'asc' | 'desc';

export const MAX_TASKS_PER_REQUEST = 200;

const useTasks = () => {
  const dispatch: Dispatch = useDispatch();
  const session = useUserSessionState();
  const [filters, setFilters] = useState<Filter>(initialFilters);
  const [loading, setLoading] = useState<boolean>(false);
  const [order, setOrder] = useState<Order>();
  const [orderBy, setOrderBy] = useState<keyof Task>('end_time');

  const handleOrderChange = (order: Order) => {
    setOrder(order);
  };

  const handleOrderByChange = (property: keyof Task) => {
    setOrderBy(property);
  };

  const q = useMemo(() => {
    const params: TasksQueryParams = {};
    const isDefault = !(filters.assignedToMe || filters.assignedByMe || filters.assignee);
    if (filters.assignedToMe) {
      params.assignee_id = filters.assignedToMe.id;
    }

    if (filters.assignedByMe) {
      params.creator_id = filters.assignedByMe.id;
    }

    if (filters.assignee) {
      params.assignee_id = filters.assignee.id;
      if (session && filters.assignee.id !== session.id) {
        params.creator_id = session.id;
      }
    }

    if (filters.status.length) {
      params.status_id = filters.status;
    } else {
      params.status_id = [1, 2, 3, 6];
    }

    if (filters.archive) {
      params.status_id = [4, 5];
    }

    if (session && isDefault) {
      params.user_id = session.id;
    }

    if (filters.start_time) {
      params.start_time_min = filters.start_time;
    }

    if (filters.end_time) {
      params.end_time_max = filters.end_time;
    }

    return params;
  }, [filters, session]);

  const handleChangeFilter = async (filterName: string, value: string | number | number[]) => {
    setLoading(true);

    const newFilters = { ...filters };

    newFilters[filterName] = value;

    if (filterName === 'assignedToMe') {
      delete newFilters.archive;
      delete newFilters.assignedByMe;
      delete newFilters.start_time;
      delete newFilters.end_time;
    }

    if (filterName === 'assignedByMe') {
      delete newFilters.archive;
      delete newFilters.assignedToMe;
      delete newFilters.start_time;
      delete newFilters.end_time;
    }

    if (filterName === 'archive') {
      delete newFilters.assignedByMe;
      delete newFilters.assignedToMe;
    }

    setFilters(newFilters);
    setLoading(false);
  };

  const updateTasks = useCallback(() => {
    setLoading(true);
    return dispatch(getAllTasks({ q, ipp: MAX_TASKS_PER_REQUEST }, Boolean(filters.assignedByMe))).then((data) => {
      setLoading(false);
    });
  }, [dispatch, q, filters.assignedByMe]);

  useEffect(() => {
    if (!session) {
      return;
    }
    updateTasks();
  }, [updateTasks, session, filters]);

  return {
    filters,
    loading,
    handleChangeFilter,
    updateTasks,
    order,
    handleOrderChange,
    orderBy,
    handleOrderByChange,
  };
};
export default useTasks;
