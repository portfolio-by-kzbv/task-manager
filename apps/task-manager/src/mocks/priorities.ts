export const priorities = [
    {
      id: 0,
      name: ''
    },
    {
      id: 1,
      name: 'Low',
      color: '#99A5B1'
    },
    {
      id: 2,
      name: 'Medium',
      color: '#3781CB'
    },
    {
      id: 3,
      name: 'High',
      color: '#D93939'
    }
  ];
