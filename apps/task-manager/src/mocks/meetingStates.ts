export const meetingStateOld = {
  state: "OLD",
  timeToNowLabel: "Wed 28 Apr",
  hours: "10:30 — 11:15",
  date: "28.04.21",
}

export const meetingStateFuture = {
  state: "FUTURE",
  timeToNowLabel: "in 42 minutes",
  hours: "15:00 — 15:30",
  date: "10.05.21",
}

export const meetingsStates = [{
  state: "OLD",
  timeToNowLabel: "Wed 28 Apr",
  hours: "10:30 — 11:15",
  date: "28.04.21",
},
{
  state: "FUTURE",
  timeToNowLabel: "in 42 minutes",
  hours: "15:00 — 15:30",
  date: "10.05.21",
}]
