import { Meeting } from '@dar/api-interfaces';

export const meeting: Meeting = {
  id: '7ruo218bc9pgel8f1qno4qq11l',
  title: 'Call with Madina',
  description: '',
  startTime: '2021-04-28T04:30:00.000Z',
  endTime: '2021-04-28T05:15:00.000Z',
  createdAt: '2021-04-27T22:28:38.000Z',
  updatedAt: '2021-04-27T22:28:38.000Z',
  owner: null,
  secretary: 'snursultan@dar.kz',
  participants: ['mkazybayeva@dar.kz', 'snursultan@dar.kz'],
  externalParticipants: [],
  ownerEmail: 'snursultan@dar.kz',
  location: null,
  videoConfUrl: '',
  recurrence: null,
  recurringEventId: null,
  status: 'confirmed',
};

export const meetings: Meeting[] = [
  {
    id: '7ruo218bc9pgel8f1qno4qq11l',
    title: 'Call with Madina',
    description: '',
    startTime: '2021-04-28T04:30:00.000Z',
    endTime: '2021-04-28T05:15:00.000Z',
    createdAt: '2021-04-27T22:28:38.000Z',
    updatedAt: '2021-04-27T22:28:38.000Z',
    owner: null,
    secretary: 'snursultan@dar.kz',
    participants: ['mkazybayeva@dar.kz', 'snursultan@dar.kz'],
    externalParticipants: [],
    ownerEmail: 'snursultan@dar.kz',
    location: null,
    videoConfUrl: '',
    recurrence: null,
    recurringEventId: null,
    status: 'confirmed',
  },
  {
    id: 'rmsfe89pigvb3lt20l5gtdcrhk',
    title: 'Test',
    description: '',
    startTime: '2021-05-10T09:00:00.000Z',
    endTime: '2021-05-10T09:30:59.000Z',
    createdAt: '2021-05-10T00:50:42.000Z',
    updatedAt: '2021-05-10T00:50:42.000Z',
    owner: null,
    secretary: 'snursultan@dar.kz',
    participants: ['a.akhmetov@dar.kz', 'snursultan@dar.kz'],
    externalParticipants: [],
    ownerEmail: 'snursultan@dar.kz',
    location: null,
    videoConfUrl: '',
    recurrence: null,
    recurringEventId: null,
    status: 'confirmed',
  },
];
