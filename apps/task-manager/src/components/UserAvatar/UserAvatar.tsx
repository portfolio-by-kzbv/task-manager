import React, { useState } from 'react';
import { makeStyles, Avatar } from '@material-ui/core';

import * as userUtils from '../../utils/user';
import { User } from '@dar/api-interfaces';

const useStyles = makeStyles(() => ({
  root: {
    fontSize: '16px',
    '&.MuiAvatar-colorDefault': {
      background: 'linear-gradient(180deg, #0ca9ea 0%, #59c7f4 97.92%)',
      color: '#fff',
    },
  },
}));

type Props = {
  user: User | undefined;
  className?: string;
};

const UserAvatar = React.forwardRef<any, Props>(({ user, className = '' }, ref) => {
  const classes = useStyles();
  const [loaded, setLoaded] = useState(false);

  return (
    <Avatar
      ref={ref}
      alt={user ? user.displayName : 'No user'}
      src={user && user.photoUrl}
      classes={classes}
      className={className}
      imgProps={{ onLoad: () => setLoaded(true) }}
    >
      {!loaded && userUtils.extractLetters(user)}
    </Avatar>
  );
});

export default React.memo(UserAvatar);
