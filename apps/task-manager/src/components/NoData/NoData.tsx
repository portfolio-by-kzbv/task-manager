import React from 'react';
import { Typography, makeStyles, SvgIconTypeMap } from '@material-ui/core';
import { OverridableComponent } from '@material-ui/core/OverridableComponent';

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    marginTop: '8px',
  },
}));

type Props = {
  text: string;
  Icon?: OverridableComponent<SvgIconTypeMap<Record<string, unknown>, 'svg'>>;
};

const NoData: React.FunctionComponent<Props> = ({ text, Icon = null }) => {
  const classes = useStyles();

  return (
    <div className={classes.root}>
      {Icon && <Icon color="disabled" />}
      <Typography variant="h3" color="textSecondary" className={classes.text}>
        {text}
      </Typography>
    </div>
  );
};

export default NoData;
