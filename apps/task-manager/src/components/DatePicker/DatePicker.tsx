import React from 'react';
import { KeyboardDatePicker } from '@material-ui/pickers';
import DateRangeIcon from '@material-ui/icons/DateRange';
import { InputLabel } from '@material-ui/core';

type Props = {
  value?: Date;
  onChange: (value: Date) => void;
  label?: string;
  disabled?: boolean;
  fullWidth?: boolean;
  disablePast?: boolean;
  error?: boolean;
  helperText?: string;
  defaultValue?: string | number | Date;
  minDate?: Date;
  emptyLabel?: string;
};

const DatePicker: React.FunctionComponent<Props> = ({
  value,
  onChange,
  label,
  disabled,
  fullWidth,
  disablePast,
  error,
  helperText,
  defaultValue,
  minDate,
  emptyLabel,
}) => {
  return (
    <div>
      {label && <InputLabel>{label}</InputLabel>}
      <KeyboardDatePicker
        disableToolbar
        format="dd/MM/yyyy"
        variant="inline"
        inputVariant="outlined"
        value={value}
        disablePast={disablePast}
        onChange={onChange}
        KeyboardButtonProps={{
          'aria-label': 'change date',
        }}
        keyboardIcon={<DateRangeIcon fontSize="small" />}
        error={error}
        helperText={helperText}
        fullWidth={fullWidth}
        disabled={disabled}
        initialFocusedDate={defaultValue}
        autoOk
        emptyLabel={emptyLabel}
        minDate={minDate}
      />
    </div>
  );
};

export default DatePicker;
