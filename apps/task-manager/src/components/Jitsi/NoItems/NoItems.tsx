import React from 'react';
import PropTypes from 'prop-types';
import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  container: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: theme.spacing(4),
  },
  title: {
    fontSize: '1.125rem',
  },
  subTitle: {
    textAlign: 'center',
    maxWidth: '253px',
  },
}));

type Props = {
  title: string;
  subTitle: string; 
};

const NoItems: React.FunctionComponent<Props> = ({ title, subTitle }) => {
  const classes = useStyles();
  return (
    <div className={classes.container}>
      <strong className={classes.title}>{title}</strong>
      <span className={classes.subTitle}>{subTitle}</span>
    </div>
  );
};

export default NoItems;
