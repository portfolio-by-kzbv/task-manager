import React, { useState } from 'react';
import { Drawer, Tabs, Tab, Box } from '@material-ui/core';

import TabPanel from '../../TabPanel/TabPanel';
import MeetingMoM from '../MeetingMoM/MeetingMoM';
import MeetingNotes from '../MeetingNotes/MeetingNotes';

import { useStyles, useTabStyles, useTabsStyles } from './useStyles';

function JitsiMeetingDetails({ meeting }) {
  const classes = useStyles();
  const tabsClasses = useTabsStyles();
  const tabClasses = useTabStyles();

  const [tab, setTab] = useState(0);
  const handleChange = (_, newTab) => {
    setTab(newTab);
  };

  return (
    <Drawer
      variant="persistent"
      anchor="left"
      open={true}
      className={classes.drawer}
      id={'jitsiDetails'}
    >
      <div className={classes.header}>
        <Box display="flex" marginLeft="20px">
          <Tabs
            classes={tabsClasses}
            centered
            value={tab}
            onChange={handleChange}
          >
            <Tab classes={tabClasses} label="MOM" />
            <Tab classes={tabClasses} label="Notes" />
          </Tabs>
        </Box>
      </div>
      <div className={classes.content}>
        <TabPanel value={tab} index={0} className={classes.tabPanel}>
          <MeetingMoM meetingId={meeting.id} />
        </TabPanel>
        <TabPanel value={tab} index={1} className={classes.tabPanel}>
          <MeetingNotes meetingId={meeting.id} />
        </TabPanel>
      </div>
    </Drawer>
  );
}

export default JitsiMeetingDetails;
