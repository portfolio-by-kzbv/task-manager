import { makeStyles } from '@material-ui/core';

import JITSI from '../../../constants/jitsi';

export const useStyles = makeStyles((theme) => ({
  drawer: {
    width: JITSI.DETAILS_MENU_WIDTH,
    height: JITSI.FRAME_HEIGHT,
    flexShrink: 0,

    '& > .comms-MuiPaper-root': {
      overflow: 'hidden',
      position: 'relative',
      border: 'none',
    },
  },
  header: {
    padding: theme.spacing(2),
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#2A3948',
  },
  content: {
    backgroundColor: '#172737',
    flexGrow: 1,
    display: 'flex',
    flexDirection: 'column',
  },
  tabPanel: {
    flexGrow: 1,
    color: theme.palette.grey[50],
  },
}));

export const useTabsStyles = makeStyles(() => ({
  indicator: {
    display: 'none',
  },
  root: { borderBottom: 'none' },
}));

export const useTabStyles = makeStyles((theme) => ({
  root: {
    background: '#C7DAE5',
    textTransform: 'none',
    minWidth: '90px',
    color: theme.palette.grey[700],

    '&:last-child': {
      borderTopRightRadius: theme.spacing(1),
      borderBottomRightRadius: theme.spacing(1),
    },

    '&:first-child': {
      borderTopLeftRadius: theme.spacing(1),
      borderBottomLeftRadius: theme.spacing(1),
    },
  },
  selected: {
    background: theme.palette.primary[200],
    color: theme.palette.grey[50],
  },
}));
