import React, { useState, useEffect, useMemo, useRef } from 'react';
import { Box } from '@material-ui/core';
import { Link } from 'react-router-dom';
import Fullscreen from '@material-ui/icons/Fullscreen';
import FullscreenExitIcon from '@material-ui/icons/FullscreenExit';

import JitsiMeetingDetails from './JitsiMeetingDetails';
import Spinner from '../Spinner/Spinner';
import { useUserSessionState, useUsersState } from '../../selectors/users';
import { connectJitstApi, removeJitsiApi, createJitsiMeeting, isJitsiScriptConnect } from '../../utils/jitsi';
import { isObjectEmpty } from '../../utils/general';
import JITSI from '../../constants/jitsi';

import { DeleteNoteModal } from './MeetingNotes/DeleteNoteModal';
import { DeleteMomModal } from './MeetingMoM/DeleteMomModal';
import useStyles from './useStyles';
import ScreenRecorder from './ScreenRecorder/ScreenRecorder';

const MEETINGS_PATH = '/productivity/meetings';

function JitsiMeeting({ meeting }) {
  const session = useUserSessionState();
  const users = useUsersState();
  const user = useMemo(() => users.find((u) => u.workEmail === session?.workEmail), [users, session?.workEmail]);
  const [jitsiAPI, setJitsiAPI] = useState<any>({});
  const jitsiContainerClasses = useStyles();
  const [loading, setLoading] = useState(false);
  const [isConferenceEnd, setIsConferenceEnd] = useState(false);
  const [isFullScreen, setIsFullScreen] = useState(false);
  const containerRef = useRef<HTMLDivElement>(null);
  const canvasRef = useRef<HTMLCanvasElement>(null);

  useEffect(() => {
    if (meeting && !isObjectEmpty(jitsiAPI)) {
      jitsiAPI.executeCommand('subject', meeting.title);
    }
  }, [meeting, jitsiAPI]);

  useEffect(() => {
    if (user && !isObjectEmpty(jitsiAPI)) {
      jitsiAPI.executeCommands({
        displayName: user.displayName,
        email: user.workEmail,
        avatarUrl: user.photoUrl,
      });
      jitsiAPI.executeCommand('toggleAudio');
      jitsiAPI.executeCommand('toggleVideo');
    }
  }, [user, jitsiAPI]);

  useEffect(() => {
    const cb = (event) => {
      if (event.key === 's') event.stopPropagation();
    };

    const initialiseJitsi = async () => {
      function startConference() {
        const _jitsi = createJitsiMeeting(meeting.id);
        setJitsiAPI(_jitsi);
        _jitsi.on('videoConferenceLeft', () => {
          setIsConferenceEnd(true);
          if (isFullScreen) closeFullScreen();
        });
        const iframe = document.getElementById(JITSI.CONFERENCE_FRAME_ID);
        if (iframe) {
          iframe.addEventListener('keydown', cb);
          iframe.addEventListener('keypress', cb);
        }
      }

      if (meeting && !isJitsiScriptConnect()) {
        setLoading(true);
        await connectJitstApi(startConference);
        setLoading(false);
      }
    };

    initialiseJitsi();

    return () => {
      const iframe = document.getElementById(JITSI.CONFERENCE_FRAME_ID);
      if (iframe) {
        iframe.removeEventListener('keydown', cb);
        iframe.removeEventListener('keydown', cb);
      }
      removeJitsiApi(jitsiAPI);
      setJitsiAPI({});
    };
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [meeting]);

  function openFullscreen() {
    const docElmWithBrowsersFullScreenFunctions = document.getElementById(JITSI.VIEW_CONTAINER_ID) as HTMLElement & {
      webkitRequestFullscreen(): Promise<void>;
      msRequestFullscreen(): Promise<void>;
    };

    if (docElmWithBrowsersFullScreenFunctions.requestFullscreen) {
      docElmWithBrowsersFullScreenFunctions.requestFullscreen();
    } else if (docElmWithBrowsersFullScreenFunctions.webkitRequestFullscreen) {
      /* Safari */
      docElmWithBrowsersFullScreenFunctions.webkitRequestFullscreen();
    } else if (docElmWithBrowsersFullScreenFunctions.msRequestFullscreen) {
      /* IE11 */
      docElmWithBrowsersFullScreenFunctions.msRequestFullscreen();
    }
  }

  function closeFullScreen() {
    const docWithBrowsersExitFunctions = document as Document & {
      webkitExitFullscreen(): Promise<void>;
      msExitFullscreen(): Promise<void>;
    };

    if (docWithBrowsersExitFunctions.exitFullscreen) {
      docWithBrowsersExitFunctions.exitFullscreen();
    } else if (docWithBrowsersExitFunctions.webkitExitFullscreen) {
      /* Safari */
      docWithBrowsersExitFunctions.webkitExitFullscreen();
    } else if (docWithBrowsersExitFunctions.msExitFullscreen) {
      /* IE11 */
      docWithBrowsersExitFunctions.msExitFullscreen();
    }
  }

  useEffect(() => {
    const docIsFullScreen = document as Document & {
      mozFullScreen: boolean;
      webkitIsFullScreen: boolean;
      msFullscreenElement: boolean;
    };

    const exitHandler = () => {
      setIsFullScreen(
        !docIsFullScreen.fullscreenElement &&
          !docIsFullScreen.webkitIsFullScreen &&
          !docIsFullScreen.mozFullScreen &&
          !docIsFullScreen.msFullscreenElement
          ? false
          : true
      );
    };

    document.addEventListener('fullscreenchange', exitHandler);
    document.addEventListener('webkitfullscreenchange', exitHandler);
    document.addEventListener('mozfullscreenchange', exitHandler);
    document.addEventListener('MSFullscreenChange', exitHandler);

    return () => {
      document.removeEventListener('fullscreenchange', exitHandler);
      document.removeEventListener('webkitfullscreenchange', exitHandler);
      document.removeEventListener('mozfullscreenchange', exitHandler);
      document.removeEventListener('MSFullscreenChange', exitHandler);
    };
  }, [isFullScreen]);

  const handleFullScreen = () => (isFullScreen ? closeFullScreen() : openFullscreen());

  const jitsiFrame = document.getElementById(JITSI.CONFERENCE_FRAME_ID);

  return (
    <>
      <div id={JITSI.VIEW_CONTAINER_ID} ref={containerRef} className={jitsiContainerClasses.root}>
        {loading ? (
          <div className={jitsiContainerClasses.LoadingContainer}>
            <Spinner size={50} />
          </div>
        ) : (
          <>
            <JitsiMeetingDetails meeting={meeting} />
            {isConferenceEnd && (
              <div className={jitsiContainerClasses.ConferenceEndContainer}>
                <Box display="flex" flexDirection="column" alignItems="center">
                  <strong className={jitsiContainerClasses.title}>Conference is over</strong>
                  <span className={jitsiContainerClasses.subTitle}>Thank you for using DMS!</span>
                  <Link className={jitsiContainerClasses.link} to={MEETINGS_PATH}>
                    Back to Meetings
                  </Link>
                </Box>
              </div>
            )}
          </>
        )}
        <Box id={JITSI.CONTAINER_ID} display="flex" flexDirection="column" width="100%" position="relative">
          {isFullScreen ? (
            <FullscreenExitIcon
              className={jitsiContainerClasses.fullScreenIcon}
              onClick={handleFullScreen}
              color="primary"
              fontSize="large"
            />
          ) : (
            <Fullscreen
              className={jitsiContainerClasses.fullScreenIcon}
              onClick={handleFullScreen}
              color="primary"
              fontSize="large"
            />
          )}
          <DeleteNoteModal isFullScreen={isFullScreen} />
          <DeleteMomModal isFullScreen={isFullScreen} />
          <canvas
            id="background-canvas"
            ref={canvasRef}
            style={{ position: 'absolute', top: '-99999999px', left: '-9999999999px' }}
          ></canvas>
        </Box>
      </div>
      {jitsiFrame ? <ScreenRecorder container={jitsiFrame} canvas={canvasRef.current} /> : null}
    </>
  );
}

export default React.memo(
  JitsiMeeting,
  (prevProps, nextProps) => prevProps.meeting.id === nextProps.meeting.id && window.JitsiMeetExternalAPI
);
