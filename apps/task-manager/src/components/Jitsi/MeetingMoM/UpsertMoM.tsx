import React, { useEffect, useState } from 'react';
import { Button, Box, makeStyles, Theme, createTheme, MuiThemeProvider, Typography } from '@material-ui/core';
import { stateToHTML } from 'draft-js-export-html';
import { MoM } from '@dar/api-interfaces';
import { useUserSessionState } from '@dar/selectors/users';
import MUIRichTextEditor from 'mui-rte';
import { EditorState, ContentState, convertToRaw, convertFromHTML } from 'draft-js';

export const defaultTheme: Theme = createTheme();

Object.assign(defaultTheme, {
  overrides: {
    MUIRichTextEditor: {
      root: {
        padding: '10px 0',
      },
      container: {
        display: 'flex',
        flexDirection: 'column-reverse',
        border: '1px solid #CED7DF',
        borderRadius: 6,
        '&:focus': {
          borderColor: 'red',
        },
      },
      editor: {
        backgroundColor: '#fff',
        padding: '10px 16px',
        minHeight: '58px',
        maxHeight: '234px',
        overflow: 'auto',
        borderRadius: '6px 6px 0 0',
      },
      hidePlaceholder: {
        display: 'block',
      },
      toolbar: {
        border: 'none',
        backgroundColor: '#FFF',
        borderRadius: '0 0 6px 6px',
      },
      placeHolder: {
        backgroundColor: '#FFF',
        paddingLeft: 20,
        width: 'inherit',
        position: 'absolute',
        top: '16px',
        margin: 0,
      },
      anchorLink: {
        color: '#333333',
        textDecoration: 'underline',
      },
    },
  },
});

const useStyles = makeStyles((theme) => ({
  form: {
    padding: `0 ${theme.spacing(3)}px ${theme.spacing(1.25)}px`,
  },
  input: {
    marginBottom: theme.spacing(1),

    '&>div': {
      backgroundColor: theme.palette.grey[200],
      borderRadius: theme.spacing(1),
      color: theme.palette.text.primary,
    },
  },
  btn: {
    borderRadius: theme.spacing(1),
  },
}));

type Props = {
  meetingMom: MoM;
  meetingId: string;
  onSubmit: (mom: MoM) => void;
  buttonText: string;
  onCancel: () => void;
  isEdit?: boolean;
  loading?: boolean;
};

const UpsertMoM: React.FunctionComponent<Props> = ({
  meetingMom,
  meetingId,
  onSubmit,
  buttonText,
  onCancel,
  isEdit = false,
  loading,
}) => {
  const [text, setText] = useState(meetingMom ? meetingMom.content : '');
  const [error, setError] = useState(null);
  const classes = useStyles({ isError: Boolean(error) });
  const session = useUserSessionState();
  const defaultContent = EditorState.createEmpty().getCurrentContent();
  const [content, setContent] = useState(JSON.stringify(convertToRaw(defaultContent)));

  useEffect(() => {
    const contentState = convertFromHTML(meetingMom ? meetingMom.content : '');
    const state = contentState && ContentState.createFromBlockArray(contentState.contentBlocks, contentState.entityMap);
    setContent(JSON.stringify(convertToRaw(state)));
  }, [meetingMom]);

  const handleChange = (state: EditorState) => {
    const content = stateToHTML(state.getCurrentContent());
    setText(content);

    if (!state.getCurrentContent().hasText()) {
      setError('Please add some text');
    } else {
      setError(null);
    }
  };

  const handleSubmit = (event) => {
    if (loading) {
      return;
    }
    event.preventDefault();

    if (!error) {
      const payload = {
        ...meetingMom,
        content: text.trim(),
        meeting_id: meetingId,
        author_id: session.id,
      };
      onSubmit(payload);
    }
  };

  return (
    <form onSubmit={handleSubmit} className={classes.form}>
      <MuiThemeProvider theme={defaultTheme}>
        <MUIRichTextEditor
          defaultValue={content}
          label="Write text..."
          controls={['bold', 'italic', 'strikethrough', 'link', 'numberList', 'bulletList']}
          onChange={handleChange}
        />
      </MuiThemeProvider>
      {error ? (
        <Typography color="primary" style={{ lineHeight: '20px', paddingBottom: '16px' }}>
          {error}
        </Typography>
      ) : (
        <div style={{ height: '36px' }}></div>
      )}
      <Box display="flex" justifyContent="space-between">
        <Button variant="contained" color="primary" type="submit" fullWidth className={classes.btn}>
          {buttonText}
        </Button>
        {isEdit && (
          <Button variant="contained" color="secondary" onClick={onCancel} fullWidth className={classes.btn}>
            Cancel
          </Button>
        )}
      </Box>
    </form>
  );
};

export default UpsertMoM;
