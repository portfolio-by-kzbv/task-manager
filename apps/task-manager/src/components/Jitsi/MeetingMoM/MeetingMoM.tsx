import React, { useEffect, useState, useCallback, useMemo } from 'react';
import { useDispatch } from 'react-redux';
import { Box } from '@material-ui/core';

import Spinner from '@dar/components/Spinner/Spinner';
import UpsertMom from './UpsertMoM';
import ViewMoM from './ViewMoM';
import { useMeetingMomState } from '@dar/selectors/meeting-mom.selector';
import { getMoM, deleteMoM, createMoM, editMoM, setDeleteMomId } from '@dar/actions/meeting-mom.action';
import { useMeeting } from '@dar/selectors/meetings.selector';
import NoItems from '../NoItems/NoItems';
import { useUserSessionState, useUsersState } from '@dar/selectors/users';
import { MoM, User } from '@dar/api-interfaces';
import { Dispatch as CustomDispatch } from '@dar/api-interfaces';
import PromptModal from '@dar/components/Modal/PromptModal/PromptModal';
import meetings from '@dar/services/meetings';

type Props = {
  meetingId: string;
};

const MeetingMoM: React.FunctionComponent<Props> = ({ meetingId }) => {
  const [isEdit, setIsEdit] = useState(false);
  const [loading, setLoading] = useState(false);
  const users = useUsersState();
  const meeting = useMeeting(meetingId);
  const session = useUserSessionState();
  const { mom, deleteMomModalOpen, deleteMomId } = useMeetingMomState();
  const dispatch: CustomDispatch = useDispatch();
  const [secretary, setSecretary] = useState<User | undefined>();

  useEffect(() => {
    meetings
      .getSettings(meetingId)
      .then((res) => {
        const secretar =
          users.find((user) => user.id === res?.secretary_id) ||
          users.find((user) => user.workEmail?.toLowerCase() === meeting.ownerEmail?.toLowerCase());
        setSecretary(secretar);
      })
      .catch((err) => {
        console.error(err.message);
      });
  }, [meetingId, users, meeting]);

  const isSecretary = useMemo(() => meeting && secretary && session?.id === secretary.id, [
    session?.id,
    meeting,
    secretary,
  ]);

  useEffect(() => {
    setLoading(true);
    dispatch(getMoM({ meeting_id: meetingId })).finally(() => {
      setLoading(false);
    });
  }, [dispatch, meetingId]);

  const handleCreate = (payload: MoM) => dispatch(createMoM(payload));

  const handleEdit = useCallback(
    (payload: MoM) => {
      setLoading(true);
      return dispatch(editMoM(mom.id, payload))
        .then(() => {
          return dispatch(getMoM({ meeting_id: meetingId }));
        })
        .finally(() => {
          setLoading(false);
          setIsEdit(false);
        });
    },
    [dispatch, mom, meetingId]
  );

  const handleDelete = useCallback(() => {
    setLoading(true);
    dispatch(deleteMoM(deleteMomId)).finally(() => {
      dispatch(setDeleteMomId(null));
      setLoading(false);
    });
  }, [dispatch, deleteMomId]);

  return (
    <Box display="flex" flexDirection="column" height="100%">
      <Box flexGrow="3" marginBottom="24px" overflow="auto">
        {loading ? (
          <Spinner />
        ) : mom ? (
          <ViewMoM
            meetingMom={mom}
            onDelete={() => dispatch(setDeleteMomId(mom.id))}
            onEdit={() => setIsEdit(true)}
            isSecretary={isSecretary}
            secretary={secretary}
            isEdit={isEdit}
          />
        ) : (
          <NoItems title="No MOM yet" subTitle="Capture important details from meetings." />
        )}
        {isSecretary && isEdit && mom && (
          <UpsertMom
            meetingMom={mom}
            meetingId={meetingId}
            onSubmit={handleEdit}
            loading={loading}
            buttonText="Save"
            onCancel={() => setIsEdit(false)}
            isEdit={isEdit}
          />
        )}
      </Box>
      {isSecretary && !isEdit && !mom && (
        <UpsertMom
          meetingMom={mom}
          meetingId={meetingId}
          onSubmit={handleCreate}
          buttonText="Create"
          onCancel={() => setIsEdit(false)}
        />
      )}
      <PromptModal
        open={deleteMomModalOpen}
        onClose={() => dispatch(setDeleteMomId(null))}
        onCancel={() => dispatch(setDeleteMomId(null))}
        onConfirm={handleDelete}
        propmtType="danger"
        title="Delete MoM?"
        text="A deleted MoM cannot be recovered."
        confirmText="Delete"
        confirmColor="secondary"
      />
    </Box>
  );
};

export default MeetingMoM;
