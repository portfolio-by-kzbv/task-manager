import React, { useCallback, useState } from 'react';
import { Button, Typography } from '@material-ui/core';
import { useDispatch } from 'react-redux';

import { deleteMoM, setDeleteMomId } from '../../../store/actions/meeting-mom.action';
import { useMeetingMomState } from '../../../selectors/meeting-mom.selector';
import useStyles from '../useStyles';
import { Dispatch as CustomDispatch } from '@dar/api-interfaces';

export const DeleteMomModal = ({ isFullScreen }) => {
  const dispatch: CustomDispatch = useDispatch();
  const { deleteMomId, deleteMomModalOpen } = useMeetingMomState();
  const [loading, setLoading] = useState(false);
  const jitsiContainerClasses = useStyles();

  const handleDelete = useCallback(() => {
    setLoading(true);
    dispatch(deleteMoM(deleteMomId)).finally(() => {
      dispatch(setDeleteMomId(null));
      setLoading(false);
    });
  }, [dispatch, deleteMomId]);

  return (
    <div
      className={`${jitsiContainerClasses.modal} ${
        isFullScreen && deleteMomModalOpen ? jitsiContainerClasses.showModal : null
      }`}
    >
      <Typography variant="h2">Do you want to delete MoM?</Typography>
      <div className={jitsiContainerClasses.buttons}>
        <Button onClick={() => dispatch(setDeleteMomId(null))} color={'primary'}>
          Cancel
        </Button>
        <Button onClick={handleDelete} color={'secondary'}>
          Confirm
        </Button>
      </div>
    </div>
  );
};
