import React from 'react';
import { Box, Button, makeStyles, Typography } from '@material-ui/core';
import parse from 'html-react-parser';
import UserAvatar from '../../UserAvatar';
import { MoM, User } from '@dar/api-interfaces';

const useStyles = makeStyles((theme) => ({
  root: {
    padding: `${theme.spacing(1)}px ${theme.spacing(3)}px`,
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    marginTop: theme.spacing(3),

    '&:last-child': {
      borderBottom: 'none',
    },
  },
  textContainer: {
    marginBottom: theme.spacing(3),
    marginTop: theme.spacing(3),
    textAlign: 'left',
  },
  text: {
    '&.checked': {
      textDecoration: 'line-through',
    },
    whiteSpace: 'pre-line',
  },
  actionContainer: {
    display: 'flex',
  },
  btn: {
    minWidth: 'max-content',
    fontWeight: 'normal',
    fontSize: '1rem',
    marginRight: '40px',
  },
  name: {
    fontWeight: 'bold',
    fontSize: '0.875rem',
  },
  status: {
    fontSize: '0.875rem',
  },
}));

type Props = {
  meetingMom: MoM;
  onDelete: () => void;
  onEdit: () => void;
  isSecretary: boolean;
  secretary: User;
  isEdit: boolean;
};

const ViewMoM: React.FunctionComponent<Props> = ({ meetingMom, onDelete, onEdit, isSecretary, secretary, isEdit }) => {
  const classes = useStyles();

  return (
    !isEdit && (
      <Box className={classes.root}>
        {secretary && (
          <Box display="flex">
            <UserAvatar user={secretary} />
            <Box display="flex" flexDirection="column" marginLeft="8px">
              <span className={classes.name}>{secretary.displayName}</span>
              <span className={classes.status}>Secretary</span>
            </Box>
          </Box>
        )}
        <Box className={classes.textContainer}>
          <Typography className="DraftView" variant="body1">{parse(meetingMom.content)}</Typography>
        </Box>
        {isSecretary && (
          <Box className={classes.actionContainer}>
            <Button color="primary" onClick={onEdit} className={classes.btn}>
              Edit
            </Button>
            <Button onClick={onDelete} className={classes.btn}>
              Delete
            </Button>
          </Box>
        )}
      </Box>
    )
  );
};

export default ViewMoM;
