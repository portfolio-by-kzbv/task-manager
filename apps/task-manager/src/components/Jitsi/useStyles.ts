import { makeStyles } from '@material-ui/core';

import JITSI from '../../constants/jitsi';

const FLEX_CENTER_PROPS = {
  display: 'flex',
  alignItems: 'center',
  justifyContent: 'center',
};

export default makeStyles((theme) => ({
  root: {
    display: 'flex',
    background: JITSI.BACKGROUND_GRADIENT,
    height: JITSI.FRAME_HEIGHT,
    position: 'relative',

    [`& #${JITSI.CONFERENCE_FRAME_ID}`]: {
      order: 2,
    },

    '&:fullscreen #jitsiDetails': {
      height: '100vh',
    },

    '&:fullscreen iframe': {
      height: '100vh !important',
    },
  },
  LoadingContainer: {
    width: '100%',
    height: 'inherit',
    ...FLEX_CENTER_PROPS,
  },
  ConferenceEndContainer: {
    position: 'absolute',
    left: JITSI.DETAILS_MENU_WIDTH,
    width: `calc(100% - ${JITSI.DETAILS_MENU_WIDTH})`,
    height: '100%',
    zIndex: 2,
    background: JITSI.BACKGROUND_GRADIENT,
    ...FLEX_CENTER_PROPS,
  },
  title: {
    fontSize: '1.875rem',
    color: theme.palette.grey[600],
  },
  subTitle: {
    fontSize: '1.25rem',
    color: theme.palette.grey[400],
    textAlign: 'center',
  },
  link: {
    borderRadius: theme.spacing(0.5),
    marginTop: theme.spacing(3),
    backgroundColor: theme.palette.primary[200],
    color: theme.palette.grey[100],
    textAlign: 'center',
    padding: theme.spacing(1, 4.5),
  },
  fullScreenIcon: {
    alignSelf: 'flex-end',
    cursor: 'pointer',
    marginRight: theme.spacing(1),
    marginTop: theme.spacing(1),
    color: theme.palette.grey[600],
  },
  modal: {
    position: 'absolute',
    transform: 'translate(-50%, -50%)',
    width: '440px',
    backgroundColor: '#fff',
    display: 'none',
    zIndex: 999,
    top: '50%',
    left: '50%',
    flexDirection: 'column',
    alignItems: 'center',
    borderRadius: 5,
    paddingBottom: 10,
    paddingTop: 20,
  },
  showModal: {
    display: 'flex',
  },
  buttons: {
    display: 'flex',
    justifyContent: 'space-beetween',
    marginTop: 10,
  },
}));
