import React, { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import { List } from '@material-ui/core';

import Spinner from '../../Spinner/Spinner';
import PromptModal from '../../Modal/PromptModal/PromptModal';
import MeetingNote from './MeetingNote';
import CreateNote from './CreateNote';
import { listNotes, removeNote, setDeleteNoteId } from '@dar/actions/meeting-notes.action';
import NoItems from '../NoItems';
import { Dispatch as CustomDispatch } from '@dar/api-interfaces';

import { useStyles } from './meetinNotes.styles';
import { useUserSessionState } from '@dar/selectors/users';
import { Notes } from '@dar/api-interfaces';
import { useMeetingNotesState } from '@dar/selectors/meeting-notes.selector';

type Props = {
  meetingId: string;
};

const MeetingNotes: React.FunctionComponent<Props> = ({ meetingId }) => {
  const classes = useStyles();
  const dispatch: CustomDispatch = useDispatch();
  const { notes, deleteNoteId, deleteModalOpen } = useMeetingNotesState();
  const session = useUserSessionState();
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(true);
    dispatch(listNotes({ meeting_id: meetingId, author_id: session.id })).finally(() => {
      setLoading(false);
    });
  }, [dispatch, meetingId, session]);

  const handleDelete = () => {
    setLoading(true);
    dispatch(removeNote(deleteNoteId)).finally(() => {
      dispatch(setDeleteNoteId(null));
      setLoading(false);
    });
  };

  const onDeleteClick = (noteId: number) => {
    dispatch(setDeleteNoteId(noteId));
  };

  return (
    <div className={classes.root}>
      <div className={classes.items}>
        {loading ? (
          <Spinner />
        ) : notes.length ? (
          <List className={classes.list}>
            {notes.map((note: Notes) => (
              <MeetingNote key={note.id} note={note} onDelete={onDeleteClick} />
            ))}
          </List>
        ) : (
          <NoItems title="No notes yet" subTitle="Write down important points so you don't forget." />
        )}
      </div>
      <CreateNote meetingId={meetingId} />
      <PromptModal
        open={deleteModalOpen}
        onClose={() => dispatch(setDeleteNoteId(null))}
        onCancel={() => dispatch(setDeleteNoteId(null))}
        onConfirm={handleDelete}
        propmtType='danger'
        title="Delete note?"
        text="A deleted note cannot be recovered."
        confirmText="Delete"
        confirmColor="secondary"
      />
    </div>
  );
};

export default MeetingNotes;
