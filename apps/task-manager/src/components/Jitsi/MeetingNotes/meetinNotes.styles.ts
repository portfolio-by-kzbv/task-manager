import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
  },
  list: {
    flexBasis: 0,
    padding: 0,
  },
  items: {
    flexGrow: 3,
    overflowY: 'auto',
    marginBottom: theme.spacing(4),
  },
  noData: {
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: theme.spacing(8),
  },
  noDataTitle: {
    fontSize: '1.125rem',
  },
  noDataSubTitle: {
    textAlign: 'center',
  },
}));
