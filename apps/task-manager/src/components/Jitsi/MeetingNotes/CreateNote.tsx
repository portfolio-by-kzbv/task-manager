import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { TextField, Button, makeStyles } from '@material-ui/core';

import { createNote, listNotes } from '@dar/actions/meeting-notes.action';
import Spinner from '../../Spinner/Spinner';
import { Dispatch as CustomDispatch } from '@dar/api-interfaces';
import { useUserSessionState } from '@dar/selectors/users';

const useStyles = makeStyles((theme) => ({
  form: {
    padding: `${theme.spacing(1.25)}px ${theme.spacing(3)}px ${theme.spacing(1.25)}px`,
  },
  input: (props) => ({
    marginBottom: theme.spacing(1),

    '&>div': {
      backgroundColor: theme.palette.grey[200],
      borderRadius: theme.spacing(1),
      color: theme.palette.text.primary,
    },
  }),
  btn: {
    borderRadius: theme.spacing(1),
  },
}));

function CreateNote({ meetingId }) {
  const [text, setText] = useState('');
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const dispatch: CustomDispatch = useDispatch();
  const classes = useStyles({ isError: Boolean(error) });
  const session = useUserSessionState();

  const handleChange = (event) => {
    setText(event.target.value);

    if (!event.target.value) {
      setError('Please, add some text');
    } else {
      setError(null);
    }
  };

  const handleSubmit = (event) => {
    event.preventDefault();

    if (!error && text.length) {
      setLoading(true);
      dispatch(createNote({ meeting_id: meetingId, content: text.trim(), author_id: session.id }))
        .then(() => {
          setLoading(false);
          setText('');
          setError(null);
        })
        .then(() => {
          dispatch(listNotes({ meeting_id: meetingId, author_id: session.id }));
        })
        .catch((error) => {
          setLoading(false);
          setError(error.message);
        });
    } else if (!text.length) {
      setError('Please, add some text');
    }
  };

  return (
    <form onSubmit={handleSubmit} className={classes.form}>
      {loading ? (
        <Spinner />
      ) : (
        <>
          <TextField
            variant="outlined"
            fullWidth
            margin="normal"
            multiline
            rows={5}
            value={text}
            onChange={handleChange}
            disabled={loading}
            error={Boolean(error)}
            helperText={error ? error : ''}
            className={classes.input}
            placeholder="Write text..."
          />
          <Button className={classes.btn} variant="contained" color="primary" fullWidth type="submit">
            Create
          </Button>
        </>
      )}
    </form>
  );
}

export default CreateNote;
