import React, { useState, useRef } from 'react';
import cx from 'classnames';
import { Typography, TextField, Box, makeStyles } from '@material-ui/core';

import { useOutsideClick } from '../../../hooks/useOutsideClick';
import { Notes } from '@dar/api-interfaces';

const useStyles = makeStyles((theme) => ({ 
titleEdit: {
  border: 'none',
  padding: '0',
  borderBottom: `1px solid ${theme.palette.primary[200]}`,
  fontSize: 14,
  lineHeight: 20,
  borderRadius: '0',
  '&:hover, &:focus': {
    border: 'none',
    borderBottom: `1px solid ${theme.palette.primary[200]}`,
    outline: 'none',
  },
},
}));

type Props = {
  note: Notes;
  onEdit: (data: Notes) => void;
  onDelete: () => void;
};

const NoteText: React.FC<Props> = ({ note, onEdit, onDelete }) => {
  const ref = useRef(null);
  const [isEditing, setIsEditing] = useState(false);
  const [value, setValue] = useState(note.content);
  const classes = useStyles();

  const handleEdit = () => {
    if (value && typeof value === 'string' && value.trim() && value !== note.content) {
      const data = {
        ...note,
        content: value,
      };

      onEdit(data);
    } else if (!value || typeof value !== 'string' || !value.trim()) {
      onDelete();
    }

    setIsEditing(false);
  };

  useOutsideClick(ref, handleEdit);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => setValue(event.target.value);

  const handleSubmit = (event: React.ChangeEvent<HTMLFormElement>) => {
    event.preventDefault();
    handleEdit();
  };

  return (
    <Box flexGrow={1}>
      {isEditing ? (
        <form onSubmit={handleSubmit}>
          <TextField
            variant="outlined"
            margin="dense"
            fullWidth
            ref={ref}
            value={value}
            onChange={handleChange}
            InputProps={{
              className: classes.titleEdit,
            }}
          />
        </form>
      ) : (
        <Typography
          variant="h6"
          onClick={() => setIsEditing(true)}
        >
          {value}
        </Typography>
      )}
    </Box>
  );
};

export default NoteText;
