import React, { useState } from 'react';
import { Button, Typography } from '@material-ui/core';
import useStyles from '../useStyles';
import { Dispatch as CustomDispatch } from '@dar/api-interfaces';
import { useDispatch } from 'react-redux';
import { useMeetingNotesState } from '@dar/selectors/meeting-notes.selector';
import { removeNote, setDeleteNoteId } from '@dar/actions/meeting-notes.action';

export const DeleteNoteModal = ({ isFullScreen }) => {
  const dispatch: CustomDispatch = useDispatch();
  const { deleteNoteId, deleteModalOpen } = useMeetingNotesState();
  const [loading, setLoading] = useState(false);
  const jitsiContainerClasses = useStyles();

  const handleDelete = () => {
    setLoading(true);
    dispatch(removeNote(deleteNoteId)).then(() => {
      dispatch(setDeleteNoteId(null));
      setLoading(false);
    });
  };

  return (
    <div
      className={`${jitsiContainerClasses.modal} ${
        isFullScreen && deleteModalOpen ? jitsiContainerClasses.showModal : null
      }`}
    >
      <Typography variant="h2">Do you want to delete note?</Typography>
      <div className={jitsiContainerClasses.buttons}>
        <Button onClick={() => dispatch(setDeleteNoteId(null))} color={'primary'}>
          Cancel
        </Button>
        <Button onClick={handleDelete} color={'secondary'}>
          Confirm
        </Button>
      </div>
    </div>
  );
};
