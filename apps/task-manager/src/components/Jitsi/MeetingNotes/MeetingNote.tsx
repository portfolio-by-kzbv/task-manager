import React, { useCallback, useState } from 'react';
import { ListItem, makeStyles, IconButton } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import { Notes } from '@dar/api-interfaces';
import { Dispatch } from '@dar/api-interfaces';
import { useDispatch } from 'react-redux';
import { editNote, removeNote } from '@dar/actions/meeting-notes.action';
import NoteText from './NoteText';

const useStyles = makeStyles((theme) => ({
  root: {
    height: '40px',
    display: 'flex',
    justifyContent: 'space-between',
    '&:hover, &:focus': {
      background: `${theme.palette.grey[100]}`,
    },
  },
  actionContainer: {
    width: 60,
    display: 'flex',
    justifyContent: 'space-between',
  },
  deleteButton: {
    padding: '0',
  },
}));

type Props = {
  note: Notes;
  onDelete: (noteId: number) => void;
};

const MeetingNote: React.FunctionComponent<Props> = ({ note, onDelete }) => {
  const classes = useStyles();
  const dispatch: Dispatch = useDispatch();
  const [loading, setLoading] = useState(false);

  const handleEdit = useCallback(
    (data: Notes) => {
      setLoading(true);
      dispatch(editNote(data.id, data)).then(() => {
        setLoading(false);
      });
    },
    [dispatch]
  );

  const handleDelete = () => {
    onDelete(note.id);
  };

  return (
    <ListItem className={classes.root}>
      <NoteText note={note} onEdit={handleEdit} onDelete={handleDelete} />
      <div className={classes.actionContainer}>
        <IconButton aria-label="delete todo item" onClick={handleDelete} edge="end" className={classes.deleteButton}>
          <DeleteIcon />
        </IconButton>
      </div>
    </ListItem>
  );
};

export default MeetingNote;
