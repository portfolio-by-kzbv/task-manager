import { Button, makeStyles } from '@material-ui/core';
import React, { useEffect, useRef, useState } from 'react';
import { saveAs } from 'file-saver';

type Props = {
  container: HTMLElement;
  canvas: HTMLCanvasElement;
};

const useStyles = makeStyles((theme) => ({
  btn: {
    minWidth: 'max-content',
    fontWeight: 'normal',
    fontSize: '1rem',
    marginRight: '40px',
  },
}));

const ScreenRecorder: React.FC<Props> = ({ container, canvas }) => {
  const classes = useStyles();
  const [isRecording, setIsRecording] = useState(false);
  const initialRef = useRef(false);

  useEffect(() => {
    if (isRecording) {
      startRecording();
      return;
    }
    if (!initialRef.current) {
      initialRef.current = true;
      return;
    }
    stopRecording();
  }, [isRecording]);

  const toogleRecord = () => {
    setIsRecording((s) => !s);
  };

  return (
    <div>
      <Button color="primary" onClick={toogleRecord} className={classes.btn}>
        {!isRecording ? 'Start recording' : 'Stop recording'}
      </Button>
    </div>
  );
};

let recorder;
let recordingData = [];
let recorderStream;

async function startRecording() {
  let gumStream, gdmStream;
  recordingData = [];

  try {
    gumStream = await navigator.mediaDevices.getUserMedia({ video: false, audio: true });
    gdmStream = await (navigator.mediaDevices as any).getDisplayMedia({
      video: { displaySurface: 'window' },
      audio: { channelCount: 2 },
    });
  } catch (e) {
    console.error('capture failure', e);
    return;
  }

  recorderStream = gumStream ? mixer(gumStream, gdmStream) : gdmStream;
  recorder = new (window as any).MediaRecorder(recorderStream, { mimeType: 'video/webm' });

  recorder.ondataavailable = (e) => {
    console.log(e);
    if (e.data && e.data.size > 0) {
      recordingData.push(e.data);
    }
  };

  recorder.onstop = () => {
    console.log('ON STOP');
    recorderStream.getTracks().forEach((track) => track.stop());
    gumStream.getTracks().forEach((track) => track.stop());
    gdmStream.getTracks().forEach((track) => track.stop());
  };

  recorderStream.addEventListener('inactive', () => {
    console.log('Capture stream inactive');
    stopRecording();
  });

  recorder.start();
  console.log('started recording');
}

function stopRecording() {
  console.log('Stopping recording');
  if (!recorder) {
    return;
  }
  recorder.stop();
  setTimeout(() => {
    const blob = new Blob(recordingData, { type: 'video/webm' });
    console.log(blob);
    const url = window.URL.createObjectURL(blob);
    saveAs(blob, 'recording.webm');
  }, 100);
}

/**
 * Mixes multiple audio tracks and the first video track it finds
 * */
function mixer(stream1, stream2) {
  const ctx = new AudioContext();
  const dest = ctx.createMediaStreamDestination();

  if (stream1.getAudioTracks().length > 0) ctx.createMediaStreamSource(stream1).connect(dest);

  if (stream2.getAudioTracks().length > 0) ctx.createMediaStreamSource(stream2).connect(dest);

  let tracks = dest.stream.getTracks();
  tracks = tracks.concat(stream1.getVideoTracks()).concat(stream2.getVideoTracks());

  return new MediaStream(tracks);
}

/**
 * Returns a filename based ono the Jitsi room name in the URL and timestamp
 * */
function getFilename() {
  const now = new Date();
  const timestamp = now.toISOString();
  const room = new RegExp(/(^.+)\s\|/).exec(document.title);
  if (room && room[1] !== '') return `${room[1]}_${timestamp}`;
  else return `recording_${timestamp}`;
}

export default ScreenRecorder;
