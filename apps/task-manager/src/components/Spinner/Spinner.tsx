import React from 'react';
import CircularProgress from '@material-ui/core/CircularProgress';

import cx from 'classnames';
import { makeStyles } from '@material-ui/core';

const useStyles = makeStyles(() => ({
  container: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    height: '45px',
    width: '45px',
    margin: '0 auto',

    '&.fullscreen': {
      background: 'rgba(255, 255, 255, 0.5)',
      position: 'fixed',
      top: 0,
      left: 0,
      width: '100vw',
      height: '100vh',
      zIndex: 9999,
    },

    '&.absolute': {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%',
      background: 'rgba(255, 255, 255, 0.5)',
      zIndex: 10,
    },
  },
}));

type Props = {
  fullscreen?: boolean;
  size?: number;
  absolute?: boolean;
};

const Spinner: React.FC<Props> = ({ fullscreen = false, size = 40, absolute = false }) => {
  const classes = useStyles();

  return (
    <div
      className={cx(classes.container, {
        fullscreen,
        absolute,
      })}
    >
      <CircularProgress size={size} />
    </div>
  );
};

export default React.memo(Spinner);
