import React, { useState, useEffect, useContext } from 'react';
import { TextField, InputAdornment, IconButton } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import ClearIcon from '@material-ui/icons/Clear';
import { useDispatch } from 'react-redux';
import { filterTasks } from '../../store/actions/tasks.action';
import { TasksQueryParams } from '@dar/api-interfaces';
import { useDebounce } from 'use-debounce';
import { useUserSessionState } from '@dar/selectors/users';
import TaskManagerFiltersContext from '@dar/contexts/TaskManagerFiltersContext';

const TaskSearch = () => {
  const [searchText, setSearchText] = useState(null);
  const [searchValue] = useDebounce(searchText, 400);
  const dispatch = useDispatch();
  const session = useUserSessionState();
  const { updateTasks } = useContext(TaskManagerFiltersContext);

  useEffect(() => {
    if (searchValue === null || !session) {
      return;
    }
    if (!searchValue) {
      updateTasks();
      return;
    }
    const q: TasksQueryParams = { query: searchValue, user_id: session.id };
    dispatch(filterTasks({ q }));
  }, [dispatch, searchValue, session, updateTasks]);

  return (
    <TextField
      value={searchText}
      onChange={(event) => setSearchText(event.target.value)}
      size="medium"
      variant="standard"
      placeholder="Search task"
      InputProps={{
        startAdornment: (
          <InputAdornment position="start">
            <SearchIcon />
          </InputAdornment>
        ),
        endAdornment: searchText ? (
          <InputAdornment position="end">
            <IconButton
              size="small"
              onClick={() => {
                setSearchText('');
              }}
            >
              <ClearIcon fontSize="inherit" />
            </IconButton>
          </InputAdornment>
        ) : null,
      }}
    />
  );
};

export default TaskSearch;
