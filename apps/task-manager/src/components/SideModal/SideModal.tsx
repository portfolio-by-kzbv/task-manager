import React from 'react';
import { Drawer, Backdrop } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

type Props = {
  heading?: string;
  openDrawer: boolean;
  onClose: () => void;
  size?: 'small' | 'large';
  classes?: any;
  zIndex?: number;
};

const useStyles = makeStyles({
  drawerPaper: {
    border: 'none',
    height: '100vh',
    boxShadow: '0px 4px 16px rgba(0, 0, 0, 0.16), 0px 2px 4px rgba(0, 0, 0, 0.1)',
    width: (props: any) => props.width,
    zIndex: 1300,
  },
  backdrop: {
    zIndex: (props: any) => props.zIndex,
  },
});

const SideModal: React.FunctionComponent<Props> = ({ children, openDrawer, onClose, size, zIndex }) => {
  const classes = useStyles({
    width: size === 'small' ? '500px' : '83%',
    zIndex: zIndex ?? 1200,
  });

  return (
    <React.Fragment>
      <Backdrop
        open={openDrawer}
        onClick={onClose}
        classes={{
          root: classes.backdrop,
        }}
      />
      <Drawer
        variant="temporary"
        anchor="right"
        open={openDrawer}
        onClose={onClose}
        classes={{
          paper: classes.drawerPaper,
        }}
      >
        {children}
      </Drawer>
    </React.Fragment>
  );
};

export default SideModal;
