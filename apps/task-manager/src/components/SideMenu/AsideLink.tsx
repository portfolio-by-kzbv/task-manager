import React, { useState, useMemo, forwardRef } from 'react';
import { NavLink } from 'react-router-dom';

import { ListItem, ListItemText, ListItemIcon, Badge } from '@material-ui/core';

import CustomIcon from '../CustomIcon/CustomIcon';
import { listItemStyles, textStyles, badgeStyles } from './aside.styles';

interface AsideLink {
  name: string;
  to: string;
  id: string;
  notifications: number;
}
interface Props {
  icon: string;
  text: string;
  to: string;
  id: string;
  link: AsideLink;
  external?: boolean;
  blank?: boolean;
}

const AsideLink: React.FunctionComponent<Props> = ({ icon, text, to, id, external = false, blank = false }) => {
  const classes = listItemStyles();
  const textClasses = textStyles();
  const badgeClasses = badgeStyles();

  const [selected, setSelected] = useState(false);

  const renderLink = useMemo(
    () =>
      forwardRef<HTMLAnchorElement>((itemProps, ref) =>
        external ? (
          // eslint-disable-next-line jsx-a11y/anchor-has-content
          <a href={to} ref={ref} {...itemProps} target={blank ? '_blank' : '_self'} />
        ) : (
          <NavLink
            to={to}
            ref={ref}
            {...itemProps}
            isActive={(match, _) => {
              // match returns null if there is no match at all
              setSelected(Boolean(match));
              return Boolean(match);
            }}
          />
        )
      ),
    [to, external, blank]
  );

  const notificationsSum = 0;

  return (
    <li id={id}>
      <ListItem button component={renderLink} selected={selected} classes={classes}>
        <ListItemIcon className={classes.listItemIcon}>
          <CustomIcon icon={icon} fill="#6D7C8B" size="28px" />
        </ListItemIcon>
        <ListItemText primary={text} classes={textClasses} />
        <Badge badgeContent={notificationsSum} color="secondary" className={badgeClasses.badge}>
          <span className={badgeClasses.badgeContent} />
        </Badge>
      </ListItem>
    </li>
  );
};

export default AsideLink;
