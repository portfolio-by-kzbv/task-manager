import { makeStyles } from '@material-ui/core';

export const listItemStyles = makeStyles((theme) => ({
  root: {
    color: theme.palette.grey[600],
    fontWeight: 600,
    padding: theme.spacing(3, 4, 3, 7),
    '&:hover, &:focus': {
      background: theme.palette.grey[200],
      color: theme.palette.grey[600],
    },
    '&:focus': {
      textDecoration: 'none',
      outline: 'none',
    },
  },
  selected: {
    '&.Mui-selected': {
      backgroundColor: '#E4E8EB',
      fontWeight: theme.typography.fontWeightBold,
      '&:hover': {
        background: theme.palette.grey[200],
      },
      '&::before': {
        content: '""',
        display: 'block',
        width: '4px',
        height: '100%',
        background: '#6D7C8B',
        position: 'absolute',
        top: 0,
        left: 0,
      },
    },
  },
  listItemIcon: {
    minWidth: '20px',
    marginRight: '12px',
  },
}));

export const textStyles = makeStyles({
  primary: {
    fontWeight: 600,
  },
});

export const badgeStyles = makeStyles({
  badge: {
    marginRight: '10px',
  },
  badgeContent: {
    minWidth: '20px',
  },
});

export const sideMenuStyles = makeStyles((theme) => ({
  root: {
    height: 'calc(100vh - 54px)',
    width: '300px',
  },
  paper: {
    position: 'relative',
    padding: theme.spacing(5, 0),
    border: 'none',
    borderRight: '1px solid #D9E7EE',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
    background: '#f5f8fa',
  },
  logo: {
    fontSize: '14px',
    fontWeight: 600,
    color: theme.palette.text.secondary,
    display: 'flex',
    alignItems: 'flex-end',
    justifyContent: 'center',
    lineHeight: 1,

    '& > img': {
      height: '16px',
      marginLeft: theme.spacing(1),
    },
  },
}));
