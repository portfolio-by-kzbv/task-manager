const links = [
  {
    name: 'Tasks',
    to: '/productivity/tasks',
    icon: 'clipboard',
    id: 'productivity',
    notifications: 0,
  },
  {
    name: 'Meetings',
    to: '/productivity/meetings',
    icon: 'user-group',
    id: 'productivity',
    notifications: 0,
  },
  {
    name: 'Workchat',
    to: '/workchat/dartech-team?dms=true',
    icon: 'message',
    id: 'workchat',
    notifications: 0,
    external: true,
    blank: true,
  },
];

export default links;
