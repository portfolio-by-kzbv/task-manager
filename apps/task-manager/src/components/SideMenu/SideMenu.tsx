import React from 'react';
import { Drawer, List } from '@material-ui/core';
import AsideLink from './AsideLink';
import links from './links';
import { sideMenuStyles } from './aside.styles';

const SideMenu = () => {
  const classes = sideMenuStyles();

  return (
    <Drawer variant="permanent" anchor="left" classes={{ root: classes.root, paper: classes.paper }}>
      <List>
        {links.map((linkItem, i) => (
          <AsideLink
            key={linkItem.icon}
            text={linkItem.name}
            to={linkItem.to}
            icon={linkItem.icon}
            id={linkItem.id}
            link={linkItem}
            external={linkItem.external}
            blank={linkItem.blank}
          />
        ))}
      </List>
    </Drawer>
  );
};

export default SideMenu;
