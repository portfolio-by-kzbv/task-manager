import React, { useState, useContext } from 'react';
import { useDispatch } from 'react-redux';
import { Menu, MenuItem, MenuProps, IconButton, withStyles } from '@material-ui/core';
import MoreHorizIcon from '@material-ui/icons/MoreHoriz';

import TaskManagerModalsContext from '../../../contexts/TaskManagerModalsContext';
import { createTask } from '../../../store/actions/tasks.action';
import Spinner from '../../Spinner/Spinner';
import { Dispatch } from '@dar/api-interfaces';
import { useHistory } from 'react-router-dom';

const StyledMenu = withStyles({
  paper: {
    border: '1px solid #CED7DF',
    boxShadow: '0px 4px 10px rgba(0, 0, 0, 0.15), 0px 2px 4px rgba(0, 0, 0, 0.2)',
    borderRadius: '4px',
    width: '120px',
    minWidth: '120px',
  },
})((props: MenuProps) => (
  <Menu
    elevation={0}
    getContentAnchorEl={null}
    anchorOrigin={{
      vertical: 'bottom',
      horizontal: 'right',
    }}
    transformOrigin={{
      vertical: 'top',
      horizontal: 'right',
    }}
    {...props}
  />
));

type Props = {
  icon?: React.ReactNode;
  task: any;
};

const TaskActions: React.FC<Props> = ({ icon, task }) => {
  const { changeTaskId, deleteModalToggle, handleDetailsModalOpen } = useContext(TaskManagerModalsContext);
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const [loading, setLoading] = useState(false);
  const openMenu = Boolean(anchorEl);
  const dispatch: Dispatch = useDispatch();
  const history = useHistory();

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const handleDeleteClick = () => {
    changeTaskId(task.id);
    deleteModalToggle();
    handleClose();
  };

  const handleDuplicateClick = () => {
    const duplicateTask = {
      ...task,
      title: task.title + ' (dup.)',
      status: 'New',
      status_id: 1,
    };
    handleClose();
    setLoading(true);
    dispatch(createTask(duplicateTask)).then((result: any) => {
      const lastAddedTask = result.action.payload;
      handleDetailsModalOpen(lastAddedTask.id);
      history.push(`/productivity/tasks/${lastAddedTask.id}`);
      setLoading(false);
    });
  };

  return (
    <React.Fragment>
      {loading && <Spinner />}
      {!loading && (
        <>
          <IconButton
            color="default"
            aria-label="more"
            aria-controls="task-menu"
            aria-haspopup="true"
            onClick={handleClick}
          >
            {icon ? icon : <MoreHorizIcon />}
          </IconButton>
          <StyledMenu id="task-actions" anchorEl={anchorEl} keepMounted open={openMenu} onClose={handleClose}>
            <MenuItem key="duplicate" onClick={handleDuplicateClick}>
              Duplicate
            </MenuItem>
            <MenuItem key="delete" onClick={handleDeleteClick}>
              Delete
            </MenuItem>
          </StyledMenu>
        </>
      )}
    </React.Fragment>
  );
};

export default TaskActions;
