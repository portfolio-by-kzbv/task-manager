import React, { useState } from 'react';
import SwipeableViews from 'react-swipeable-views';
import { Tabs, Tab, useTheme } from '@material-ui/core';

import { useStyles } from './taskDetails.styles';

import CommentsPanel from './DetailPanels/CommentsPanel/CommentsPanel';
import AttachmentsPanel from './DetailPanels/AttachmentsPanel/AttachmentsPanel';
import MeetingsPanel from './DetailPanels/MeetingsPanel';
import LinksPanel from './DetailPanels/LinksPanel/LinksPanel';

function a11yProps(index: number) {
  return {
    id: `simple-tab-${index}`,
    'aria-controls': `simple-tabpanel-${index}`,
  };
}

const AdditionalTaskDetails = () => {
  const classes = useStyles();
  const theme = useTheme();

  const [value, setValue] = useState(0);

  const handleChange = (event: React.ChangeEvent<Record<string, unknown>>, newValue: number) => {
    setValue(newValue);
  };

  const handleChangeIndex = (index: number) => {
    setValue(index);
  };

  return (
    <div className={classes.container}>
      <Tabs indicatorColor="primary" value={value} onChange={handleChange} aria-label="task filters">
        <Tab label="Comments" {...a11yProps(0)} />
        <Tab label="Attachments" {...a11yProps(1)} />
        <Tab label="Meetings" {...a11yProps(2)} />
        <Tab label="Links" {...a11yProps(3)} />
      </Tabs>
      <SwipeableViews
        axis={theme.direction === 'rtl' ? 'x-reverse' : 'x'}
        index={value}
        onChangeIndex={handleChangeIndex}
        className={classes.swipeable}
      >
        <CommentsPanel value={value} index={0} />
        <AttachmentsPanel
          variant="compact"
          allowedFileTypes={['pdf', 'png', 'jpeg', 'docx']}
          maximumFileSize={25}
          fileNumberLimit={7}
        />
        <MeetingsPanel value={value} index={2} />
        <LinksPanel value={value} index={3} />
      </SwipeableViews>
    </div>
  );
};

export default AdditionalTaskDetails;
