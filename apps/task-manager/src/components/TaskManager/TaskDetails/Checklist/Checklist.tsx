import React, { useMemo } from 'react';
import { List, makeStyles } from '@material-ui/core';
import { Container } from 'react-smooth-dnd';

import ToDoItem from './ToDoItem/ToDoItem';
import { Todo } from '@dar/api-interfaces';

const useStyles = makeStyles({
  list: {
    overflowX: 'hidden',
    overflowY: 'auto',
    marginTop: 16,
    height: 'calc(100% - 40px)',
  },
});

type Props = {
  todos: Todo[];
};

const Checklist: React.FC<Props> = ({ todos }) => {
  const classes = useStyles();
  const todoList = useMemo(() => todos, [todos]);

  return (
    <List className={classes.list}>
      <Container>
        {todoList.map((todo) => (
          <ToDoItem todo={todo} key={todo.id} />
        ))}
      </Container>
    </List>
  );
};

export default Checklist;
