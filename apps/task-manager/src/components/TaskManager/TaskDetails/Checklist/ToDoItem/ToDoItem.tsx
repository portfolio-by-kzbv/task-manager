import React, { useState, useCallback, useContext, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import cx from 'classnames';
import moment from 'moment';
import Checkbox from '@material-ui/core/Checkbox';
import { ListItem, ListItemIcon, IconButton, Typography } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import Spinner from '@dar/components/Spinner/Spinner';
import ToDoText from './ToDoText';
import { editTodo, deleteTodo } from '@dar/actions/checklist.action';
import { useStyles } from '../checklist.styles';
import { Dispatch } from '@dar/api-interfaces';
import { KeyboardDatePicker } from '@material-ui/pickers';
import { Todo } from '@dar/api-interfaces';
import TaskManagerModalsContext from '@dar/contexts/TaskManagerModalsContext';
import TodayIcon from '@material-ui/icons/Today';
import { toast } from '@dartech/dms-ui';

const ToDoItem: React.FC<{ todo: Todo }> = ({ todo }) => {
  const classes = useStyles();
  const [loading, setLoading] = useState(false);
  const [isHovered, setIsHovered] = useState(false);
  const dispatch: Dispatch = useDispatch();
  const { task, canEdit } = useContext(TaskManagerModalsContext);
  const [isError, setIsError] = useState(false);
  const [isOpen, setIsOpen] = useState(false);
  const [isExpired, setIsExpired] = useState(false);
  const [isToday, setIsToday] = useState(false);

  useEffect(() => {
    if (todo.due_time) {
      const dateNow = new Date();
      const dueDate = new Date(todo.due_time);

      setIsExpired(dueDate < dateNow);
      setIsToday(
        dueDate.getDate() === dateNow.getDate() &&
          dueDate.getMonth() === dateNow.getMonth() &&
          dueDate.getFullYear() === dateNow.getFullYear()
      );
    }
  }, [todo.due_time]);

  const handleEdit = useCallback(
    (data: Todo) => {
      setLoading(true);
      dispatch(editTodo(data)).then(() => {
        setLoading(false);
      });
    },
    [dispatch]
  );

  const toggleDone = (event: React.ChangeEvent<HTMLInputElement>) => {
    const data = {
      ...todo,
      is_done: event.target.checked,
    };

    handleEdit(data);
  };

  const handleSchedule = (todoDueDate) => {
    const taskStartDate = new Date(task.start_time);
    const taskEndDate = new Date(task.end_time);
    todoDueDate = new Date(todoDueDate);
    todoDueDate.setHours(23, 59, 59);

    if (todoDueDate >= taskStartDate && todoDueDate <= taskEndDate) {
      setIsError(false);
      const data = {
        ...todo,
        due_time: todoDueDate,
      };

      handleEdit(data);

      const dateNow = new Date();
      setIsExpired(todoDueDate < dateNow);
      setIsToday(
        todoDueDate.getDate() === dateNow.getDate() &&
          todoDueDate.getMonth() === dateNow.getMonth() &&
          todoDueDate.getFullYear() === dateNow.getFullYear()
      );
    } else {
      setIsError(true);
      setIsOpen(false);
      setIsHovered(false);
      if (todoDueDate < taskStartDate) {
        toast.warning('Todo due time should be later than task start date', {
          duration: 3000,
        });
      } else {
        toast.warning('Todo due time should be earlier than task end date', {
          duration: 3000,
        });
      }
    }
  };

  const handleDelete = () => {
    setLoading(true);
    dispatch(deleteTodo(todo.id))
      .then(() => {
        setLoading(false);
      })
      .catch(() => {
        setLoading(false);
      });
  };

  return (
    <ListItem
      className={cx(classes.root)}
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
    >
      {loading && <Spinner size={20} absolute />}

      <ListItemIcon className={classes.listIcon}>
        <Checkbox size="medium" disabled={!canEdit} disableRipple checked={todo.is_done} onChange={toggleDone} />
      </ListItemIcon>

      <div className={classes.textAndDate}>
        {todo.due_time && (
          <Typography className={classes.date}>{moment(todo.due_time).format('DD/MM/yyyy')}</Typography>
        )}
        <ToDoText disabled={!canEdit} todo={todo} onEdit={handleEdit} onDelete={handleDelete} />
      </div>

      {isHovered && canEdit ? (
        <div className={classes.toolBarRight}>
          <IconButton
            aria-label="edit todo item"
            onClick={() => setIsOpen(true)}
            edge="end"
            className={classes.deleteButton}
          >
            <TodayIcon />
          </IconButton>
          <IconButton aria-label="delete todo item" onClick={handleDelete} edge="end" className={classes.deleteButton}>
            <DeleteIcon />
          </IconButton>
          <KeyboardDatePicker
            open={isOpen}
            onClose={() => setIsOpen(false)}
            value={todo.due_time}
            onChange={handleSchedule}
            format="dd/MM/yyyy"
            error={isError}
            style={{ width: '0', height: '0', visibility: 'hidden' }}
            variant="inline"
            disableToolbar
            autoOk
          />
        </div>
      ) : (
        (isExpired && (
          <div className={[classes.labelWrapper, classes.expiredWrapper].join(' ')}>
            <Typography className={[classes.label, classes.expiredLabel].join(' ')}>Expired</Typography>
          </div>
        )) ||
        (isToday && (
          <div className={[classes.labelWrapper, classes.todayWrapper].join(' ')}>
            <Typography className={[classes.label, classes.todayLabel].join(' ')}>Today</Typography>
          </div>
        ))
      )}
    </ListItem>
  );
};

export default ToDoItem;
