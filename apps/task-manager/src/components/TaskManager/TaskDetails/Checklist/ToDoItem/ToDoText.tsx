import React, { useState, useRef } from 'react';
import cx from 'classnames';
import { Typography, TextField, Box } from '@material-ui/core';

import { useOutsideClick } from '../../../../../hooks/useOutsideClick';
import { useStyles } from '../checklist.styles';
import { Todo } from '@dar/api-interfaces';

type Props = {
  todo: Todo;
  onEdit: (data: Todo) => void;
  onDelete: () => void;
  disabled?: boolean;
};

const ToDoText: React.FC<Props> = ({ todo, onEdit, onDelete, disabled }) => {
  const ref = useRef(null);
  const classes = useStyles();
  const [isEditing, setIsEditing] = useState(false);
  const [value, setValue] = useState(todo.title);

  const handleEdit = () => {
    if (value && typeof value === 'string' && value.trim() && value !== todo.title) {
      const data = {
        ...todo,
        title: value,
        updatedAt: new Date().toISOString(),
      };

      onEdit(data);
    } else if (!value || typeof value !== 'string' || !value.trim()) {
      onDelete();
    }

    setIsEditing(false);
  };

  useOutsideClick(ref, handleEdit);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => setValue(event.target.value);

  const handleSubmit = (event: React.ChangeEvent<HTMLFormElement>) => {
    event.preventDefault();
    handleEdit();
  };

  return (
    <Box flexGrow={1}>
      {(isEditing && !disabled) ? (
        <form onSubmit={handleSubmit}>
          <TextField
            variant="outlined"
            margin="dense"
            fullWidth
            ref={ref}
            value={value}
            onChange={handleChange}
            InputProps={{
              className: classes.titleEdit,
            }}
            disabled={disabled}
          />
        </form>
      ) : (
        <Typography
          className={cx(classes.text, { done: todo.is_done })}
          variant="h6"
          onClick={() => setIsEditing(true)}
        >
          {value}
        </Typography>
      )}
    </Box>
  );
};

export default ToDoText;
