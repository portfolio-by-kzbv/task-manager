import React, { useContext } from 'react';
import { Box, makeStyles, Typography } from '@material-ui/core';

import CheckIcon from '@material-ui/icons/Check';

import Spinner from '../../../Spinner/Spinner';
import Checklist from './Checklist';
import CreateToDo from './CreateToDo';
import { useChecklistState } from '../../../../selectors/checklist.selector';
import TaskManagerModalsContext from '@dar/contexts/TaskManagerModalsContext';

const useStyles = makeStyles({
  content: {
    minHeight: 'fit-content',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
});

const ToDo: React.FC = () => {
  const classes = useStyles();
  const { loading, checklist } = useChecklistState();
  const { canEdit, isAssignee } = useContext(TaskManagerModalsContext);

  return (
    <Box id="todo-list" className={classes.content}>
      {loading ? (
        <Spinner />
      ) : (
        <>
          {(canEdit || isAssignee) && <CreateToDo />}
          {checklist.length ? (
            <Checklist todos={checklist} />
          ) : (
            <Box
              display="flex"
              justifyContent="space-between"
              alignItems="center"
              flexDirection="column"
              marginTop="30px"
            >
              <CheckIcon fontSize="small" />
              <Typography>No tasks yet</Typography>
            </Box>
          )}
        </>
      )}
    </Box>
  );
};

export default ToDo;
