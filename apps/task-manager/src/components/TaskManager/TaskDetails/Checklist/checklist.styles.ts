import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  root: {
    height: '40px',
    display: 'flex',
    justifyContent: 'space-between',
    padding: theme.spacing(0, 3, 0, 3),
    marginBottom: theme.spacing(1),
    borderRadius: '8px',
    '&:hover, &:focus': {
      background: `${theme.palette.grey[100]}`,
    },
  },
  listIcon: {
    minWidth: 'unset',
    marginRight: 10,
  },
  text: {
    '&.done': {
      textDecoration: 'line-through',
    },
  },
  titleEdit: {
    border: 'none',
    padding: '0',
    borderBottom: `1px solid ${theme.palette.primary[200]}`,
    fontSize: 16,
    lineHeight: 24,
    borderRadius: '0',
    '&:hover, &:focus': {
      border: 'none',
      borderBottom: `1px solid ${theme.palette.primary[200]}`,
      outline: 'none',
    },
  },
  formControl: {
    '&:hover, &:focus': {
      border: 'none',
      borderBottom: `1px solid ${theme.palette.primary[200]}`,
      outline: 'none',
    },
  },
  textAndDate: {
    flexGrow: 2,
  },
  date: {
    fontSize: '12px',
    color: `${theme.palette.grey[400]}`,
  },
  toolBarRight: {
    width: 60,
    display: 'flex',
    justifyContent: 'space-between',
  },
  deleteButton: {
    padding: '0',
  },
  labelWrapper: {
    padding: theme.spacing(1, 1, 1, 1),
    borderRadius: '4px',
  },
  expiredWrapper: {
    background: `${theme.palette.error[100]}`,
  },
  todayWrapper: {
    background: `${theme.palette.warning[100]}`,
  },
  label: {
    textTransform: 'uppercase',
    fontSize: '12px',
    fontWeight: 1000,
  },
  expiredLabel: {
    color: `${theme.palette.error[600]}`,
  },
  todayLabel: {
    color: `${theme.palette.warning[500]}`,
  },
}));
