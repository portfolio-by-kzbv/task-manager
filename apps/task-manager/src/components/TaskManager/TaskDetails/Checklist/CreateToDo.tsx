import React, { useState } from 'react';
import { useDispatch } from 'react-redux';
import { useParams } from 'react-router-dom';
import { TextField, InputAdornment, IconButton, makeStyles } from '@material-ui/core';
import AddCircleIcon from '@material-ui/icons/AddCircle';

import Spinner from '../../../Spinner/Spinner';
import { createTodo } from '../../../../store/actions/checklist.action';
import { Dispatch } from '@dar/api-interfaces';

const useStyles = makeStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'flex-start',
    position: 'relative',
    marginTop: '16px'
  },
});

const CreateToDo: React.FC = () => {
  const { id } = useParams<{ id: string; }>();
  const classes = useStyles();
  const [title, setTitle] = useState('');
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState('');
  const dispatch: Dispatch = useDispatch();

  const handleTextChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setTitle(event.target.value);
    if (event.target.value) {
      setError('');
    }
  };

  const handleSubmit = (event: React.FormEvent<HTMLFormElement> | React.MouseEvent<HTMLButtonElement>) => {
    event.preventDefault();

    if (title) {
      setLoading(true);
      const data = {
        task_id: +id,
        title,
        is_done: false,
      };
      dispatch(createTodo(data)).then(() => {
        setTitle('');
        setLoading(false);
      });
    } else {
      setError('Please, add some text');
    }
  };

  return (
    <form className={classes.root} onSubmit={handleSubmit}>
      {loading && <Spinner absolute size={20} />}
      <TextField
        placeholder="Add to do list item"
        variant="standard"
        fullWidth
        value={title}
        onChange={handleTextChange}
        disabled={loading}
        error={Boolean(error)}
        helperText={error}
        InputProps={{
          endAdornment: (
            <InputAdornment position="end">
              <IconButton aria-label="add todo item" onClick={handleSubmit} edge="end">
                <AddCircleIcon />
              </IconButton>
            </InputAdornment>
          ),
        }}
      />
    </form>
  );
};

export default CreateToDo;
