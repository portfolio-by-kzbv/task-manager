import { useState, useEffect, useCallback, useContext, useMemo } from 'react';
import { Task, TaskDTO } from '@dar/api-interfaces';
import { useDispatch } from 'react-redux';
import { editTask, updateTaskStatus } from '@dar/actions/tasks.action';
import TaskManagerFiltersContext from '@dar/contexts/TaskManagerFiltersContext';
import { useUsersState } from '@dar/selectors/users';

import { Dispatch } from '@dar/api-interfaces';
import { TaskErrors } from '@dar/api-interfaces';
import { toast } from '@dartech/dms-ui';

const emptyValues = {
  title: '',
  description: '',
  assignee_id: null,
  creator_id: null,
  priority: '',
  status_id: 0,
  group_id: '',
  start_time: '',
  end_time: '',
  company_id: 0,
};
const useTaskEditForm = (task: Task) => {
  const allUsers = useUsersState();
  const dispatch: Dispatch = useDispatch();
  const [loading, setLoading] = useState<boolean>(false);
  const { updateTasks } = useContext(TaskManagerFiltersContext);
  const [errors, setErrors] = useState<TaskErrors>({
    title: '',
    dates: '',
  });
  const [values, setValues] = useState<Task>(emptyValues);
  const assignee = useMemo(() => allUsers.find((user) => user.id === task?.assignee_id), [allUsers, task]);

  useEffect(() => {
    if (assignee && !values.assignee) {
      setValues((v) => ({ ...v, assignee }));
    }
  }, [values, assignee]);

  useEffect(() => {
    if (!task) {
      return;
    }
    setValues(task);
  }, [task]);

  const handleChange = (name: string, value: unknown) => {
    setValues({ ...values, [name]: value });
  };

  useEffect(() => {
    if (!values) {
      return;
    }
    const { title, start_time, end_time } = values;
    const err: TaskErrors = {};

    if (!title?.length) {
      err.title = 'Please, fill the title';
    }

    if (title?.length < 3) {
      err.title = 'The name must be at least 3 characters long';
    }

    if (new Date(start_time) > new Date(end_time)) {
      err.dates = 'The start date should be no later than task end date';
    }

    setErrors(err);
  }, [values]);

  const getPayload = () => {
    const {
      assignee,
      status_id,
      status,
      cluster,
      project,
      title,
      description,
      start_time,
      priority,
      end_time,
      creator_id,
    } = values;
    const payload: TaskDTO = {
      assignee_id: assignee.id,
      status_id: status_id,
      group_id: cluster?.id,
      project_id: project?.id,
      title,
      description,
      status,
      start_time,
      end_time,
      priority,
      creator_id,
    };
    return payload;
  };

  const handleEdit = useCallback(
    (payload: TaskDTO) => {
      setLoading(true);
      return dispatch(editTask(task.id, payload))
        .then(() => {
          return dispatch(updateTaskStatus(task.id, { status_id: payload.status_id }));
        })
        .finally(() => {
          updateTasks();
          setLoading(false);
        });
    },
    [dispatch, updateTasks, task]
  );

  const handleSubmit = () => {
    if (new Date(values.start_time) > new Date(values.end_time)) {
      toast.warning('The start date should be no later than task end date', { duration: 3000 });
    }
    if (!Object.keys(errors).length) {
      const task = getPayload();
      return handleEdit(task);
    } else {
      setErrors(errors);
    }
  };

  const resetForm = () => {
    setValues(emptyValues);
  };

  return {
    task,
    values,
    errors,
    loading,
    handleChange,
    handleSubmit,
    resetForm,
  };
};

export default useTaskEditForm;
