import React, { useState, useContext, useEffect, useMemo, useRef } from 'react';
import { useParams, useHistory } from 'react-router-dom';
import { Box, Grid, Typography, Button, IconButton, TextField, ClickAwayListener } from '@material-ui/core';
import MoreVertIcon from '@material-ui/icons/MoreVert';
import CloseIcon from '@material-ui/icons/Close';
import CreateIcon from '@material-ui/icons/Create';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import FlagIcon from '@material-ui/icons/Flag';

import SideModal from '../../SideModal/SideModal';
import DatePicker from '@dar/components/DatePicker/DatePicker';
import AdditionalTaskDetails from './AdditionalTaskDetails';
import AssigneeSelect from '../../Selects/AssigneeSelect';
import PrioritySelect from '../../Selects/PrioritySelect';
import StatusSelect from '../../Selects/StatusSelect';
import Spinner from '../../Spinner/Spinner';
import ToDo from './Checklist/ToDo';
import CreateLink from '@dar/components/TaskManager/CreateLink/CreateLink';

import TaskManagerModalsContext from '../../../contexts/TaskManagerModalsContext';
import useTaskEditForm from './useTaskEditForm';
import { useStyles } from './taskDetails.styles';
import TaskActions from './TaskActions';
import { User } from '@dar/api-interfaces';
import AutoCompleteSelect from '../../Selects/AutoCompleteSelect';
import { useDispatch } from 'react-redux';
import { getProjects } from '../../../store/actions/projects.actions';
import { useClusters } from '../../../selectors/cluster.selector';
import { getClusters } from '../../../store/actions/clusters.action';
import { useProjects } from '../../../selectors/projects.selector';
import UserInfo from '@dar/components/Users/UserInfo';
import { priorities } from '../../../mocks/priorities';
import { useStatusesState } from '@dar/selectors/task-status.selector';
import { getChecklist } from '@dar/actions/checklist.action';
import { toast } from '@dartech/dms-ui';
import { isValidDate } from '../../../utils/date';

const TASKS_PATH = '/productivity/tasks';

const TaskDetails = () => {
  const classes = useStyles();
  const {
    detailsModalOpen,
    handleDetailsModalClose,
    handleDetailsModalOpen,
    canEdit,
    isAssignee,
    isCreateLinkModalOpened,
    closeCreateLinkModal,
    task,
    loading: taskLoading,
  } = useContext(TaskManagerModalsContext);

  const { id } = useParams<{ id: string }>();
  const [editTitle, setEditTitle] = useState(false);
  const [priorityColor, setPriorityColor] = useState('#000');
  const [statusColor, setStatusColor] = useState('#000');
  const dispatch = useDispatch();
  const { clusters } = useClusters();
  const { projects } = useProjects();
  const { statuses } = useStatusesState();
  const history = useHistory();

  const { values, errors, loading, handleChange, handleSubmit, resetForm } = useTaskEditForm(task);

  const handleClickAway = () => {
    setEditTitle(false);
  };

  const handleSaveAndClose = () => {
    handleSubmit()?.then(() => {
      handleClose();
    });
  };

  const handleClose = () => {
    history.push(TASKS_PATH);
    setTimeout(() => {
      handleDetailsModalClose();
      resetForm();
    }, 0);
  };

  useEffect(() => {
    if (!id || detailsModalOpen) {
      return;
    }
    handleDetailsModalOpen(+id);
  }, [handleDetailsModalOpen, id, detailsModalOpen]);

  useEffect(() => {
    const q = { task_id: +id };
    dispatch(getClusters());
    dispatch(getProjects());
    dispatch(getChecklist({ q }));
  }, [dispatch, id]);

  useEffect(() => {
    const taskPriority = priorities.find((item) => item.name.toLowerCase() === task?.priority?.toLowerCase());
    setPriorityColor(taskPriority?.color ? taskPriority.color : '#000');
  }, [task]);

  useEffect(() => {
    const taskStatus = statuses.find((item) => item.id === task?.status_id);
    setStatusColor(taskStatus?.color ? taskStatus.color : '#000');
  }, [task, statuses]);

  return (
    <>
      <SideModal openDrawer={detailsModalOpen} onClose={handleClose} size="large">
        {(loading || taskLoading) && <Spinner absolute />}
        <Grid container>
          <div className={classes.header}>
            <Grid item container lg={8} xs={12}>
              {editTitle && canEdit ? (
                <ClickAwayListener onClickAway={handleClickAway}>
                  <TextField
                    className={classes.formControl}
                    autoFocus
                    id="task-title-edit"
                    value={values.title}
                    onChange={(event) => handleChange('title', event.target.value)}
                    error={Boolean(errors.title)}
                    helperText={errors.title}
                    InputProps={{
                      className: classes.titleEdit,
                    }}
                    onKeyPress={(ev) => {
                      if (ev.key === 'Enter') {
                        handleClickAway();
                      }
                    }}
                  />
                </ClickAwayListener>
              ) : (
                <Box display="flex" alignItems="center">
                  <Typography variant="h2" onClick={() => setEditTitle(true)}>
                    {values.title}
                  </Typography>
                  {canEdit && (
                    <IconButton aria-label="menu" onClick={() => setEditTitle(true)}>
                      <CreateIcon />
                    </IconButton>
                  )}
                </Box>
              )}
            </Grid>

            <Grid item container lg={4} xs={12} className={classes.buttons}>
              {(canEdit || isAssignee) && (
                <>
                  <Box ml={4}>
                    <Button
                      variant="contained"
                      color="primary"
                      size="medium"
                      onClick={handleSaveAndClose}
                      type="submit"
                    >
                      Save & Close
                    </Button>
                  </Box>
                  <Box ml={4}>
                    <TaskActions task={task} icon={<MoreVertIcon />} />
                  </Box>
                </>
              )}
              <Box ml={4}>
                <IconButton aria-label="close" color="default" onClick={handleClose}>
                  <CloseIcon />
                </IconButton>
              </Box>
            </Grid>
          </div>
        </Grid>

        {Object.keys(values).length !== 0 && (
          <div className={classes.details}>
            <Box>
              <Typography variant="body2">Status</Typography>
              <Box ml={5} display="grid">
                {canEdit || isAssignee ? (
                  <StatusSelect
                    variant="filled"
                    value={values.status_id}
                    onChange={(event) => handleChange('status_id', event.target.value)}
                    currentStatus={task && task.status}
                  />
                ) : (
                  <Box display="flex" alignItems="center">
                    <Box mr={1}>
                      <FiberManualRecordIcon fontSize="small" style={{ fill: statusColor }} />
                    </Box>
                    <Typography variant="h6">{task?.status}</Typography>
                  </Box>
                )}
              </Box>
            </Box>

            <Box ml={10}>
              <Typography variant="body2">Assign to</Typography>
              <Box ml={5} display="grid">
                {canEdit ? (
                  <AssigneeSelect value={values.assignee} onSelect={(value: User) => handleChange('assignee', value)} />
                ) : (
                  values.assignee && <UserInfo user={values.assignee} />
                )}
              </Box>
            </Box>

            <Box ml={10}>
              <Typography variant="body2">Priority</Typography>
              <Box ml={5} display="grid">
                {canEdit ? (
                  <PrioritySelect
                    variant="filled"
                    value={values.priority}
                    onChange={(event) => handleChange('priority', event.target.value)}
                  />
                ) : (
                  values.priority && (
                    <Box display="flex" alignItems="center">
                      <Box mr={1}>
                        <FlagIcon fontSize="small" style={{ fill: priorityColor }} />
                      </Box>
                      <Typography variant="h6">{task?.priority}</Typography>
                    </Box>
                  )
                )}
              </Box>
            </Box>
          </div>
        )}

        <div className={classes.info}>
          <Grid item container lg={5} xs={12}>
            <div className={classes.description}>
              <Box display="flex" flexDirection="row" justifyContent="space-between">
                <Box width={1} marginRight="12px">
                  <DatePicker
                    value={new Date(values.start_time)}
                    onChange={(value) => {
                      if (isValidDate(value)) {
                        if (new Date(value) > new Date(values.end_time)) {
                          toast.warning('Task start date should not be later than task end date.', {
                            duration: 3000,
                          });
                        }
                        handleChange('start_time', new Date(value.setHours(0, 0, 0)));
                      }
                    }}
                    label="Start Date"
                    disabled={!canEdit}
                    fullWidth
                    disablePast
                  />
                </Box>
                <Box width={1} marginLeft="12px">
                  <DatePicker
                    value={new Date(values.end_time)}
                    onChange={(value) => {
                      if (isValidDate(value)) {
                        if (new Date(value) < new Date(values.start_time)) {
                          toast.warning('Task end date should not be earlier than task start date.', {
                            duration: 3000,
                          });
                        }
                        handleChange('end_time', new Date(value.setHours(23, 59, 59)));
                      }
                    }}
                    label="End Date"
                    disabled={!canEdit}
                    fullWidth
                    disablePast
                  />
                </Box>
              </Box>

              <div className={classes.block}>
                <TextField
                  id="task-description"
                  label="Description"
                  onChange={(event) => handleChange('description', event.target.value)}
                  multiline
                  rows={4}
                  fullWidth
                  value={values.description ?? ''}
                  disabled={!canEdit}
                  InputProps={{
                    style: { height: 'auto' },
                  }}
                />
              </div>

              <div className={classes.block}>
                <Box display="flex" flexDirection="row" justifyContent="space-between">
                  <Box width={1} marginRight="12px">
                    {canEdit ? (
                      <AutoCompleteSelect
                        variant="outlined"
                        value={values.cluster ?? ''}
                        error={errors.cluster}
                        label="Cluster"
                        prompt="Select"
                        valueId="id"
                        valueName="name"
                        options={clusters}
                        onSelect={(value: any) => handleChange('cluster', value)}
                      />
                    ) : (
                      <Box mb={4}>
                        <Typography>Cluster</Typography>
                        <Typography>{values.cluster ? values.cluster.name : '-----'}</Typography>
                      </Box>
                    )}
                  </Box>
                  <Box width={1} marginLeft="12px">
                    {canEdit ? (
                      <AutoCompleteSelect
                        variant="outlined"
                        value={values.project ?? ''}
                        error={errors.project}
                        label="Project"
                        prompt="Select"
                        valueId="id"
                        valueName="name"
                        options={projects}
                        onSelect={(value: any) => handleChange('project', value)}
                      />
                    ) : (
                      <Box mb={4}>
                        <Typography>Project</Typography>
                        <Typography>{values.project ? values.project.name : '-----'}</Typography>
                      </Box>
                    )}
                  </Box>
                </Box>
              </div>
              <div className={classes.checklist}>
                <Typography variant="h4">Checklist</Typography>
                <ToDo />
              </div>
            </div>
          </Grid>

          <Grid item container lg={7} xs={12}>
            <AdditionalTaskDetails />
          </Grid>
        </div>
      </SideModal>
      <CreateLink
        openDrawer={isCreateLinkModalOpened}
        onClose={() => closeCreateLinkModal()}
        callback={() => closeCreateLinkModal()}
      />
    </>
  );
};

export default TaskDetails;
