import React, { useCallback, useContext, useEffect, useState } from 'react';

import { Button, Collapse, Container, List, makeStyles, Paper, Typography } from '@material-ui/core';

import AddIcon from '@material-ui/icons/Add';

import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import LinksItem from './LinksItem';

import { Task } from '@dar/api-interfaces';
import { useDispatch } from 'react-redux';
import TaskManagerModalsContext from '@dar/contexts/TaskManagerModalsContext';
import { deleteAndUpdateLink } from '@dar/actions/links.action';
import { LinkActionTypes, LinkTypes, UseTasksContext } from '@dar/contexts/TaskManagerModalsContext/useTasks';
import { getCluster } from '@dar/actions/clusters.action';
import { getProject } from '@dar/actions/projects.actions';
import { Dispatch } from '@dar/api-interfaces';

type Props = {
  linkName: LinkTypes;
  tasks: Task[];
};

const useLinksCollapseStyles = makeStyles((theme) => ({
  paper: {
    // padding: '8px 7px',
  },
  button: {
    paddingLeft: '0',
    paddingRight: '0',
    '&:hover': {
      backgroundColor: 'initial',
      borderColor: 'transparent',
    },
  },
  buttonContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textSecondary: { '& > path ': { fill: theme.palette.primary[200] } },
}));

const LinksCollapse: React.FC<Props> = ({ linkName, tasks = [] }) => {
  const linksCollapseClasses = useLinksCollapseStyles();
  const [open, setOpen] = useState(false);
  const {
    openCreateLinkModal,
    changeLinkType,
    changeLinkActionType,
    changeLinkForm,
    taskID,
    canEdit, 
  } = useContext<UseTasksContext>(TaskManagerModalsContext);

  const handleAdd = () => {
    changeLinkType(linkName);
    changeLinkActionType(LinkActionTypes.CREATE);
    openCreateLinkModal();
  };

  const dispatch = useDispatch();
  const promiseDispatch: Dispatch = useDispatch();

  const handleDelete = (task_id: number) => {
    if (linkName === LinkTypes.AFFECTS) {
      dispatch(deleteAndUpdateLink(taskID, task_id, taskID));
    } else {
      dispatch(deleteAndUpdateLink(task_id, taskID, taskID));
    }
  };

  const handleEdit = (task: Task) => {
    changeLinkActionType(LinkActionTypes.EDIT);
    changeLinkType(linkName);

    if (task.group_id) {
      promiseDispatch(getCluster(task.group_id)).then((res) => {
        changeLinkForm({ cluster: res.value });
      });
    } else {
      changeLinkForm({
        cluster: null,
      });
    }

    if (task.project_id) {
      promiseDispatch(getProject(task.project_id)).then((res) => {
        changeLinkForm({
          project: res.value,
        });
      });
    } else {
      changeLinkForm({
        project: null,
      });
    }

    changeLinkForm({
      task_id: taskID,
      task,
    });
    openCreateLinkModal();
  };

  return (
    <Paper
      variant="outlined"
      classes={{
        root: linksCollapseClasses.paper,
      }}
    >
      <Container
        classes={{
          root: linksCollapseClasses.buttonContainer,
        }}
      >
        <Button
          variant="text"
          color="secondary"
          size="large"
          aria-label="expand row"
          startIcon={open ? <KeyboardArrowDownIcon /> : <KeyboardArrowRightIcon />}
          onClick={() => setOpen(!open)}
          classes={{
            root: linksCollapseClasses.button,
            textSecondary: linksCollapseClasses.textSecondary,
          }}
        >
          <Typography variant={'h5'}>{`${linkName} (${tasks.length})`}</Typography>
        </Button>
        {canEdit && (
          <Button
            variant="text"
            color="primary"
            onClick={handleAdd}
            classes={{
              root: linksCollapseClasses.button,
              textSecondary: linksCollapseClasses.textSecondary,
            }}
          >
            <AddIcon fontSize="inherit" />
            {'Add'}
          </Button>
        )}
      </Container>
      <Collapse in={open} timeout="auto" unmountOnExit>
        <Container>
          <List>
            {tasks.map((task) => (
              <LinksItem
                task={task}
                key={task.id}
                onDelete={() => {
                  handleDelete(task.id);
                }}
                onEdit={() => {
                  handleEdit(task);
                }}
              />
            ))}
          </List>
        </Container>
      </Collapse>
    </Paper>
  );
};

export default LinksCollapse;
