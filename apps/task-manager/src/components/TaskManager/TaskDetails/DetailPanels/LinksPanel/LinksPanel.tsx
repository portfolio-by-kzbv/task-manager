import { getAffects, getDepends, getLinks } from '@dar/actions/links.action';
import TaskManagerModalsContext from '@dar/contexts/TaskManagerModalsContext';
import { LinkTypes } from '@dar/contexts/TaskManagerModalsContext/useTasks';
import { useLinks } from '@dar/selectors/links.selector';
import { makeStyles } from '@material-ui/core';
import React, { useContext, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import LinksCollapse from './LinksCollapse';
interface PanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

const useStyles = makeStyles((theme) => ({
  container: {
    marginTop: '25px',

    '& > *': {
      marginBottom: '8px',
    },

    '&::last-child()': {
      marginBottom: '0',
    },
  },
}));

const LinksPanel = ({ children, value, index, ...other }: PanelProps) => {
  const classes = useStyles();

  const { taskID } = useContext(TaskManagerModalsContext);

  const { links, affects, depends } = useLinks();

  const dispatch = useDispatch();

  useEffect(() => {
    if (taskID) {
      dispatch(getLinks(taskID));
    }
  }, [taskID, dispatch]);

  useEffect(() => {
    if (links?.affects) {
      dispatch(getAffects(links.affects));
    }

    if (links?.depends) {
      dispatch(getDepends(links.depends));
    }
  }, [links, dispatch]);

  return (
    <div
      className={classes.container}
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      <LinksCollapse linkName={LinkTypes.AFFECTS} tasks={affects} />
      <LinksCollapse linkName={LinkTypes.DEPENDS} tasks={depends} />
    </div>
  );
};

export default LinksPanel;
