import {
  Box,
  IconButton,
  ListItem,
  ListItemIcon,
  ListItemSecondaryAction,
  ListItemText,
  makeStyles,
  Typography,
} from '@material-ui/core';
import React, {  useEffect, useState, useContext } from 'react';

import NoteIcon from '@material-ui/icons/Note';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';

import { Task, TaskStatus } from '@dar/api-interfaces';
import { useStatusesState } from '@dar/selectors/task-status.selector';
import TaskManagerModalsContext from '@dar/contexts/TaskManagerModalsContext';

type Props = {
  task: Task;
  onDelete: () => void;
  onEdit: () => void;
};

const useStyles = makeStyles((theme) => ({
  container: {
    '&:hover': {
      background: '#E6ECF1',
    },
  },
  multiline: {
    display: 'flex',
    flexDirection: 'column-reverse',
  },
  primary: {
    color: theme.palette.primary[200],
  },
  secondary: {
    color: '#8A96A1',
  },
}));

const LinksItem: React.FC<Props> = ({ task, onDelete, onEdit }) => {
  const classes = useStyles();
  const [statusColor, setStatusColor] = useState('#000');
  const [isHovered, setIsHovered] = useState(false);
  const { statuses } = useStatusesState();
  const { canEdit } = useContext(TaskManagerModalsContext);

  useEffect(() => {
    const taskStatus = statuses.find((item: TaskStatus) => item.id === task.status_id);
    setStatusColor(taskStatus?.color ? taskStatus.color : '#000');
  }, [task.status_id, statuses]);

  return (
    <ListItem
      onMouseEnter={() => setIsHovered(true)}
      onMouseLeave={() => setIsHovered(false)}
      classes={{
        container: classes.container,
      }}
    >
      <ListItemIcon>
        <NoteIcon color="secondary" />
      </ListItemIcon>
      <ListItemText
        primary={task.title}
        secondary={`ID: ${task.id}`}
        classes={{
          multiline: classes.multiline,
          primary: classes.primary,
          secondary: classes.secondary,
        }}
      />
      <ListItemSecondaryAction
        // Need to double event listeners since
        // hover effect on parent dissapears when hover ListItemSecondayAction
        onMouseEnter={() => setIsHovered(true)}
        onMouseLeave={() => setIsHovered(false)}
      >
        {(isHovered && canEdit) ? (
          <React.Fragment>
            <IconButton edge="end" aria-label="edit" onClick={onEdit}>
              <EditIcon />
            </IconButton>
            <IconButton edge="end" aria-label="delete" onClick={onDelete}>
              <DeleteIcon />
            </IconButton>
          </React.Fragment>
        ) : (
          <Box display="flex" alignItems="center">
            <Box mr={1}>
              <FiberManualRecordIcon fontSize="small" style={{ fill: statusColor }} />
            </Box>
            <Typography variant="h6">{task.status}</Typography>
          </Box>
        )}
      </ListItemSecondaryAction>
    </ListItem>
  );
};

export default LinksItem;
