import React from 'react';

import { Typography} from '@material-ui/core';

import { useStyles } from '../taskDetails.styles';
import PeopleAltIcon from '@material-ui/icons/PeopleAlt';

interface PanelProps {
    children?: React.ReactNode;
    index: any;
    value: any;
}

const MeetingsPanel = ({ children, value, index, ...other } : PanelProps) => {
    const classes = useStyles();

    return (
        <div 
            className={classes.container}
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}>
            <PeopleAltIcon/>
            <Typography variant='h2'>
                No meetings yet
            </Typography>
            <Typography variant='body1'>
                Start a meeting with your team members
            </Typography>
        </div>
    )
}

export default MeetingsPanel; 