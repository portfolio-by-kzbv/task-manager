import styled from 'styled-components';
import { theme } from '@dartech/dms-ui';

const Attachments = styled.div`
  .AttachmentsWrapper {
    height: auto;
    min-width: 490px;
    border: 1px dashed #b7c3cc;
    border-radius: 4px;
    box-sizing: content-box;
    padding: 32px;
  }
  .AttachmentsBig {
    background: ${theme.palette.grey[50]};

    &:hover {
      border: 2px solid ${theme.palette.primary[200]};
      margin: -1px;
    }
  }
  .AttachmentsWrapperWide {
    width: 608px;
  }
  .AttachmentsWrapperEmpty {
    height: 240px;
    background-color: ${theme.palette.grey[100]};
  }
  .AttachmentsWithError {
    border: 2px ${theme.palette.error[600]} solid;
    margin: -1px;

    &:hover {
      border: 2px ${theme.palette.primary[600]} solid;
    }
  }
  .AttachmentsCompact {
    display: flex;
    flex-direction: column;
    border: none;
    align-items: center;

    &:hover {
      border: none;
    }
  }
  .AttachmentsError {
    margin-top: 4px;
    font-size: 12px;
    text-align: left;
    color: ${theme.palette.error[600]};
    margin-bottom: -8px;
  }
`;

export const Styled = {
  Attachments,
};
