import React, { useEffect, useContext, useState } from 'react';
import { useDispatch } from 'react-redux';
import cn from 'classnames';
import { Dispatch } from '@dar/api-interfaces';

import { Typography, Box } from '@material-ui/core';
import AttachmentsUpload from '@dar/components/Attachment/AttachmentsUpload/AttachmentsUpload';
import AttachmentsList from '@dar/components/Attachment/AttachmentsList/AttachmentsList';
import Spinner from '@dar/components/Spinner';
import TaskManagerModalsContext from '@dar/contexts/TaskManagerModalsContext';
import AttachmentIcon from '@material-ui/icons/Attachment';

import { Styled } from './attachments.styles';
import { getMimeTypes } from '../../../../../utils/general';
import { getAttachments, uploadAttachment } from '@dar/actions/task-attachments.action';
import { useAttachmentsState } from '@dar/selectors/task-attachments.selector';

const FILE_TOO_BIG_ERROR = 'The file seems to be out of size 25 Mb';
const FILE_TYPE_NOT_ALLOWED =
  'Oops, error while uploading file. Files of this type are not allowed, attach another file';

export interface AttachmentsPanelProps {
  variant?: 'big' | 'compact';
  error?: string;
  allowedFileTypes?: string[];
  allowedFileMimeTypes?: string[];
  maximumFileSize?: number;
  fileNumberLimit?: number;
  children?: React.ReactNode;
}

const AttachmentsPanel: React.FC<AttachmentsPanelProps> = ({
  variant = 'compact',
  error = '',
  allowedFileTypes = [],
  allowedFileMimeTypes = [],
  maximumFileSize = 250,
  children,
  ...otherProps
}) => {
  const [isFilesLimitReached, setFilesLimitReached] = useState(false);
  const { taskID } = useContext(TaskManagerModalsContext);
  const dispatch: Dispatch = useDispatch();
  const { attachments, loading } = useAttachmentsState();

  useEffect(() => {
    if (taskID) {
      const q = { task_id: taskID };
      dispatch(getAttachments({ q }));
    }
  }, [taskID, dispatch]);

  const dragOver = (e: any) => {
    e.preventDefault();
  };

  const dragEnter = (e: any) => {
    e.preventDefault();
  };

  const dragLeave = (e: any) => {
    e.preventDefault();
  };

  const fileDrop = (e: any) => {
    e.preventDefault();
    const files = e.dataTransfer.files;
    if (files.length) {
      handleDragAndDropFiles(files);
    }
  };

  const validateFile = (file: File) => {
    const k = 1000;

    if (maximumFileSize && file.size > maximumFileSize * k * k) {
      alert(FILE_TOO_BIG_ERROR);
      return false;
    }

    const mimeTypes =
      allowedFileTypes.length && !allowedFileMimeTypes.length
        ? getMimeTypes(allowedFileTypes) || []
        : allowedFileMimeTypes;
    if (!mimeTypes.includes(file.type)) {
      alert(FILE_TYPE_NOT_ALLOWED);
      return false;
    }

    return true;
  };

  const handleDragAndDropFiles = (files: File[]) => {
    for (let i = 0; i < files.length; i++) {
      if (validateFile(files[i])) {
        handleSelectFile(files[i]);
      }
    }
  };

  const handleSelectFile = (selectedFile: File) => {
    if (validateFile(selectedFile)) {
      console.log('selectedFile in AttachmentPanel: ', selectedFile);
      const data = new FormData();
      data.append('attachment', selectedFile);
      dispatch(uploadAttachment(taskID, data)).then((result) => {
        const q = { task_id: taskID };
        dispatch(getAttachments({ q }));
      });
    }
  };

  return (
    <Styled.Attachments onDragOver={dragOver} onDragEnter={dragEnter} onDragLeave={dragLeave} onDrop={fileDrop}>
      {loading && <Spinner absolute />}

      <div
        className={cn('AttachmentsWrapper', {
          AttachmentsWithError: error,
          AttachmentsBig: variant === 'big',
          AttachmentsCompact: variant === 'compact',
        })}
      >
        {attachments?.length > 0 ? (
          <AttachmentsList attachments={attachments} variant={variant} />
        ) : (
          <Box display="flex" flexDirection="column" height="100%" justifyContent="center" alignItems="center">
            <AttachmentIcon />
            <Typography variant="h2">No attachments yet</Typography>
            <Box mt={2} width={300} textAlign="center">
              <Typography>Start uploading relevant files here</Typography>
            </Box>
          </Box>
        )}
        {error && variant === 'compact' && <div className="AttachmentsError">{error}</div>}

        <AttachmentsUpload
          variant={variant}
          error={error}
          allowedFileTypes={allowedFileTypes}
          maximumFileSize={maximumFileSize}
          isFilesLimitReached={isFilesLimitReached}
          onFileSelect={handleSelectFile}
        />
      </div>
    </Styled.Attachments>
  );
};

export default AttachmentsPanel;
