import React, { useState, useEffect } from 'react';

import { Popover, Grid, TextField, Button, IconButton } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import DoneIcon from '@material-ui/icons/Done';
import CloseIcon from '@material-ui/icons/Close';
import AttachFileIcon from '@material-ui/icons/AttachFile';

type TAnchor = HTMLElement | null;

type TAttachmentData = {
  file?: File;
};

interface IUploadFilePopoverProps {
  anchor: TAnchor;
  onSubmit: (data: TAttachmentData, insert: boolean) => void;
}

type TUploadFilePopoverState = {
  anchor: TAnchor;
  isCancelled: boolean;
};

const cardPopverStyles = makeStyles({
  root: {
    padding: 10,
    maxWidth: 350,
  },
  textField: {
    width: '100%',
  },
  input: {
    display: 'none',
  },
});

const UploadFilePopover: React.FunctionComponent<IUploadFilePopoverProps> = (props) => {
  const classes = cardPopverStyles(props);
  const [state, setState] = useState<TUploadFilePopoverState>({
    anchor: null,
    isCancelled: false,
  });
  const [data, setData] = useState<TAttachmentData>({});

  useEffect(() => {
    setState({
      anchor: props.anchor,
      isCancelled: false,
    });
    setData({
      file: undefined,
    });
  }, [props.anchor]);

  return (
    <Popover
      anchorEl={state.anchor}
      open={state.anchor !== null}
      onExited={() => {
        props.onSubmit(data, !state.isCancelled);
      }}
      anchorOrigin={{
        vertical: 'bottom',
        horizontal: 'right',
      }}
      transformOrigin={{
        vertical: 'top',
        horizontal: 'left',
      }}
    >
      <Grid container spacing={1} className={classes.root}>
        <Grid item xs={10}>
          <TextField
            className={classes.textField}
            disabled
            value={data.file?.name || ''}
            placeholder="Click icon to attach image"
          />
        </Grid>
        <Grid item xs={2}>
          <input
            accept="application/*, text/plain, image/*, video/*"
            className={classes.input}
            id="contained-button-file"
            type="file"
            onChange={(event) => {
              setData({
                ...data,
                file: event.target.files?.[0],
              });
            }}
          />
          <label htmlFor="contained-button-file">
            <IconButton color="primary" aria-label="upload image" component="span">
              <AttachFileIcon />
            </IconButton>
          </label>
        </Grid>
        <Grid item container xs={12} justify="flex-end">
          <Button
            onClick={() => {
              setState({
                anchor: null,
                isCancelled: true,
              });
            }}
          >
            <CloseIcon />
          </Button>
          <Button
            onClick={() => {
              setState({
                anchor: null,
                isCancelled: false,
              });
            }}
          >
            <DoneIcon />
          </Button>
        </Grid>
      </Grid>
    </Popover>
  );
};

export default UploadFilePopover;
