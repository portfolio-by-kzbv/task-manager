import React, { useContext, useMemo, useEffect, useState } from 'react';
import moment from 'moment';
import parse from 'html-react-parser';
import { ButtonGroup, Button, Box, Typography } from '@material-ui/core';

import { useUserSessionState, useUserState } from '@dar/selectors/users';
import ReplyIcon from '@material-ui/icons/Reply';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AccountCircleIcon from '@material-ui/icons/AccountCircle';
import UserAvatar from '@dar/components/UserAvatar';
import { Attachment, Comment, Dispatch } from '@dar/api-interfaces';
import { useStyles } from './comments.styles';
import { useDispatch } from 'react-redux';
import { deleteComment } from '@dar/actions/task-comments.action';
import TaskManagerContext from '@dar/contexts/TaskManagerContext';
import { COMMENT_ACTION } from '@dar/contexts/TaskManagerContext/useTasks';
import { useCommentState } from '@dar/selectors/task-comments.selector';
import { getCommentsAttachments } from '@dar/actions/task-attachments.action';
import AttachmentItem from '@dar/components/Attachment/AttachmentsItem/AttachmentItem';
import api from '@dar/services/api';
import Spinner from '@dar/components/Spinner';
import AttachmentItemCompact from '@dar/components/Attachment/AttachmentsItem/AttachmentItemCompact';

interface Props {
  comment: Comment;
}

const CommentCard: React.FunctionComponent<Props> = ({ comment }) => {
  const classes = useStyles();
  const user = useUserState(comment.user_id);
  const session = useUserSessionState();
  const dispatch: Dispatch = useDispatch();
  const isCreator = useMemo(() => comment.user_id === session?.id, [comment, session]);
  const { changeCommentAction } = useContext(TaskManagerContext);
  const parentComment = useCommentState(comment.parent_id);
  const parentCommentUser = useUserState(parentComment?.user_id);
  const [attachments, setAttachments] = useState([]);
  const [loading, setLoading] = useState(false);

  const handleDelete = () => {
    dispatch(deleteComment(comment.id));
  };

  const handleAction = (action: COMMENT_ACTION) => {
    changeCommentAction(comment, action);
  };

  const handleDownload = async (attachment: Attachment) => {
    const resAttachment = await api.attachments.getAttachment(attachment.id);
    api.attachments.downloadAttachment(resAttachment.link, attachment.name);
  };

  useEffect(() => {
    setLoading(true);
    if (comment.id) {
      const q = { comment_id: comment.id };
      dispatch(getCommentsAttachments({ q })).then((result) => {
        setAttachments(result.value);
        setLoading(false);
      });
    }
  }, [comment, dispatch]);

  return (
    <Box key={comment.id} mt={2} display="flex" flexDirection="row" className={classes.comment}>
      {user ? <UserAvatar user={user} className={classes.userAvatar} /> : <AccountCircleIcon fontSize="large" />}
      <Box display="flex" flexDirection="column">
        <div className="comment-actions">
          <ButtonGroup variant="outlined" orientation="horizontal" color="secondary">
            <Button aria-label="reply-comment" onClick={() => handleAction(COMMENT_ACTION.REPLY)} color="secondary">
              <ReplyIcon fontSize="small" />
            </Button>
            {isCreator && [
              <Button aria-label="delete-comment" onClick={handleDelete} color="secondary">
                <DeleteIcon fontSize="small" />
              </Button>,
              <Button aria-label="edit-comment" onClick={() => handleAction(COMMENT_ACTION.EDIT)} color="secondary">
                <EditIcon fontSize="small" />
              </Button>,
            ]}
          </ButtonGroup>
        </div>

        <Box mb={1} ml={3} display="flex" flexDirection="row">
          <Box color="black" fontWeight={500}>
            <Typography>{user ? user.displayName : 'unknown'}</Typography>
          </Box>
          <Box ml={2} color={'#8A96A1'}>
            <Typography>{moment(comment.created_at).format(' HH:MM A')}</Typography>
          </Box>
        </Box>

        <Box ml={3}>
          {comment.parent_id && parentComment && (
            <p className="blockquote">
              <Box color="black" fontWeight={600}>
                <Typography>{parentCommentUser ? parentCommentUser.displayName : 'unknown'}</Typography>
              </Box>
              <Typography variant="body1">{parse(parentComment.text)}</Typography>
            </p>
          )}
          <Typography variant="body1">{parse(comment.text)}</Typography>
          <Box display="flex" flexDirection="row" justifyContent="flex-start">
            {loading && attachments.length > 0 && <Spinner />}
            {attachments &&
              attachments.map((attachment, key) => (
                <AttachmentItemCompact attachment={attachment} key={key} onClick={() => handleDownload(attachment)} />
              ))}
          </Box>
        </Box>
      </Box>
    </Box>
  );
};

export default CommentCard;
