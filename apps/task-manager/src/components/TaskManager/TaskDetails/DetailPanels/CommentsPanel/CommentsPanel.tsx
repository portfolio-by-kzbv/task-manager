import React, { useContext, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { Typography, Box } from '@material-ui/core';
import QuestionAnswerIcon from '@material-ui/icons/QuestionAnswer';

import { useStyles } from './comments.styles';
import CommentsInput from './CommentsInput';
import CommentsList from './CommentsList';

import TaskManagerModalsContext from '@dar/contexts/TaskManagerModalsContext';
import { getComments } from '@dar/actions/task-comments.action';
import { useCommentsState } from '@dar/selectors/task-comments.selector';
import Spinner from '@dar/components/Spinner';

interface PanelProps {
  children?: React.ReactNode;
  index: any;
  value: any;
}

const CommentsPanel = ({ children, value, index, ...other }: PanelProps) => {
  const classes = useStyles();
  const dispatch = useDispatch();
  const { taskID } = useContext(TaskManagerModalsContext);
  const { loading, comments } = useCommentsState();

  useEffect(() => {
    if (taskID) {
      const q = { task_id: taskID };
      dispatch(getComments({ q, ipp: 200 }));
    }
  }, [taskID, dispatch]);

  return (
    <div
      role="tabpanel"
      className={classes.content}
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {loading && <Spinner absolute />}
      {comments?.length > 0 ? (
        <CommentsList comments={comments} />
      ) : (
        <Box display="flex" flexDirection="column" height="100%" justifyContent="center" alignItems="center">
          <QuestionAnswerIcon fontSize="large" />
          <Typography variant="h2">No comments yet</Typography>
          <Box mt={2} width={300} textAlign="center">
            <Typography>Start conversation with your team members by typing your comments below</Typography>
          </Box>
        </Box>
      )}
      <CommentsInput />
    </div>
  );
};

export default CommentsPanel;
