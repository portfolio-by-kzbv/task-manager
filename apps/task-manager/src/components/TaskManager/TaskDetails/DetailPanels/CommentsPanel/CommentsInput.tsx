import React, { useState, useMemo, useContext, useEffect, useRef } from 'react';
import MUIRichTextEditor, { TAutocompleteItem, TMUIRichTextEditorRef } from 'mui-rte';
import { EditorState, ContentState, convertToRaw, convertFromHTML } from 'draft-js';
import { convertFromHTML as draftConvertFromHTML } from 'draft-convert';
import { stateToHTML } from 'draft-js-export-html';
import { useDispatch } from 'react-redux';

import { createTheme, Theme, MuiThemeProvider } from '@material-ui/core/styles';
import SendIcon from '@material-ui/icons/Send';
import AttachFileIcon from '@material-ui/icons/AttachFile';

import UserInfo from '@dar/components/Users/UserInfo';
import { Comment, Dispatch, User } from '@dar/api-interfaces';
import { useUserSessionState, useUsersState } from '@dar/selectors/users';
import TaskManagerModalsContext from '@dar/contexts/TaskManagerModalsContext';
import TaskManagerContext from '@dar/contexts/TaskManagerContext';
import { appendComment, createComment, editComment, updateComment } from '@dar/actions/task-comments.action';
import { uploadCommentAttachment } from '@dar/actions/task-attachments.action';
import UploadFilePopover from '../AttachmentsPanel/UploadFilePopover';
import { COMMENT_ACTION } from '@dar/contexts/TaskManagerContext/useTasks';
import AttachmentItem from '@dar/components/Attachment/AttachmentsItem/AttachmentItem';
import AttachmentItemCompact from '@dar/components/Attachment/AttachmentsItem/AttachmentItemCompact';

export const defaultTheme: Theme = createTheme();

Object.assign(defaultTheme, {
  overrides: {
    MUIRichTextEditor: {
      root: {
        padding: '0 24px 10px',
      },
      container: {
        display: 'flex',
        flexDirection: 'column-reverse',
        border: '1px solid #CED7DF',
        borderRadius: 6,
        '&:focus': {
          borderColor: 'red',
        },
      },
      editor: {
        backgroundColor: '#fff',
        padding: '10px 16px',
        minHeight: '58px',
        maxHeight: '234px',
        overflow: 'auto',
        borderRadius: '6px 6px 0 0',
      },
      hidePlaceholder: {
        display: 'block',
      },
      toolbar: {
        border: 'none',
        backgroundColor: '#FFF',
        borderRadius: '0 0 6px 6px',
      },
      placeHolder: {
        backgroundColor: '#FFF',
        paddingLeft: 20,
        width: 'inherit',
        position: 'absolute',
        top: '16px',
        margin: 0,
      },
      anchorLink: {
        color: '#333333',
        textDecoration: 'underline',
      },
    },
  },
});

const CommentsInput = () => {
  const dispatch: Dispatch = useDispatch();
  const defaultContent = convertToRaw(EditorState.createEmpty().getCurrentContent());
  const allUsers = useUsersState();
  const session = useUserSessionState();
  const { taskID } = useContext(TaskManagerModalsContext);
  const { commentAction } = useContext(TaskManagerContext);
  const [content, setContent] = useState(JSON.stringify(defaultContent));
  const [isEdit, setIsEdit] = useState<boolean>(false);
  const [isReply, setIsReply] = useState<boolean>(false);
  const [files, setFiles] = useState<File[]>([]);
  const ref = useRef<TMUIRichTextEditorRef>(null);
  const [anchor, setAnchor] = useState<HTMLElement | null>(null);

  useEffect(() => {
    const comment = commentAction.comment;
    let contentState = convertFromHTML(comment ? comment.text : '');
    switch (commentAction.action) {
      case COMMENT_ACTION.REPLY: {
        setIsReply(true);
        setIsEdit(false);
        const state = draftConvertFromHTML(`<blockquote>${comment.text}</blockquote><br/>`);
        setContent(JSON.stringify(convertToRaw(state)));
        return;
      }
      case COMMENT_ACTION.EDIT:
        setIsEdit(true);
        setIsReply(false);
        contentState = convertFromHTML(comment.text);
        break;
      default:
        break;
    }
    const state = contentState && ContentState.createFromBlockArray(contentState.contentBlocks, contentState.entityMap);
    setContent(JSON.stringify(convertToRaw(state)));
  }, [commentAction.action, commentAction.comment]);

  const reset = () => {
    setFiles([]);
    setContent(JSON.stringify(defaultContent));
    setIsReply(false);
    setIsEdit(false);
  };
  const onSubmit = (state: EditorState) => {
    const content = stateToHTML(state.getCurrentContent());
    if (content) {
      const data: Comment = isEdit
        ? {
            ...commentAction.comment,
            text: content,
          }
        : isReply
        ? {
            task_id: taskID,
            parent_id: commentAction.comment.id,
            text: content.split('</blockquote>').pop(),
            user_id: session?.id,
            user_name: session?.displayName,
            created_at: new Date(),
          }
        : {
            task_id: taskID,
            text: content,
            user_id: session?.id,
            user_name: session?.displayName,
            created_at: new Date(),
          };

      if ((!data.text || !state.getCurrentContent().hasText()) && files.length === 0) {
        return;
      }
      dispatch(isEdit ? editComment(commentAction.comment.id, data) : createComment(data)).then((commentResult) => {
        if (files.length) {
          const promises = files.map((file) => {
            const data = new FormData();
            data.append('attachment', file);
            return dispatch(uploadCommentAttachment(taskID, commentResult.value.id, data));
          });
          return Promise.all(promises)
            .then((result) => {
              reset();
              dispatch(isEdit ? updateComment(data) : appendComment(commentResult.value));
            })
            .catch((err) => {
              console.log(err);
            });
        }
        reset();
        dispatch(isEdit ? updateComment(data) : appendComment(commentResult.value));
      });
    }
  };

  const onChange = (state: EditorState) => {
    if (ref && ref.current && isReply) {
      ref.current.focus();
    }
    if (!state.getCurrentContent().hasText()) {
      // console.log('empty');
    }
  };

  const onFocus = () => {
    // console.log('Focus on MUIRichTextEditor');
  };

  const onBlur = () => {
    // console.log('Blur, focus lost on MUIRichTextEditor');
  };

  const staff: TAutocompleteItem[] = useMemo(() => {
    return allUsers.map((user: User) => ({
      keys: [`${user.displayName}`, `${user.workEmail}`],
      value: '@' + user.displayName,
      content: <UserInfo user={user} />,
    }));
  }, [allUsers]);

  const onAttachmentDelete = (index: number) => {
    setFiles((s) => s.filter((f, i) => i !== index));
  };

  return (
    <MuiThemeProvider theme={defaultTheme}>
      <UploadFilePopover
        anchor={anchor}
        onSubmit={(data, insert) => {
          if (data.file.size > 25000000) {
            // FIXME: add toaster
            alert('File size may not exceed 25MB');
            return;
          }
          if (insert && data.file) {
            setFiles((s) => [...s, data.file]);
          }
          setAnchor(null);
        }}
      />
      <MUIRichTextEditor
        defaultValue={content}
        label="Type your message..."
        id="task-comment-input"
        controls={[
          'attachment',
          'bold',
          'italic',
          'strikethrough',
          'link',
          'numberList',
          'bulletList',
          'add-attachment',
          'send',
        ]}
        onChange={onChange}
        onFocus={onFocus}
        onBlur={onBlur}
        ref={ref}
        customControls={[
          {
            name: 'send',
            icon: <SendIcon />,
            type: 'callback',
            onClick: (_editorState) => {
              onSubmit(_editorState);
            },
            inlineStyle: {
              backgroundColor: 'black',
              color: 'white',
            },
          },
          {
            name: 'add-attachment',
            icon: <AttachFileIcon />,
            type: 'callback',
            onClick: (_editorState, _name, anchor) => {
              setAnchor(anchor);
            },
          },
          {
            name: 'attachment',
            type: 'block',
            component: (blockProps) => {
              return files.length ? (
                <div style={{ display: 'flex', margin: '1rem', flexDirection: 'column' }}>
                  {files.map((f, i) => (
                    <AttachmentItemCompact file={f} onDelete={() => onAttachmentDelete(i)} />
                  ))}
                </div>
              ) : null;
            },
          },
        ]}
        autocomplete={{
          strategies: [
            {
              items: staff,
              triggerChar: '@',
              insertSpaceAfter: true,
            },
          ],
        }}
        draftEditorProps={{
          handleDroppedFiles: (_selectionState, files) => {
            if (files.length && (files[0] as File).name !== undefined) {
              setFiles((s) => [...s, files[0] as File]);
              return 'handled';
            }
            return 'not-handled';
          },
        }}
      />
    </MuiThemeProvider>
  );
};

export default CommentsInput;
