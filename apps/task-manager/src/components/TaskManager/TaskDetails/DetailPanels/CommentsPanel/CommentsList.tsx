import React, { useMemo } from 'react';
import { List, Typography, Box } from '@material-ui/core';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';

import Spinner from '@dar/components/Spinner/Spinner';
import CommentCard from './CommentCard';
import { Comment } from '@dar/api-interfaces';
import { useStyles } from './comments.styles';
import { nameOfDates } from '../../../../../utils/date';

interface Props {
  comments: Comment[];
  loading?: boolean;
}

const CommentsList: React.FunctionComponent<Props> = ({ comments, loading }) => {
  const classes = useStyles();

  const groups = useMemo(() => {
    const commentsGroups = comments?.reduce((groups, comment) => {
      const created_at = comment.created_at.toString();
      const date = created_at.split('T')[0];
      if (!groups[date]) {
        groups[date] = [];
      }
      groups[date].push(comment);
      return groups;
    }, {});

    return commentsGroups;
  }, [comments]);

  const commentsGroupByDay = useMemo(() => {
    const commentsGroups = Object.keys(groups).map((date) => {
      return {
        date,
        comments: groups[date],
      };
    });
    const sortedCommentsByDate = commentsGroups.sort((a, b) => +new Date(a.date) - +new Date(b.date));
    return sortedCommentsByDate;
  }, [groups]);

  const commentDate = (date) => {
    return nameOfDates(date);
  };

  return (
    <List className={classes.list}>
      {commentsGroupByDay.map((group, key) => (
        <Box key={key}>
          <Typography className={classes.date}>
            {commentDate(group.date)}
            <ExpandMoreIcon fontSize="small" />
          </Typography>
          {group.comments.map((comment: Comment) => (
            <CommentCard comment={comment} />
          ))}
        </Box>
      ))}
      {loading && <Spinner />}
    </List>
  );
};

export default CommentsList;
