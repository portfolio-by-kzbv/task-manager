import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  list: {
    overflowX: 'hidden',
    overflowY: 'auto',
    marginTop: 16,
    flex: 1,
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'flex-end',
  },
  date: {
    color: '#8A96A1',
    fontSize: '12px',
    lineHeight: '16px',
    alignItems: 'center',
    display: 'flex',
    justifyContent: 'center',
  },
  content: {
    height: '100%',
    minHeight: 'fit-content',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',
  },
  comment: {
    padding: '12px 32px',
    position: 'relative',
    transition: '.2s ease-in-out',
    '& .comment-actions': {
      height: '40px',
      display: 'none',
      position: 'absolute',
      top: '-20px',
      right: '40px',
      transition: '.2s ease-in-out',
      background: '#FFFFFF',
      boxShadow: '0px 4px 10px rgba(0, 0, 0, 0.15)',
      borderRadius: 4,
      '& div, button': { height: 'inherit' },
    },
    '&:hover': {
      background: '#E6ECF1',
      '& .comment-actions': {
        display: 'block',
      },
    },
  },
  userAvatar: {
    margin: 0,
  },
});
