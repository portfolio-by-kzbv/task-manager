import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  header: {
    boxShadow: '0px 4px 6px rgba(0, 0, 0, 0.12)',
    display: 'flex',
    justifyContent: 'space-between',
    padding: '24px',
    width: '100%',
  },
  buttons: {
    alignItems: 'center',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  details: {
    display: 'flex',
    padding: '20px 32px',
    border: '1px solid #E0E3EA',
    '& > div': {
      display: 'flex',
      alignItems: 'center',
    },
  },
  info: {
    height: 'calc(100vh - 170px)',
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
  },
  description: {
    height: 'inherit',
    width: 'inherit',
    padding: '24px 32px',
    borderRight: '1px solid #E0E3EA',
    display: 'flex',
    flexDirection: 'column',
  },
  block: {
    display: 'flex',
    flexDirection: 'column',
    marginTop: '22px',
    '& > div': { height: 'auto' },
  },
  checklist: {
    marginTop: '30px',
  },
  container: {
    background: '#F7F8FA',
    height: 'inherit',
    width: 'inherit',
    display: 'flex',
    flexDirection: 'column',
  },
  swipeable: {
    height: 'calc(100% - 20px)',
    '& > div': { height: '100%' },
  },
  badge: {
    border: `2px solid ${theme.palette.background.paper}`,
  },
  titleEdit: {
    border: 'none',
    padding: '0',
    borderBottom: `1px solid ${theme.palette.primary[200]}`,
    fontSize: 24,
    lineHeight: 30,
    borderRadius: '0',
    '&:hover, &:focus': {
      border: 'none',
      borderBottom: `1px solid ${theme.palette.primary[200]}`,
      outline: 'none',
    },
  },
  formControl: {
    '&:hover, &:focus': {
      border: 'none',
      borderBottom: `1px solid ${theme.palette.primary[200]}`,
      outline: 'none',
    },
  },
}));
