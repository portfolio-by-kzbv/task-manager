import { createLink, deleteLink, getLinks } from '@dar/actions/links.action';
import { Cluster, LinksRequest, Project, Task } from '@dar/api-interfaces';
import TaskManagerModalsContext from '@dar/contexts/TaskManagerModalsContext';

import { useState, useCallback, useContext, useEffect } from 'react';
import { useDispatch } from 'react-redux';

import { Dispatch as CustomDispatch } from '@dar/api-interfaces';
import { LinkErrors } from '@dar/api-interfaces';
import { LinkActionTypes, LinkTypes, UseTasksContext } from '@dar/contexts/TaskManagerModalsContext/useTasks';
import { useProjects } from '@dar/selectors/projects.selector';
import { useClusters } from '@dar/selectors/cluster.selector';
import { useLinks } from '@dar/selectors/links.selector';

export interface LinkForm {
  task_id?: number;
  dependent_task_id?: number;
  cluster?: Cluster;
  project?: Project;
  task?: Task;
}

const initialValues: LinkForm = {
  task_id: null,
};

const useLinkForm = (vals: LinkForm, callback, onClose) => {
  const dispatch: CustomDispatch = useDispatch();
  const { linkType, linkActionType, linkForm, taskID } = useContext<UseTasksContext>(TaskManagerModalsContext);
  const { loading: clusterLoading } = useClusters();
  const { loading: projectLoading } = useProjects();
  const { loading: tasksLoading } = useLinks();

  const [values, setValues] = useState<LinkForm>({
    ...initialValues,
    ...vals,
    ...linkForm,
  });
  const [errors, setErrors] = useState<LinkErrors>({
    cluster: '',
    project: '',
    task: '',
  });
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    setLoading(clusterLoading || projectLoading || tasksLoading);
  }, [clusterLoading, projectLoading, tasksLoading]);

  const clear = () =>
    setValues({
      ...initialValues,
    });

  const handleChange = (name, value) => {
    setValues({ ...values, [name]: value });
  };

  useEffect(() => {
    setValues((prev) => ({ ...prev, ...linkForm }));
  }, [linkForm]);

  const preSubmitValidation = () => {
    const { task } = values;
    const err: LinkErrors = {};

    // if (!cluster) {
    //   err.cluster = 'Please, fill the cluster field';
    // }

    // if (!project) {
    //   err.project = 'Please, fill the project field';
    // }

    if (!task) {
      err.task = 'Please, fill the task field';
    }

    return err;
  };

  const getPayload = (): LinksRequest => {
    const { task } = values;
    switch (linkType) {
      case LinkTypes.AFFECTS:
        return {
          task_id: taskID,
          dependent_task_id: task?.id,
        };
      case LinkTypes.DEPENDS:
        return {
          task_id: task?.id,
          dependent_task_id: taskID,
        };
    }
  };

  const handleCreate = useCallback(
    (payload: LinksRequest) => {
      setLoading(true);
      dispatch(createLink(payload))
        .then(() => {
          onClose();
          dispatch(getLinks(taskID));
          setLoading(false);
          if (callback) {
            callback();
          }
        })
        .catch(() => {
          setLoading(false);
        });
    },
    [dispatch, onClose, taskID, callback]
  );

  const isModified = (payload: LinksRequest): boolean => {
    if (linkType === LinkTypes.AFFECTS) {
      return payload.dependent_task_id !== linkForm.task.id;
    } else {
      return payload.task_id !== linkForm.task.id;
    }
  };

  const handleEdit = useCallback(
    (payload: LinksRequest) => {
      if (!isModified(payload)) {
        if (callback) {
          callback();
        }
        return;
      }
      setLoading(true);
      dispatch(
        linkType === LinkTypes.AFFECTS ? deleteLink(taskID, linkForm.task.id) : deleteLink(linkForm.task.id, taskID)
      )
        .then(() => dispatch(createLink(payload)))
        .then(() => {
          onClose();
          dispatch(getLinks(taskID));
          setLoading(false);
          if (callback) {
            callback();
          }
        })
        .catch(() => {
          setLoading(false);
        });
    },
    [dispatch, onClose, taskID, callback, linkType, linkForm]
  );

  const handleSubmit = (event) => {
    event.preventDefault();
    const errors = preSubmitValidation();
    if (!Object.keys(errors).length) {
      const link = getPayload();
      if (linkActionType === LinkActionTypes.CREATE) {
        handleCreate(link);
        clear();
      } else {
        handleEdit(link);
      }
    } else {
      setErrors(errors);
      // clear();
    }
  };

  return {
    values,
    errors,
    loading,
    clear,
    handleChange,
    handleSubmit,
  };
};

export default useLinkForm;
