import React, { useContext, useEffect, useMemo } from 'react';
import { Box } from '@material-ui/core';

import { useStyles } from './createLink.styles';
import useLinkForm, { LinkForm } from './useLinkForm';
import { useCreateModalStyles } from '@dar/components/CreateModal/createModal.styles';
import TaskManagerModalsContext from '@dar/contexts/TaskManagerModalsContext';
import CreateModal from '@dar/components/CreateModal/CreateModal';
import AutoCompleteSelect from '@dar/components/Selects/AutoCompleteSelect';
import { UseTasksContext } from '@dar/contexts/TaskManagerModalsContext/useTasks';
import { useClusters } from '@dar/selectors/cluster.selector';
import { useProjects } from '@dar/selectors/projects.selector';
import { Cluster, Project, Task, TasksQueryParams } from '@dar/api-interfaces';
import { MAX_TASKS_PER_REQUEST } from '@dar/contexts/TaskManagerFiltersContext/useTasks';
import { useDispatch } from 'react-redux';
import { useLinks } from '@dar/selectors/links.selector';
import { getLinksTasks } from '@dar/actions/links.action';

type Props = {
  openDrawer: boolean;
  onClose: () => void;
  formValues?: LinkForm;
  callback?: any;
};

const CreateLink: React.FunctionComponent<Props> = ({ openDrawer, onClose, formValues, callback }) => {
  const classes = useStyles();
  const createModalClasses = useCreateModalStyles();

  const { values, errors, loading, clear, handleChange, handleSubmit } = useLinkForm(formValues, callback, onClose);

  const { linkType, linkActionType, taskID } = useContext<UseTasksContext>(TaskManagerModalsContext);

  const { clusters } = useClusters();
  const { projects } = useProjects();
  const { linksTasks: tasks } = useLinks();

  const dispatch = useDispatch();

  const q = useMemo(() => {
    const params: TasksQueryParams = {};

    if (values.cluster) {
      params.group_id = values.cluster.id;
    }

    if (values.project) {
      params.project_id = values.project.id;
    }

    return params;
  }, [values]);

  useEffect(() => {
    if (!openDrawer) {
      return;
    }
    dispatch(getLinksTasks({ q, ipp: MAX_TASKS_PER_REQUEST }));
  }, [q, dispatch, openDrawer]);

  const onModalCancel = () => {
    onClose();
    clear();
  };

  return (
    <CreateModal
      openDrawer={openDrawer}
      onClose={onModalCancel}
      onSubmit={handleSubmit}
      loading={loading}
      title={`${linkActionType === 'Create' ? `Create New` : linkActionType} ${linkType}`}
      buttonName={`${linkActionType} ${linkType}`}
      zIndex={1300}
    >
      <Box p={8} style={{ height: '100%' }}>
        <div className={createModalClasses.block}>
          <AutoCompleteSelect
            variant="outlined"
            value={values.cluster}
            error={errors.cluster}
            label="Select cluster"
            prompt="Select"
            valueId="id"
            valueName="name"
            options={clusters}
            onSelect={(value: Cluster) => handleChange('cluster', value)}
          />
        </div>

        <div className={createModalClasses.block}>
          <AutoCompleteSelect
            variant="outlined"
            value={values.project}
            error={errors.project}
            label="Select project"
            prompt="Select"
            valueId="id"
            valueName="name"
            options={projects}
            onSelect={(value: Project) => handleChange('project', value)}
          />
        </div>

        <div className={createModalClasses.block}>
          <AutoCompleteSelect
            variant="outlined"
            value={values.task}
            valueId="id"
            valueName="title"
            error={errors.task}
            label="Select task"
            prompt="Select"
            options={tasks?.filter((task) => task.id !== taskID)}
            onSelect={(value: Task) => handleChange('task', value)}
          />
        </div>
      </Box>
    </CreateModal>
  );
};

export default CreateLink;
