import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  option: {
    fontSize: 15,
    '& > span': {
      marginRight: 10,
      fontSize: 18,
    },
  },
  advancedOptions: {
    display: 'none',
    transition: 'display, 0.5s linear',
    marginBottom: '20px',
  },
  advancedOptionsOpen: {
    display: 'block',
    transition: 'display, 0.5s linear',
  },
}));
