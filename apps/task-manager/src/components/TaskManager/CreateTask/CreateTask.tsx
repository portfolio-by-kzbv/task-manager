import React, { useEffect, useState } from 'react';
import { TextField, Button, Box } from '@material-ui/core';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ArrowDropUpIcon from '@material-ui/icons/ArrowDropUp';

import DatePicker from '../../DatePicker/DatePicker';
import AssigneeSelect from '../../Selects/AssigneeSelect';
import StatusSelect from '../../Selects/StatusSelect';
import PrioritySelect from '../../Selects/PrioritySelect';

import { useStyles } from './createTask.styles';
import useTaskForm from './useTaskForm';
import CreateModal from '../../CreateModal/CreateModal';
import { useCreateModalStyles } from '../../CreateModal/createModal.styles';
import AutoCompleteSelect from '../../Selects/AutoCompleteSelect';
import { getProjects } from '../../../store/actions/projects.actions';
import { useClusters } from '../../../selectors/cluster.selector';
import { getClusters } from '../../../store/actions/clusters.action';
import { useProjects } from '../../../selectors/projects.selector';
import { useDispatch } from 'react-redux';
import { toast } from '@dartech/dms-ui';
import { Cluster, Project, User } from '@dar/api-interfaces';

type Props = {
  openDrawer: boolean;
  onClose: () => void;
  formValues?: any;
  callback?: any;
};

const CreateTask: React.FunctionComponent<Props> = ({ openDrawer, onClose, formValues, callback }) => {
  const classes = useStyles();
  const createModalClasses = useCreateModalStyles();
  const [advancedOptionsOpen, setAdvancedOptionsOpen] = useState(false);
  const { clusters } = useClusters();
  const { projects } = useProjects();
  const dispatch = useDispatch();

  const { values, errors, loading, clear, handleChange, handleSubmit } = useTaskForm(formValues, callback, onClose);

  const onModalCancel = () => {
    onClose();
    clear();
  };

  const onFormSubmit = (event) => {
    handleSubmit(event);
  };

  useEffect(() => {
    dispatch(getClusters());
    dispatch(getProjects());
  }, [dispatch]);

  return (
    <CreateModal
      openDrawer={openDrawer}
      onClose={onModalCancel}
      onSubmit={onFormSubmit}
      loading={loading}
      title="Create New Task"
      buttonName="Create Task"
    >
      <Box p={8} className={createModalClasses.box}>
        <div className={createModalClasses.block}>
          <TextField
            value={values.title}
            label="Task"
            size="medium"
            placeholder="Task"
            required
            fullWidth
            onChange={(event) => handleChange('title', event.target.value)}
            disabled={loading}
            error={Boolean(errors.title)}
            helperText={errors.title ? errors.title : ''}
          />
        </div>

        <div className={createModalClasses.block}>
          <AssigneeSelect
            variant="outlined"
            value={values.assignee}
            label={'Assignee'}
            isRequired
            error={errors.assignee}
            onSelect={(value: User) => handleChange('assignee', value)}
          />
        </div>

        <div className={`${classes.advancedOptions} ${advancedOptionsOpen ? classes.advancedOptionsOpen : ''}`}>
          <div className={createModalClasses.block}>
            <Box width="210px">
              <DatePicker
                value={values.start_time}
                onChange={(value) => {
                  if (value > values.end_time) {
                    toast.warning('Task end date is changed.', {
                      duration: 3000,
                    });
                  }
                  handleChange('start_time', value);
                }}
                label="Start Date"
                fullWidth={true}
                disablePast={true}
              />
            </Box>
            <Box width="210px">
              <DatePicker
                value={values.end_time}
                onChange={(value) => handleChange('end_time', value)}
                label="End Date"
                fullWidth={true}
                disablePast={true}
                minDate={values.start_time}
              />
            </Box>
          </div>

          <div className={createModalClasses.block}>
            <StatusSelect
              variant="standard"
              label="Status"
              value={values.status}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => handleChange('status', event.target.value)}
              currentStatus={'Initial'}
              minWidth={210}
            />
            <PrioritySelect
              variant="standard"
              label="Priority"
              value={values.priority}
              defaultValue="Medium"
              onChange={(event: React.ChangeEvent<HTMLInputElement>) => handleChange('priority', event.target.value)}
              minWidth={210}
            />
          </div>

          <div className={createModalClasses.block}>
            <TextField
              id="new-task-description"
              label="Description"
              onChange={(event) => handleChange('description', event.target.value)}
              multiline
              rows={4}
              fullWidth
              value={values.description}
              InputProps={{
                style: { height: 'auto' },
              }}
            />
          </div>
          <div className={createModalClasses.block}>
            <Box width={1} marginRight="7px">
              <AutoCompleteSelect
                variant="outlined"
                value={values.cluster}
                error={errors.cluster}
                label="Select cluster"
                prompt="Select"
                valueId="id"
                valueName="name"
                options={clusters}
                onSelect={(value: Cluster) => handleChange('cluster', value)}
              />
            </Box>
            <Box width={1} marginLeft="7px">
              <AutoCompleteSelect
                variant="outlined"
                value={values.project}
                error={errors.project}
                label="Select project"
                prompt="Select"
                valueId="id"
                valueName="name"
                options={projects}
                onSelect={(value: Project) => handleChange('project', value)}
              />
            </Box>
          </div>
        </div>
        <Button
          variant="text"
          color="secondary"
          size="small"
          endIcon={advancedOptionsOpen ? <ArrowDropUpIcon /> : <ArrowDropDownIcon />}
          onClick={() => setAdvancedOptionsOpen(!advancedOptionsOpen)}
        >
          {advancedOptionsOpen ? 'Show less' : 'Advanced option'}
        </Button>
      </Box>
    </CreateModal>
  );
};

export default CreateTask;
