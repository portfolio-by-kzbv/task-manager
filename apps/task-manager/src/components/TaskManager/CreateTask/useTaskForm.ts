import { useState, useEffect, useCallback, useContext } from 'react';
import { useDispatch } from 'react-redux';
import { createTask } from '@dar/actions/tasks.action';
import { useUserSessionState } from '@dar/selectors/users';
import { Dispatch } from '@dar/api-interfaces';
import { TaskErrors } from '@dar/api-interfaces';
import TaskManagerFiltersContext from '@dar/contexts/TaskManagerFiltersContext';

const initialValues = {
  assignee_id: '',
  title: '',
  description: '',
  status: 1,
  priority: 'Medium',
  start_time: new Date(new Date().setHours(0, 0, 0)),
  end_time: new Date(new Date().setHours(23, 59, 59)),
  project: '',
  cluster: '',
};

const useTaskForm = (vals, callback, onClose) => {
  const creator = useUserSessionState();
  const dispatch: Dispatch = useDispatch();
  const { updateTasks } = useContext(TaskManagerFiltersContext);
  const [values, setValues] = useState({
    ...initialValues,
    ...vals,
  });
  const [errors, setErrors] = useState<TaskErrors>({
    title: '',
    assignee: '',
  });
  const [loading, setLoading] = useState(false);

  useEffect(() => {
    if (vals && vals.start_time) {
      setValues((v) => ({ ...v, end_time: new Date(vals.start_time) }));
    }
  }, [vals]);

  const clear = () => {
    setValues({
      ...initialValues,
    });
    setErrors({
      title: '',
      assignee: '',
    });
  };

  const handleChange = (name: string, value) => {
    if (name === 'start_time' && value > values.end_time) {
      setValues({ ...values, start_time: value, end_time: value });
    } else if (name === 'end_time' && value < values.start_time) {
      setValues({ ...values, end_time: values.start_time });
    } else {
      setValues({ ...values, [name]: value });
    }
  };

  const preSubmitValidation = () => {
    const { title, assignee } = values;
    const err: TaskErrors = {};

    if (!title.length) {
      err.title = 'Please, fill the title';
    }

    if (!assignee) {
      err.assignee = 'Please, fill the assignee field';
    }

    return err;
  };

  const getPayload = () => {
    const { title, description, start_time, end_time, assignee, priority, cluster, project } = values;

    const payload = {
      creator_id: creator?.id,
      assignee_id: assignee?.id,
      title: title,
      description: description,
      priority: priority,
      start_time: start_time,
      end_time: end_time,
      group_id: cluster.id,
      project_id: project.id,
    };
    return payload;
  };

  const handleCreate = useCallback(
    (payload) => {
      setLoading(true);
      dispatch(createTask(payload))
        .then(() => {
          onClose();
          dispatch(updateTasks());
          setLoading(false);
          if (callback) {
            callback();
          }
        })
        .catch(() => {
          setLoading(false);
        });
    },
    [dispatch, updateTasks, onClose, callback]
  );

  const handleSubmit = (event) => {
    event.preventDefault();
    const errors = preSubmitValidation();
    if (!Object.keys(errors).length) {
      const task = getPayload();
      handleCreate(task);
      clear();
    } else {
      setErrors(errors);
    }
  };

  return {
    values,
    errors,
    loading,
    clear,
    handleChange,
    handleSubmit,
  };
};

export default useTaskForm;
