import { useMemo } from 'react';

import { Task } from '@dar/api-interfaces';

import { useTasksState } from '../../../selectors/tasks.selector';

const useTaskList = (status: string) => {
  const { tasks, filteredTasks, isFiltered } = useTasksState();

  const allTasks = useMemo(() => {
    return isFiltered ? filteredTasks : tasks;
  }, [tasks, isFiltered, filteredTasks]);

  const updTasks = useMemo(() => {
    const filterByStatus = (items: Task[]) => {
      return items && items.filter((item) => item.status === status);
    };
    return filterByStatus(allTasks);
  }, [allTasks, status]);

  return {
    updTasks,
    allTasks,
  };
};

export default useTaskList;
