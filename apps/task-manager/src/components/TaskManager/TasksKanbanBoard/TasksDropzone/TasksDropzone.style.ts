import { makeStyles } from '@material-ui/core';

type StyleProps = {
  isOver: boolean;
  item: {
    id: number,
    status: string
  }
  canDrop: boolean;
  borderColor: string;
};

export const dropzoneStyles = makeStyles({
  dropzone: {
    width: 350,
    height: '100%',
    marginRight: 7,
    padding: '12px 12px 0 12px',
    backgroundColor: '#F5F8FA',
    borderTop: (props: StyleProps) => {
      return `4px solid ${props.borderColor}`;
    },
    overflowY: 'scroll',
    boxSizing: 'border-box',
    flexShrink: 0,
    borderRadius: '2px 2px 0px 0px',
    opacity: ({ canDrop, item }: StyleProps) => item ? (canDrop ? 1 : 0.5) : 1,
    '&::-webkit-scrollbar-track': {
      backgroundColor: '#F5F5F5',
      borderRadius: '4px'
    },
    '&::-webkit-scrollbar': {
      width: '12px',
      backgroundColor: '#F5F5F5'
    },
    '&::-webkit-scrollbar-thumb': {
      borderRadius: '10px',
      backgroundColor: '#8A96A1'
    }
  },
  dropLocation: {
    width: '100%',
    height: ({ isOver, canDrop }: StyleProps) => isOver && canDrop ? 106 : 0,
    transition: '.3s',
    boxSizing: 'border-box',
    border: ({ isOver, canDrop }: StyleProps) => isOver && canDrop ?'1px dashed #007F9B' : 'none',
    background: '#FFFFFF',
    opacity: 0.7,
    borderRadius: 4,
    boxShadow: '0px 2px 8px rgba(0, 0, 0, 0.12)',
    marginBottom: 4
  },
  statusCode: {
    fontSize: 12,
    marginBottom: 12
  }
});