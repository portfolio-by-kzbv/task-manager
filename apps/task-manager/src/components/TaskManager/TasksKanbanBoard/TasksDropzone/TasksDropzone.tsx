import React, { useContext, useMemo } from 'react';
import { useDrop } from 'react-dnd';
import { Typography } from '@material-ui/core';

import { dropzoneStyles } from './TasksDropzone.style';
import { TaskCard } from '../TaskCard/TaskCard';
import { Task } from '@dar/api-interfaces';
import TaskManagerFiltersContext from '../../../../contexts/TaskManagerFiltersContext';
import { updateTaskStatus } from '../../../../store/actions/tasks.action';
import { useDispatch } from 'react-redux';
import { Dispatch } from '@dar/api-interfaces';
import { TaskStatus } from '@dar/api-interfaces';
import useTaskList from '../useTaskList';
import { getStatusTransitions } from '../../../../utils/status';
import { useStatusesState } from '../../../../selectors/task-status.selector';

interface TasksDropzoneProps {
  index: number;
  currentStatus: TaskStatus;
  selectTask: (task: Task) => void;
}

const statusColors: string[] = ['#3781CB', '#976AD1', '#FFA530', '#2DB77B', '#ED4C36', '#99A5B1'];

export const TasksDropzone: React.FC<TasksDropzoneProps> = ({ index, currentStatus, selectTask }) => {
  const { updateTasks } = useContext(TaskManagerFiltersContext);
  const { updTasks: tasks } = useTaskList(currentStatus.name);
  const borderColor = statusColors[index];
  const dispatch: Dispatch = useDispatch();
  const { statuses: statusList } = useStatusesState();

  const [{ isOver, canDrop, item }, dropRef] = useDrop(() => ({
    accept: 'task',
    drop: ({ id }) => dispatch(updateTaskStatus(id, { status_id: currentStatus.id })).then(() => updateTasks()),
    canDrop: ({ status }) => {
      const filteredStatuses = getStatusTransitions(status, statusList);
      return status !== currentStatus.name && filteredStatuses.includes(currentStatus);
    },
    collect: (monitor) => ({
      isOver: monitor.isOver(),
      canDrop: monitor.canDrop(),
      item: monitor.getItem(),
    }),
  }));
  const styles = dropzoneStyles({ isOver, item, canDrop, borderColor });

  return (
    <div ref={dropRef} className={styles.dropzone}>
      <Typography color="secondary" className={styles.statusCode}>
        {currentStatus.name.toUpperCase()} ({tasks.length})
      </Typography>
      <div className={styles.dropLocation} />
      {tasks.map((task) => (
        <TaskCard key={task.id} task={task} />
      ))}
    </div>
  );
};
