import React, { useState, useRef } from 'react';
import { Typography } from '@material-ui/core';
import { DndProvider } from 'react-dnd';
import { HTML5Backend } from 'react-dnd-html5-backend';
import { KeyboardArrowRight, KeyboardArrowLeft } from '@material-ui/icons';
import cn from 'classnames';

import { Task } from '@dar/api-interfaces';
import { tasksKanbanStyles } from './TasksKanbanBoard.style';
import { TasksDropzone } from './TasksDropzone/TasksDropzone';
import { useStatusesState } from '../../../selectors/task-status.selector';

const TasksKanbanBoard: React.FC = () => {
  const styles = tasksKanbanStyles();
  const kanbanRef = useRef<HTMLDivElement>(null);
  const { statuses: statusList } = useStatusesState();
  const [selectedTask, setSelectedTask] = useState<Task | null>();
  let scrollToken = null;

  const handleClose = () => setSelectedTask(null);

  const mouseEnter = (direction: string) => {
    scrollToken = setInterval(function () {
      if (!kanbanRef.current) return;
      direction === 'right' ? kanbanRef.current.scrollBy(5, 0) : kanbanRef.current.scrollBy(-5, 0);
    }, 5);
  };

  const mouseLeave = () => clearInterval(scrollToken);

  const onScroll = () => {
    const leftArrow = document.getElementById('left-arrow');
    kanbanRef.current.scrollLeft === 0 ? (leftArrow.style.display = 'none') : (leftArrow.style.display = 'flex');
  };

  // if (error) return <Typography color="error">{JSON.stringify(error)}</Typography>;

  return (
    <DndProvider backend={HTML5Backend}>
      <div className={styles.kanban}>
        {/* {(leadsLoading || statusLoading)  && <Spinner absolute/>} */}
        <div className={styles.list} ref={kanbanRef} onScroll={onScroll}>
          {statusList &&
            statusList.map((status, i) => (
              <TasksDropzone
                index={i}
                key={`dropzone${i}`}
                currentStatus={status}
                selectTask={(task: Task) => setSelectedTask(task)}
              />
            ))}
        </div>

        <div
          className={cn(styles.scroll, styles.right)}
          onMouseEnter={() => mouseEnter('right')}
          onMouseLeave={mouseLeave}
        >
          <KeyboardArrowRight fontSize="large" classes={{ root: styles.icon }} />
        </div>
        <div
          className={cn(styles.scroll, styles.left)}
          id="left-arrow"
          onMouseEnter={() => mouseEnter('left')}
          onMouseLeave={mouseLeave}
        >
          <KeyboardArrowLeft fontSize="large" classes={{ root: styles.icon }} />
        </div>
      </div>
    </DndProvider>
  );
};

export default TasksKanbanBoard;
