import React, { useState, useEffect, useContext, useMemo } from 'react';
import { Card, Typography, Box } from '@material-ui/core';
import { useDrag } from 'react-dnd';

import { taskCardStyles } from './TaskCard.style';
import { Task } from '@dar/api-interfaces';
import FlagIcon from '@material-ui/icons/Flag';
import { priorities } from '../../../../mocks/priorities';
import UserAvatar from '../../../UserAvatar';
import { useUsersState } from '../../../../selectors/users';
import TaskManagerModalsContext from '../../../../contexts/TaskManagerModalsContext';
import { useHistory } from 'react-router-dom';

interface TaskCardProps {
  task: Task;
}

export const TaskCard: React.FC<TaskCardProps> = ({ task }) => {
  const { handleDetailsModalOpen } = useContext(TaskManagerModalsContext);
  const [priorityColor, setPriorityColor] = useState('#000');
  const allUsers = useUsersState();
  const assignee = useMemo(() => {
    return allUsers.find((user) => user.id?.toLowerCase() === task.assignee_id.toLowerCase());
  }, [allUsers, task?.assignee_id]);
  const [{ isDragging }, dragRef] = useDrag(() => ({
    type: 'task',
    item: { id: task.id, status: task.status },
    collect: (monitor) => ({
      isDragging: monitor.isDragging(),
    }),
  }));
  const history = useHistory();

  const classes = taskCardStyles({ isDragging });

  useEffect(() => {
    const taskPriority = priorities.find((item) => item.name.toLowerCase() === task.priority?.toLowerCase());
    setPriorityColor(taskPriority?.color ? taskPriority.color : '#000');
  }, [task.priority]);

  const handleOpen = () => {
    handleDetailsModalOpen(task.id);
    history.push(`/productivity/tasks/${task.id}`);
  };

  return (
    <Card variant="outlined" className={classes.card} ref={dragRef} onClick={handleOpen}>
      <Box display="flex" justifyContent="space-between">
        <Typography variant="h4" className={classes.title}>
          {task.title}
        </Typography>
        <FlagIcon style={{ fill: priorityColor }} />
      </Box>

      <Box display="flex" justifyContent="flex-end" alignItems="center">
        <Typography color="secondary" className={classes.source}>
          ID: {task.id}
        </Typography>
        {assignee && <UserAvatar className={classes.avatar} user={assignee} />}
      </Box>
    </Card>
  );
};
