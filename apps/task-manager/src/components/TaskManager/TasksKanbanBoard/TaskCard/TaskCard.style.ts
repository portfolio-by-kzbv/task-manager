import { makeStyles } from '@material-ui/core';

type StyleProps = {
  isDragging: boolean;
};

export const taskCardStyles = makeStyles((theme) => ({
  card: {
    width: '100%',
    minHeight: '78px',
    maxHeight: '134px',
    padding: '8px 12px',
    boxSizing: 'border-box',
    cursor: 'pointer',
    marginBottom: 4,
    border: '1px solid #E0E3EA',
    opacity: (props: StyleProps) => props.isDragging ? 0.5 : 1,
    transition: 'box-shadow .3s',
    '&:hover': {
      boxShadow: '0 0 11px rgba(33,33,33,.2)'
    },
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between'  
  },
  title: {
    fontSize: 14
  },
  text: {
    paddingTop: 5,
    fontSize: 12
  },
  source: {
    fontSize: 12,
    cursor: 'pointer'
  },
  avatar: {
    margin: '0 0 0 8px !important',
    width: '20px',
    height: '20px'
  },
}));
