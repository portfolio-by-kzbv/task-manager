import { makeStyles } from '@material-ui/core';

export const tasksKanbanStyles = makeStyles((theme) => ({
  kanban: {
    height: 'calc(100% - 177px)',
    width: '100%',
    position: 'relative',
    overflow: 'hidden'
  },
  list: {
    display: 'flex',
    height: '100%',
    width: '100%',
    overflowX: 'scroll',
  },
  scroll: {
    background: '#101F2B',
    opacity: 0.12,
    height: 80,
    width: 90,
    position: 'absolute',
    transform: 'translateY(-50%)',
    top: '50%',
    display: 'flex',
    alignItems: 'center',
  },
  right: {
    borderRadius: '50px 0px 0px 50px',
    right: -45,
    paddingLeft: 10
  },
  left: {
    display: 'none',
    borderRadius: '0 50px 50px 0',
    left: -45,
    paddingRight: 10,
    justifyContent: 'flex-end'
  },
  icon: { color: '#FFFFFF' },
  hide: { display: 'none' }
}));