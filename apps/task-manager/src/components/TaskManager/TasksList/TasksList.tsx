import React, { useContext } from 'react';

import TasksCollapse from './TasksCollapse';
import TasksListHead from './TasksListHead';
import { TableContainer, Table, TableBody, Paper } from '@material-ui/core';

import useTaskList from './useTaskList';
import TaskManagerFiltersContext from '../../../contexts/TaskManagerFiltersContext';
import { Task } from '@dar/api-interfaces';
import TasksArchiveList from './TasksArchiveList';

const TasksList = () => {
  const { overdueTasks, tasksForToday, tasksForThisWeek, tasksForLater, allTasks } = useTaskList();
  const { order, orderBy, filters, handleOrderChange, handleOrderByChange } = useContext(TaskManagerFiltersContext);

  const handleRequestSort = (event: React.MouseEvent<unknown>, property: keyof Task) => {
    const isAsc = orderBy === property && order === 'asc';
    handleOrderChange(isAsc ? 'desc' : 'asc');
    handleOrderByChange(property);
  };

  return (
    <TableContainer component={Paper}>
      <Table aria-label="collapsible table">
        <TasksListHead order={order} orderBy={orderBy} onRequestSort={handleRequestSort} />
        {!filters.archive ? (
          <TableBody>
            {overdueTasks.length !== 0 && <TasksCollapse tasks={overdueTasks} key={0} filter="Overdue" />}
            <TasksCollapse tasks={tasksForToday} key={1} filter="Today" />
            <TasksCollapse tasks={tasksForThisWeek} key={2} filter="This week" />
            <TasksCollapse tasks={tasksForLater} key={3} filter="Later" />
          </TableBody>
        ) : (
          <TableBody>
            <TasksArchiveList tasks={allTasks} />
          </TableBody>
        )}
      </Table>
    </TableContainer>
  );
};

export default TasksList;
