import React, { useState, useCallback, useEffect, useMemo, useContext } from 'react';
import { Link } from 'react-router-dom';
import { theme } from '@dartech/dms-ui';
import moment from 'moment';
import { useDispatch } from 'react-redux';

import { TaskStatus, Task } from '@dar/api-interfaces';
import { User } from '@dar/api-interfaces';

import { Avatar, TableRow, TableCell, Box, Typography, makeStyles } from '@material-ui/core';
import FlagIcon from '@material-ui/icons/Flag';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import CheckCircleOutlineIcon from '@material-ui/icons/CheckCircleOutline';

import TaskActions from '../TaskDetails/TaskActions';
import Spinner from '../../Spinner/Spinner';
import StatusSelect from '@dar/components/Selects/StatusSelect';
import PromptModal from '@dar/components/Modal/PromptModal/PromptModal';

import { useStatusesState } from '../../../selectors/task-status.selector';
import { priorities } from '../../../mocks/priorities';
import { useUserSessionState, useUsersState } from '../../../selectors/users';
import TaskManagerModalsContext from '../../../contexts/TaskManagerModalsContext';
import { editTask, updateTaskStatus } from '@dar/actions/tasks.action';
import TaskManagerFiltersContext from '@dar/contexts/TaskManagerFiltersContext';

import { Dispatch } from '@dar/api-interfaces';

type Props = {
  task: Task;
  isOverdue: boolean;
};

interface LocationState {
  background?: string;
}

const useTableRowStyles = makeStyles((theme) => ({
  date: {
    backgroundColor: theme.palette.error[100],
    color: theme.palette.error[600],
    borderRadius: 12,
    padding: theme.spacing(0.5, 2),
    textAlign: 'center',
  },
  avatar: {
    margin: '0 6px',
    width: theme.spacing(8),
    height: theme.spacing(8),
    '&.MuiAvatar-colorDefault': {
      background: 'linear-gradient(180deg, #0ca9ea 0%, #59c7f4 97.92%)',
      color: '#fff',
    },
  },
}));

const TaskItem: React.FunctionComponent<Props> = ({ task, isOverdue }) => {
  const { handleDetailsModalOpen } = useContext(TaskManagerModalsContext);
  const session = useUserSessionState();
  const dispatch: Dispatch = useDispatch();
  const { updateTasks } = useContext(TaskManagerFiltersContext);
  const [values, setValues] = useState<Task>({
    ...task,
    status_id: task.status_id || 1,
  });

  const tableRowClasses = useTableRowStyles();

  const [priorityColor, setPriorityColor] = useState<string>('#000');
  const [statusColor, setStatusColor] = useState<string>('#000');
  const [taskStatus, setTaskStatus] = useState<string>('');
  const [loading, setLoading] = useState<boolean>(false);
  const [assignee, setAssignee] = useState<User>(null);
  const [changeTaskStatusModalOpen, setChangeTaskStatusModalOpen] = useState<boolean>(false);

  const { statuses } = useStatusesState();
  const allUsers = useUsersState();
  const isAssignee = useMemo(() => Boolean(task?.assignee_id === session?.id), [task, session]);
  const isCreator = useMemo(() => Boolean(task?.creator_id === session?.id), [task, session]);
  const taskLink = useMemo(() => `/productivity/tasks/${task.id}`, [task.id]);

  useEffect(() => {
    setValues({
      ...task,
      status_id: task.status_id || 1,
    });
  }, [task]);

  useEffect(() => {
    setLoading(true);
    const users = Object.values(allUsers);
    const user = users.find((item: User) => item.id === task.assignee_id) as User;
    setAssignee(user);
    setLoading(false);
  }, [allUsers, task.assignee_id]);

  useEffect(() => {
    const taskPriority = priorities.find((item) => item.name.toLowerCase() === task.priority?.toLowerCase());
    setPriorityColor(taskPriority?.color ? taskPriority.color : '#000');
  }, [task.priority]);

  useEffect(() => {
    const taskStatus = statuses.find((item: TaskStatus) => item.id === values.status_id);
    if (!taskStatus) {
      return;
    }
    setTaskStatus(taskStatus.name);
    setStatusColor(taskStatus?.color ? taskStatus.color : '#000');
  }, [values.status_id, statuses]);

  const handleTaskStatusModal = (event) => {
    setChangeTaskStatusModalOpen(true);
    handleChange('status_id', event.target.value);
  };

  const handleChange = (name: string, value: unknown) => {
    setValues({ ...values, [name]: value });
  };

  const getPayload = () => {
    const { priority, status_id } = values;
    const payload = {
      ...values,
      status_id: status_id,
      priority: priority,
    };

    return payload;
  };

  const handleEdit = useCallback(
    (payload) => {
      setLoading(true);
      dispatch(updateTaskStatus(payload.id, { status_id: payload.status_id })).finally(() => {
        updateTasks();
        setLoading(false);
      });
    },
    [dispatch, updateTasks]
  );

  const handleTaskStatusChange = () => {
    const task = getPayload();
    handleEdit(task);
    setChangeTaskStatusModalOpen(false);
  };

  return (
    <>
      {loading && <Spinner absolute />}
      <TableRow key={task.id}>
        <TableCell width="25%" component="th" scope="row">
          <Link
            to={{
              pathname: taskLink,
            }}
          >
            <Box display="flex" alignItems="center" onClick={() => handleDetailsModalOpen(task.id)}>
              <Box>
                <CheckCircleOutlineIcon style={{ color: theme.palette.grey[200], opacity: isAssignee ? 1 : 0 }} />
              </Box>
              <Typography
                variant="h6"
                style={{ color: theme.palette.primary[200], cursor: 'pointer', marginLeft: '28px' }}
              >
                {task.title}
              </Typography>
            </Box>
          </Link>
        </TableCell>
        <TableCell width="20%" align="left">
          {assignee &&
            (isAssignee ? (
              <Box display="flex" alignItems="center">
                <Typography variant="h6">Me</Typography>
              </Box>
            ) : (
              <Box display="flex" alignItems="center">
                <Box mr={2}>
                  <Avatar alt={assignee?.displayName} src={assignee?.photoUrl} className={tableRowClasses.avatar} />
                </Box>
                <Typography variant="h6">{assignee.displayName}</Typography>
              </Box>
            ))}
        </TableCell>
        <TableCell width="10%" align="left">
          <Box display="flex" alignItems="center">
            {isAssignee || isCreator ? (
              <StatusSelect
                variant="filled"
                value={values.status_id}
                onChange={(event) => handleTaskStatusModal(event)}
                currentStatus={task && task.status}
              />
            ) : (
              <>
                <Box mr={1}>
                  <FiberManualRecordIcon fontSize="small" style={{ fill: statusColor }} />
                </Box>
                <Typography variant="h6">{task.status}</Typography>
              </>
            )}
          </Box>
        </TableCell>
        <TableCell width="10%" align="left">
          {task.priority ? <FlagIcon style={{ fill: priorityColor }} /> : <> - </>}
        </TableCell>
        <TableCell width="13%" align="left">
          <div className={isOverdue ? tableRowClasses.date : ''}>
            <Typography variant="h6">
              {task.end_time ? <>{moment(task.end_time).format('DD MMM YYYY')}</> : <> - </>}
            </Typography>
          </div>
        </TableCell>
        <TableCell width="5%" align="center">
          <TaskActions task={task} />
        </TableCell>
      </TableRow>

      <PromptModal
        open={changeTaskStatusModalOpen}
        onClose={() => setChangeTaskStatusModalOpen(false)}
        onCancel={() => setChangeTaskStatusModalOpen(false)}
        onConfirm={handleTaskStatusChange}
        title={`Change task status to ${taskStatus}?`}
        confirmText="Change"
        confirmColor="secondary"
      />
    </>
  );
};

export default TaskItem;
