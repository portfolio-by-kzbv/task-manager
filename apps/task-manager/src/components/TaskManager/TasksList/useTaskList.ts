import { useMemo } from 'react';
import moment from 'moment';
import { Task } from '@dar/api-interfaces';

import { useTasksState } from '../../../selectors/tasks.selector';

type Order = 'asc' | 'desc';

const useTaskList = () => {
  const { tasks, filteredTasks, isFiltered } = useTasksState();
  const today = moment();

  function descendingComparator<T>(a: T, b: T, orderBy: keyof T) {
    if (b[orderBy] < a[orderBy]) {
      return -1;
    }
    if (b[orderBy] > a[orderBy]) {
      return 1;
    }
    return 0;
  }

  function getComparator<Key extends keyof any>(
    order: Order,
    orderBy: Key
  ): (a: { [key in Key]?: number | string | Date }, b: { [key in Key]?: number | string | Date }) => number {
    return order === 'desc'
      ? (a, b) => descendingComparator(a, b, orderBy)
      : (a, b) => -descendingComparator(a, b, orderBy);
  }

  function stableSort<T>(array: T[], comparator: (a: T, b: T) => number) {
    const stabilizedThis = array.map((el, index) => [el, index] as [T, number]);
    stabilizedThis.sort((a, b) => {
      const order = comparator(a[0], b[0]);
      if (order !== 0) return order;
      return a[1] - b[1];
    });
    return stabilizedThis.map((el) => el[0]);
  }

  const allTasks = useMemo(() => {
    return isFiltered ? filteredTasks : tasks;
  }, [tasks, isFiltered, filteredTasks]);

  const overdueTasks = useMemo(() => {
    const filterForOverdue = (items: Task[]) => {
      return items && items.filter((item) => moment(item.end_time).isBefore(today, 'day'));
    };
    return filterForOverdue(allTasks);
  }, [allTasks, today]);

  const tasksForToday = useMemo(() => {
    const filterForToday = (items: Task[]) => {
      return items && items.filter((item) => today.isSame(item.end_time, 'day'));
    };
    return filterForToday(allTasks);
  }, [allTasks, today]);

  const tasksForThisWeek = useMemo(() => {
    const filterForThisWeek = (items: Task[]) => {
      return items && items.filter((item) => today.isSame(item.end_time, 'week'));
    };
    return filterForThisWeek(allTasks);
  }, [allTasks, today]);

  const tasksForLater = useMemo(() => {
    const filterForLater = (items: Task[]) => {
      return items && items.filter((item) => today.isBefore(item.end_time));
    };
    return filterForLater(allTasks);
  }, [allTasks, today]);
  
  return {
    overdueTasks,
    tasksForToday,
    tasksForThisWeek,
    tasksForLater,
    allTasks,
    stableSort,
    getComparator,
  };
};

export default useTaskList;
