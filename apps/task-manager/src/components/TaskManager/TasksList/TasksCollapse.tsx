import React, { useState, useContext } from 'react';
import {
  Table,
  TableContainer,
  TableRow,
  TableCell,
  TableBody,
  Button,
  Collapse,
  Paper,
  makeStyles,
} from '@material-ui/core';

import KeyboardArrowRightIcon from '@material-ui/icons/KeyboardArrowRight';
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown';
import TaskItem from './TaskItem';

import TaskManagerFiltersContext from '../../../contexts/TaskManagerFiltersContext';
import useTaskList from './useTaskList';
import { Task } from '@dar/api-interfaces';

type Props = {
  filter: string;
  tasks: Task[];
};

const useFilterCellStyles = makeStyles((theme) => ({
  row: {
    color: theme.palette.grey[800],
    '&:hover': {
      backgroundColor: 'initial',
    },
  },
  button: {
    '&:hover': {
      backgroundColor: 'initial',
      borderColor: 'transparent',
    },
  },
  textSecondary: { '& > path ': { fill: theme.palette.primary[200] } },
}));

const useTaskCellStyles = makeStyles({
  cell: {
    padding: '0 !important',
    height: 'fit-content',
  },
  row: {
    border: 'none',
  },
});

const TasksCollapse: React.FunctionComponent<Props> = ({ filter, tasks }) => {
  const [open, setOpen] = useState(filter === 'Today' || filter === 'Overdue' ? true : false);
  const filterCellClasses = useFilterCellStyles();
  const taskCellClasses = useTaskCellStyles();
  const { loading, order, orderBy } = useContext(TaskManagerFiltersContext);
  const { stableSort, getComparator } = useTaskList();
  const page = 0;
  const rowsPerPage = tasks.length;
  const isOverdue = filter === 'Overdue';

  return (
    <React.Fragment>
      <TableRow className={filterCellClasses.row}>
        <TableCell colSpan={12}>
          <Button
            variant="text"
            color="secondary"
            aria-label="expand row"
            startIcon={open ? <KeyboardArrowDownIcon /> : <KeyboardArrowRightIcon />}
            onClick={() => setOpen(!open)}
            classes={{
              root: filterCellClasses.button,
              textSecondary: filterCellClasses.textSecondary,
            }}
          >
            {filter}
          </Button>
        </TableCell>
      </TableRow>

      <TableRow className={taskCellClasses.row}>
        <TableCell colSpan={12} className={taskCellClasses.cell}>
          <Collapse in={open} timeout="auto" unmountOnExit>
            <TableContainer style={{ maxHeight: 360 }} component={Paper}>
              <Table size="small" aria-label="tasks">
                <TableBody>
                  {stableSort(tasks as any[], getComparator(order, orderBy))
                    .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                    .map((task: Task) => {
                      return <TaskItem task={task} key={task.id} isOverdue={isOverdue} />;
                    })}
                </TableBody>
              </Table>
            </TableContainer>
          </Collapse>
        </TableCell>
      </TableRow>
    </React.Fragment>
  );
};

export default TasksCollapse;
