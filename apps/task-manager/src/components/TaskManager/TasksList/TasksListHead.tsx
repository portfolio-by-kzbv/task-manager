import React from 'react';
import { TableHead, TableRow, TableCell, TableSortLabel, createStyles, makeStyles } from '@material-ui/core';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { Task } from '@dar/api-interfaces';

type HeadCell = {
  id: keyof Task;
  align?: 'right' | 'center' | 'left' | 'inherit' | 'justify' | undefined;
  label: string;
  width: string;
  sortable: boolean;
};

const headCells: HeadCell[] = [
  { id: 'title', label: 'Title', width: '25%', sortable: false },
  { id: 'assignee_id', align: 'left', label: 'Assigned to', width: '20%', sortable: true },
  { id: 'status', align: 'left', label: 'Status', width: '10%', sortable: true },
  { id: 'priority', align: 'left', label: 'Priority', width: '10%', sortable: true },
  { id: 'end_time', align: 'left', label: 'Due date', width: '15%', sortable: true },
  { id: 'menu', label: ' ', width: '5%', sortable: false },
];

type TasksListHeadProps = {
  onRequestSort?: (event: React.MouseEvent<unknown>, property: keyof Task) => void;
  order?: 'asc' | 'desc';
  orderBy?: string;
};

const useStyles = makeStyles(() =>
  createStyles({
    visuallyHidden: {
      border: 0,
      clip: 'rect(0 0 0 0)',
      height: 1,
      margin: -1,
      overflow: 'hidden',
      padding: 0,
      position: 'absolute',
      top: 20,
      width: 1,
    },
  })
);

const TasksListHead = (props: TasksListHeadProps) => {
  const classes = useStyles();
  const { order, orderBy, onRequestSort } = props;
  const createSortHandler = (property: keyof Task) => (event: React.MouseEvent<unknown>) => {
    onRequestSort(event, property);
  };

  return (
    <TableHead>
      <TableRow>
        {headCells.map((headCell) => (
          <TableCell
            key={headCell.id}
            align={headCell.align}
            sortDirection={orderBy === headCell.id ? order : false}
            width={headCell.width}
          >
            <TableSortLabel
              active={orderBy === headCell.id}
              direction={orderBy === headCell.id ? order : 'asc'}
              onClick={headCell.sortable ? createSortHandler(headCell.id) : undefined}
              IconComponent={ArrowDropDownIcon}
              hideSortIcon={!headCell.sortable}
            >
              {headCell.label}
              {orderBy === headCell.id ? (
                <span className={classes.visuallyHidden}>
                  {order === 'desc' ? 'sorted descending' : 'sorted ascending'}
                </span>
              ) : null}
            </TableSortLabel>
          </TableCell>
        ))}
      </TableRow>
    </TableHead>
  );
};

export default TasksListHead;
