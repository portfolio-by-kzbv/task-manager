import React, { useContext } from 'react';
import { Table, TableRow, TableBody, makeStyles, TableCell } from '@material-ui/core';

import TaskItem from './TaskItem';

import TaskManagerFiltersContext from '../../../contexts/TaskManagerFiltersContext';
import useTaskList from './useTaskList';
import { Task } from '@dar/api-interfaces';

type Props = {
  tasks: Task[];
};

const useFilterCellStyles = makeStyles((theme) => ({
  row: {
    color: theme.palette.grey[800],
    '&:hover': {
      backgroundColor: 'initial',
    },
  },
  button: {
    '&:hover': {
      backgroundColor: 'initial',
      borderColor: 'transparent',
    },
  },
  textSecondary: { '& > path ': { fill: theme.palette.primary[200] } },
}));

const useTaskCellStyles = makeStyles({
  cell: {
    padding: '0 !important',
    height: 'fit-content',
  },
  row: {
    border: 'none',
  },
});

const TasksArchiveList: React.FunctionComponent<Props> = ({ tasks }) => {
  const taskCellClasses = useTaskCellStyles();
  const { loading, order, orderBy } = useContext(TaskManagerFiltersContext);
  const { stableSort, getComparator } = useTaskList();
  const page = 0;
  const rowsPerPage = tasks.length;

  return (
    <TableRow className={taskCellClasses.row}>
      <TableCell colSpan={12} className={taskCellClasses.cell}>
        <Table size="small" aria-label="tasks">
          <TableBody>
            {stableSort(tasks as any[], getComparator(order, orderBy))
              .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
              .map((task: Task) => {
                return <TaskItem task={task} key={task.id} isOverdue={false} />;
              })}
          </TableBody>
        </Table>
      </TableCell>
    </TableRow>
  );
};

export default TasksArchiveList;
