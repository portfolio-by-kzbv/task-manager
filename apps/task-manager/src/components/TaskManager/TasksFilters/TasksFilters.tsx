import React, { useContext, useMemo, useState, useEffect } from 'react';
import { Tabs, Tab, Box, makeStyles } from '@material-ui/core';
import StatusSelect from '../../Selects/StatusSelect';
import AssigneeSelect from '../../Selects/AssigneeSelect';

import TaskManagerFiltersContext from '../../../contexts/TaskManagerFiltersContext';
import { User } from '@dar/api-interfaces';
import { useUserSessionState } from '../../../selectors/users';
import TaskSearch from '@dar/components/TaskSearch/TaskSearch';
import DatePicker from '@dar/components/DatePicker/DatePicker';

const useStyles = makeStyles((theme) => ({
  container: {
    marginBottom: theme.spacing(4),
    display: 'flex',
    flexDirection: 'row',
    width: '100%',
  },
  filters: {
    marginLeft: '28px',
  },
}));

const TasksFilters = () => {
  const classes = useStyles();
  const { filters, handleChangeFilter } = useContext(TaskManagerFiltersContext);
  const session = useUserSessionState();
  const statusFilterValue = useMemo(() => filters.status, [filters.status]);
  const assigneeFilterValue = useMemo(() => filters.assignee, [filters.assignee]);

  const [filteredBy, setFilteredBy] = useState(0);

  return (
    <React.Fragment>
      <div className={classes.container}>
        <Tabs indicatorColor="primary" value={filteredBy}>
          <Tab label="All Tasks" onClick={() => [setFilteredBy(0), handleChangeFilter('assignedToMe', null)]} />
          <Tab label="Assigned to Me" onClick={() => [setFilteredBy(1), handleChangeFilter('assignedToMe', session)]} />
          <Tab label="My assignments" onClick={() => [setFilteredBy(2), handleChangeFilter('assignedByMe', session)]} />
          <Tab label="Tasks archive" onClick={() => [setFilteredBy(3), handleChangeFilter('archive', session)]} />
        </Tabs>
      </div>

      <div className={classes.container}>
        <TaskSearch />
        <Box ml={5} display="grid">
          <AssigneeSelect
            value={assigneeFilterValue}
            resetOnClear={true}
            onSelect={(value: User) => handleChangeFilter('assignee', value)}
            prompt={'Assigned to'}
          />
        </Box>
        {!filters.archive && (
          <Box ml={5} display="grid">
            <StatusSelect
              variant="filled"
              value={statusFilterValue}
              onChange={(event: React.ChangeEvent<HTMLInputElement>) =>
                handleChangeFilter('status', event.target.value)
              }
              isFilter
            />
          </Box>
        )}
      </div>
      {filters.archive && (
        <div className={classes.container}>
          <Box display="grid">
            <DatePicker
              emptyLabel={'Filter by start date'}
              value={filters.start_time ?? null}
              onChange={(value) => {
                const date = new Date(value);
                date.setHours(0);
                date.setMinutes(0);
                date.setSeconds(0);
                handleChangeFilter('start_time', date);
              }}
              label="Start date"
              fullWidth={true}
            />
          </Box>
          <Box ml={5} display="grid">
            <DatePicker
              emptyLabel={'Filter by end date'}
              value={filters.end_time ?? null}
              onChange={(value) => {
                const date = new Date(value);
                date.setHours(23);
                date.setMinutes(59);
                date.setSeconds(59);
                handleChangeFilter('end_time', date);
              }}
              label="End date"
              fullWidth={true}
            />
          </Box>
        </div>
      )}
    </React.Fragment>
  );
};

export default TasksFilters;
