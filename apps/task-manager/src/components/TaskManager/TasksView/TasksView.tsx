import React, { useMemo } from 'react';

import TasksList from '../TasksList/TasksList';
import TasksFilters from '../TasksFilters/TasksFilters';
import TasksKanbanBoard from '../TasksKanbanBoard/TasksKanbanBoard';

type Props = {
  view: 'list' | 'kanban' | 'calendar';
};

const TasksView: React.FunctionComponent<Props> = ({ view }) => {
  const renderView = useMemo(() => {
    if (view === 'list') {
      return <TasksList />;
    } else if (view === 'kanban') {
      return <TasksKanbanBoard />;
    }
  }, [view]);

  return (
    <React.Fragment>
      <TasksFilters />
      {renderView}
    </React.Fragment>
  );
};

export default TasksView;
