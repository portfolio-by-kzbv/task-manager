import React, { useMemo } from 'react';

import { ThemeProvider, Box, withStyles, LinearProgress, IconButton } from '@material-ui/core';
import { Styled } from './attachment-item.styles';
import { theme } from '@dartech/dms-ui';

import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import DeleteIcon from '@material-ui/icons/Delete';
import GetAppIcon from '@material-ui/icons/GetApp';
import { useUserSessionState } from '@dar/selectors/users';
import { Attachment } from '@dar/api-interfaces';

interface AttachmentsItemProps {
  file?: File;
  onClick?: (file: Attachment | File) => void | undefined;
  onDelete?: (file: Attachment | File) => void | undefined;
  attachment?: Attachment;
}

const FILENAME_DISPLAY_LENGTH = 18;

const AttachmentItemCompact: React.FC<AttachmentsItemProps> = ({
  file,
  onClick = undefined,
  onDelete = undefined,
  attachment,
}) => {
  const session = useUserSessionState();
  const attachmentItem = useMemo(() => {
    return file ?? attachment;
  }, [file, attachment]);

  const isFile = file instanceof File;

  const canEdit = useMemo(
    () =>
      attachmentItem && 'user_id' in attachmentItem
        ? attachmentItem.user_id.toLowerCase() === session?.id.toLowerCase()
        : false,
    [attachmentItem, session]
  );

  const title = useMemo(() => {
    if (!attachmentItem.name) {
      return '';
    }
    if (attachmentItem.name.length <= FILENAME_DISPLAY_LENGTH) {
      return attachmentItem.name;
    }
    const [fileName, ...typesArr] = attachmentItem.name.split('.');
    const types = typesArr.join('.');
    const visibleNameLength = FILENAME_DISPLAY_LENGTH - 1 - types.length;
    const visibleNamePart = fileName.substring(0, visibleNameLength - 3) + '...';
    const visibleFilename = `${visibleNamePart}.${types}`;
    return visibleFilename;
  }, [attachmentItem]);

  const fileSize = useMemo(() => {
    if (!attachmentItem.size) return null;
    const k = 1000;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    const fileSize = attachmentItem.size;
    const i = Math.floor(Math.log(fileSize) / Math.log(k));
    return parseFloat((fileSize / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
  }, [attachmentItem]);

  const handleClick = () => {
    if (onClick) {
      onClick(attachmentItem);
    }
  };

  const handleDelete = () => {
    if (onDelete) {
      onDelete(attachmentItem);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <Styled.AttachmentItem>
        <div className="AttachmentItemCompact">
          <div className="ContentWrapper">
            <Box justifyContent="flex-start" display="flex">
              <InsertDriveFileIcon className="AttachmentIcon" fontSize="small" />
              <span className="AttachmentItemTitle">
                {title}
                {fileSize && <span className="AttachmentItemSize"> ({fileSize})</span>}
              </span>
            </Box>
            {canEdit && (
              <Box>
                <IconButton aria-label="download todo item" onClick={handleClick} edge="start">
                  <GetAppIcon fontSize="small" />
                </IconButton>
                <IconButton aria-label="delete todo item" onClick={handleDelete} edge="end">
                  <DeleteIcon fontSize="small" />
                </IconButton>
              </Box>
            )}
            {isFile && (
              <Box>
                <IconButton aria-label="delete todo item" onClick={handleDelete} edge="end">
                  <DeleteIcon fontSize="small" />
                </IconButton>
              </Box>
            )}
          </div>
        </div>
      </Styled.AttachmentItem>
    </ThemeProvider>
  );
};

export default AttachmentItemCompact;
