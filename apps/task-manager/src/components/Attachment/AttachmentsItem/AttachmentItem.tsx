import React, { useMemo } from 'react';

import { ThemeProvider, Typography, withStyles, LinearProgress } from '@material-ui/core';
import { Styled } from './attachment-item.styles';
import { theme } from '@dartech/dms-ui';

import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import CancelIcon from '@material-ui/icons/Cancel';
import { useUserSessionState } from '@dar/selectors/users';
import { Attachment } from '@dar/api-interfaces';

interface AttachmentsItemProps {
  onClick?: (file: Attachment) => void;
  onDelete?: (file: Attachment) => void;
  attachment?: Attachment;
}

const FILENAME_DISPLAY_LENGTH = 18;

const Loader = withStyles((theme) => ({
  root: {
    height: 4,
    borderRadius: 0,
  },
  colorPrimary: {
    backgroundColor: theme.palette.grey[100],
  },
  bar: {
    borderRadius: 0,
    backgroundColor: '#6787e3',
  },
}))(LinearProgress);

const AttachmentItem: React.FC<AttachmentsItemProps> = ({ onClick = undefined, onDelete = undefined, attachment }) => {
  const session = useUserSessionState();
  const attachmentItem = attachment;
  const canEdit = useMemo(
    () =>
      attachmentItem && 'user_id' in attachmentItem
        ? attachmentItem.user_id.toLowerCase() === session?.id.toLowerCase()
        : false,
    [attachmentItem, session]
  );

  const title = useMemo(() => {
    if (!attachmentItem.name) {
      return '';
    }
    if (attachmentItem.name.length <= FILENAME_DISPLAY_LENGTH) {
      return attachmentItem.name;
    }
    const [fileName, ...typesArr] = attachmentItem.name.split('.');
    const types = typesArr.join('.');
    const visibleNameLength = FILENAME_DISPLAY_LENGTH - 1 - types.length;
    const visibleNamePart = fileName.substring(0, visibleNameLength - 3) + '...';
    const visibleFilename = `${visibleNamePart}.${types}`;
    return visibleFilename;
  }, [attachmentItem]);

  const fileSize = useMemo(() => {
    if (!attachmentItem.size) return null;
    const k = 1000;
    const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB'];
    const fileSize = attachmentItem.size;
    const i = Math.floor(Math.log(fileSize) / Math.log(k));
    return parseFloat((fileSize / Math.pow(k, i)).toFixed(2)) + ' ' + sizes[i];
  }, [attachmentItem]);

  const handleClick = () => {
    if (onClick) {
      onClick(attachmentItem);
    }
  };

  const handleDelete = () => {
    if (onDelete) {
      onDelete(attachmentItem);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <Styled.AttachmentItem>
        <div className="AttachmentItem">
          {canEdit ? <CancelIcon className="CloseIcon" fontSize="small" onClick={handleDelete} /> : null}
          <div className="ContentWrapper" onClick={handleClick}>
            <div className="AttachmentItemIconWrapper">
              <InsertDriveFileIcon className="AttachmentIcon" fontSize="small" />
            </div>
            {attachmentItem?.isLoading && (
              <Loader variant="determinate" value={(attachmentItem.loadingProgress || 0) * 100} />
            )}
            <div className="AttachmentItemTitleWrapper">
              <span className="AttachmentItemTitle">
                {title}
                {fileSize && <span className="AttachmentItemSize"> ({fileSize})</span>}
              </span>
            </div>
          </div>
          {attachmentItem.error && <Typography className="AttachmentError">{attachmentItem.error}</Typography>}
        </div>
      </Styled.AttachmentItem>
    </ThemeProvider>
  );
};

export default AttachmentItem;
