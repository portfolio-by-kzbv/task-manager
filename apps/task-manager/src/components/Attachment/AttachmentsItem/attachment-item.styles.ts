import styled from 'styled-components';
import { theme } from '@dartech/dms-ui';

const AttachmentItem = styled.div`
  .AttachmentItem {
    position: relative;
    margin-left: 16px;
    margin-bottom: 24px;
    cursor: pointer;
    width: fit-content;
  }
  .ContentWrapper {
    width: 178px;
    height: 112px;
    display: flex;
    flex-direction: column;
    background: ${theme.palette.grey[100]};
  }
  .AttachmentItemIconWrapper {
    flex: 1;
    display: flex;
    align-items: center;
    justify-content: center;
  }
  .AttachmentIcon {
    height: 30px;
    width: auto;
  }
  .AttachmentItemTitleWrapper {
    width: 100%;
    min-height: 32px;
    background: ${theme.palette.grey[200]};
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .AttachmentItemTitle {
    font-size: 12px;
    font-weight: 400;
    color: #424242;
  }
  .AttachmentItemSize {
    color: ${theme.palette.primary[200]};
  }
  .CloseIcon {
    width: 20px;
    height: 20px;
    position: absolute;
    top: -10px;
    right: -10px;
    z-index: 10;
  }
  .AttachmentError {
    margin-top: 4px;
    margin-bottom: -8px;
    font-size: 12px;
    color: ${theme.palette.error[600]};
    text-align: left;
  }
  .LoadingIndicator {
    height: 4px;
    padding: 0;
    margin: 0;
    position: absolute;
    bottom: 32px;
    background: #6787e3;
  }
  .AttachmentItemCompact {
    width: 608px;
    height: 40px;
    display: flex;
    flex-direction: row;
    justify-content: space-between;
    align-items: center;
    border-radius: 4px;
    cursor: pointer;
    background: #FFFFFF;
    border: 1px solid #E0E3EA;
    margin-bottom: 4px;

    &:hover {
      background: #E6ECF1;
      border-color: transparent;

      .ControlsWrapper {
        .DeleteIcon {
          display: block;
        }
      }
    }

    .ContentWrapper {
      height: 40px;
      width: 100%;
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;
      margin: 0 14px 0 20px;
      background: none;

      .Spinner {
        color: ${theme.palette.grey[500]};
      }

      .AttachmentIcon {
        height: 22px;
        width: auto;
      }

      .AttachmentItemTitle {
        font-size: 14px;
        font-weight: 400;
        margin-left: 10px;
      }
    }

    .ControlsWrapper {
      display: flex;
      flex-direction: row;
      justify-content: flex-end;
      margin-right: 20px;

      .DeleteIcon {
        height: 15px;
        width: auto;
        display: none;
      }
    }

    .DeletedFileContentWrapper {
      font-size: 14px;
      font-weight: 400;
      marginleft: 12px;

      .UndoButton {
        color: ${theme.palette.primary[200]};
      }
    }
  }
`;

export const Styled = {
  AttachmentItem,
};
