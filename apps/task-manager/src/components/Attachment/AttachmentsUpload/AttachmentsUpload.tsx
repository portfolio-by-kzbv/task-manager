import React, { ChangeEvent, useRef, useMemo } from 'react';
import cn from 'classnames';
import { Button } from '@material-ui/core';
import { Styled } from './attachments-upload.styles';

const UPLOAD_FILE_TEXT = 'Drag and drop your file here';
const MAXIMUM_NUMBER_REACHED_TEXT = 'Maximum number of files reached';

interface AttachmentsUploadProps {
  variant?: 'big' | 'compact';
  attachmentsCount?: number;
  name?: string;
  allowedFileTypes?: string[];
  maximumFileSize?: number;
  isFilesLimitReached?: boolean;
  error?: string;
  inputRef?: React.LegacyRef<HTMLInputElement>;
  onFileSelect?: (selectedFile: File) => void | undefined;
}

const AttachmentsUpload: React.FC<AttachmentsUploadProps> = ({
  variant = 'big',
  attachmentsCount = 0,
  name = 'attachments',
  allowedFileTypes = [],
  maximumFileSize = 50,
  isFilesLimitReached = false,
  error = '',
  inputRef = null,
  onFileSelect = undefined,
  ...otherProps
}) => {
  const hiddenFileInputRef = useRef<HTMLInputElement>(null);

  const mainText = useMemo(() => (isFilesLimitReached ? MAXIMUM_NUMBER_REACHED_TEXT : UPLOAD_FILE_TEXT), [
    isFilesLimitReached,
  ]);

  const secondaryText = useMemo(() => {
    const fileTypesString = ' ' + allowedFileTypes.map((fileType) => `.${fileType}`).join(', ');
    const sizeLimitString = `${maximumFileSize}MB`;
    return `Permitted files ${fileTypesString} no larger than ${sizeLimitString}`;
  }, [allowedFileTypes, maximumFileSize]);

  const handleFileInputCLick = () => {
    hiddenFileInputRef.current?.click();
  };

  const handleFileSelect = (e: ChangeEvent<HTMLInputElement>) => {
    const selectedFile = e.target?.files ? e.target?.files[0] : null;
    if (selectedFile && onFileSelect) {
      onFileSelect(selectedFile);
      console.log('selectedFile: ', selectedFile);
    }
    if (hiddenFileInputRef.current) {
      hiddenFileInputRef.current.value = '';
    }
  };

  return (
    <Styled.AttachmentsUpload>
      <input type="hidden" name={name} ref={inputRef} />
      <div
        className={cn({
          AttachmentUploadCompact: variant === 'compact',
          AttachmentsUploadLowHeight: variant === 'compact' || (variant === 'big' && attachmentsCount),
          AttachmentsUploadBigWithoutFiles: variant === 'big' && !attachmentsCount,
          AttachmentsUploadCompactWithError: variant === 'compact' && error,
        })}
      >
        <div className="ContentWrapper">
          <div className="TextWrapper">
            <p className="MainText">{mainText}</p>
            <p className="SecondaryText">{secondaryText}</p>
          </div>
          <input
            type="file"
            accept={allowedFileTypes.map((type) => '.' + type).join(',')}
            hidden
            ref={hiddenFileInputRef}
            onChange={handleFileSelect}
          />
          <Button variant="contained" color="secondary" onClick={handleFileInputCLick} disabled={isFilesLimitReached}>
            Select file
          </Button>
        </div>
        {error && !attachmentsCount && variant !== 'compact' && <div className="Error">{error}</div>}
      </div>
    </Styled.AttachmentsUpload>
  );
};

export default AttachmentsUpload;
