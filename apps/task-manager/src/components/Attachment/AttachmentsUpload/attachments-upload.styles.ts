import styled from 'styled-components';
import { theme } from '@dartech/dms-ui';

const AttachmentsUpload = styled.div`
  .AttachmentsUploadBigWithoutFiles {
    position: relative;
    width: 100%;
    height: 240px;
    border-radius: 4px;

    & .ContentWrapper {
      text-align: center;
      padding-top: 70px;
      display: flex;
      flex-direction: column;
      align-items: center;

      & .TextWrapper {
        margin-bottom: 24px;
      }
    }
  }
  .AttachmentsUploadLowHeight {
    width: 100%;
    min-height: 80px;
    height: 80px;
    background: ${theme.palette.grey[100]};
    border-top: 1px dashed #b7c3cc;
    border-radius: 4px;

    & .ContentWrapper {
      height: 100%;
      min-height: 80px;
      margin-left: 20px;
      margin-right: 20px;
      display: flex;
      flex-direction: row;
      justify-content: space-between;
      align-items: center;

      & .TextWrapper {
        text-align: left;

        & .MainText {
          font-size: 14px;
          font-weight: 400;
          line-height: 18px;
          margin-bottom: 4px;
        }

        & .SecondaryText {
          font-size: 12px;
          line-height: 16px;
          margin-bottom: 0;
        }
      }
    }
  }
  .TextWrapper {
    text-align: center;
  }
  .MainText {
    margin-top: 0;
    margin-bottom: 8px;
    font-family: Golos, Helvetics, sans-serif;
    font-weight: 500;
    font-size: 16px;
    line-height: 20px;
    color: ${theme.palette.grey[700]};
  }
  .SecondaryText {
    margin-top: 0;
    margin-bottom: 0;
    font-family: Golos, Helvetics, sans-serif;
    font-weight: 400;
    font-size: 12px;
    line-height: 16px;
    max-width: 320px;
    color: ${theme.palette.grey[500]};
  }
  .AttachmentsUploadBig.AttachmentsUploadWithoutFiles {
    text-align: center;
    paddingtop: 70px;
  }
  .AttachmentsUploadBig.AttachmentsUploadWithFiles {
    text-align: left;
  }
  .Error {
    width: 100%;
    position: absolute;
    bottom: 42px;
    font-size: 12px;
    text-align: center;
    color: ${theme.palette.error[600]};
  }
  .AttachmentUploadCompact {
    width: 608px;
    height: 80px;
    background: ${theme.palette.grey[50]};
    border: 1px dashed #b7c3cc;
    border-radius: 4px;

    &:hover {
      border: 2px ${theme.palette.primary[200]} solid;
      margin: -1px;
    }
  }
  .AttachmentsUploadCompactWithError {
    border: 2px ${theme.palette.error[600]} solid;
    margin: -1px;

    &:hover {
      border: 2px ${theme.palette.primary[600]} solid;
    }
  }
`;

export const Styled = {
  AttachmentsUpload,
};
