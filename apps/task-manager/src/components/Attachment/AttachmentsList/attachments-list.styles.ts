import styled from 'styled-components';

const AttachmentsList = styled.div`
  .AttachmentsListWrapper {
    width: 100%;
    height: auto;
    min-height: 160px;
    max-height: 296px;
    overflow: hidden;
  }
  .AttachmentsList {
    height: auto;
    width: 608px;
    padding-top: 28px;
    display: flex;
    flex-direction: row;
    flex-wrap: wrap;
  }
  .AttachmentsListNoScrolling {
    width: 608px;
    height: auto;
  }
  .AttachmentsListCompact {
    display: flex;
    flex-direction: column;
    height: auto;
    width: 100%;
  }
  .AttachmentListReadOnly {
    .AttachmentsList {
      margin-left: -16px;
    }
  }
`;

export const Styled = {
  AttachmentsList,
};
