import React from 'react';
import { useDispatch } from 'react-redux';
import { Dispatch } from '@dar/api-interfaces';

import AttachmentItem from '@dar/components/Attachment/AttachmentsItem/AttachmentItem';
import { Attachment } from '@dar/api-interfaces';
import api from '@dar/services/api';
import Spinner from '@dar/components/Spinner/Spinner';
import { deleteAttachment } from '@dar/actions/task-attachments.action';
import { Styled } from './attachments-list.styles';
import { useAttachmentsState } from '@dar/selectors/task-attachments.selector';
import AttachmentItemCompact from '../AttachmentsItem/AttachmentItemCompact';

interface AttachmentsListProps {
  variant?: 'big' | 'compact';
  attachments?: Attachment[];
  showWithoutScroll?: boolean;
}

const AttachmentsList: React.FC<AttachmentsListProps> = ({
  variant = 'big',
  attachments = [],
  showWithoutScroll = false,
  ...otherProps
}) => {
  const dispatch: Dispatch = useDispatch();
  const { loading } = useAttachmentsState();

  if (!attachments.length) {
    return null;
  }

  const handleDownload = async (attachment: Attachment) => {
    const resAttachment = await api.attachments.getAttachment(attachment.id);
    api.attachments.downloadAttachment(resAttachment.link, attachment.name);
  };

  const handleDelete = (attachment_id: number) => {
    dispatch(deleteAttachment(attachment_id));
  };

  return (
    <Styled.AttachmentsList>
      <div>
        {loading && <Spinner absolute />}
        <div className="AttachmentsListCompact">
          {attachments.map((attachment) => (
            <AttachmentItemCompact
              key={attachment.id}
              attachment={attachment}
              onClick={() => handleDownload(attachment)}
              onDelete={() => handleDelete(attachment.id)}
            />
          ))}
        </div>
      </div>
    </Styled.AttachmentsList>
  );
};

export default AttachmentsList;
