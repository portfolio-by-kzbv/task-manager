import React from 'react';
import moment from 'moment';
import { IconButton, Typography, Fade, TextField } from '@material-ui/core';
import AccessAlarmIcon from '@material-ui/icons/AccessAlarm';
import EditIcon from '@material-ui/icons/Edit';
import CloseIcon from '@material-ui/icons/Close';
import CheckOutlinedIcon from '@material-ui/icons/CheckOutlined';
import useStyles from './EditableTime.style';

type Props = {
  loading?: boolean;
  isEditing?: boolean;
  hovered?: boolean;
  value: Date;
  canEdit?: boolean;
  initialValue: Date;
  setHovered: (value: boolean) => void;
  toggleIsEditing: () => void;
  onChange: (value: string) => void;
  onClear: () => void;
  onSubmit: () => void;
};

const EditableTimeScreen: React.FunctionComponent<Props> = ({
  loading = false,
  isEditing = false,
  hovered = false,
  value,
  canEdit = false,
  initialValue,
  setHovered,
  toggleIsEditing,
  onChange,
  onClear,
  onSubmit,
}) => {
  const classes = useStyles();

  return canEdit && isEditing ? (
    <div className={classes.root}>
      <TextField
        type="time"
        variant="outlined"
        margin="dense"
        value={moment(value).format('HH:mm')}
        onChange={(event) => onChange(event.target.value)}
        disabled={loading}
      />
      <IconButton className={classes.button} size="small" onClick={onClear}>
        <CloseIcon fontSize="small" />
      </IconButton>
      <IconButton className={classes.button} size="small" onClick={onSubmit}>
        <CheckOutlinedIcon fontSize="small" />
      </IconButton>
    </div>
  ) : (
    <div className={classes.root}>
      <Typography className={classes.text} onMouseOver={() => setHovered(true)} onMouseOut={() => setHovered(false)}>
        <AccessAlarmIcon className={classes.dateIcon} fontSize="small" color="primary" />
        {moment(initialValue).format('HH:mm')}
        <Fade in={canEdit && hovered}>
          <IconButton onClick={toggleIsEditing} size="small" className={classes.editButton}>
            <EditIcon fontSize="small" color="disabled" />
          </IconButton>
        </Fade>
      </Typography>
    </div>
  );
};

export default EditableTimeScreen;
