import { makeStyles } from '@material-ui/core';

export default makeStyles({
  root: {
    display: 'flex',
    alignItems: 'flex-start',
    width: '200px',
  },
  text: {
    display: 'flex',
    alignItems: 'center',
    fontWeight: 600,
    lineHeight: '48px',
  },
  button: {
    marginLeft: '8px',
    marginTop: '14px',
  },
  dateIcon: {
    marginRight: '8px',
  },
  editButton: {
    marginLeft: '8px',
  },
});
