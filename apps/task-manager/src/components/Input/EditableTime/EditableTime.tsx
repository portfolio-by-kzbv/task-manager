import React from 'react';

import useEditableTime from './useEditableTime';
import EditableTimeScreen from './EditableTimeScreen';

type Props = {
  canEdit?: boolean;
  initialValue: Date;
  onSave: (value) => Promise<void>;
};

const EditableTime: React.FunctionComponent<Props> = ({ canEdit = false, initialValue, onSave }) => {
  const { models, operations } = useEditableTime(initialValue);
  const { value } = models;
  const { setValue, setIsEditing, setLoading, setHovered } = operations;

  const toggleIsEditing = () => setIsEditing((isEditing) => !isEditing);

  const onChange = (time) => {
    const t = time.split(':');
    const newTime = new Date(value).setHours(t[0], t[1], 0);
    setValue(new Date(newTime));
  };

  const onClear = () => {
    setValue(initialValue);
    setIsEditing(false);
    setHovered(false);
  };

  const onSubmit = () => {
    setLoading(true);

    onSave(value)
      .then(() => {
        setIsEditing(false);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const screenProps = {
    ...models,
    canEdit,
    initialValue,
    setHovered,
    toggleIsEditing,
    onChange,
    onClear,
    onSubmit,
  };

  return <EditableTimeScreen {...screenProps} />;
};

export default EditableTime;
