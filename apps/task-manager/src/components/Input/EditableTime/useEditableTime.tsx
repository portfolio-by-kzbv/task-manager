import { useState, useEffect } from 'react';

const useEditableTime = (initialValue: Date) => {
  const [isEditing, setIsEditing] = useState(false);
  const [hovered, setHovered] = useState(false);
  const [loading, setLoading] = useState(false);
  const [value, setValue] = useState(initialValue);

  useEffect(() => {
    setValue(initialValue);
  }, [initialValue]);

  const models = {
    loading,
    isEditing,
    hovered,
    value,
  };

  const operations = {
    setLoading,
    setIsEditing,
    setHovered,
    setValue,
  };

  return { models, operations };
};

export default useEditableTime;
