import React from 'react';

import TimePickerScreen from './TimePickerScreen';

type Props = {
  disabled?: boolean;
  onSelect: (value: string) => void;
  value: string;
  label: string;
  error?: string;
};

const TimePicker: React.FunctionComponent<Props> = ({ disabled = false, error = '', value, label, onSelect }) => {
  const options = [];

  for (let i = 9; i < 24; i++) {
    for (let j = 0; j < 60; j += 15) {
      const hours = i < 10 ? `0${i}` : i;
      const minutes = j < 10 ? `0${j}` : j;
      options.push(`${hours}:${minutes}`);
    }
  }

  for (let i = 0; i < 9; i++) {
    for (let j = 0; j < 60; j += 15) {
      const hours = i < 10 ? `0${i}` : i;
      const minutes = j < 10 ? `0${j}` : j;
      options.push(`${hours}:${minutes}`);
    }
  }

  return (
    <TimePickerScreen
      disabled={disabled}
      options={options}
      value={value}
      onSelect={onSelect}
      error={error}
      label={label}
    />
  );
};

export default TimePicker;
