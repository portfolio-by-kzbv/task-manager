import React, { useEffect } from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete';
import { TextField } from '@material-ui/core';

import useStateCallback from '../../../hooks/useStateCallback';
import { getHoursFromInput, getMinutesFromInput, isAddTimeSeparator, isRemoveTimeSeparator } from '../../../utils/time';

const TIME_INPUT_REGEXP = /[^0-9:]/g;
const ONLY_DIGITS_REGEXP = /\D/g;
const TIME_SEPARATOR = ':';
const TIME_INPUT_MAX_SIZE = 5;
const MAX_SIZE_NO_TIME_SEPARATOR = TIME_INPUT_MAX_SIZE - 1;

type Props = {
  disabled?: boolean;
  options?: string[];
  value: string;
  label: string;
  error?: string;
  onSelect: (value: string) => void;
};

const TimePickerScreen: React.FunctionComponent<Props> = ({
  disabled = false,
  options = [],
  value,
  label,
  error = '',
  onSelect,
}) => {
  const [inputValue, setInputValue] = useStateCallback(value);

  useEffect(() => {
    setInputValue(value);
  }, [value, setInputValue]);

  const onBlur = () => {
    const onlyNumberValue = inputValue.replace(ONLY_DIGITS_REGEXP, '');

    const filledValue =
      onlyNumberValue.length > MAX_SIZE_NO_TIME_SEPARATOR
        ? onlyNumberValue.substr(0, MAX_SIZE_NO_TIME_SEPARATOR)
        : new Array(MAX_SIZE_NO_TIME_SEPARATOR - onlyNumberValue.length)
            .fill('0')
            .reduce((accum, item) => `${accum}${item}`, onlyNumberValue);

    const valueWithTimeSeparator = getHoursFromInput(filledValue) + TIME_SEPARATOR + getMinutesFromInput(filledValue);
    setInputValue(valueWithTimeSeparator, (value) => {
      onSelect(value);
    });
  };

  const onInputChange = (e) => {
    const value = e ? e.target.value : '';

    if (typeof value === 'undefined') {
      setInputValue('');
    }

    if (typeof value === 'string') {
      let onlyNumberValue = value.replace(TIME_INPUT_REGEXP, '');
      const currentValueLength = value.length;
      const previousValueLength = inputValue.length;

      if (isAddTimeSeparator(currentValueLength, previousValueLength)) {
        onlyNumberValue = getHoursFromInput(onlyNumberValue) + TIME_SEPARATOR + (onlyNumberValue[2] || '');
      } else if (isRemoveTimeSeparator(currentValueLength, previousValueLength)) {
        onlyNumberValue = getHoursFromInput(onlyNumberValue);
      }

      if (onlyNumberValue.length > TIME_INPUT_MAX_SIZE) {
        onlyNumberValue = onlyNumberValue.substr(0, TIME_INPUT_MAX_SIZE);
      }

      setInputValue(onlyNumberValue, (onlyNumberValue) => {
        if (onlyNumberValue.length === TIME_INPUT_MAX_SIZE) {
          onSelect(onlyNumberValue);
        }
      });
    }
  };

  return (
    <Autocomplete
      disabled={disabled}
      onChange={(_, value) => {
        value = value || '';
        setInputValue(value);
        if (value !== '') {
          onSelect(value);
        }
      }}
      options={options}
      value={value}
      freeSolo={true}
      onBlur={onBlur}
      onInputChange={onInputChange}
      inputValue={inputValue}
      renderInput={(params) => (
        <TextField
          {...params}
          variant="outlined"
          label={label}
          margin="normal"
          error={Boolean(error)}
          helperText={error ? error : ''}
        />
      )}
    />
  );
};

export default TimePickerScreen;
