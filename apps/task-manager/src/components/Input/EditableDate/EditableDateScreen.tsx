import React from 'react';
import moment from 'moment';
import { Fade, IconButton, Typography } from '@material-ui/core';
import CalendarTodayOutlinedIcon from '@material-ui/icons/CalendarTodayOutlined';
import EditIcon from '@material-ui/icons/Edit';
import CloseIcon from '@material-ui/icons/Close';
import CheckOutlinedIcon from '@material-ui/icons/CheckOutlined';

import DatePicker from '../../DatePicker/DatePicker';

import useStyles from './EditableDate.style';

type Props = {
  initialValue: string;
  value: Date;
  onChange: (date: Date) => void;
  loading: boolean;
  canEdit: boolean;
  isEditing: boolean;
  onClear: () => void;
  onSubmit: () => void;
  toggleIsEditing: () => void;
  hovered: boolean;
  setHovered: (arg: boolean) => void;
};

const EditableDateScreen: React.FunctionComponent<Props> = ({
  initialValue,
  value,
  onChange,
  loading,
  canEdit,
  isEditing,
  onClear,
  onSubmit,
  toggleIsEditing,
  hovered,
  setHovered,
}) => {
  const classes = useStyles();

  return canEdit && isEditing ? (
    <div className={classes.root}>
      <DatePicker value={value} onChange={onChange} disabled={loading} />
      <IconButton className={classes.button} size="small" onClick={onClear}>
        <CloseIcon fontSize="small" />
      </IconButton>
      <IconButton className={classes.button} size="small" onClick={onSubmit}>
        <CheckOutlinedIcon fontSize="small" />
      </IconButton>
    </div>
  ) : (
    <div className={classes.root}>
      <Typography className={classes.text} onMouseOver={() => setHovered(true)} onMouseOut={() => setHovered(false)}>
        <CalendarTodayOutlinedIcon className={classes.dateIcon} fontSize="small" color="primary" />
        {moment(initialValue).format('DD.MM.YY')}
        <Fade in={canEdit && hovered}>
          <IconButton onClick={toggleIsEditing} size="small" className={classes.editButton}>
            <EditIcon fontSize="small" color="disabled" />
          </IconButton>
        </Fade>
      </Typography>
    </div>
  );
};

export default EditableDateScreen;
