import { useState, useEffect } from 'react';

const useEditableDate = initialValue => {
  const [isEditing, setIsEditing] = useState(false);
  const [hovered, setHovered] = useState(false);
  const [loading, setLoading] = useState(false);
  const [value, setValue] = useState(initialValue);

  useEffect(() => {
    setValue(initialValue);
  }, [initialValue]);

  const models = {
    isEditing,
    hovered,
    loading,
    value
  };

  const operations = {
    setIsEditing,
    setHovered,
    setLoading,
    setValue
  };

  return { models, operations };
};

export default useEditableDate;
