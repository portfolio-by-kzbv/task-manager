import React from 'react';

import EditableDateScreen from './EditableDateScreen';
import useEditableDate from './useEditableDate';

type Props = {
  canEdit?: boolean;
  initialValue: string;
  onSave: (arg) => Promise<void>;
};

const EditableDate: React.FunctionComponent<Props> = ({ canEdit = false, initialValue, onSave }) => {
  const { models, operations } = useEditableDate(initialValue);
  const { setIsEditing, setLoading, setValue, setHovered } = operations;

  const toggleIsEditing = () => setIsEditing((isEditing) => !isEditing);
  const onChange = (date: Date) => {
    setValue(new Date(date));
  };
  const onClear = () => {
    setValue(initialValue);
    setIsEditing(false);
    setHovered(false);
  };

  const onSubmit = () => {
    setLoading(true);

    onSave(models.value)
      .then(() => {
        setIsEditing(false);
      })
      .finally(() => {
        setLoading(false);
      });
  };

  const screenProps = {
    ...models,
    setHovered,
    initialValue,
    canEdit,
    toggleIsEditing,
    onChange,
    onClear,
    onSubmit,
  };

  return <EditableDateScreen {...screenProps} />;
};

export default EditableDate;
