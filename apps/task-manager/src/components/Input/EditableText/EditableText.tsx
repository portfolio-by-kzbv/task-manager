import React from 'react';

import EditableTextScreen from './EditableTextScreen';
import useEditableText from './useEditableText';

type Props = {
  valueKey?: string;
  initialValue: string;
  onSave: (value) => Promise<void>;
  typographyProps?: Record<string, unknown>;
  placeholder?: string;
  validation?: (value) => string;
  canEdit?: boolean;
};

const EditableText: React.FunctionComponent<Props> = ({
  valueKey = '',
  initialValue,
  onSave,
  typographyProps = {},
  placeholder = '',
  validation = () => null,
  canEdit = false,
}) => {
  const { models, operations } = useEditableText(initialValue);
  const { value, error } = models;
  const { setIsEditing, setLoading, setError, setValue, setHovered } = operations;

  const toggleIsEditing = () => setIsEditing((isEditing) => !isEditing);

  const onSubmit = (event: React.SyntheticEvent) => {
    event.preventDefault();

    if (!error && !validation(value)) {
      setLoading(true);
      onSave({ [valueKey]: value })
        .then(() => {
          setIsEditing(false);
        })
        .finally(() => {
          setLoading(false);
        });
    } else {
      setError(validation(value));
    }
  };

  const onChange = (event) => {
    setValue(event.target.value);
    setError(validation(event.target.value));
  };

  const onClear = () => setValue('');

  const screenProps = {
    ...models,
    setHovered,
    initialValue,
    typographyProps,
    placeholder,
    canEdit,
    toggleIsEditing,
    onSubmit,
    onChange,
    onClear,
  };

  return <EditableTextScreen {...screenProps} />;
};

export default EditableText;
