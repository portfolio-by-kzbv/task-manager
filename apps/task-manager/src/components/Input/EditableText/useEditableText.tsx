import { useState, useEffect } from 'react';

const useEditableText = initialValue => {
  const [loading, setLoading] = useState(false);
  const [isEditing, setIsEditing] = useState(false);
  const [hovered, setHovered] = useState(false);
  const [value, setValue] = useState(initialValue);
  const [error, setError] = useState('');

  useEffect(() => {
    setValue(initialValue);
  }, [initialValue]);

  const models = {
    loading,
    isEditing,
    hovered,
    value,
    error
  };

  const operations = {
    setLoading,
    setIsEditing,
    setHovered,
    setValue,
    setError
  };

  return { models, operations };
};

export default useEditableText;
