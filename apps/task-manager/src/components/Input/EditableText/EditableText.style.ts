import { makeStyles } from '@material-ui/core';

export default makeStyles({
  form: {
    display: 'flex',
    alignItems: 'flex-start',
    width: '80%'
  },
  button: {
    marginLeft: '8px',
    marginTop: '14px'
  },
  text: {
    display: 'flex',
    alignItems: 'center'
  },
  editButton: {
    marginLeft: '8px'
  }
});
