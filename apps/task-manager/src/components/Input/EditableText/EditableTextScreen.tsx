import React, { useMemo } from 'react';
import { TextField, Typography, IconButton, Fade, InputAdornment, CircularProgress } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';
import CloseIcon from '@material-ui/icons/Close';
import CheckOutlinedIcon from '@material-ui/icons/CheckOutlined';
import { isStringHtmlCode } from '../../../utils/general';

import useStyles from './EditableText.style';

type Props = {
  loading?: boolean;
  isEditing?: boolean;
  hovered?: boolean;
  value: string;
  error?: string;
  setHovered: (arg: boolean) => void;
  initialValue: string;
  typographyProps?: Record<string, unknown>;
  placeholder?: string;
  canEdit?: boolean;
  toggleIsEditing: () => void;
  onSubmit: (event: React.SyntheticEvent) => void;
  onChange: (event) => void;
  onClear: () => void;
};

const EditableTextScreen: React.FunctionComponent<Props> = ({
  loading = false,
  isEditing = false,
  hovered = false,
  value,
  error = '',
  setHovered,
  initialValue,
  typographyProps = {},
  placeholder = '',
  canEdit = false,
  toggleIsEditing,
  onSubmit,
  onChange,
  onClear,
}) => {
  const classes = useStyles();

  const isHtmlCode = useMemo(() => isStringHtmlCode(initialValue), [initialValue]);

  return !isHtmlCode && canEdit && isEditing ? (
    <form onSubmit={onSubmit} className={classes.form}>
      <TextField
        variant="outlined"
        margin="dense"
        value={value}
        fullWidth
        color="primary"
        onChange={onChange}
        error={Boolean(error)}
        helperText={error ? error : ''}
        placeholder={placeholder}
        InputProps={{
          endAdornment: loading ? (
            <InputAdornment position="end">
              <CircularProgress size={18} />
            </InputAdornment>
          ) : null,
        }}
      />
      <IconButton type="button" className={classes.button} onClick={onClear} size="small">
        <CloseIcon fontSize="small" />
      </IconButton>
      <IconButton type="submit" className={classes.button} size="small">
        <CheckOutlinedIcon fontSize="small" />
      </IconButton>
    </form>
  ) : isHtmlCode ? (
    <div dangerouslySetInnerHTML={{ __html: initialValue }} />
  ) : (
    <Typography
      onMouseOver={() => setHovered(true)}
      onMouseOut={() => setHovered(false)}
      className={classes.text}
      {...typographyProps}
    >
      {initialValue}
      <Fade in={canEdit && hovered}>
        <IconButton onClick={toggleIsEditing} size="small" className={classes.editButton}>
          <EditIcon fontSize="small" color="disabled" />
        </IconButton>
      </Fade>
    </Typography>
  );
};

export default EditableTextScreen;
