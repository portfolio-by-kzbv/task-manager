import React, { useMemo, useState } from 'react';
import { TextField } from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import UserInfo from './UserInfo';
import { User } from '@dar/api-interfaces';

import { useUsersState } from '../../selectors/users';
import { compareUsers } from '../../utils/user';

type Props = {
  onSelect: (val: User) => void;
  label?: string;
  error?: string;
  value?: undefined | User;
  margin: string;
};

const SelectUsers: React.FunctionComponent<Props> = ({ onSelect, label = '', error, value = null, margin = '' }) => {
  const allUsers = useUsersState();

  const [inputValue, setInputValue] = useState<string>('');

  const userOptions = useMemo(() => {
    return Object.values(allUsers)
      .sort(compareUsers)
      .map((user: User) => ({
        ...user,
      }));
  }, [allUsers]);

  const getOptionSelected = (option: User, value: User) => value && option.id === value.id;

  const getOptionLabel = (option: User) => {
    const user = userOptions.find((o) => o.id === option.id);
    return user ? user.displayName : '';
  };

  const onChange = (_, user: User) => {
    onSelect(user);
    // setInputValue('');
  };

  const onInputChange = (_, val: string) => {
    setInputValue(val);
  };

  const textFieldOptions = {
    margin: null,
  };
  if (margin) {
    textFieldOptions.margin = margin;
  }
  return (
    <Autocomplete
      fullWidth
      options={userOptions}
      groupBy={(option) => option.firstName[0]}
      getOptionLabel={getOptionLabel}
      value={value}
      inputValue={inputValue}
      onChange={onChange}
      onInputChange={onInputChange}
      getOptionSelected={getOptionSelected}
      renderInput={(params) => (
        <TextField
          {...params}
          {...textFieldOptions}
          variant="outlined"
          placeholder={label}
          error={Boolean(error)}
          helperText={error ? error : ''}
        />
      )}
      renderOption={(option) => <UserInfo user={option} />}
    />
  );
};

export default SelectUsers;
