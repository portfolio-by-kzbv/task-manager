import React, { useEffect, useState } from 'react';
import { Typography, makeStyles, IconButton, Fade } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

import { isObjectEmpty } from '../../utils/general';
import UserAvatar from '../UserAvatar/UserAvatar';
import NoUserPopover from './NoUserPopover';
import { User } from '@dar/api-interfaces';

const useStyles = makeStyles({
  root: {
    display: 'flex',
    alignItems: 'center',
  },
  avatar: {
    margin: '0 8px 0 0',
  },
  content: {
    display: 'flex',
    alignItems: 'center',
  },
  helpButton: {
    marginLeft: '8px',
  },
  deleteButton: {
    marginLeft: '16px',
  },
});

type Props = {
  user: User | undefined;
  onDelete?: () => void;
};

const UserInfo: React.FunctionComponent<Props> = ({ user, onDelete }) => {
  const [hovered, setHovered] = useState(false);

  const classes = useStyles();
  return (
    <div className={classes.root} onMouseOver={() => setHovered(true)} onMouseOut={() => setHovered(false)}>
      <UserAvatar user={user} className={classes.avatar} />
      <div className={classes.content}>
        <div>
          <Typography variant="h5">{user && user.displayName ? user.displayName : 'No fullname'}</Typography>
          <Typography variant="subtitle1" color="textSecondary">
            {user && user.jobTitle ? user.jobTitle : 'no user role'}
          </Typography>
        </div>
        {isObjectEmpty(user) && <NoUserPopover className={classes.helpButton} />}
      </div>
      {onDelete && (
        <Fade in={hovered}>
          <IconButton className={classes.deleteButton} size="small" onClick={onDelete}>
            <CloseIcon fontSize="small" />
          </IconButton>
        </Fade>
      )}
    </div>
  );
};

export default UserInfo;
