import React, { useState } from 'react';
import { makeStyles, Fade, IconButton } from '@material-ui/core';
import EditIcon from '@material-ui/icons/Edit';

import SelectUsers from './SelectUsers';
import UserInfo from './UserInfo';
import Spinner from '../Spinner/Spinner';
import { User } from '@dar/api-interfaces';

const useStyles = makeStyles({
  root: {
    display: 'flex',
    alignItems: 'center',
    position: 'relative',
  },
  editButton: {
    marginLeft: '16px',
  },
});

type Props = {
  user: User;
  canEdit: boolean;
  label: string;
  onEdit: (user: User) => Promise<string>;
};

const EditableUser: React.FunctionComponent<Props> = ({ user, canEdit, label, onEdit }) => {
  const [hovered, setHovered] = useState(false);
  const [editing, setEditing] = useState(false);
  const [loading, setLoading] = useState(false);

  const classes = useStyles();

  const onSelectUser = (user: User) => {
    setLoading(true);
    onEdit(user).finally(() => {
      setLoading(false);
      setEditing(false);
    });
  };

  return editing ? (
    <div className={classes.root}>
      <SelectUsers onSelect={onSelectUser} label={label} margin="normal" />
      {loading && <Spinner absolute={true} size={20} />}
    </div>
  ) : (
    <div className={classes.root} onMouseOver={() => setHovered(true)} onMouseOut={() => setHovered(false)}>
      <UserInfo user={user} />
      {canEdit && (
        <Fade in={hovered}>
          <IconButton className={classes.editButton} size="small" onClick={() => setEditing(true)}>
            <EditIcon fontSize="small" />
          </IconButton>
        </Fade>
      )}
    </div>
  );
};

export default EditableUser;
