import React, { useEffect } from 'react';
import { Prompt } from 'react-router-dom';

const PREVENT_RELOAD_MESSAGE =
  'It looks like you have been editing something. If you leave before saving, your changes will be lost.';
const PREVENT_RELOCATE_MESSAGE = `${PREVENT_RELOAD_MESSAGE} Leave the page?`;

type Props = {
  isBlocked: boolean;
};

const CustomPrompt: React.FunctionComponent<Props> = ({
  isBlocked = false,
}) => {
  useEffect(() => {
    const callback = (e) => {
      if (!isBlocked) return undefined;

      const confirmationMessage = PREVENT_RELOAD_MESSAGE;
      const event = e || window.event;

      event.returnValue = confirmationMessage; //Gecko + IE
      return confirmationMessage; //Gecko + Webkit, Safari, Chrome etc.
    };

    window.addEventListener('beforeunload', callback);

    return () => window.removeEventListener('beforeunload', callback);
  }, [isBlocked]);

  return <Prompt when={!!isBlocked} message={PREVENT_RELOCATE_MESSAGE} />;
};

export default CustomPrompt;
