import React from 'react';
import clsx from 'clsx';

import {
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  Typography,
  DialogActions,
  Button,
  makeStyles,
} from '@material-ui/core';

const usePromptModalStyles = makeStyles(theme => ({
  backdrop: {
    zIndex: -1
  },
  dialogPaper: {
    width: '500px'
  },
  dialogTitle: {
    padding: theme.spacing(6),
    color: theme.palette.grey[50],
  },
  dialogContent: {
    padding: theme.spacing(3, 6),
    color: theme.palette.grey[50],
  },
  promptDefault: {
    backgroundColor: theme.palette.grey[50],
    color: theme.palette.grey[800],
  },
  promptPrimary: {
    backgroundColor: theme.palette.primary[700],
    color: theme.palette.grey[50],
  },
  promptDanger: {
    backgroundColor: theme.palette.error[600],
    color: theme.palette.grey[50],
  },
  dialogActions: {
    padding: theme.spacing(4, 6),
  }
}));

type Props = {
  open?: boolean;
  title: string;
  text?: string;
  propmtType?: 'default' | 'primary' | 'danger';
  onClose?: () => void;
  onCancel?: () => void;
  onConfirm: () => void;
  cancelText?: string;
  confirmText?: string;
  cancelColor?: 'inherit' | 'default' | 'primary' | 'secondary';
  confirmColor?: 'inherit' | 'default' | 'primary' | 'secondary';
};

const PromptModal: React.FunctionComponent<Props> = ({
  open = false,
  title,
  text,
  propmtType = 'default',
  onClose,
  onCancel,
  onConfirm,
  cancelText = 'Cancel',
  confirmText = 'Confirm',
  cancelColor = 'primary',
  confirmColor = 'primary',
}) => {
  const classes = usePromptModalStyles();

  return (
    <Dialog open={open} 
      aria-labelledby="delete-task" 
      PaperProps={{ className: classes.dialogPaper  }}
      BackdropProps={{
          className:  classes.backdrop
      }}>
      <DialogTitle 
        className={clsx(classes.dialogTitle, {
          [classes.promptDefault]: propmtType === 'default',
          [classes.promptPrimary]: propmtType === 'primary',
          [classes.promptDanger]: propmtType === 'danger',
        })}
        id="dialog-title">
          {title}
        </DialogTitle>
      <DialogContent 
        className={clsx(classes.dialogContent, {
          [classes.promptDefault]: propmtType === 'default',
          [classes.promptPrimary]: propmtType === 'primary',
          [classes.promptDanger]: propmtType === 'danger',
        })}>
        <DialogContentText id="dialog-description" style={{ color: (propmtType === 'default') ? '#101F2B' : '#FFF' }}>
          {text}
        </DialogContentText>
      </DialogContent>
      <DialogActions className={classes.dialogActions}>
        <Button onClick={onCancel} color={cancelColor} variant="contained" size="large">
          {cancelText}
        </Button>
        <Button onClick={onConfirm} color={confirmColor} variant="contained" size="large">
          {confirmText}
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default PromptModal;
