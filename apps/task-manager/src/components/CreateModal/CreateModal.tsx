import React from 'react';
import { Typography, IconButton, Button } from '@material-ui/core';
import CloseIcon from '@material-ui/icons/Close';

import { useCreateModalStyles } from './createModal.styles';
import SideModal from '@dar/components/SideModal/SideModal';
import Spinner from '@dar/components/Spinner/Spinner';

type Props = {
  openDrawer: boolean;
  onClose: () => void;
  onSubmit: (event: React.SyntheticEvent) => void;
  loading: boolean;
  title: string;
  zIndex?: number;
  buttonName: string;
};

const CreateModal: React.FunctionComponent<Props> = ({
  children,
  openDrawer,
  onClose,
  onSubmit,
  loading,
  title,
  zIndex,
  buttonName,
}) => {
  const classes = useCreateModalStyles();

  return (
    <SideModal openDrawer={openDrawer} onClose={onClose} size="small" zIndex={zIndex}>
      {loading && <Spinner absolute />}
      <div className={classes.header}>
        <Typography variant="h3" className={classes.headerText}>
          {title}
        </Typography>
        <IconButton onClick={onClose} aria-label="close" color="default" className={classes.iconButton}>
          <CloseIcon />
        </IconButton>
      </div>
      <form onSubmit={onSubmit} className={classes.form}>
        {children}
        <div className={classes.footer}>
          <Button variant="outlined" color="secondary" size="large" disabled={loading} onClick={onClose}>
            Cancel
          </Button>
          <Button variant="contained" color="primary" size="large" type="submit" disabled={loading}>
            {buttonName}
          </Button>
        </div>
      </form>
    </SideModal>
  );
};

export default CreateModal;
