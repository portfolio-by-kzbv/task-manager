import { makeStyles } from '@material-ui/core';

export const useCreateModalStyles = makeStyles((theme) => ({
  header: {
    background: '#387E7F',
    display: 'flex',
    justifyContent: 'space-between',
    padding: '14px 24px',
    width: '100%',
    alignItems: 'center',
  },
  container: {
    padding: '24px',
    height: 'calc(100% - 128px)',
    position: 'relative',
  },
  block: {
    width: '100%',
    marginBottom: '20px',
    display: 'flex',
    justifyContent: 'space-between',
  },
  form: {
    display: 'flex',
    flexDirection: 'column',
    flex: 1,
  },
  footer: {
    background: '#F7F8FA',
    padding: '12px 24px',
    display: 'flex',
    justifyContent: 'flex-end',
    width: '100%',
    '& > button': {
      marginLeft: '12px',
    },
  },
  headerText: {
    color: '#FFF',
  },
  box: {
    flex: 1,
  },
  iconButton: {
    background: 'transparent !important',
    border: 'none !important',
    color: '#FFF !important',
    '& > svg': { fill: 'inherit' },
  },
}));
