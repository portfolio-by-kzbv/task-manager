import React from 'react';
import { ReactSVG } from 'react-svg';
import { environment } from '../../environments/environment';

interface Props {
  icon: string;
  className?: string;
  size?: string;
  display?: string;
  fill?: string;
  onClick?: () => void;
}

const CustomIcon: React.FunctionComponent<Props> = ({
  icon,
  className,
  size = '25px',
  display = 'inline-block',
  fill,
  onClick,
}) => {
  return (
    <ReactSVG
      className={className}
      src={`${environment.assetsBase}/svg/${icon}.svg`}
      onClick={onClick}
      beforeInjection={(svg) => {
        const paths = svg.querySelectorAll('path');
        paths.forEach((path) => (path.style.fill = fill));
        svg.classList.add('svg-class-name');
        svg.setAttribute(
          'style',
          `width: ${size}; height: ${size}; fill: ${fill};
            display:${display}; vertical-align: middle`
        );
      }}
    />
  );
};

export default CustomIcon;
