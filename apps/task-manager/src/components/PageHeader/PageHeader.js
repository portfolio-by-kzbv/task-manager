import React from 'react';
import PropTypes from 'prop-types';
import { Typography, makeStyles } from '@material-ui/core';

const useStyles = makeStyles(theme => ({
  root: {
    paddingTop: theme.spacing(2),
    height: '50px',
    display: 'flex',
    alignItems: 'center'
  }
}));

function PageHeader({ title }) {
  const classes = useStyles();
  return (
    <Typography variant="h1" classes={classes}>
      {title}
    </Typography>
  );
}

PageHeader.propTypes = {
  title: PropTypes.string.isRequired
};

export default PageHeader;
