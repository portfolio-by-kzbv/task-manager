import React from 'react';
import {
  Box,
  FormControl,
  Select,
  InputLabel,
} from '@material-ui/core';
import { filterStyles } from './selects.styles';

type Props = {
    value: string | number | number[];
    label?: string;
    defaultValue?: string;
    variant?: "filled" | "outlined" | "standard" | undefined;
    onChange: (event: any) => void;
    minWidth?: number;
    isFilter?: boolean;
    multiselect?: boolean;
    renderValue?: (value: unknown) => React.ReactNode;
}

const TemplateFilter: React.FunctionComponent<Props> =({ children, label, value, defaultValue, onChange, variant, minWidth, isFilter, multiselect, renderValue }) => {
  const classes = filterStyles();

  return (
      <Box display="flex" alignItems="center" justifyContent="flex-end">
        <FormControl className={classes.formControl}>
          <InputLabel htmlFor={'select-'+value}>{label}</InputLabel>
          <Select
            variant={variant}
            multiple={multiselect}
            label={label}
            value={value}
            defaultValue={defaultValue}
            onChange={onChange}
            inputProps={{
              name: label,
              id: 'select-'+label,
              className: classes.selectInput,
            }}
            MenuProps={{
              getContentAnchorEl: null,
              className: classes.selectMenu,
              anchorOrigin: {
                vertical: "bottom",
                horizontal: "left",
              }
            }}
            className={classes.select}
          displayEmpty={isFilter}
          renderValue={renderValue}
            style={{ minWidth: minWidth ? minWidth : 'fit-content' }}
          >
            {children}
          </Select>
        </FormControl>
      </Box>
  );
}

export default TemplateFilter;
