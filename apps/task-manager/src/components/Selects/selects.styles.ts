import { makeStyles } from '@material-ui/core';

export const filterStyles = makeStyles((theme) => ({
  menuItem: {
    paddingLeft: '16px',
    paddingRight: '16px',
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 120,
    '&:hover, &:active, &:focus': {
      border: 'none !important',
      outlineColor: 'transparent !important',
    },
  },
  select: {
    color: theme.palette.primary[200],
  },
  selectInput: {
    padding: '0',
    transition: 'none',
    display: 'flex',
    alignItems: 'center',
  },
  selectMenu: {
    marginTop: '12px',
    boxShadow: '0px 3px 6px rgba(0, 0, 0, 0.14)',
    borderRadius: '4px',
  },
  listItemIcon: {
    minWidth: 'fit-content',
    paddingRight: 8,
  },
}));

export const assigneeSelectStyles = makeStyles((theme) => ({
  selectMenu: {
    marginTop: '12px',
    boxShadow: '0px 3px 6px rgba(0, 0, 0, 0.14)',
    borderRadius: '4px',
    '& > ul': { paddingTop: '0' },
  },
  avatar: {
    margin: '0 6px',
    width: theme.spacing(8),
    height: theme.spacing(8),
    '&.MuiAvatar-colorDefault': {
      background: 'linear-gradient(180deg, #0ca9ea 0%, #59c7f4 97.92%)',
      color: '#fff',
    },
  },
  searchInput: {
    display: 'block',
    textAlign: 'center',
    padding: '11px 0',
    backgroundColor: theme.palette.grey[100],
  },
  inputRoot: {
    minWidth: 180,
    padding: '12px',
    border: 'none',
    '&:hover': { border: 'none' },
    '&:focus': { border: 'none', borderBottom: '1px solid red' }
  },
  wrapper: {
    width: '100%',
    display: 'flex',
    flexDirection: 'column',
    alignItems: 'stretch',
    justifyContent: 'center',
  },
  root: {
    width: '100%',
    fontSize: 16,
  },
  popper: {
    border: '1px solid #E0E3EA',
    boxShadow: '0px 4px 10px rgba(0, 0, 0, 0.15), 0px 2px 4px rgba(0, 0, 0, 0.2)',
    borderRadius: 4,
    width: 300,
    zIndex: 5000,
    display: 'flex',
    backgroundColor: theme.palette.grey[100],
  },
  outlinedPopper: {
    width: 436,
  },
  inputBase: {
    width: '100%',
    borderRadius: 4,
    border: '2px solid #ced7df',
    '& input': {
      padding: '10px 12px',
      transition: theme.transitions.create(['border-color', 'box-shadow']),
      fontSize: 14,
      '&:focus': {
        borderColor: theme.palette.primary[200],
      },
    },
  },
  inputAdorement: {
    margin: 0,
  },
  clearInputButton: {
    padding: '12px 14.5px 12px 12px',
  },
  text: {
    color: theme.palette.primary[200],
  },
  outlinedButton: {
    width: '100%',
    textAlign: 'left',
    border: '1px solid #CED7DF',
    borderRadius: 4,
    alignItems: 'center',
    padding: '10px 8px',
    display: 'inline-flex',
    justifyContent: 'space-between',
    '&:hover': { border: `1px solid ${theme.palette.primary[200]}` },
  },
  error: {
    fontSize: 12,
    color: theme.palette.error[600],
  },
}));
