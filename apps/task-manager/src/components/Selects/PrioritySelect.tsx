import React from 'react';
import { MenuItem, ListItemIcon } from '@material-ui/core';
import FlagIcon from '@material-ui/icons/Flag';
import { priorities } from '../../mocks/priorities';
import TemplateFilter from './TemplateFilter';
import { filterStyles } from './selects.styles';

type ChildProps = {
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  value: string;
  label?: string;
  defaultValue?: string;
  variant?: 'filled' | 'outlined' | 'standard' | undefined;
  minWidth?: number;
};

const PrioritySelect: React.FC<ChildProps> = ({ onChange, value, defaultValue, label, variant, minWidth }) => {
  const classes = filterStyles();
  return (
    <TemplateFilter
      value={value ? value.toLowerCase() : ''}
      onChange={onChange}
      label={label}
      variant={variant}
      defaultValue={defaultValue}
      minWidth={minWidth}
    >
      {priorities
        .filter((option) => option.name)
        .map((option) => (
          <MenuItem key={option.id} value={option.name.toLowerCase()} className={classes.menuItem}>
            {option.color && (
              <ListItemIcon className={classes.listItemIcon}>
                <FlagIcon style={{ fill: option.color }} />
              </ListItemIcon>
            )}
            {option.name}
          </MenuItem>
        ))}
    </TemplateFilter>
  );
};

export default PrioritySelect;
