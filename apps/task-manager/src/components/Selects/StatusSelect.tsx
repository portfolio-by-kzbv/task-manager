import React, { useState, useEffect, useMemo } from 'react';
import { useDispatch } from 'react-redux';
import { MenuItem, ListItemIcon, Checkbox } from '@material-ui/core';
import FiberManualRecordIcon from '@material-ui/icons/FiberManualRecord';
import Spinner from '../Spinner/Spinner';
import TemplateFilter from './TemplateFilter';
import { filterStyles } from './selects.styles';
import { addTaskStatusColor } from '../../utils/task';
import { useStatusesState } from '../../selectors/task-status.selector';
import { TaskStatus } from '@dar/api-interfaces';
import { getStatusTransitions } from '../../utils/status';

type ChildProps = {
  onChange: (event: React.ChangeEvent<HTMLInputElement>) => void;
  value: number | number[];
  defaultValue?: string;
  label?: string;
  variant?: 'filled' | 'outlined' | 'standard' | undefined;
  minWidth?: number;
  isFilter?: boolean;
  currentStatus?: string;
};

const StatusSelect: React.FC<ChildProps> = ({
  onChange,
  value,
  defaultValue,
  variant,
  label,
  minWidth,
  isFilter,
  currentStatus,
}) => {
  const classes = filterStyles();

  const [loading, setLoading] = useState(false);
  const [statusList, setStatusList] = useState<TaskStatus[]>([]);
  const { statuses } = useStatusesState();

  const filteredStatuses = useMemo(() => {
    return getStatusTransitions(currentStatus, statusList);
  }, [statusList, currentStatus]);

  useEffect(() => {
    const updStatuses: TaskStatus[] = addTaskStatusColor(statuses);
    setStatusList(updStatuses);
    setLoading(false);
  }, [statuses]);

  return (
    <>
      {loading && <Spinner absolute />}
      <TemplateFilter
        value={value}
        onChange={onChange}
        label={label}
        defaultValue={defaultValue}
        variant={variant}
        minWidth={minWidth}
        isFilter
        multiselect={isFilter}
        renderValue={
          isFilter
            ? () => {
                const valuesLength = (value as number[]).length;
                return `Statuses${valuesLength ? ` (${valuesLength})` : ''}`;
              }
            : null
        }
      >
        {filteredStatuses.length
          ? filteredStatuses.map((option: TaskStatus) => (
              <MenuItem key={option.id} value={option.id} className={classes.menuItem}>
                {isFilter && <Checkbox checked={Boolean((value as number[]).find((v) => v === option.id))} />}
                <ListItemIcon className={classes.listItemIcon}>
                  <FiberManualRecordIcon style={{ fill: option.color }} />
                </ListItemIcon>
                {option.name}
              </MenuItem>
            ))
          : null}
      </TemplateFilter>
    </>
  );
};

export default StatusSelect;
