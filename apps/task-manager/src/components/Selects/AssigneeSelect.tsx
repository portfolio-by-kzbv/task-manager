import React, { useMemo, useRef, useState } from 'react';
import cx from 'classnames';
import {
  Box,
  Avatar,
  ClickAwayListener,
  InputBase,
  Popper,
  ButtonBase,
  Typography,
  IconButton,
  InputAdornment,
  InputLabel,
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';

import { useUsersState } from '../../selectors/users';
import { compareUsers } from '../../utils/user';
import { assigneeSelectStyles } from './selects.styles';
import { User } from '@dar/api-interfaces';
import ClearIcon from '@material-ui/icons/Clear';
import CustomPopper from './CustomPopper';

type Props = {
  onSelect: (value: User) => void;
  value: User;
  label?: string;
  isRequired?: boolean;
  variant?: 'text' | 'outlined';
  error?: string;
  prompt?: string;
  resetOnClear?: boolean;
};

const AssigneeSelect: React.FunctionComponent<Props> = ({
  onSelect,
  value,
  label,
  isRequired,
  variant,
  error,
  prompt,
  resetOnClear,
}) => {
  const classes = assigneeSelectStyles();
  const allUsers = useUsersState();
  const [inputValue, setInputValue] = useState('');

  const userOptions = useMemo(() => {
    return Object.values(allUsers)
      .sort(compareUsers)
      .map((user: User) => ({
        ...user,
      }));
  }, [allUsers]);

  const getOptionSelected = (option: any, value: any) => value && option.id === value.id;

  const onChange = (event: any, val: any, reason: string) => {
    console.log(val);
    onSelect(val);
    if (reason === 'select-option') {
      setInputValue(val.displayName);
      setAnchorEl(null);
    }
    if (reason === 'remove-option') {
      setInputValue('');
    }
  };

  const onInputChange = (__: any, val: any) => {
    setInputValue(val);
  };
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);

  const open = Boolean(anchorEl);
  const popperRef = useRef(null);

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  return (
    <div className={classes.wrapper}>
      {label && <InputLabel required={isRequired}>{label}</InputLabel>}
      <ButtonBase
        disableRipple
        onClick={handleClick}
        className={cx({ [classes.outlinedButton]: variant === 'outlined' })}
      >
        <Box display="flex" alignItems="center">
          {value ? (
            <React.Fragment>
              <Avatar alt={value?.displayName} src={value?.photoUrl} className={classes.avatar} />
              <Typography variant="h6" className={classes.text}>
                {value.displayName}
              </Typography>
            </React.Fragment>
          ) : (
            <Typography variant="h6" className={classes.text}>
              {prompt ? prompt : 'Select User'}
            </Typography>
          )}
        </Box>
        <ArrowDropDownIcon />
      </ButtonBase>
      <Popper
        ref={popperRef}
        open={open}
        anchorEl={anchorEl}
        placement="bottom-start"
        disablePortal
        className={cx(classes.popper, {
          [classes.outlinedPopper]: variant === 'outlined',
        })}
      >
        <ClickAwayListener onClickAway={() => setAnchorEl(null)}>
          <Autocomplete
            fullWidth
            options={userOptions}
            getOptionLabel={(option) => option?.displayName || ''}
            value={value}
            inputValue={inputValue}
            onChange={onChange}
            onInputChange={onInputChange}
            getOptionSelected={getOptionSelected}
            noOptionsText="Nothing found"
            className={classes.inputRoot}
            renderInput={(params) => (
              <InputBase
                disabled={false}
                placeholder="Search"
                ref={params.InputProps.ref}
                inputProps={params.inputProps}
                autoFocus
                className={classes.inputBase}
                endAdornment={
                  <InputAdornment position="end" className={classes.inputAdorement}>
                    <IconButton
                      size="small"
                      className={classes.clearInputButton}
                      onClick={() => {
                        setInputValue('');
                        if (resetOnClear) {
                          onSelect(null);
                        }
                      }}
                    >
                      <ClearIcon fontSize="inherit" />
                    </IconButton>
                  </InputAdornment>
                }
              />
            )}
            renderOption={(option) => (
              <div style={{ display: 'flex' }}>
                <Avatar alt={option.displayName} src={option.photoUrl} className={classes.avatar}></Avatar>
                <Box>
                  {option.displayName}
                  <br />
                  {option.jobTitle}
                </Box>
              </div>
            )}
            PopperComponent={(props) => <CustomPopper {...props} popperRef={popperRef} />}
          />
        </ClickAwayListener>
      </Popper>
      <div className={classes.error}>{error ? error : ''}</div>
    </div>
  );
};

export default AssigneeSelect;
