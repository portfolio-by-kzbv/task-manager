import React, { useMemo, useState, ReactNode, useRef, PropsWithChildren } from 'react';
import cx from 'classnames';
import {
  Box,
  ClickAwayListener,
  InputBase,
  Popper,
  ButtonBase,
  Typography,
  InputLabel,
  IconButton,
  InputAdornment,
} from '@material-ui/core';
import Autocomplete from '@material-ui/lab/Autocomplete';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import ClearIcon from '@material-ui/icons/Clear';

import { assigneeSelectStyles } from './selects.styles';
import CustomPopper from './CustomPopper';

interface Props<ObjectType> {
  onSelect: (value: ObjectType) => void;
  value: ObjectType;
  valueId: keyof ObjectType;
  valueName: keyof ObjectType;
  variant?: 'text' | 'outlined';
  error?: string;
  prompt: string;
  label?: string;
  options: ObjectType[];
  renderOption?: (option: ObjectType) => ReactNode;
  renderSelected?: (value: ObjectType) => ReactNode;
}

const AutoCompleteSelect = <ObjectType,>({
  onSelect,
  value,
  valueId,
  valueName,
  variant,
  error,
  prompt,
  label,
  options = [],
  renderOption,
  renderSelected,
}: PropsWithChildren<Props<ObjectType>>) => {
  const classes = assigneeSelectStyles();
  const [inputValue, setInputValue] = useState('');
  const [anchorEl, setAnchorEl] = useState<null | HTMLElement>(null);
  const open = Boolean(anchorEl);
  const popperRef = useRef(null);

  const memoOptions = useMemo(() => {
    return Object.values(options).map((option) => ({
      ...option,
    }));
  }, [options]);

  const getOptionSelected = (option: ObjectType, value: ObjectType) => value && option[valueId] === value[valueId];

  const onChange = (event: React.ChangeEvent, val: ObjectType) => {
    onSelect(val);
    if (value) {
      setInputValue(`${value[valueName]}`);
      setAnchorEl(null);
    } else {
      setInputValue('');
    }
  };

  const onInputChange = (__: unknown, val: string) => {
    setInputValue(val);
  };

  const handleClick = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorEl(event.currentTarget);
  };

  return (
    <div className={classes.wrapper}>
      {label && <InputLabel>{label}</InputLabel>}
      <ButtonBase
        disableRipple
        onClick={handleClick}
        className={cx({ [classes.outlinedButton]: variant === 'outlined' })}
      >
        <Box display="flex" alignItems="center">
          {value ? (
            renderSelected ? (
              renderSelected(value)
            ) : (
              <Typography variant="h6" className={classes.text}>
                {value[valueName]}
              </Typography>
            )
          ) : (
            <Typography variant="h6" className={classes.text}>
              {prompt}
            </Typography>
          )}
        </Box>
        <ArrowDropDownIcon />
      </ButtonBase>
      <Popper
        ref={popperRef}
        open={open}
        anchorEl={anchorEl}
        placement="bottom-start"
        disablePortal
        className={cx(classes.popper, { [classes.outlinedPopper]: variant === 'outlined' })}
      >
        <ClickAwayListener onClickAway={() => setAnchorEl(null)}>
          <Autocomplete<ObjectType, undefined, false, undefined>
            fullWidth
            options={memoOptions}
            getOptionLabel={(option) => (option?.[valueName] ? option[valueName] + '' : '')}
            value={value}
            inputValue={inputValue}
            onChange={onChange}
            onInputChange={onInputChange}
            getOptionSelected={getOptionSelected}
            noOptionsText="Nothing found"
            className={classes.inputRoot}
            renderInput={(params) => (
              <InputBase
                disabled={false}
                placeholder="Search"
                ref={params.InputProps.ref}
                inputProps={params.inputProps}
                autoFocus
                className={classes.inputBase}
                endAdornment={
                  <InputAdornment position="end" className={classes.inputAdorement}>
                    <IconButton
                      size="small"
                      className={classes.clearInputButton}
                      onClick={() => {
                        setInputValue('');
                      }}
                    >
                      <ClearIcon fontSize="inherit" />
                    </IconButton>
                  </InputAdornment>
                }
              />
            )}
            renderOption={renderOption}
            PopperComponent={(props) => <CustomPopper {...props} popperRef={popperRef} />}
          />
        </ClickAwayListener>
      </Popper>
      <div className={classes.error}>{error ? error : ''}</div>
    </div>
  );
};

export default AutoCompleteSelect;
