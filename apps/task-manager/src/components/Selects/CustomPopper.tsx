import { Popper, PopperProps } from '@material-ui/core';
import React, { MutableRefObject } from 'react';

const CustomPopper: React.FC<PopperProps & { popperRef: MutableRefObject<HTMLElement> }> = ({
  popperRef,
  ...otherProps
}) => {
  return (
    <Popper
      {...otherProps}
      style={{ width: `${popperRef && popperRef.current ? popperRef.current.offsetWidth : 436}px` }}
    />
  );
};

export default CustomPopper;
