import React, { useState } from 'react';
import { makeStyles, Grid } from '@material-ui/core';

import UserInfo from '../../Users/UserInfo';
import Spinner from '../../../components/Spinner/Spinner';
import SelectUsers from '../../../components/Users/SelectUsers';

const useStyles = makeStyles({
  grid: {
    position: 'relative',
  },
  editButton: {
    marginLeft: '8px',
  },
  chip: {
    marginRight: '8px',
    marginTop: '8px',
  },
  button: {
    marginTop: '8px',
  },
  usersList: {
    width: '100%',
    display: 'flex',
  },
});

type Props = {
  users;
  canEdit;
  addParticipant;
  deleteParticipant;
  owner;
};

const Participants: React.FunctionComponent<Props> = ({
  users,
  canEdit,
  addParticipant,
  deleteParticipant,
  owner,
}) => {
  const [loading, setLoading] = useState(false);

  const classes = useStyles();

  const onDeleteUser = (deletingUser) => {
    setLoading(true);
    deleteParticipant(deletingUser.workEmail.toLowerCase()).finally(() => {
      setLoading(false);
    });
  };

  const onAddUser = (user) => {
    if (user) {
      setLoading(true);
      addParticipant(user.workEmail.toLowerCase()).finally(() => {
        setLoading(false);
      });
    }
  };

  return (
    <Grid container spacing={1} alignItems="center" className={classes.grid}>
      {loading && <Spinner absolute={true} />}
      {users.map((user) => (
        <Grid item key={user.id} md={4} sm={6} xs={12}>
          <UserInfo
            user={user}
            onDelete={
              canEdit && user.id !== owner.id ? () => onDeleteUser(user) : null
            }
          />
        </Grid>
      ))}
      {canEdit && !loading && (
        <Grid item md={4} sm={6} xs={12}>
          <SelectUsers
            onSelect={onAddUser}
            label="Add member"
            margin="normal"
          />
        </Grid>
      )}
    </Grid>
  );
};

export default Participants;
