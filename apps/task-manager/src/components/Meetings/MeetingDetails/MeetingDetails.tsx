import React, { useState, useMemo, useEffect } from 'react';
import {
  Dialog,
  DialogContent,
  Typography,
  Grid,
  DialogActions,
  Tabs,
  Tab,
  IconButton,
  Box,
  Tooltip,
} from '@material-ui/core';
import { Link } from 'react-router-dom';
import copy from 'copy-to-clipboard';
import FileCopyOutlinedIcon from '@material-ui/icons/FileCopyOutlined';

import { DialogTitle } from './DialogTitle';
import ExternalParticipants from './ExternalParticipants';
import MeetingActionButtons from '../MeetingActionButtons/MeetingActionButtons';
import EditableText from '@dar/components/Input/EditableText/EditableText';
import EditableDate from '@dar/components/Input/EditableDate/EditableDate';
import EditableTime from '@dar/components/Input/EditableTime/EditableTime';
import UserInfo from '@dar/components/Users/UserInfo';
import Participants from './Participants';
import TabPanel from '@dar/components/TabPanel/TabPanel';
import useMeetingDetails from './useMeetingDetails';
import { useStyles } from './meetDetails.styles';
import MeetingMoM from '@dar/components/Jitsi/MeetingMoM/MeetingMoM';
import MeetingNotes from '@dar/components/Jitsi/MeetingNotes/MeetingNotes';
import { Meeting, MeetingSettings, User } from '@dar/api-interfaces';
import SelectUsers from '@dar/components/Users/SelectUsers';
import { isObjectEmpty } from '../../../utils/general';
import { useUsersState } from '@dar/selectors/users';
import meetings from '@dar/services/meetings';
import { toast } from '@dartech/dms-ui';

type Props = {
  onClose: () => void;
  open: boolean;
  meeting: Meeting;
  onDelete?: () => void;
};

const MeetingDetails: React.FunctionComponent<Props> = ({ onClose, open, meeting, onDelete }) => {
  const {
    owner,
    participants,
    externalParticipants,
    canEdit,
    canDelete,
    isOld,
    titleValidation,
    descriptionValidation,
    setDate,
    setStartTime,
    setEndTime,
    addParticipant,
    deleteParticipant,
    onEdit,
  } = useMeetingDetails(meeting);
  const classes = useStyles();
  const allUsers = useUsersState();
  const [tab, setTab] = useState(0);
  const [tooltipText, setTooltipText] = useState('Copy link');
  const [secretaryError, setSecretaryError] = useState(false);
  const [secretary, setSecretary] = useState<User | undefined>();
  const [settings, setSettings] = useState<MeetingSettings>();

  useEffect(() => {
    if (!open) {
      return;
    }
    meetings
      .getSettings(meeting.id)
      .then((res) => {
        if (res) {
          setSettings(res);
        }
        const secretar =
          allUsers.find((user) => user.id === res?.secretary_id) ||
          allUsers.find((user) => user.workEmail?.toLowerCase() === meeting.ownerEmail?.toLowerCase());
        setSecretary(secretar);
      })
      .catch((err) => {
        console.error(err.message);
      });
  }, [open, meeting, allUsers]);

  const updateSecretary = (secretary) => {
    if (meeting && secretary) {
      meetings
        .updateSettings(meeting.id, { ...(settings ?? {}), meeting_id: meeting.id, secretary_id: secretary.id })
        .then(() => {
          toast.success('Changes were saved', { duration: 2000 });
        });
    }
  };

  const meetingLink = useMemo(() => `/productivity/meetings?id=${meeting.id}`, [meeting]);

  const handleTabChange = (_, newTab) => {
    setTab(newTab);
  };

  const onCopyClick = () => {
    copy(window.location.origin + meetingLink);
    setTooltipText('Copied!');

    setTimeout(() => setTooltipText('Copy link'), 1000);
  };

  return (
    <Dialog
      open={open}
      onClose={onClose}
      fullWidth
      maxWidth="md"
      scroll="paper"
      PaperProps={{ style: { zIndex: 1210 } }}
    >
      <DialogTitle
        onClose={() => {
          if (canEdit && isObjectEmpty(secretary)) {
            setSecretaryError(true);
          } else {
            onClose();
          }
        }}
        onEdit={onEdit}
        validation={titleValidation}
        title={meeting.title}
        canEdit={canEdit}
      />
      <DialogContent className={classes.content}>
        <Tabs value={tab} indicatorColor="primary" textColor="primary" onChange={handleTabChange}>
          <Tab label="Details" />
          <Tab label="MoM" />
          <Tab label="Notes" />
        </Tabs>
        <TabPanel value={tab} index={0} className={classes.tabPanel}>
          <>
            <Typography variant="h5" gutterBottom>
              Description
            </Typography>
            <EditableText
              canEdit={canEdit}
              valueKey="description"
              initialValue={meeting.description}
              placeholder="Description"
              validation={descriptionValidation}
              onSave={onEdit}
            />

            <div className={classes.dateSection}>
              <div>
                <Typography variant="h5" gutterBottom>
                  Date
                </Typography>
                <EditableDate canEdit={canEdit} initialValue={meeting.startTime} onSave={setDate} />
              </div>
              <div>
                <Typography variant="h5" gutterBottom>
                  Start time
                </Typography>
                <EditableTime canEdit={canEdit} initialValue={new Date(meeting.startTime)} onSave={setStartTime} />
              </div>
              <div>
                <Typography variant="h5" gutterBottom>
                  End time
                </Typography>
                <EditableTime canEdit={canEdit} initialValue={new Date(meeting.endTime)} onSave={setEndTime} />
              </div>
            </div>

            <div className={classes.section}>
              <Typography variant="h5" gutterBottom>
                Meeting link
              </Typography>
              <Box display="flex" alignItems="center">
                <Box mr={1}>
                  <Link target="_blank" to={meetingLink}>
                    {window.location.origin + meetingLink}
                  </Link>
                </Box>
                <Tooltip title={tooltipText}>
                  <IconButton onClick={onCopyClick} size="small">
                    <FileCopyOutlinedIcon fontSize="small" />
                  </IconButton>
                </Tooltip>
              </Box>
            </div>

            <div className={classes.section}>
              <Typography variant="h5" gutterBottom>
                Members
              </Typography>
              <Participants
                users={participants}
                owner={owner}
                canEdit={canEdit}
                addParticipant={addParticipant}
                deleteParticipant={deleteParticipant}
              />
            </div>

            <div className={classes.section}>
              <Typography variant="h5" gutterBottom>
                External participants
              </Typography>
              <ExternalParticipants
                addExternalParticipant={addParticipant}
                deleteExternalParticipant={deleteParticipant}
                participants={externalParticipants}
                canEdit={canEdit}
              />
            </div>

            <Grid container spacing={1} className={classes.section}>
              <Grid item md={4} sm={6} xs={12}>
                <Typography variant="h5" gutterBottom>
                  Created by
                </Typography>
                <div style={{ marginTop: '16px' }}>
                  <UserInfo user={owner} />
                </div>
              </Grid>

              <Grid item md={4} sm={6} xs={12}>
                <Typography variant="h5" gutterBottom>
                  Secretary
                </Typography>
                {canEdit ? (
                  <SelectUsers
                    value={secretary}
                    onSelect={(value) => {
                      setSecretary(value);
                      updateSecretary(value);
                      setSecretaryError(false);
                    }}
                    margin="normal"
                  />
                ) : (
                  <UserInfo user={secretary} />
                )}
                {secretaryError && (
                  <Typography color="primary">Please, indicate the secretary of the meeting.</Typography>
                )}
              </Grid>
            </Grid>
          </>
        </TabPanel>
        <TabPanel value={tab} index={1} className={classes.tabPanel}>
          <MeetingMoM meetingId={meeting.id} />
        </TabPanel>
        <TabPanel value={tab} index={2} className={classes.tabPanel}>
          <MeetingNotes meetingId={meeting.id} />
        </TabPanel>
        <DialogActions>
          <MeetingActionButtons
            visible={{ join: !isOld, delete: canDelete, details: false }}
            meetingId={meeting.id}
            onDeleteClick={onDelete}
          />
        </DialogActions>
      </DialogContent>
    </Dialog>
  );
};

export default MeetingDetails;
