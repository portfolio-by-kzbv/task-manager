import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  content: {
    padding: theme.spacing(1, 4),
  },
  section: {
    marginTop: theme.spacing(3),

    '&:first-child': {
      marginTop: 0,
    },
  },
  dateSection: {
    display: 'flex',
    marginTop: theme.spacing(2),
    height: theme.spacing(20),

    '& > *': {
      marginRight: theme.spacing(2),
    },
  },
  valueName: {
    marginRight: theme.spacing(1),
  },
  tabPanel: {
    paddingTop: theme.spacing(2),
    minHeight: '400px',
  },
}));
