import { useMemo } from 'react';
import { useUserSessionState, useUsersState } from '@dar/selectors/users';
import { useMeetingState } from '@dar/selectors/meetingsStates';
import { MEETING_STATES } from '../../../constants/meeting-states';

import { editMeeting } from '@dar/actions/meetings.action';
import { useDispatch } from 'react-redux';
import { Meeting } from '../../../models/meeting';
import { Dispatch as CustomDispatch } from '@dar/api-interfaces';
import { toast } from '@dartech/dms-ui';

const useMeetingDetails = (meeting) => {
  const meetingState = useMeetingState(meeting.id);
  const allUsers = useUsersState();
  const session = useUserSessionState();
  const dispatch: CustomDispatch = useDispatch();

  const userList = useMemo(() => {
    const updatedUserList = {};

    allUsers.forEach((user) => {
      if (user.workEmail) {
        updatedUserList[user.workEmail.toLowerCase()] = user;
      }
    });

    return updatedUserList;
  }, [allUsers]);

  const owner = useMemo(
    () => (userList[meeting.ownerEmail?.toLowerCase()] ? userList[meeting.ownerEmail?.toLowerCase()] : {}),
    [meeting, userList]
  );

  const [participants, externalParticipants] = useMemo(() => {
    const participants = [];
    const externalParticipants = [];

    meeting.participants.forEach((p) => {
      if (userList[p.toLowerCase()]) {
        participants.push(userList[p.toLowerCase()]);
        return;
      }

      externalParticipants.push(p);
    });

    return [participants, externalParticipants];
  }, [meeting, userList]);

  const isOld = useMemo(() => Boolean(meetingState && meetingState.state === MEETING_STATES.OLD), [meetingState]);

  const canEdit = useMemo(
    () => Boolean(meeting.ownerEmail.toLowerCase() === session?.workEmail.toLowerCase() && !isOld),
    [meeting, session, isOld]
  );

  const canDelete = meeting.ownerEmail === session?.workEmail.toLowerCase();

  const descriptionValidation = (value: string) => {
    if (value && value.length > 63) {
      return 'Description must not exceed 64 characters';
    }
  };

  const titleValidation = (value: string) => {
    if (!value) {
      return 'Please, fill the title';
    }
    if (value && value.length > 44) {
      return 'Title must not exceed 45 characters';
    }
  };

  const setDate = (date) => {
    const startTime = new Date(date);
    startTime.setHours(new Date(meeting.startTime).getHours());
    startTime.setMinutes(new Date(meeting.startTime).getMinutes());

    const endTime = new Date(date);
    endTime.setHours(new Date(meeting.endTime).getHours());
    endTime.setMinutes(new Date(meeting.endTime).getMinutes());

    return onEdit({
      startTime,
      endTime: new Date(new Date(endTime).getTime() + 60000),
    });
  };

  const setStartTime = (startTime) => {
    return onEdit({ startTime });
  };

  const setEndTime = (endTime) => {
    return onEdit({
      endTime: new Date(new Date(endTime).getTime() + 60000),
    });
  };

  const addParticipant = (participant) => onEdit({ participants: [...meeting.participants, participant] });

  const deleteParticipant = (participant) =>
    onEdit({
      participants: [...meeting.participants.filter((p) => p !== participant)],
    });

  const onEdit = (newValue) => {
    const editedMeeting = Meeting.from({
      ...meeting,
      ...newValue,
    }).editPayload();
    return dispatch(editMeeting(editedMeeting)).then(() => {
      toast.success('Changes were saved', { duration: 2000 });
    });
  };

  return {
    owner,
    participants,
    externalParticipants,
    canEdit,
    canDelete,
    isOld,
    titleValidation,
    descriptionValidation,
    setDate,
    setStartTime,
    setEndTime,
    addParticipant,
    deleteParticipant,
    onEdit,
  };
};

export default useMeetingDetails;
