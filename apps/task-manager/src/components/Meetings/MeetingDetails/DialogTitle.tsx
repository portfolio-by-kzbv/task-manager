import React from 'react';
import { IconButton, makeStyles } from '@material-ui/core';
import MuiDialogTitle from '@material-ui/core/DialogTitle';
import CloseIcon from '@material-ui/icons/Close';
import EditableText from '../../Input/EditableText/EditableText';

const useStyles = makeStyles((theme) => ({
  closeButton: {
    position: 'absolute',
    right: theme.spacing(1),
    top: theme.spacing(1),
  },
  title: {
    display: 'flex',
    justifyContent: 'center',
  },
}));

type Props = {
  title: string;
  onClose;
  validation: (value: any) => string;
  onEdit: (value: any) => Promise<void>;
  canEdit: boolean;
};

export const DialogTitle: React.FunctionComponent<Props> = ({
  title,
  onClose,
  validation,
  onEdit,
  canEdit,
  ...other
}) => {
  const classes = useStyles();

  return (
    <MuiDialogTitle disableTypography className={classes.title} {...other}>
      <EditableText
        typographyProps={{ variant: 'h2' }}
        valueKey="title"
        initialValue={title}
        placeholder="Title"
        validation={validation}
        onSave={onEdit}
        canEdit={canEdit}
      />
      <IconButton aria-label="close" className={classes.closeButton} onClick={onClose}>
        <CloseIcon />
      </IconButton>
    </MuiDialogTitle>
  );
};
