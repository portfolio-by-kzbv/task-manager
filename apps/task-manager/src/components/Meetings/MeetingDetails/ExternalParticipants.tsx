import React, { useState, useEffect } from 'react';
import {
  Grid,
  Chip,
  makeStyles,
  Box,
  TextField,
  Button,
} from '@material-ui/core';

import Spinner from '../../Spinner/Spinner';

const useStyles = makeStyles({
  grid: {
    position: 'relative',
  },
  buttonStyle: {
    marginLeft: '8px',
  },
});

type Props = {
  participants;
  canEdit;
  addExternalParticipant;
  deleteExternalParticipant;
};

const ExternalParticipants: React.FunctionComponent<Props> = ({
  participants,
  canEdit,
  addExternalParticipant,
  deleteExternalParticipant,
}) => {
  const [value, setValue] = useState('');
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);

  const classes = useStyles();

  useEffect(() => {
    const regEx = new RegExp(/^[^\s@]+@[^\s@]+\.[^\s@]+$/);
    if (value && !regEx.test(value.toLowerCase())) {
      setError('Enter valid email');
    } else {
      setError(null);
    }
  }, [value]);

  const handleDelete = (participant) => {
    setLoading(true);
    deleteExternalParticipant(participant).finally(() => {
      setLoading(false);
    });
  };

  const handleAdd = () => {
    if (!value) {
      setError('Please, enter an email');
    } else if (participants.includes(value)) {
      setError('Already in list');
    } else {
      setLoading(true);
      addExternalParticipant(value).finally(() => {
        setValue('');
        setLoading(false);
      });
    }
  };

  return (
    <Grid container spacing={1} alignItems="center" className={classes.grid}>
      {loading && <Spinner absolute={true} />}

      {participants.map((participant, index) => (
        <Grid item key={index} md={4} sm={6} xs={12}>
          <Chip
            label={participant}
            variant="outlined"
            color="primary"
            onDelete={canEdit ? () => handleDelete(participant) : null}
          />
        </Grid>
      ))}
      {canEdit && (
        <Grid item md={4} sm={6} xs={12}>
          <Box display="flex" alignItems="center">
            <TextField
              fullWidth
              placeholder="Add member"
              variant="outlined"
              margin="dense"
              value={value}
              onChange={(event) => setValue(event.target.value)}
              disabled={loading}
              error={Boolean(error)}
              helperText={error ? error : ''}
            />
            <Button color="primary" onClick={handleAdd} className={classes.buttonStyle}>
              Add
            </Button>
          </Box>
        </Grid>
      )}
    </Grid>
  );
};

export default ExternalParticipants;
