import React from 'react';
import AvatarGroup from '@material-ui/lab/AvatarGroup';
import { Tooltip } from '@material-ui/core';

import UserAvatar from '../../UserAvatar';
import { User } from '@dar/api-interfaces';

type Props = {
  users: User[];
  limit: number;
};

const ListAvatars: React.FunctionComponent<Props> = ({ users, limit }) => (
  <AvatarGroup max={limit}>
    {users &&
      users.map((user) =>
        user ? (
          <Tooltip key={user.id} title={user.displayName}>
            <UserAvatar user={user} />
          </Tooltip>
        ) : null
      )}
  </AvatarGroup>
);

export default ListAvatars;
