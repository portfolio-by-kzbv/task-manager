import React, { useState } from 'react';
import moment from 'moment';
import {
  TextField,
  Button,
  Switch,
  FormControlLabel,
  Chip,
  Typography,
} from '@material-ui/core';

import Spinner from '@dar/components/Spinner/Spinner';
import DatePicker from '@dar/components/DatePicker/DatePicker';
import SelectUsers from '@dar/components/Users/SelectUsers';
import ExternalParticipants from './ExternalParticipants';
import UserAvatar from '@dar/components/UserAvatar/UserAvatar';
import TimePicker from '@dar/components/Input/TimePicker/TimePicker';
import useMeetingForm from './useMeetingForm';
import SelectRecurrence from './SelectRecurrence';
import ModalRecurrence from './ModalRecurrence';

import { useStyles } from './createMeet.styles';

type Props = {
  formValues?;
  callback?;
};

const CreateMeetingForm: React.FunctionComponent<Props> = ({
  formValues,
  callback,
}) => {
  const [hasExtMembers, setHasExtMembers] = useState(false);
  const {
    values,
    errors,
    loading,
    modalIsOpen,
    clear,
    onDeleteParticipant,
    onAddParticipant,
    handleChange,
    handleSubmit,
    handleOpenModal,
    handleCloseModal,
  } = useMeetingForm(formValues, callback);

  const classes = useStyles();

  const toggleExtMembers = () => {
    setHasExtMembers(!hasExtMembers);
    handleChange('externalParticipants', []);
  };

  return (
    <form onSubmit={handleSubmit} className={classes.form}>
      {loading && <Spinner absolute />}
      <TextField
        placeholder="Title"
        variant="standard"
        margin="normal"
        fullWidth
        value={values.title}
        onChange={(event) => handleChange('title', event.target.value)}
        disabled={loading}
        error={Boolean(errors.title)}
        helperText={errors.title ? errors.title : ''}
      />
      <TextField
        placeholder="Description"
        variant="standard"
        fullWidth
        margin="normal"
        multiline
        rows={3}
        value={values.description}
        onChange={(event) => handleChange('description', event.target.value)}
        disabled={loading}
        error={Boolean(errors.description)}
        helperText={errors.description ? errors.description : ''}
      />
      <div className={classes.paddingTop}>
        <DatePicker
          value={values.date}
          onChange={(value) => handleChange('date', value)}
          label="Date"
          disabled={loading}
          fullWidth={true}
          disablePast={true}
        />
      </div>
      <div className={classes.box}>
        <TimePicker
          label="Start time"
          value={moment(values.startTime).format('HH:mm')}
          onSelect={(value) => handleChange('startTime', value)}
          disabled={loading}
        />
        <TimePicker
          label="End time"
          value={moment(values.endTime).format('HH:mm')}
          onSelect={(value) => handleChange('endTime', value)}
          disabled={loading}
          error={errors.endTime}
        />
      </div>

      <div>
        <Typography variant="h5">Members</Typography>
        <div className={classes.participants}>
          {values.participants.map((user) => (
            <Chip
              key={user.id}
              avatar={<UserAvatar user={user} />}
              label={user.displayName}
              onDelete={() => onDeleteParticipant(user)}
              variant="outlined"
              color="default"
            />
          ))}
        </div>
      </div>

      <SelectUsers
        value={null}
        onSelect={onAddParticipant}
        label="Add members"
        margin="normal"
      />
      <div className={classes.paddingTop}>
        <FormControlLabel
          control={
            <Switch
              checked={hasExtMembers}
              onChange={toggleExtMembers}
              color="default"
              className={classes.switchMargin}
            />
          }
          label="External members"
          labelPlacement="end"
        />
      </div>

      {hasExtMembers && (
        <ExternalParticipants
          members={values.externalParticipants}
          setMembers={(value) => handleChange('externalParticipants', value)}
          loading={loading}
        />
      )}

      <div className={classes.paddingTop}>
        <Typography variant="h5">Secretary</Typography>
        <SelectUsers
          value={values.secretary}
          onSelect={(value) => handleChange('secretary', value)}
          error={errors.secretary ? errors.secretary : ''}
          margin="normal"
        />
      </div>

      <div>
        <Typography variant="h5">Recurrence</Typography>
        <SelectRecurrence
          value={values.recurrence}
          onSelect={(value) => handleChange('recurrence', value)}
          meetingDate={values.date}
          handleOpenModal={handleOpenModal}
        />
      </div>

      <div className={classes.box}>
        <Button
          variant="contained"
          color="primary"
          type="submit"
          disabled={loading}
        >
          Create
        </Button>
        <Button
          variant="contained"
          type="button"
          onClick={clear}
          disabled={loading}
        >
          Clear
        </Button>
      </div>

      <ModalRecurrence open={modalIsOpen} handleCloseModal={handleCloseModal} />
    </form>
  );
};

export default CreateMeetingForm;
