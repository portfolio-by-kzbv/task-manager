import React from 'react';
import Autocomplete from '@material-ui/lab/Autocomplete/Autocomplete';
import { TextField } from '@material-ui/core';

import { basicOptions } from './useSelectRecurrence';

const SelectRecurrenceView = ({ onSelect, value, options, handleOpenModal }) => (
  <Autocomplete
    fullWidth
    onChange={(_, recType) => {
      if (!recType) {
        return;
      }
      if (recType.title === 'Custom...') {
        handleOpenModal();
        return;
      }
      onSelect(recType);
    }}
    options={options || []}
    getOptionLabel={(option) => option?.title}
    getOptionSelected={(option, v) => v && option?.title === v.title}
    value={value || basicOptions[0]}
    renderInput={(params) => <TextField {...params} margin="normal" variant="outlined" />}
    disableClearable
  />
);

export default SelectRecurrenceView;
