import { useMemo, useEffect } from 'react';
import moment from 'moment';
import { isValidDate } from '../../../../utils/date';

export const basicOptions = [
  { title: 'Does not repeat', value: '' },
  { title: 'Daily', value: 'RRULE:FREQ=DAILY' },
  {
    title: 'Every weekday (Monday to Friday)',
    value: 'RRULE:FREQ=WEEKLY;BYDAY=MO,TU,WE,TH,FR',
  },
];

const useSelectRecurrence = ({ onSelect, value, meetingDate }) => {
  const options = useMemo(() => {
    const currentDay = moment(meetingDate).format('DD');
    const currentWeekDay = moment(meetingDate).format('dddd');
    const currentMonth = moment(meetingDate).format('MMMM');

    return isValidDate(meetingDate)
      ? basicOptions.concat([
          {
            title: `Weekly on ${currentWeekDay}`,
            value: `RRULE:FREQ=WEEKLY;BYDAY=${currentWeekDay
              .toUpperCase()
              .substr(0, 2)}`,
          },
          {
            title: `Monthly on the ${currentMonth} ${currentDay}`,
            value: 'RRULE:FREQ=MONTHLY',
          },
          {
            title: `Annually on ${currentMonth} ${currentDay}`,
            value: 'RRULE:FREQ=YEARLY',
          },
          { title: 'Custom...', value: '' },
        ])
      : basicOptions;
  }, [meetingDate]);

  useEffect(() => {
    if (value && !basicOptions.map((o) => o.title).includes(value.title)) {
      onSelect(basicOptions[0]);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [meetingDate]);

  return options;
};

export default useSelectRecurrence;
