import React from 'react';

import SelectRecurrenceView from './SelectRecurrenceView';
import useSelectRecurrence from './useSelectRecurrence';

const SelectRecurrence = ({
  onSelect,
  value,
  meetingDate,
  handleOpenModal,
}) => {
  const options = useSelectRecurrence({ onSelect, value, meetingDate });
  const screenProps = { options, onSelect, value, handleOpenModal };

  return <SelectRecurrenceView {...screenProps} />;
};

export default SelectRecurrence;
