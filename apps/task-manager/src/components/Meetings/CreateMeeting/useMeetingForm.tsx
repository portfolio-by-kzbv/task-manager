import { useState, useEffect, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { createMeeting } from '@dar/actions/meetings.action';
import { useUserSessionState, useUsersState } from '@dar/selectors/users';

import { MeetingErrors, Dispatch as CustomDispatch } from '@dar/api-interfaces';
import { addHalfHour, roundTime } from '../../../utils/time';

const initialValues = {
  title: '',
  description: '',
  date: new Date(),
  participants: [],
  externalParticipants: [],
  startTime: roundTime(new Date()),
  endTime: addHalfHour(roundTime(new Date())),
  recurrence: { title: 'Does not repeat', value: '' },
};

const useMeetingForm = (vals, callback) => {
  const dispatch: CustomDispatch = useDispatch();
  const allUsers = useUsersState();
  const session = useUserSessionState();
  // TODO: determine values type
  const [values, setValues] = useState({
    ...initialValues,
    ...vals,
  });
  const [errors, setErrors] = useState<MeetingErrors>({});
  const [loading, setLoading] = useState(false);
  const [modalIsOpen, setModalIsOpen] = useState(false);

  useEffect(() => {
    if (vals && vals.startTime) {
      setValues((v) => ({ ...v, endTime: addHalfHour(vals.startTime) }));
    }
  }, [vals]);

  useEffect(() => {
    if (session?.id && Array.isArray(allUsers)) {
      setValues((v) => ({
        ...v,
        secretary: allUsers.find((u) => u.id === session?.id),
      }));
    }
  }, [session?.id, allUsers]);

  useEffect(() => {
    const { title, description, endTime, startTime, secretary } = values;
    const err: MeetingErrors = {};
    const timeDifference = (endTime.getTime() - startTime.getTime()) / 1000 / 60;

    if (title && title.length > 44) {
      err.title = 'Title must not exceed 45 characters';
    } else if (err.title) {
      delete err.title;
    }

    if (description && description.length > 63) {
      err.description = 'Description must not exceed 64 characters';
    } else if (err.description) {
      delete err.description;
    }

    if (timeDifference < 30) {
      err.endTime = 'min 30 minutes';
    } else if (err.endTime) {
      delete err.endTime;
    }

    if (secretary && err.secretary) {
      delete err.secretary;
    }

    setErrors(err);
  }, [values]);

  const clear = useCallback(
    () =>
      setValues({
        secretary: allUsers.find((u) => u.id === session?.id),
        ...initialValues,
      }),
    [allUsers, session?.id]
  );

  const handleChange = (name: string, value: Date | Array<unknown> | unknown) => {
    if (name === 'date') {
      onDateChange(value);
    } else if (name === 'startTime') {
      onStartTimeChange(value);
    } else if (name === 'endTime') {
      onEndTimeChanged(value);
    } else {
      setValues({ ...values, [name]: value });
    }
  };

  const onDateChange = (date) => {
    const newDate = new Date(date);

    const startTime = new Date(date);
    startTime.setHours(new Date(values.startTime).getHours());
    startTime.setMinutes(new Date(values.startTime).getMinutes());

    const endTime = new Date(date);
    endTime.setHours(new Date(values.endTime).getHours());
    endTime.setMinutes(new Date(values.endTime).getMinutes());

    setValues({ ...values, date: newDate, startTime, endTime });
  };

  const onStartTimeChange = (time) => {
    const t = time ? time.split(':') : ['00', '00'];
    if (+t[0] > 23 || +t[1] > 59) {
      setErrors((errors) => ({
        ...errors,
        startTime: 'Invalid Date',
      }));
      return;
    }
    const newStartTime = new Date(values.date).setHours(t[0], t[1], 0);
    setValues({
      ...values,
      startTime: new Date(newStartTime),
      endTime: addHalfHour(new Date(newStartTime)),
    });
  };

  const onEndTimeChanged = (time) => {
    const t = time ? time.split(':') : ['00', '00'];
    if (+t[0] > 23 || +t[1] > 59) {
      setErrors((errors) => ({
        ...errors,
        endTime: 'Invalid Date',
      }));
      return;
    }
    const newEndTime = new Date(values.date).setHours(t[0], t[1], 0);
    setValues({ ...values, endTime: new Date(newEndTime) });
  };

  const onDeleteParticipant = (user) => {
    handleChange(
      'participants',
      values.participants.filter((participant) => participant.id !== user.id)
    );
  };

  const onAddParticipant = (user) => {
    if (user && !values.participants.includes(user)) {
      handleChange('participants', [...values.participants, user]);
    }
  };

  const preSubmitValidation = () => {
    const { title, secretary } = values;
    const err: MeetingErrors = {
      ...errors,
    };

    if (!title.length) {
      err.title = 'Please, fill the title';
    }

    if (!secretary) {
      err.secretary = 'Please, fill the secretary field';
    }

    return err;
  };

  const getPayload = () => {
    const {
      title,
      description,
      startTime,
      endTime,
      participants,
      externalParticipants,
      secretary,
      recurrence,
    } = values;
    const participantsList = [{ id: 1, userId: session.id, email: session.workEmail, meetingId: 1 }];

    if (session.id !== secretary.id) {
      participantsList.push({ id: 2, userId: secretary.id, email: secretary.workEmail, meetingId: 2 });
    }
    participants.forEach((participant, index) => {
      if (participant.id !== session.id && participant.id !== secretary.id) {
        participantsList.push({
          id: index,
          userId: participant.id,
          email: participant.workEmail,
          meetingId: index,
        });
      }
    });
    const payload = {
      id: null,
      title,
      description,
      startTime,
      endTime,
      participants: participantsList.map((p) => p.email),
      secretary: secretary.id,
      owner: session.id,
      ownerEmail: session.workEmail,
      externalParticipants,
      recurrence: recurrence ? recurrence.value : null,
    };

    if (!payload.recurrence) {
      delete payload.recurrence;
    }

    return payload;
  };

  const handleCreate = useCallback(
    (payload) => {
      setLoading(true);
      dispatch(createMeeting(payload))
        .then(() => {
          if (callback) {
            callback();
          }
        })
        .finally(() => {
          setLoading(false);
          clear();
        });
    },
    [dispatch, callback, clear]
  );

  const handleSubmit = (event) => {
    event.preventDefault();
    const errors = preSubmitValidation();

    if (!Object.keys(errors).length) {
      const meeting = getPayload();
      handleCreate(meeting);
    } else {
      console.log(errors);
      setErrors(errors);
    }
  };

  const handleOpenModal = () => setModalIsOpen(true);
  const handleCloseModal = (customRecurrence: { title: string; value: string }) => {
    handleChange('recurrence', customRecurrence && customRecurrence.value ? customRecurrence : values.recurrence);
    setModalIsOpen(false);
  };

  return {
    values,
    errors,
    loading,
    modalIsOpen,
    clear,
    onDeleteParticipant,
    onAddParticipant,
    handleChange,
    handleSubmit,
    handleOpenModal,
    handleCloseModal,
  };
};

export default useMeetingForm;
