import React from 'react';
import { Dialog, DialogTitle, DialogContent, DialogActions } from '@material-ui/core';
import Close from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';
import Box from '@material-ui/core/Box';

import CreateMeetingForm from './CreateMeetingForm';
import { useStyles } from './createMeet.styles';

const CreateMeetingModal = ({ open, handleClose, date }) => {
  const classes = useStyles();

  return (
    <Dialog open={open} onClose={handleClose}>
      <Box display="flex" justifyContent="space-between">
        <DialogTitle>Create new meeting</DialogTitle>
        <DialogActions className={classes.actionContainer}>
          <IconButton color="primary" onClick={handleClose} component="span">
            <Close />
          </IconButton>
        </DialogActions>
      </Box>
      <DialogContent>
        <CreateMeetingForm
          formValues={{
            date,
            startTime: date
          }}
          callback={handleClose}
        />
      </DialogContent>
    </Dialog>
  );
};

export default CreateMeetingModal;
