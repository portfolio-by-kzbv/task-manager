import React from 'react';
import ModalRecurrenceView from './ModalRecurrenceView';
import useModalRecurrence from './useModalRecurrence';

import { isValidDate, getNextDay } from '../../../../utils/date';
import { generateRecurringMeetingEndDateString } from '../../../../utils/meetings';

type Props = {
  open?: boolean;
  handleCloseModal?: (customRecurrence?: { title: string; value: string }) => void;
};

const ModalRecurrence: React.FunctionComponent<Props> = ({ open, handleCloseModal }) => {
  const { models, operations } = useModalRecurrence();

  const { currentOption, weekDays, repeatedWeekDays, meetingEndType, intervals, occurrences, endDate, errors } = models;

  const {
    setCurrentOption,
    setRepeatedWeekDays,
    setMeetingEndType,
    setIntervals,
    setOccurrences,
    setEndDate,
    setErrors,
  } = operations;

  const handleClose = () => handleCloseModal();
  const handleSubmit = () => {
    let finalString = '';

    finalString += `RRULE:FREQ=${currentOption === 'day' ? 'DAILY' : currentOption.toUpperCase() + 'LY'};`;

    if (meetingEndType === 'On') {
      finalString += `UNTIL=${generateRecurringMeetingEndDateString(endDate)};`;
    } else if (meetingEndType === 'After') {
      finalString += `COUNT=${occurrences};`;
    }

    if (intervals > 1) {
      finalString += `INTERVAL=${intervals};`;
    }

    if (currentOption === 'week') {
      finalString += `BYDAY=${repeatedWeekDays.join(',')};`;
    }

    finalString = finalString.substring(0, finalString.length - 1);

    handleCloseModal({ title: 'Custom...', value: finalString });
  };

  const handleChangeOption = (e) =>
    setCurrentOption((lastOption) => {
      if (lastOption === 'week') {
        setRepeatedWeekDays([weekDays[1].value]);
      }
      return e.target.value;
    });

  const handleChangeWeekDays = (weekDay) =>
    setRepeatedWeekDays((pickedWeekDays) => {
      const tempWeekDays = [...pickedWeekDays];
      const currentWeekDayIndex = tempWeekDays.findIndex((w) => w === weekDay);
      if (currentWeekDayIndex === -1) {
        tempWeekDays.push(weekDay);
      } else {
        tempWeekDays.splice(currentWeekDayIndex, 1);
      }

      return tempWeekDays.length === 0 ? [weekDays[1].value] : tempWeekDays;
    });

  const handleChangeEndType = (e) => {
    setMeetingEndType((endType) => {
      if (endType === 'On' && errors.endDate) {
        setErrors((errors) => ({
          ...errors,
          endDate: '',
        }));
        setEndDate(getNextDay());
      } else if (endType === 'After' && errors.occurrences) {
        setErrors((errors) => ({
          ...errors,
          occurrences: '',
        }));
        setOccurrences(1);
      }
      return e.target.value;
    });
  };
  const handleChangeInterval = (e) => handleNumberInput(e.target.value, 'interval', setIntervals);
  const handleChangeOccurrences = (e) => handleNumberInput(e.target.value, 'occurrences', setOccurrences);

  const handleChangeEndDate = (value) => {
    if (!isValidDate(value)) {
      setErrors((errors) => ({
        ...errors,
        endDate: 'Invalid Date',
      }));
    } else if (new Date(value).getDate() < new Date().getDate()) {
      setErrors((errors) => ({
        ...errors,
        endDate: 'Past dates are not allowed',
      }));
    } else if (errors.endDate !== '') {
      setErrors((errors) => ({
        ...errors,
        endDate: '',
      }));
    }

    setEndDate(value);
  };

  const screenProps = {
    ...models,
    open,
    handleClose,
    handleSubmit,
    handleChangeOption,
    handleChangeWeekDays,
    handleChangeEndType,
    handleChangeInterval,
    handleChangeOccurrences,
    handleChangeEndDate,
  };

  const handleNumberInput = (value, fieldName, updateValue) => {
    if (value < 1) {
      setErrors((errors) => ({
        ...errors,
        [fieldName]: value < 1 ? 'Invalid value' : '',
      }));
    } else if (errors[fieldName] !== '') {
      setErrors((errors) => ({
        ...errors,
        [fieldName]: '',
      }));
    }

    updateValue(value);
  };

  return <ModalRecurrenceView {...screenProps} />;
};

export default ModalRecurrence;
