import React from 'react';
import cx from 'classnames';
import DialogTitle from '@material-ui/core/DialogTitle';
import Dialog from '@material-ui/core/Dialog';
import Button from '@material-ui/core/Button';
import DialogActions from '@material-ui/core/DialogActions';
import TextField from '@material-ui/core/TextField';
import Box from '@material-ui/core/Box';
import Typography from '@material-ui/core/Typography';
import MenuItem from '@material-ui/core/MenuItem';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import FormControl from '@material-ui/core/FormControl';

import DatePicker from '../../../DatePicker/DatePicker';

import { useStyles } from './ModalRecurrence.styles';

const ModalRecurrenceView = ({
  handleSubmit,
  handleClose,
  open,
  options,
  currentOption,
  handleChangeOption,
  weekDays,
  repeatedWeekDays,
  handleChangeWeekDays,
  meetingEndType,
  handleChangeEndType,
  intervals,
  handleChangeInterval,
  occurrences,
  handleChangeOccurrences,
  endDate,
  handleChangeEndDate,
  errors,
}) => {
  const style = useStyles();

  return (
    <Dialog onClose={handleClose} aria-labelledby="simple-dialog-title" open={open}>
      <Box className={style.popup} style={{ zIndex: 1251 }}>
        <DialogTitle id="simple-dialog-title" className={style.popupHeader}>
          Custom recurrence
        </DialogTitle>
        <Box padding="0 24px">
          <Box display="flex" alignItems="center" className={style.repeat}>
            <Typography className={style.repeatText}>Repeat every</Typography>
            <TextField
              type="number"
              InputLabelProps={{
                shrink: true,
              }}
              InputProps={{
                inputProps: { min: 1 },
              }}
              className={cx(style.select, {
                [style.numberInputError]: Boolean(errors.interval),
              })}
              value={intervals}
              onChange={handleChangeInterval}
              error={Boolean(errors.interval)}
              helperText={errors.interval}
            />
            <TextField
              select
              value={currentOption}
              onChange={handleChangeOption}
              variant="outlined"
              className={style.select}
              InputProps={{className: style.frequencyOptionSelect}}
            >
              {options.map((option) => (
                <MenuItem key={option.value} value={option.value}>
                  {option.label}
                </MenuItem>
              ))}
            </TextField>
          </Box>

          {currentOption === 'week' && (
            <Box style={{ display: 'flex', alignItems: 'baseline', marginBottom: '32px' }}>
              <Typography className={style.repeatText}>Repeat on</Typography>
              <Box display="flex" className={style.weekDayContainer}>
                {weekDays.map((w) => (
                  <Box
                    key={w.value}
                    className={cx(style.weekDayCircle, {
                      [style.weekDayCircleActive]: repeatedWeekDays.find((weekDay) => weekDay === w.value),
                    })}
                    onClick={() => handleChangeWeekDays(w.value)}
                  >
                    {w.label}
                  </Box>
                ))}
              </Box>
            </Box>
          )}

          <Box>
            <Typography className={style.repeatText}>Ends</Typography>
            <FormControl component="fieldset" margin="normal">
              <RadioGroup value={meetingEndType} onChange={handleChangeEndType}>
                <FormControlLabel value="Never" control={<Radio color="primary" />} label="Never" />
                <Box display="flex" justifyContent="space-between">
                  <FormControlLabel value="On" control={<Radio color="primary" />} label="On" />
                  <Box marginLeft="15px">
                    <DatePicker
                      value={endDate}
                      onChange={handleChangeEndDate}
                      label={errors.endDate}
                      disabled={meetingEndType !== 'On'}
                      fullWidth={false}
                      disablePast={true}
                      error={Boolean(errors.endDate)}
                    />
                  </Box>
                </Box>
                <Box display="flex" justifyContent="space-between">
                  <FormControlLabel value="After" control={<Radio color="primary" />} label="After" />
                  <TextField
                    type="number"
                    InputLabelProps={{
                      shrink: true,
                    }}
                    InputProps={{
                      inputProps: { min: 1 },
                      endAdornment: (
                        <Typography className={style.occurenceText}>
                          {occurrences === 1 ? 'occurrence' : 'occurences'}
                        </Typography>
                      ),
                    }}
                    disabled={meetingEndType !== 'After'}
                    value={occurrences}
                    onChange={handleChangeOccurrences}
                    error={Boolean(errors.occurrences)}
                    helperText={errors.occurrences}
                    className={cx(style.select, style.occurenceInput)}
                  />
                </Box>
              </RadioGroup>
            </FormControl>
          </Box>
        </Box>
        <DialogActions>
          <Button onClick={handleClose} color="secondary">
            Cancel
          </Button>
          <Button
            onClick={handleSubmit}
            color="primary"
            disabled={
              Boolean(errors.interval) ||
              Boolean(errors.endDate && meetingEndType === 'On') ||
              Boolean(errors.occurrences && meetingEndType === 'After')
            }
          >
            Done
          </Button>
        </DialogActions>
      </Box>
    </Dialog>
  );
};

export default ModalRecurrenceView;
