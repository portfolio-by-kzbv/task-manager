import { useState } from 'react';

import { getNextDay } from '../../../../utils/date';

const options = [
  {
    value: 'day',
    label: 'day'
  },
  {
    value: 'week',
    label: 'week'
  },
  {
    value: 'month',
    label: 'month'
  },
  {
    value: 'year',
    label: 'year'
  }
];

const weekDays = [
  {
    value: 'SU',
    label: 'S'
  },
  {
    value: 'MO',
    label: 'M'
  },
  {
    value: 'TU',
    label: 'T'
  },
  {
    value: 'WE',
    label: 'W'
  },
  {
    value: 'TH',
    label: 'T'
  },
  {
    value: 'FR',
    label: 'F'
  },
  {
    value: 'SA',
    label: 'S'
  }
];

const useModalRecurrence = () => {
  const [currentOption, setCurrentOption] = useState(options[1].value);
  const [repeatedWeekDays, setRepeatedWeekDays] = useState([weekDays[1].value]);
  const [meetingEndType, setMeetingEndType] = useState('Never');

  const [intervals, setIntervals] = useState(1);
  const [occurrences, setOccurrences] = useState(1);

  const [errors, setErrors] = useState({
    interval: '',
    occurrences: '',
    endDate: ''
  });

  const [endDate, setEndDate] = useState(() => getNextDay());

  const models = {
    options,
    currentOption,
    weekDays,
    repeatedWeekDays,
    meetingEndType,
    intervals,
    occurrences,
    endDate,
    errors
  };
  const operations = {
    setCurrentOption,
    setRepeatedWeekDays,
    setMeetingEndType,
    setIntervals,
    setOccurrences,
    setEndDate,
    setErrors
  };

  return { models, operations };
};

export default useModalRecurrence;
