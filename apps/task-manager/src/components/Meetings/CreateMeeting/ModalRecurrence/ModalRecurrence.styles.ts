import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles((theme) => ({
  popup: {
    width: '100%',
    minWidth: '450px',
    margin: '0 auto',
  },
  popupHeader: {
    color: theme.palette.primary[200],
  },
  repeat: {
    marginBottom: '32px',
  },
  repeatText: {
    minWidth: '124px',
  },
  select: {
    width: '100%',
    maxWidth: '100px',
    textAlign: 'center',
  },
  numberInputError: {
    marginBottom: '-25px',
  },
  frequencyOptionSelect: {
    height: '40px',
    marginLeft: '15px',
  },
  weekDayContainer: {
    marginTop: '10px',
  },
  weekDayCircle: {
    width: '24px',
    height: '24px',
    borderRadius: '50%',
    background: '#dadce0',
    color: '#80868b',
    transition: 'backround-color .1s linear, color .1s lineear',
    marginRight: '5px',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontSize: '14px',
    cursor: 'pointer',
  },
  weekDayCircleActive: {
    background: theme.palette.primary.main,
    color: '#fff',
  },
  occurenceInput: {
    maxWidth: '150px',
  },
  occurenceText: {
    fontSize: '14px',
  },
}));
