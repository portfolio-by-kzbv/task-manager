import { makeStyles } from '@material-ui/core';

export const useStyles = makeStyles({
  form: {
    position: 'relative',
  },
  paddingTop: {
    padding: '16px 0 0 0',
  },
  switchMargin: {
    marginRight: '8px',
  },
  box: {
    display: 'flex',
    justifyContent: 'space-between',
    margin: '8px 0',

    '& > *': {
      width: 'calc(50% - 8px)',
    },
  },
  participants: {
    display: 'flex',
    flexWrap: 'wrap',
    padding: '8px 0 4px',
  },
  actionContainer: {
    marginRight: '10px',
  },
});
