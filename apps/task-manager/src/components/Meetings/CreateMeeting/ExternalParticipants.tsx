import React, { useState, useEffect } from 'react';
import { TextField, Chip, Button, Box, makeStyles } from '@material-ui/core';

const useStyles = makeStyles({
  root: {
    padding: '8px 0'
  },
  members: {
    display: 'flex',
    flexWrap: 'wrap',
    padding: '8px 0'
  },
  buttonStyle: {
    marginLeft: '8px',
  },
});

type Props = {
  loading: boolean;
  members: string[];
  setMembers: (members: string[]) => void;
}

const ExternalParticipants: React.FunctionComponent<Props> = ({ loading, members = [], setMembers }) => {
  const [value, setValue] = useState('');
  const [error, setError] = useState(null);

  const classes = useStyles();

  useEffect(() => {
    const regEx = new RegExp(/^[^\s@]+@[^\s@]+\.[^\s@]+$/);
    if (value && !regEx.test(value.toLowerCase())) {
      setError('Enter valid email');
    } else {
      setError(null);
    }
  }, [value]);

  const addMember = () => {
    if (!value) {
      setError('Please, enter an email');
    } else if (members.includes(value)) {
      setError('Already in list');
    } else {
      setMembers([...members, value]);
      setValue('');
    }
  };

  const deleteMember = member => {
    setMembers(members.filter(m => m !== member));
  };

  return (
    <div >
      <div className={classes.members}>
        {members.map((member, index) => (
          <Chip
            key={index}
            label={member}
            onDelete={() => deleteMember(member)}
            variant="outlined"
            color="primary"
          />
        ))}
      </div>
      <Box display="flex" alignItems="center">
        <TextField
          fullWidth
          placeholder="Add member"
          variant="outlined"
          margin="dense"
          value={value}
          onChange={event => setValue(event.target.value)}
          disabled={loading}
          error={Boolean(error)}
          helperText={error ? error : ''}
        />
        <Button color="primary" onClick={addMember} className={classes.buttonStyle}>
          Add
        </Button>
      </Box>
    </div>
  );
};

export default ExternalParticipants;
