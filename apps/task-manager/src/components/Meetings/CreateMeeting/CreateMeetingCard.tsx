import React from 'react';
import { Card, CardHeader, CardContent, makeStyles } from '@material-ui/core';
import CreateMeetingForm from './CreateMeetingForm';

const useStyles = makeStyles({
  root: {
    padding: '16px 16px 0',
    color: '#0A5ED5',
  }
});

const CreateMeetingCard: React.FunctionComponent = () => {
  const classes = useStyles();
  return (
    <Card>
      <CardHeader
        className={classes.root}
        title="New meeting"
      />
      <CardContent>
        <CreateMeetingForm />
      </CardContent>
    </Card>
  );
};

export default CreateMeetingCard;
