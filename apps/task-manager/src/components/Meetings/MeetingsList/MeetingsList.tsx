import React, { useState, useEffect } from 'react';
import { Grid, Card, CardHeader, CardContent } from '@material-ui/core';
import Pagination from '@material-ui/lab/Pagination';
import PeopleAltOutlinedIcon from '@material-ui/icons/PeopleAltOutlined';

import MeetingItem from '../Items/MeetingItem';
import Spinner from '../../../components/Spinner/Spinner';
import NoData from '../../../components/NoData/NoData';
import { useMeetingsStates } from '../../../selectors/meetingsStates';
import { sortMeetingByStartTime } from '../../../utils/meetings';
import { MEETING_STATES } from '../../../constants/meeting-states';
import { Meeting } from '@dar/api-interfaces';

type Props = {
  old: boolean;
  loading: boolean;
  meetings: Meeting[];
};

const MeetingsList: React.FunctionComponent<Props> = ({ old, loading, meetings }) => {
  const limit = 4;

  const [page, setPage] = useState(1);
  const [total, setTotal] = useState(0);
  const [meetingsList, setMeetingsList] = useState([]);
  const meetingsStates = useMeetingsStates();

  useEffect(() => {
    const list = meetings
      .filter(
        (meeting) =>
          meetingsStates[meeting.id] &&
          ((old && meetingsStates[meeting.id].state === MEETING_STATES.OLD) ||
            (!old && meetingsStates[meeting.id].state !== MEETING_STATES.OLD))
      )
      .sort(sortMeetingByStartTime);

    if (old) {
      list.reverse();
    }

    setTotal(list.length);

    setMeetingsList(list);
  }, [meetingsStates, meetings, old]);

  const onPageChange = (_, value) => {
    setPage(value);
  };

  const getSliceValues = () => {
    const start = (page - 1) * limit;
    return [start, start + limit];
  };

  return (
    <Card variant="outlined">
      <CardHeader title={old ? 'History' : 'Upcoming meetings'} />
      {loading ? (
        <Spinner />
      ) : (
        <CardContent>
          {meetingsList.length ? (
            <Grid container spacing={3}>
              {meetingsList.slice(...getSliceValues()).map((meeting) => (
                <Grid item md={6} key={meeting.id}>
                  <MeetingItem meeting={meeting} state={meetingsStates[meeting.id]} />
                </Grid>
              ))}
              {meetingsList.length > 4 && (
                <Grid item container xs={12} justify="center">
                  <Pagination color="primary" count={Math.ceil(total / limit)} page={page} onChange={onPageChange} />
                </Grid>
              )}
            </Grid>
          ) : (
            <NoData text="You have no meetings" Icon={PeopleAltOutlinedIcon} />
          )}
        </CardContent>
      )}
    </Card>
  );
};

export default MeetingsList;
