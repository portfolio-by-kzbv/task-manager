import React, { useEffect, useState } from 'react';
import { makeStyles, TextField } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';

import { useDispatch, useSelector } from 'react-redux';
import { selectMeetingsMeta } from '@dar/selectors/meetings.selector';
import { setMeetingSearchQuery } from '@dar/actions/meetings.action';

const useStyles = makeStyles(() => ({
  textField: {
    fontSize: '35px',
  },
}));

const SearchMeetings: React.FunctionComponent = () => {
  const { q } = useSelector(selectMeetingsMeta);
  const [searchText, setSearchText] = useState(q ? q : '');

  const dispatch = useDispatch();
  const classes = useStyles();

  const onSubmit = (event) => {
    event.preventDefault();
    dispatch(setMeetingSearchQuery(searchText));
  };

  useEffect(() => {
    dispatch(setMeetingSearchQuery(searchText));
  }, [searchText, dispatch]);

  return (
    <form onSubmit={onSubmit}>
      <TextField
        className={classes.textField}
        placeholder="Search meetings"
        fullWidth
        variant="standard"
        InputProps={{ endAdornment: <SearchIcon /> }}
        size="medium"
        value={searchText}
        onChange={(event) => setSearchText(event.target.value)}
      />
    </form>
  );
};

export default SearchMeetings;
