import React from 'react';
import { Fade, CardActions } from '@material-ui/core';

import MeetingActionButtons from '../MeetingActionButtons/MeetingActionButtons';

type Props = {
  visible: boolean;
  canDelete: boolean;
  meetingId: string;
  onDetailsClick: () => void;
  onDeleteClick: () => void;
  isOld: boolean;
};

const MeetingItemActions: React.FunctionComponent<Props> = ({
  visible,
  canDelete,
  meetingId,
  onDetailsClick,
  onDeleteClick,
  isOld,
}) => {
  return (
    <Fade in={visible}>
      <CardActions>
        <MeetingActionButtons
          visible={{ join:!isOld, delete: canDelete, details: true }}
          meetingId={meetingId}
          onDeleteClick={onDeleteClick}
          onDetailsClick={onDetailsClick}
        />
      </CardActions>
    </Fade>
  );
};

export default MeetingItemActions;
