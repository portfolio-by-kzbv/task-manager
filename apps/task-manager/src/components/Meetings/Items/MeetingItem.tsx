import React, { useMemo, useState } from 'react';
import cx from 'classnames';
import { Typography, Card, CardContent } from '@material-ui/core';

import ListAvatars from '../ListAvatars/ListAvatars';
import MeetingDetails from '../MeetingDetails/MeetingDetails';
import PromptModal from '@dar/components/Modal/PromptModal/PromptModal';
import MeetingItemActions from './MeetingItemActions';
import Spinner from '@dar/components/Spinner/Spinner';

import { isStringHtmlCode } from '../../../utils/general';
import { useUserSessionState, useUsersState } from '@dar/selectors/users';

import { cardStyles, headerStyles, meetingHoursStyles, membersStyles } from './meetingsItem.styles';
import { MEETING_STATES } from '../../../constants/meeting-states';
import { useDispatch } from 'react-redux';
import { deleteMeeting } from '@dar/actions/meetings.action';
import { Meeting, Dispatch as CustomDispatch } from '@dar/api-interfaces';

type Props = {
  meeting: Meeting;
  state: { state: string; timeToNowLabel: string; hours: string; date: string };
};

const MeetingItem: React.FunctionComponent<Props> = ({ meeting, state }) => {
  const [hovered, setHovered] = useState(false);
  const [loading, setLoading] = useState(false);
  const [detailsModalOpen, setDetailsModalOpen] = useState(false);
  const [deleteModalOpen, setDeleteModalOpen] = useState(false);

  const users = useUsersState();
  const session = useUserSessionState();

  const headerClasses = headerStyles();
  const cardClasses = cardStyles();
  const membersClasses = membersStyles();
  const hoursClasses = meetingHoursStyles();

  const dispatch: CustomDispatch = useDispatch();

  const participants = useMemo(() => {
    return meeting.participants.map((p) => users.find((u) => u.workEmail?.toLowerCase() === p.toLowerCase()));
  }, [meeting, users]);

  const isOld = useMemo(() => Boolean(state.state === MEETING_STATES.OLD), [state]);
  const isLive = useMemo(() => Boolean(state.state === MEETING_STATES.LIVE), [state]);

  const canDelete = session && meeting.ownerEmail === session.workEmail.toLowerCase();

  const isHtmlCode = useMemo(() => isStringHtmlCode(meeting.description), [meeting]);

  const handleDelete = () => {
    setLoading(true);
    setDeleteModalOpen(false);
    dispatch(deleteMeeting(meeting.id));
  };

  const onDeleteClick = () => {
    setDeleteModalOpen(true);
  };

  const onDetailsClick = () => {
    setDetailsModalOpen(true);
  };

  return (
    <>
      {loading && <Spinner fullscreen />}
      <Card
        classes={cardClasses}
        className={cx('', {
          old: isOld,
          live: isLive,
        })}
        onMouseOver={() => setHovered(true)}
        onMouseOut={() => setHovered(false)}
      >
        <CardContent className={headerClasses.content}>
          <div className={headerClasses.root}>
            <Typography color="primary" variant="h4">
              {state.timeToNowLabel}
            </Typography>
            <Typography
              classes={hoursClasses}
              className={cx('', {
                live: isLive,
              })}
            >
              {isLive ? 'live' : state.hours}
            </Typography>
          </div>
          <Typography variant="h3" gutterBottom>
            {meeting.title}
          </Typography>
          {isHtmlCode ? (
            <div dangerouslySetInnerHTML={{ __html: meeting.description }} />
          ) : (
            <Typography gutterBottom>{meeting.description}</Typography>
          )}
          <div className={membersClasses.root}>
            <Typography variant="h4">Members</Typography>
            <ListAvatars users={participants} limit={5} />
          </div>
        </CardContent>
        <MeetingItemActions
          visible={hovered}
          canDelete={canDelete}
          meetingId={meeting.id}
          onDetailsClick={onDetailsClick}
          onDeleteClick={onDeleteClick}
          isOld={isOld}
        />
      </Card>
      <MeetingDetails
        open={detailsModalOpen}
        onClose={() => setDetailsModalOpen(false)}
        meeting={meeting}
        onDelete={onDeleteClick}
      />
      <PromptModal
        open={deleteModalOpen}
        onClose={() => setDeleteModalOpen(false)}
        onCancel={() => setDeleteModalOpen(false)}
        onConfirm={handleDelete}
        propmtType='danger'
        title={`Delete meeting "${meeting.title}"?`}
        text="A deleted meeting cannot be recovered."
        confirmText="Delete"
        confirmColor="secondary"
      />
    </>
  );
};

export default MeetingItem;
