import { makeStyles } from '@material-ui/core';

export const cardStyles = makeStyles({
  root: {
    display: 'flex',
    flexDirection: 'column',
    height: '100%',
    '&.old': {
      background: '#F1F1F1'
    },
    '&.live': {
      background: '#F9E1E1'
    }
  }
});

export const minItemStyles = makeStyles(theme => ({
  root: {
    width: '100%',
    minHeight: '160px',
    padding: theme.spacing(2),
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'space-between',

    '&.live': {
      background: '#F9E1E1'
    },

    '&:hover, &:focus': {
      cursor: 'pointer',

      '&.MuiPaper-elevation1': {
        boxShadow: '0px 4px 8px rgba(0, 0, 0, 0.2)'
      }
    }
  }
}));

export const headerStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: theme.spacing(2)
  },
  content: {
    flex: "1 auto",
  },
  hours: {}
}));

export const meetingHoursStyles = makeStyles({
  root: {
    '&.live': {
      color: '#EF2E27',

      '&::before': {
        content: '""',
        display: 'inline-block',
        width: '12px',
        height: '12px',
        borderRadius: '50%',
        background: '#EF2E27',
        marginRight: '4px'
      }
    }
  }
});

export const footerStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end'
  },
  icon: {
    marginLeft: theme.spacing(1)
  }
}));

export const membersStyles = makeStyles({
  root: {
    marginTop: '24px'
  }
});
