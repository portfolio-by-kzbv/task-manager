import React, { useState, useMemo } from 'react';
import { Paper, Typography } from '@material-ui/core';
import PeopleAltOutlinedIcon from '@material-ui/icons/PeopleAltOutlined';
import MeetingDetails from '../MeetingDetails/MeetingDetails';

import {
  minItemStyles,
  headerStyles,
  footerStyles,
  meetingHoursStyles
} from './meetingsItem.styles';
import cx from 'classnames';
import { MEETING_STATES } from '../../../constants/meeting-states';

function MeetingMin({ meeting, state }) {
  const [detailsModalOpen, setDetailsModalOpen] = useState(false);

  const paperClasses = minItemStyles();
  const headerClasses = headerStyles();
  const hoursClasses = meetingHoursStyles();
  const footerClasses = footerStyles();

  const isLive = useMemo(() => Boolean(state.state === MEETING_STATES.LIVE), [state]);

  const handleClick = () => {
    setDetailsModalOpen(true);
  };

  return (
    <>
      <Paper
        classes={paperClasses}
        className={cx('', {
          live: isLive
        })}
        onClick={handleClick}
      >
        <div>
          <div className={headerClasses.root}>
            <Typography color="primary" variant="h3">
              {state.timeToNowLabel}
            </Typography>
            <Typography
              classes={hoursClasses}
              className={cx('', {
                live: isLive
              })}
            >
              {isLive ? 'live' : state.hours}
            </Typography>
          </div>
          <Typography variant="h3" gutterBottom>
            {meeting.title}
          </Typography>
        </div>
        <div className={footerClasses.root}>
          {meeting.participants.length}{' '}
          <PeopleAltOutlinedIcon fontSize="small" className={footerClasses.icon} />
        </div>
      </Paper>
      <MeetingDetails
        open={detailsModalOpen}
        onClose={() => setDetailsModalOpen(false)}
        meeting={meeting}
      />
    </>
  );
}

export default MeetingMin;
