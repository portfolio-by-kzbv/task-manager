import React from 'react';
import { Link } from 'react-router-dom';

import { makeStyles, Button } from '@material-ui/core';

const useStyles = makeStyles({
  root: {
    marginLeft: 'auto',
  },
});

type Props = {
  visible: { join: boolean, delete: boolean; details: boolean };
  meetingId: string;
  onDeleteClick: () => void;
  onDetailsClick?: () => void;
};

const MeetingActionButtons: React.FunctionComponent<Props> = ({
  visible,
  meetingId,
  onDeleteClick = () => {
    // do nothing
  },
  onDetailsClick = () => {
    // do nothing
  },
}) => {
  const classes = useStyles();

  return (
    <>
      {visible.join && (
        <Button color="secondary" component={Link} to={`/productivity/meetings/jitsi/${meetingId}`} classes={classes}>
          Join Meeting
        </Button>
      )}
      {visible.delete && (
        <Button color="secondary" onClick={onDeleteClick} classes={classes}>
          Delete
        </Button>
      )}
      {visible.details && (
        <Button color="primary" classes={classes} onClick={onDetailsClick}>
          Details
        </Button>
      )}
    </>
  );
};

export default MeetingActionButtons;
