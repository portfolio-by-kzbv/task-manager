import React, { useState, useEffect, useMemo } from 'react';
import { Link } from 'react-router-dom';
import {
  Card,
  CardHeader,
  Button,
  CardContent,
  List,
  ListItem,
  useTheme,
  useMediaQuery,
} from '@material-ui/core';
import PeopleAltOutlinedIcon from '@material-ui/icons/PeopleAltOutlined';
import Spinner from '@dar/components/Spinner/Spinner';
import MeetingMin from '@dar/components/Meetings/Items/MeetingMin';
import NoData from '@dar/components/NoData/NoData';

import { sortMeetingByStartTime } from '../../../utils/meetings';
import { useMeetingsState } from '@dar/selectors/meetings.selector';
import { useMeetingsStates } from '@dar/selectors/meetingsStates';
import useStyles from './upMeets.styles';

import { MEETING_STATES } from '../../../constants/meeting-states';

const UpcomingMeetings: React.FunctionComponent = () => {
  const classes = useStyles();
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.down('lg'));
  const sliceLimit = useMemo(() => (matches ? 4 : 3), [matches]);
  const [loading, setLoading] = useState(true);
  const [upcomingMeetings, setUpcomingMeetings] = useState([]);

  const meetings = useMeetingsState();
  const meetingsStates = useMeetingsStates();

  useEffect(() => {
    setUpcomingMeetings(
      meetings
        .filter(
          (meeting) =>
            meetingsStates[meeting.id] &&
            meetingsStates[meeting.id].state !== MEETING_STATES.OLD
        )
        .sort(sortMeetingByStartTime)
        .slice(0, sliceLimit)
    );
    setLoading(false);
  }, [meetingsStates, meetings, sliceLimit]);

  return (
    <Card className={classes.card}>
      <CardHeader
        title="Upcoming meetings"
        action={
          <Button component={Link} to="/meetings" color="primary">
            view all
          </Button>
        }
      />
      {loading ? (
        <Spinner />
      ) : (
        <CardContent className={classes.content}>
          {upcomingMeetings.length ? (
            <List className={classes.list}>
              {upcomingMeetings.map((meeting) => (
                <ListItem key={meeting.id} className={classes.listItem}>
                  <MeetingMin
                    meeting={meeting}
                    state={meetingsStates[meeting.id]}
                  />
                </ListItem>
              ))}
            </List>
          ) : (
            <NoData text="You have no meetings" Icon={PeopleAltOutlinedIcon} />
          )}
        </CardContent>
      )}
    </Card>
  );
};

export default UpcomingMeetings;
