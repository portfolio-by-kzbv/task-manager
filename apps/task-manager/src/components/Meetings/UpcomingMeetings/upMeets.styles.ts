import { makeStyles } from '@material-ui/core';

export default makeStyles(theme => ({
  card: {
    minHeight: '314px',
    display: 'flex',
    flexDirection: 'column'
  },
  content: {
    flexGrow: 1,
    display: 'flex',
    flexWrap: 'wrap',
    alignItems: 'center',
    justifyContent: 'center'
  },
  list: {
    width: '100%',
    display: 'flex',
    flexWrap: 'wrap',
    padding: 0
  },
  listItem: {
    width: '33.3%',
    paddingRight: 0,
    '&:first-child': {
      paddingLeft: 0
    },

    [theme.breakpoints.down('lg')]: {
      width: '50%',
      '&:nth-child(3)': {
        paddingLeft: 0
      }
    }
  }
}));
