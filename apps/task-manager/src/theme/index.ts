import { theme } from '@dartech/dms-ui';
import { AvatarGroupClassKey, AvatarGroupProps } from '@material-ui/lab';

declare module '@material-ui/core/styles/overrides' {
  interface ComponentNameToClassKey {
    MuiAvatarGroup: AvatarGroupClassKey;
  }
}
declare module '@material-ui/core/styles/props' {
  interface ComponentsPropsList {
    MuiAvatarGroup: AvatarGroupProps;
  }
}

theme.overrides = {
  ...theme.overrides,
  MuiFilledInput: {
    root: {
      minWidth: 'fit-content',
      backgroundColor: 'transparent',
      '&:hover': { backgroundColor: 'transparent' },
      '&.Mui-focused': { backgroundColor: 'transparent' },
    },
    underline: {
      '&:before, &:after, &:hover:before, &:hover:after': {
        borderBottom: 'none',
      },
    },
  },
  MuiTabs: {
    root: {
      borderBottom: '1px solid #E0E3EA',
      width: '100%',
      overflow: 'unset',
    },
    indicator: {
      backgroundColor: '#387E7F',
      borderRadius: '1px',
      height: '3px',
      bottom: '-1.5px',
    },
    fixed: {
      overflowX: 'unset',
    },
  },
  MuiTab: {
    root: {
      minWidth: 'fit-content',
      padding: '9px 15px',
      textTransform: 'none',
    },
  },
  MuiTableContainer: {
    root: {
      boxShadow: 'none',
      border: `1px solid ${theme.palette.grey[200]}`,
    },
  },
  MuiTableRow: {
    root: {
      borderBottom: `1px solid ${theme.palette.grey[200]}`,
    },
  },
  MuiBackdrop: {
    root: {
      backgroundColor: 'rgba(28, 55, 76, 0.5)',
      backdropFilter: 'blur(4px)',
      zIndex: 1200,
    },
  },
  MuiDialog: {
    paper: {
      zIndex: 1210,
    },
  },
  MuiCheckbox: {
    root: {
      padding: '0px !important',
      color: '#1350D7 !important',
      backgroundColor: 'transparent !important',
      border: '0 !important',
    },
  },
  MuiIconButton: {
    root: {
      color: '#6D7C8B',
      '&:hover': { backgroundColor: 'rgba(109, 124, 139, 0.1)' },
    },
    colorPrimary: {
      color: '#6D7C8B',
      '&:not(.MuiRadio-root):not(.MuiCheckbox-root):not(.Mui-checked):not(.MuiSwitch-switchBase):not(.MuiAutocomplete-clearIndicator) path': {
        fill: '#6D7C8B',
      },
    },
  },
  MuiInputBase: {
    root: {
      background: '#fff',
    },
  },
  MuiOutlinedInput: {
    root: {
      borderRadius: '8px',
    },
  },
  MuiAvatarGroup: {
    root: {
      padding: '8px 0 8px 8px',
    },
    avatar: {
      marginLeft: '-8px',
      marginRight: '0',
    },
  },
  MuiSelect: {
    root: {
      background: '#fff',
    },
  },
};

export default theme;
