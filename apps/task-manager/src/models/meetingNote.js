import * as Yup from 'yup';
import PropTypes from 'prop-types';
import { safeCheckValidity } from '../utils/validation';

export const MeetingNoteSchema = Yup.object()
  .shape({
    id: Yup.string().required(),
    content: Yup.string().required(),
    isChecked: Yup.boolean().required(),
    userId: Yup.string().required(),
    meetingId: Yup.string().required(),
    createdAt: Yup.date().required()
  })
  .strict(true);

export const MeetingNotePropType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  isChecked: PropTypes.bool.isRequired,
  userId: PropTypes.string.isRequired,
  meetingId: PropTypes.string.isRequired,
  createdAt: PropTypes.instanceOf(Date).isRequired
});

/**
 * @class MeetingNote
 * @property {string} id
 * @property {string} content
 * @property {boolean} isChecked
 * @property {string} userId
 * @property {number} meetingId
 * @property {Date} createdAt
 * @method isValid
 * @method data
 * @static from
 */
export class MeetingNote {
  constructor(data) {
    this.id = data.id;
    this.content = data.content;
    this.isChecked = data.isChecked;
    this.userId = data.userId;
    this.meetingId = data.meetingId;
    this.createdAt = new Date(data.createdAt);
  }

  data() {
    return {
      id: this.id,
      content: this.content,
      isChecked: this.isChecked,
      userId: this.userId,
      meetingId: this.meetingId,
      createdAt: this.createdAt
    };
  }

  /**
   * @returns {boolean} - strict model validation
   */
  isValid() {
    return safeCheckValidity(MeetingNoteSchema, this.data());
  }

  static from(data) {
    return new MeetingNote(data);
  }
}
