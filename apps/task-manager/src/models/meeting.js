import moment from 'moment';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
import { safeCheckValidity } from '../utils/validation';
import {
  generateEndMeetingStrictMomentString,
  generateStartMeetingStrictMomentString,
  roundEndMeetingDate,
} from '../utils/meetings';

export const MeetingSchema = Yup.object()
  .shape({
    id: Yup.string().required(),
    title: Yup.string().required(),
    description: Yup.string(),
    startTime: Yup.date().required(),
    endTime: Yup.date().required(),
    createdAt: Yup.date().required(),
    updatedAt: Yup.date().required(),
    owner: Yup.string().nullable(),
    externalParticipants: Yup.array(Yup.string()),
    participants: Yup.array(Yup.string()).required(),
    ownerEmail: Yup.string().nullable(),
    location: Yup.string().nullable(),
    videoConfUrl: Yup.string().nullable(),
    recurrence: Yup.string().nullable(),
    recurringEventId: Yup.string().nullable(),
    status: Yup.string().nullable(),
  })
  .strict(true);

export const MeetingPropType = PropTypes.shape({
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  startTime: PropTypes.instanceOf(Date).isRequired,
  endTime: PropTypes.instanceOf(Date).isRequired,
  createdAt: PropTypes.instanceOf(Date).isRequired,
  updatedAt: PropTypes.instanceOf(Date).isRequired,
  owner: PropTypes.string,
  participants: PropTypes.arrayOf(PropTypes.string).isRequired,
  externalParticipants: PropTypes.arrayOf(PropTypes.string).isRequired,
  ownerEmail: PropTypes.string,
  location: PropTypes.string,
  videoConfUrl: PropTypes.string,
  recurrence: PropTypes.string,
  recurringEventId: PropTypes.string,
  status: PropTypes.string,
});

/**
 * @typedef {object} Meeting
 * @property {number} id
 * @property {string} owner
 * @property {String} title
 * @property {String} description
 * @property {Date} startTime
 * @property {Date} endTime
 * @property {Date} createdAt
 * @property {Date} updatedAt
 * @property {object[]} participants
 * @property {string[]} externalParticipants
 * @property {string} ownerEmail
 * @property {string} location
 * @property {string} videoConfUrl
 */
export class Meeting {
  timeFormat = 'HH:mm';

  constructor(data) {
    this.id = data.id;
    this.title = data.title;
    this.description = data.description;
    this.startTime = new Date(data.startTime);
    this.endTime = new Date(data.endTime);
    this.createdAt = data.createdAt ? new Date(data.createdAt) : null;
    this.updatedAt = data.createdAt ? new Date(data.updatedAt) : null;
    this.owner = data.owner;
    this.participants = data.participants;
    this.externalParticipants = data.externalParticipants;
    this.ownerEmail = data.ownerEmail;
    this.location = data.location;
    this.videoConfUrl = data.videoConfUrl;
    this.recurrence = data.recurrence;
    this.recurringEventId = data.recurringEventId;
    this.status = data.status;
  }

  /**
   * @returns {Meeting} - plain object
   */
  data() {
    return {
      id: this.id,
      title: this.title,
      description: this.description,
      startTime: this.startTime,
      endTime: this.endTime,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt,
      owner: this.owner,
      participants: this.participants,
      externalParticipants: this.externalParticipants,
      ownerEmail: this.ownerEmail,
      location: this.location,
      videoConfUrl: this.videoConfUrl,
      recurrence: this.recurrence,
      recurringEventId: this.recurringEventId,
      status: this.status,
    };
  }

  createPayload() {
    return {
      title: this.title,
      description: this.description,
      startTime: generateStartMeetingStrictMomentString(this.startTime),
      endTime: generateEndMeetingStrictMomentString(this.endTime),
      participants: this.participants,
      externalParticipants: this.externalParticipants || [],
      ownerEmail: this.ownerEmail || null,
      recurrence: this.recurrence || '',
    };
  }

  editPayload() {
    return {
      id: this.id,
      recurringEventId: this.recurringEventId,
      title: this.title,
      description: this.description || '',
      startTime: generateStartMeetingStrictMomentString(this.startTime),
      endTime: generateEndMeetingStrictMomentString(this.endTime),
      createdAt: generateStartMeetingStrictMomentString(this.createdAt),
      updatedAt: generateStartMeetingStrictMomentString(this.updatedAt),
      owner: this.owner,
      participants: this.participants,
      externalParticipants: this.externalParticipants || [],
      ownerEmail: this.ownerEmail || null,
      location: this.location,
      videoConfUrl: this.videoConfUrl,
      recurrence: this.recurrence || null,
      status: this.status,
    };
  }

  /**
   * @returns {boolean} - strict model validation
   */
  isValid() {
    return safeCheckValidity(MeetingSchema, this.data());
  }

  /**
   * @returns {string}
   */
  startTimeLabel() {
    return moment(this.startTime).format(this.timeFormat);
  }
  /**
   * @returns {string}
   */
  endTimeLabel() {
    return moment(roundEndMeetingDate(this.endTime)).format(this.timeFormat);
  }
  /**
   * @returns {string}
   */
  timeRangeLabel() {
    return `${this.startTimeLabel()} - ${this.endTimeLabel()}`;
  }

  static from(data) {
    return new Meeting(data);
  }
}
