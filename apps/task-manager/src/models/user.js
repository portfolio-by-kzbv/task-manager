import * as Yup from 'yup';
import PropTypes from 'prop-types';
import { safeCheckValidity } from 'utils/validation';

export const UserSchema = Yup.object()
  .shape({
    id: Yup.string().required(),
    firstName: Yup.string().required(),
    middleName: Yup.string()
      .nullable()
      .notRequired(),
    lastName: Yup.string().required(),
    position: Yup.string()
      .nullable()
      .notRequired(),
    phoneNumber: Yup.string()
      .nullable()
      .notRequired(),
    company: Yup.string()
      .nullable()
      .notRequired(),
    photo: Yup.string()
      .nullable()
      .url()
      .notRequired(),
    login: Yup.string().required(),
    email: Yup.string()
      .email()
      .required()
  })
  .strict(true);

/**
 * @typedef {object} UserData
 * @property {string} id
 * @property {string} fullName
 * @property {string} firstName
 * @property {string} lastName
 * @property {string} role
 * @property {string} email
 * @property {string} logName
 * @property {string | null} avatar
 */
export const UserPropType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  fullName: PropTypes.string.isRequired,
  firstName: PropTypes.string.isRequired,
  lastName: PropTypes.string.isRequired,
  role: PropTypes.string.isRequired,
  email: PropTypes.string.isRequired,
  avatar: PropTypes.string
});

/**
 * @typedef {object} User
 * @property {string} id
 * @property {string} firstName
 * @property {string | null} middleName
 * @property {string} lastName
 * @property {string | null} photo
 * @property {string} login
 * @property {string} email
 * @property {string | null} position
 * @property {string | null} phoneNumber
 * @property {string | null} company
 */
export class User {
  constructor(data) {
    this.id = data.id;
    this.firstName = data.firstName;
    this.middleName = data.middleName;
    this.lastName = data.lastName;
    this.photo = data.photo;
    this.login = data.login;
    this.email = data.email;
    this.position = data.position;
    this.phoneNumber = data.phoneNumber;
    this.company = data.company;
  }

  /**
   * @returns UserData
   */
  data() {
    return {
      id: this.id,
      fullName: `${this.firstName} ${this.lastName}`.trim(),
      firstName: this.firstName,
      lastName: this.lastName,
      role: this.position || '',
      email: this.email,
      avatar: this.photo,
      logName: this.email.substring(0, this.email.lastIndexOf('@')).toLowerCase(),
      phoneNumber: this.phoneNumber
    };
  }

  /**
   * @returns User
   */
  apiDataModel() {
    return {
      id: this.id,
      firstName: this.firstName,
      middleName: this.middleName,
      lastName: this.lastName,
      photo: this.photo,
      login: this.login,
      email: this.email,
      position: this.position,
      phoneNumber: this.phoneNumber,
      company: this.company
    };
  }

  isValid() {
    return safeCheckValidity(UserSchema, this.apiDataModel());
  }

  static from(data) {
    return new User(data);
  }
}
