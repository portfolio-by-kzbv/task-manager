import moment from 'moment';
import * as Yup from 'yup';
import PropTypes from 'prop-types';
import { safeCheckValidity } from '../utils/validation';

export const MeetingMoMSchema = Yup.object()
  .shape({
    id: Yup.string().required(),
    content: Yup.string().required(),
    meetingId: Yup.string().required(),
    createdAt: Yup.date().required(),
    updatedAt: Yup.date().required()
  })
  .strict(true);

export const MeetingMoMPropType = PropTypes.shape({
  id: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired,
  meetingId: PropTypes.string.isRequired,
  createdAt: PropTypes.instanceOf(Date).isRequired,
  updatedAt: PropTypes.instanceOf(Date).isRequired
});

/**
 * @typedef {object} MeetingMoM
 * @property {string} id
 * @property {string} content
 * @property {number} meetingId
 * @property {Date} createdAt
 * @property {Date} updatedAt
 */
export class MeetingMoM {
  constructor(data) {
    this.id = data.id;
    this.content = data.content;
    this.meetingId = data.meetingId;
    this.createdAt = data.createdAt ? new Date(data.createdAt) : null;
    this.updatedAt = data.updatedAt ? new Date(data.updatedAt) : null;
  }

  /**
   * @returns {MeetingMoM} - plain object
   */
  data() {
    return {
      id: this.id,
      content: this.content,
      meetingId: this.meetingId,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    };
  }

  createPayload() {
    return {
      id: this.id,
      content: this.content,
      meetingId: this.meetingId,
      createdAt: (this.createdAt ? moment(this.createdAt) : moment()).toISOString(),
      updatedAt: moment().toISOString()
    };
  }

  /**
   * @returns {boolean} - strict model validation
   */
  isValid() {
    return safeCheckValidity(MeetingMoMSchema, this.data());
  }

  /**
   * @returns {MeetingMoM} - class instance
   */
  static from(data) {
    return new MeetingMoM(data);
  }
}
