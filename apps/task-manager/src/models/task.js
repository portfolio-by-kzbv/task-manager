import * as Yup from 'yup';
import PropTypes from 'prop-types';
import { safeCheckValidity } from 'utils/validation';

export const TaskSchema = Yup.object()
  .shape({
    id: Yup.number().required(),
    group_id: Yup.number().nullable(),
    project_id: Yup.number().nullable(),
    company_id: Yup.number().nullable(),
    creator_id: Yup.string().required(),
    reporter_id: Yup.string().nullable(),
    assignee_id: Yup.string().required(),
    title: Yup.string().required(),
    description: Yup.string().nullable(),
    status: Yup.string().nullable(),
    status_id: Yup.number().nullable(),
    priority: Yup.string().nullable(),
    start_time: Yup.date().nullable(),
    end_time: Yup.date().nullable()
  })
  .strict(true);

export const TaskPropType = PropTypes.shape({
  id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  group_id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  project_id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  company_id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  creator_id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  reporter_id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  assignee_id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  title: PropTypes.string.isRequired,
  description: PropTypes.string,
  status: PropTypes.string,
  status_id: PropTypes.oneOfType([PropTypes.string, PropTypes.number]).isRequired,
  priority: PropTypes.string,
  start_time: PropTypes.instanceOf(Date),
  end_time: PropTypes.instanceOf(Date)
});

/**
 * @typedef {object} Task
 * @property {number} id
 * @property {number} group_id
 * @property {number} project_id
 * @property {number} company_id
 * @property {number} creator_id
 * @property {number} reporter_id
 * @property {number} assignee_id
 * @property {String} title
 * @property {String} description
 * @property {String} status
 * @property {number} status_id
 * @property {String} priority
 * @property {Date} start_time
 * @property {Date} end_time
 */
export class Task {
  timeFormat = 'HH:mm';

  constructor(data) {
    this.id = data.id;
    this.group_id = data.group_id;
    this.project_id = data.project_id;
    this.company_id = data.company_id;
    this.creator_id = data.creator_id;
    this.reporter_id = data.reporter_id;
    this.assignee_id = data.assignee_id;
    this.title = data.title;
    this.description = data.description;
    this.status = data.status;
    this.status_id = data.status_id;
    this.priority = data.priority;
    this.start_time = new Date(data.start_time);
    this.end_time = new Date(data.end_time);
  }

  /**
   * @returns {Task} - plain object
   */
  data() {
    return {
      id: this.id,
      group_id: this.group_id,
      project_id: this.project_id,
      company_id: this.company_id,
      creator_id: this.creator_id,
      reporter_id: this.reporter_id,
      assignee_id: this.assignee_id,
      title: this.title,
      description: this.description,
      status: this.status,
      status_id: this.status_id,
      priority: this.priority,
      start_time: this.start_time,
      end_time: this.end_time
    };
  }

  createPayload() {
    return {
      creator_id: this.creator_id,
      assignee_id: this.assignee_id,
      title: this.title,
      description: this.description,
      status: this.status,
      status_id: this.status_id,
      priority: this.priority,
      start_time: this.start_time,
      end_time: this.end_time
    };
  }

  editPayload() {
    return {
      id: this.id,
      assignee_id: this.assignee_id,
      title: this.title,
      description: this.description || '',
      status_id: this.status_id,
      priority: this.priority,
      start_time: this.start_time,
      end_time: this.end_time
    };
  }

  /**
   * @returns {boolean} - strict model validation
   */
  isValid() {
    return safeCheckValidity(TaskSchema, this.data());
  }

  static from(data) {
    return new Task(data);
  }
}
