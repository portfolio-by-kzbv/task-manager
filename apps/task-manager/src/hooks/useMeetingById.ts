import { useMemo } from 'react';
import { useMeetingsState } from '../selectors/meetings.selector';

export default function useMeetingById(id) {
  const meetings = useMeetingsState();
  return useMemo(
    () => meetings.find((item) => String(item.id) === String(id)) || null,
    [id, meetings]
  );
}
