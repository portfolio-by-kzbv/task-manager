export const MEETING_STATES = {
  FUTURE: 'FUTURE',
  NEW: 'NEW',
  LIVE: 'LIVE',
  OLD: 'OLD'
};
