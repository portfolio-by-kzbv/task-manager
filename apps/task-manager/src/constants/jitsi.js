export default {
  API_LINK: 'https://besedka-jitsi.dar-dev.zone/external_api.js',
  SCRIPT_ID: 'jitsiScript',
  CONFERENCE_FRAME_ID: 'jitsiConferenceFrame0',
  DOMAIN: 'besedka-jitsi.dar-dev.zone',
  CONTAINER_ID: 'jitsi-container',
  FRAME_HEIGHT: '700px',
  DETAILS_MENU_WIDTH: '373px',
  BACKGROUND_GRADIENT: 'radial-gradient(50% 50% at 50% 50%,#2a3a4b 20.83%,#1e2a36 100%)',
  VIEW_CONTAINER_ID: 'jitsiView'
};
