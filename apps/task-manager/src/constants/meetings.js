export const MEETING_STATES = {
  FUTURE: 'FUTURE',
  NEW: 'NEW',
  LIVE: 'LIVE',
  OLD: 'OLD'
};

export const MAX_MEETINGS_PER_REQUEST = 200;

Object.freeze(MEETING_STATES);

export const PER_PAGE_LIMITS = [6, 10, 20, 50];

Object.freeze(PER_PAGE_LIMITS);
