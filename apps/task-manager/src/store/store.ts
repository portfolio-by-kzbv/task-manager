import { createStore, applyMiddleware, compose } from 'redux';
import promiseMiddleware from 'redux-promise-middleware';
import thunk from 'redux-thunk';

import { rootReducer } from './reducers';

const middleware = [promiseMiddleware, thunk];
const composeEnhancers =
  process.env.NODE_ENV !== 'production' &&
  typeof window === 'object' &&
  (window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] as typeof compose)
    ? (window['__REDUX_DEVTOOLS_EXTENSION_COMPOSE__'] as typeof compose)
    : compose;
const enhancer = composeEnhancers(applyMiddleware(...middleware));

export default createStore(rootReducer, enhancer);
