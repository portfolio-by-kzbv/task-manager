import { USERS_ACTIONS } from '../actions/users.action';

const initState = {
  users: [],
  user: null,
  loading: false,
};

export default (state = initState, action) => {
  switch (action.type) {
    case `${USERS_ACTIONS.LIST}_PENDING`: {
      return {
        ...state,
        loading: true,
        users: [],
      };
    }
    case `${USERS_ACTIONS.LIST}_FULFILLED`: {
      return {
        ...state,
        loading: false,
        users: action.payload,
      };
    }
    case `${USERS_ACTIONS.GET}_PENDING`: {
      return {
        ...state,
        loading: true,
        user: {},
      };
    }
    case `${USERS_ACTIONS.GET}_FULFILLED`: {
      return {
        ...state,
        loading: false,
        user: action.payload,
      };
    }
    default:
      return state;
  }
};
