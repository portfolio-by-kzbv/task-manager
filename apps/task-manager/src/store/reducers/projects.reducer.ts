import { Project } from '@dar/api-interfaces';
import { PROJECTS_ACTIONS } from '../actions/projects.actions';

interface ProjectStore {
  projects: Project[];
  project: Project;
  loading: boolean;
}

const initState: ProjectStore = {
  projects: [],
  project: null,
  loading: false,
};

export default (state: ProjectStore = initState, action) => {
  switch (action.type) {
    case `${PROJECTS_ACTIONS.LIST}_PENDING`: {
      return {
        ...state,
        loading: true,
        projects: [],
      };
    }
    case `${PROJECTS_ACTIONS.LIST}_FULFILLED`: {
      return {
        ...state,
        loading: false,
        projects: action.payload,
      };
    }
    case `${PROJECTS_ACTIONS.GET}_PENDING`: {
      return {
        ...state,
        loading: true,
        project: [],
      };
    }
    case `${PROJECTS_ACTIONS.GET}_FULFILLED`: {
      return {
        ...state,
        loading: false,
        project: action.payload,
      };
    }
    default:
      return state;
  }
};
