import { MEETING_RECORDINGS_ACTIONS } from '../actions/meetingRecordings';
import { MeetingRecording } from '../../models/meetingRecording';

const initState = {};

export default (state = initState, action) => {
  switch (action.type) {
    case `${MEETING_RECORDINGS_ACTIONS.GET}_FULFILLED`:
      return {
        ...state,
        [action.meta.meetingId]: [...action.payload]
          .map(data => MeetingRecording.fromApiData(data))
          .filter(recording => recording.isValid() && recording.isAudioType())
          .map(recording => recording.data())
      };
    case `${MEETING_RECORDINGS_ACTIONS.DELETE}_FULFILLED`:
      return {
        ...state,
        [action.meta.meetingId]: state[action.meta.meetingId].filter(recording => recording.id !== action.meta.id)
      };
    default:
      return state;
  }
};
