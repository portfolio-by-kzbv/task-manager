import { JIRA_ISSUES_ACTIONS } from '../actions/jiraIssues';

export default (state = {}, action) => {
  switch (action.type) {
    case `${JIRA_ISSUES_ACTIONS.GET_MEETING}_FULFILLED`: {
      return {
        ...state,
        [action.meta.meeting]: action.payload
      };
    }
    case `${JIRA_ISSUES_ACTIONS.CREATE}_FULFILLED`: {
      if (action.payload.properties.dmsMeeting) {
        const id = action.payload.properties.dmsMeeting.value;
        const _state = { ...state };
        const meetingIssues = [..._state[id]] || [];
        meetingIssues.unshift(action.payload);
        _state[id] = meetingIssues;
        return _state;
      }
      return state;
    }
    default:
      return state;
  }
};
