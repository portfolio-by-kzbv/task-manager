import { combineReducers } from 'redux';

import tasks from './tasks.reducer';
import statuses from './task-status.reducer';
import checklist from './checklist.reducer';
import users from './users.reducer';
import clusters from './clusters.reducer';
import projects from './projects.reducer';
import meetingsStates from './meetingsStates';
import meetingsReducer from './meetings.reducer';
import meetingMomReducer from './meeting-mom.reducer';
import meetingsNotesReducer from './meeting-notes.reducer';
import links from './links.reducer';
import comments from './task-comments.reducer';
import attachments from './task-attachments.reducer';

export const rootReducer = combineReducers({
  tasks: tasks,
  statuses: statuses,
  checklist: checklist,
  users: users,
  clusters: clusters,
  projects: projects,
  meetings: meetingsReducer,
  meetingMom: meetingMomReducer,
  meetingNotes: meetingsNotesReducer,
  meetingsStates: meetingsStates,
  links: links,
  comments: comments,
  attachments: attachments,
});

export type AppState = ReturnType<typeof rootReducer>;
