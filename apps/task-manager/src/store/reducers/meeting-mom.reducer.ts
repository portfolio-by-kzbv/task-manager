import { MoM } from '@dar/api-interfaces';
import { MEETING_MINUTES_ACTIONS } from '../actions/meeting-mom.action';

interface MoMState {
  mom: MoM;
  deleteMomModalOpen: boolean;
  deleteMomId: number;
}

const initState: MoMState = {
  mom: null,
  deleteMomModalOpen: false,
  deleteMomId: null,
};

export default (state: MoMState = initState, action) => {
  switch (action.type) {
    case `${MEETING_MINUTES_ACTIONS.GET}_PENDING`: {
      return {
        ...state,
        mom: null,
      };
    }
    case `${MEETING_MINUTES_ACTIONS.GET}_FULFILLED`: {
      return {
        ...state,
        mom: action.payload,
      };
    }
    case `${MEETING_MINUTES_ACTIONS.CREATE}_FULFILLED`: {
      const mom = action.payload;
      if (!mom) {
        return state;
      }
      return {
        ...state,
        mom: action.payload,
      };
    }
    case `${MEETING_MINUTES_ACTIONS.EDIT}_FULFILLED`: {
      return state;
    }
    case `${MEETING_MINUTES_ACTIONS.DELETE}_FULFILLED`: {
      return {
        ...state,
        mom: null,
      };
    }
    case MEETING_MINUTES_ACTIONS.SET_DELETE_MOM_ID: {
      return {
        ...state,
        deleteMomModalOpen: action.payload ? true : false,
        deleteMomId: action.payload,
      };
    }
    default:
      return state;
  }
};
