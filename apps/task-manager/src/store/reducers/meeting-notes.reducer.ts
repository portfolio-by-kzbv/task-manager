import { MEETING_NOTES_ACTIONS } from '../actions/meeting-notes.action';
import { Notes } from '@dar/api-interfaces';

interface NotesState {
  notes: Notes[];
  deleteNoteId: number;
  deleteModalOpen: boolean;
}

const initState: NotesState = {
  notes: [],
  deleteNoteId: null,
  deleteModalOpen: false,
};

export default (state: NotesState = initState, action) => {
  switch (action.type) {
    case `${MEETING_NOTES_ACTIONS.LIST}_PENDING`: {
      return {
        ...state,
        notes: [],
      };
    }
    case `${MEETING_NOTES_ACTIONS.LIST}_FULFILLED`: {
      return {
        ...state,
        notes: action.payload,
      };
    }
    case `${MEETING_NOTES_ACTIONS.CREATE}_FULFILLED`: {
      const note = action.payload;
      if (!note) {
        return state;
      }
      return {
        ...state,
        notes: [...state.notes, note],
      };
    }
    case `${MEETING_NOTES_ACTIONS.EDIT}_FULFILLED`: {
      const editedNote = action.payload;
      if (!editedNote) {
        return state;
      }
      return {
        ...state,
        notes: [...state.notes.map((note) => (note.id === editedNote.id ? editedNote : note))],
      };
    }
    case `${MEETING_NOTES_ACTIONS.REMOVE}_FULFILLED`: {
      return {
        ...state,
        notes: [...state.notes.filter(note => note.id !== action.meta.noteId)]
      };
    }
    case MEETING_NOTES_ACTIONS.SET_DELETE_ID:
      return {
        ...state,
        deleteModalOpen: action.payload ? true : false,
        deleteNoteId: action.payload,
      };
    default:
      return state;
  }
};
