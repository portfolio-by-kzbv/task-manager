import { CHECKLIST_ACTIONS } from '../actions/checklist.action';

const initState = {
  checklist: [],
  loading: false
};

export default (state = initState, action) => {
  switch (action.type) {
    case `${CHECKLIST_ACTIONS.GET}_PENDING`: {
      return {
        loading: true,
        checklist: []
      };
    }
    case `${CHECKLIST_ACTIONS.GET}_FULFILLED`: {
      return {
        loading: false,
        checklist: [...action.payload]
      };
    }
    case `${CHECKLIST_ACTIONS.CREATE}_FULFILLED`: {
      const checklist = action.payload;
      if (!checklist) {
        return state;
      } 
      return {
        ...state,
        checklist: [...state.checklist, checklist]
      };
    }
    case `${CHECKLIST_ACTIONS.EDIT}_FULFILLED`: {
      const editedChecklist = action.payload;
      if (!editedChecklist) {
        return state;
      }
      return {
        ...state,
        checklist: [
          ...state.checklist.map(todo => (todo.id === editedChecklist.id ? editedChecklist : todo))
        ]
      };
    }
    case `${CHECKLIST_ACTIONS.DELETE}_FULFILLED`: {
      return {
        ...state,
        checklist: [...state.checklist.filter(todo => todo.id !== action.meta.id)]
      };
    }
    case CHECKLIST_ACTIONS.REORDER:
      return {
        ...state,
        checklist: action.payload
      };
    default:
      return state;
  }
};
