import { LinksResponse, Task } from '@dar/api-interfaces';
import { LINKS_ACTIONS } from '../actions/links.action';

interface LinkStore {
  links: LinksResponse;
  affects: Task[];
  depends: Task[];
  linksTasks: Task[];
  loading: boolean;
}

const initState: LinkStore = {
  links: null,
  affects: [],
  depends: [],
  linksTasks: [],
  loading: false,
};

export default (state: LinkStore = initState, action): LinkStore => {
  switch (action.type) {
    case `${LINKS_ACTIONS.GET}_PENDING`:
      return {
        ...state,
        links: null,
        loading: true,
      };
    case `${LINKS_ACTIONS.GET}_FULFILLED`:
      return {
        ...state,
        links: action.payload,
        loading: false,
      };
    case `${LINKS_ACTIONS.AFFECTS_GET}_PENDING`:
      return {
        ...state,
        affects: action.payload,
        loading: true,
      };
    case `${LINKS_ACTIONS.AFFECTS_GET}_FULFILLED`:
      return {
        ...state,
        affects: action.payload,
        loading: false,
      };
    case `${LINKS_ACTIONS.DEPENDS_GET}_PENDING`:
      return {
        ...state,
        depends: action.payload,
        loading: true,
      };
    case `${LINKS_ACTIONS.DEPENDS_GET}_FULFILLED`:
      return {
        ...state,
        depends: action.payload,
        loading: false,
      };
    case `${LINKS_ACTIONS.TASKS_GET}_PENDING`:
      return {
        ...state,
        linksTasks: action.payload,
        loading: true,
      };
    case `${LINKS_ACTIONS.TASKS_GET}_FULFILLED`:
      return {
        ...state,
        linksTasks: action.payload,
        loading: false,
      };
    default:
      return state;
  }
};
