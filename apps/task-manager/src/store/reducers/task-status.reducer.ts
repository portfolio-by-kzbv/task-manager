import { STATUS_ACTIONS } from '../actions/task-status.action';

const initState = {
  statuses: []
};

export default (state = initState, action) => {
  switch (action.type) {
    case `${STATUS_ACTIONS.GET}_FULFILLED`: {
      return {
        ...state,
        statuses: [...action.payload]
      };
    }
    default:
      return state;
  }
};
