import { Comment } from '@dar/api-interfaces';
import { COMMENTS_ACTIONS } from '../actions/task-comments.action';

interface CommentsStore {
  comments: Comment[];
  loading: boolean;
}

const initState = {
  comments: [],
  loading: false,
};

export default (state = initState, action): CommentsStore => {
  switch (action.type) {
    case `${COMMENTS_ACTIONS.GET}_PENDING`:
      return {
        ...state,
        comments: null,
        loading: true,
      };
    case `${COMMENTS_ACTIONS.GET}_FULFILLED`:
      return {
        ...state,
        comments: action.payload,
        loading: false,
      };
    case `${COMMENTS_ACTIONS.CREATE}_PENDING`: {
      return {
        ...state,
        loading: true,
      };
    }
    case `${COMMENTS_ACTIONS.EDIT}_PENDING`: {
      return {
        ...state,
        loading: true,
      };
    }
    case COMMENTS_ACTIONS.UPDATE: {
      const comment = action.payload;
      return {
        ...state,
        comments: [...state.comments.map((item) => (item.id === comment.id ? comment : item))],
        loading: false,
      };
    }
    case `${COMMENTS_ACTIONS.DELETE}_PENDING`: {
      return {
        ...state,
        loading: true,
      };
    }
    case `${COMMENTS_ACTIONS.DELETE}_FULFILLED`: {
      return {
        ...state,
        comments: [...state.comments.filter((comment) => comment.id !== action.meta.id)],
        loading: false,
      };
    }
    case COMMENTS_ACTIONS.APPEND: {
      const comment = action.payload;
      return {
        ...state,
        comments: [...state.comments, comment],
        loading: false,
      };
    }
    default:
      return state;
  }
};
