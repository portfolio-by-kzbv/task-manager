import { MEETING_ACTIONS } from '../actions/meetings.action';
import { Meeting } from '../../models/meeting';

/**
 * @typedef {object} IMeetingsState
 * @property {Meeting[]} meetings
 * @property {number} page
 * @property {number} limit
 * @property {number} total
 * @property {string | null} q
 */

/**
 * @type {IMeetingsState}
 */
const initState = {
  meetings: [],
  total: 0,
  q: null,
  newMeetingsCount: 0,
};

/**
 * @param {IMeetingsState} state
 * @param {Object} action
 * @return {IMeetingsState}
 */
export default (state = initState, action) => {
  switch (action.type) {
    case MEETING_ACTIONS.SET_QUERY: {
      return {
        ...state,
        q: action.payload || '',
      };
    }
    case `${MEETING_ACTIONS.CREATE}_FULFILLED`: {
      const meeting = action.payload && Meeting.from(action.payload);
      if (!meeting || !meeting.isValid()) {
        return state;
      }
      return {
        ...state,
        meetings: [...state.meetings, meeting.data()],
      };
    }
    case `${MEETING_ACTIONS.EDIT}_FULFILLED`: {
      const meeting = action.payload && Meeting.from(action.payload);
      if (!meeting || !meeting.isValid()) {
        return state;
      }
      return {
        ...state,
        meetings: [...state.meetings.map((item) => (item.id === action.meta.id ? meeting.data() : item))],
      };
    }
    case `${MEETING_ACTIONS.DELETE}_FULFILLED`: {
      return {
        ...state,
        meetings: [...state.meetings.filter((meeting) => meeting.id !== action.meta.id)],
      };
    }
    case `${MEETING_ACTIONS.GET}_FULFILLED`: {
      const meeting = action.payload && Meeting.from(action.payload);
      if (!meeting || !meeting.isValid()) {
        return state;
      }
      return {
        ...state,
        meetings: [...state.meetings.filter((item) => item.id !== meeting.id), meeting.data()],
      };
    }
    case `${MEETING_ACTIONS.LIST}_FULFILLED`: {
      if (!isListPayloadValid(action.payload)) {
        return state;
      }
      const { meetings } = action.payload;

      const newState = { meetings };

      if (meetings.length !== state.meetings.length) {
        newState.meetings = [
          ...meetings
            .filter(Boolean)
            .map((item) => Meeting.from(item))
            .filter((item) => item.isValid())
            .map((item) => item.data()),
        ];
      }

      return {
        ...state,
        ...newState,
      };
    }
    default:
      return state;
  }
};

function isListPayloadValid(payload) {
  return Array.isArray(payload.meetings) && typeof payload.meta === 'object' && typeof payload.meta.total === 'number';
}
