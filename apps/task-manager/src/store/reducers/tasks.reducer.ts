import { TASKS_ACTIONS } from '../actions/tasks.action';

const initState = {
  tasks: [],
  loading: false,
  filteredTasks: [],
  isFiltered: false,
};

export default (state = initState, action) => {
  switch (action.type) {
    case `${TASKS_ACTIONS.CREATE}_FULFILLED`: {
      const task = action.payload;
      return {
        ...state,
        tasks: [...state.tasks, task],
      };
    }
    case `${TASKS_ACTIONS.EDIT}_FULFILLED`: {
      const task = action.payload;
      return {
        ...state,
        tasks: [...state.tasks.map((item) => (item.id === action.meta.id ? task : item))],
      };
    }
    case `${TASKS_ACTIONS.DELETE}_PENDING`: {
      return {
        ...state,
        loading: true,
      };
    }
    case `${TASKS_ACTIONS.DELETE}_FULFILLED`: {
      return {
        ...state,
        loading: false,
        tasks: [...state.tasks.filter((task) => task.id !== action.meta.id)],
      };
    }
    case `${TASKS_ACTIONS.LIST}_PENDING`: {
      return {
        ...state,
        loading: true,
      };
    }
    case `${TASKS_ACTIONS.LIST}_FULFILLED`: {
      return {
        ...state,
        tasks: [...action.payload],
        loading: false,
        isFiltered: false,
      };
    }
    case `${TASKS_ACTIONS.GET}_PENDING`: {
      return {
        ...state,
        loading: true,
      };
    }
    case `${TASKS_ACTIONS.GET}_FULFILLED`: {
      return {
        ...state,
        loading: false,
      };
    }
    case `${TASKS_ACTIONS.FILTER_TASKS}_FULFILLED`: {
      return {
        ...state,
        filteredTasks: [...action.payload],
        isFiltered: true,
      };
    }
    default:
      return state;
  }
};
