import { Cluster } from '@dar/api-interfaces';
import { CLUSTERS_ACTIONS } from '../actions/clusters.action';

interface ClusterStore {
  clusters: Cluster[];
  cluster: Cluster;
  loading: boolean;
}

const initState: ClusterStore = {
  clusters: [],
  cluster: null,
  loading: false,
};

export default (state: ClusterStore = initState, action) => {
  switch (action.type) {
    case `${CLUSTERS_ACTIONS.LIST}_PENDING`: {
      return {
        ...state,
        loading: true,
        clusters: [],
      };
    }
    case `${CLUSTERS_ACTIONS.LIST}_FULFILLED`: {
      return {
        ...state,
        loading: false,
        clusters: action.payload,
      };
    }
    case `${CLUSTERS_ACTIONS.GET}_PENDING`: {
      return {
        ...state,
        loading: true,
        cluster: null,
      };
    }
    case `${CLUSTERS_ACTIONS.GET}_FULFILLED`: {
      return {
        ...state,
        loading: false,
        cluster: action.payload,
      };
    }
    default:
      return state;
  }
};
