import { Attachment } from '@dar/api-interfaces';
import { ATTACHMENTS_ACTIONS } from '../actions/task-attachments.action';

interface AttachmentsStore {
    attachments: Attachment[],
    loading: boolean,
    attachment: Attachment,
}

const initState = {
    attachments: [],
    loading: false,
    attachment: null
};

export default (state = initState, action): AttachmentsStore => {
  switch (action.type) {
    case `${ATTACHMENTS_ACTIONS.LIST}_PENDING`:
      return {
        ...state,
        attachments: null,
        loading: true,
      };
    case `${ATTACHMENTS_ACTIONS.LIST}_FULFILLED`:
      return {
        ...state,
        attachments: action.payload,
        loading: false,
      };
      case `${ATTACHMENTS_ACTIONS.GET}_PENDING`:
      return {
        ...state,
        attachment: null,
        loading: true,
      };
      case `${ATTACHMENTS_ACTIONS.GET}_FULFILLED`:
        return {
          ...state,
          attachment: action.payload,
          loading: false,
        };
      case `${ATTACHMENTS_ACTIONS.CREATE}_PENDING`: {
        return {
          ...state,
          loading: true,
        };
      }
      case `${ATTACHMENTS_ACTIONS.CREATE}_FULFILLED`: {
        const attachment = action.payload;
        return {
          ...state,
          attachments: [...state.attachments, attachment],
          loading: false,
        };
      }
      case `${ATTACHMENTS_ACTIONS.DELETE}_PENDING`: {
      return {
        ...state,
        loading: true,
      };
    }
    case `${ATTACHMENTS_ACTIONS.DELETE}_FULFILLED`: {
      return {
        ...state,
        attachments: [...state.attachments.filter((attachment) => attachment.id !== action.meta.id)],
        loading: false,
      };
    }
    default:
      return state;
  }
};
