import { MEETING_ACTIONS } from '../actions/meetings.action';
import { SET_MEETING_STATES } from '../actions/meetingsStates';
import { Meeting } from '../../models/meeting';
import * as meetingUtils from '../../utils/meetings';

const initState = {};

export default function (state = initState, action) {
  switch (action.type) {
    case `${MEETING_ACTIONS.CREATE}_FULFILLED`: {
      const meeting = action.payload && Meeting.from(action.payload);
      if (!meeting || !meeting.isValid()) {
        return state;
      }
      return {
        ...state,
        [meeting.id]: meetingUtils.composeMeetingUIState(meeting.data()),
      };
    }
    case `${MEETING_ACTIONS.EDIT}_FULFILLED`: {
      const meeting = action.payload && Meeting.from(action.payload);
      if (!meeting || !meeting.isValid()) {
        return state;
      }
      return {
        ...state,
        [meeting.id]: meetingUtils.composeMeetingUIState(meeting.data()),
      };
    }
    case `${MEETING_ACTIONS.LIST}_FULFILLED`: {
      const { meetings } = action.payload;
      if (!Array.isArray(meetings)) {
        return state;
      }
      return {
        ...state,
        ...meetings
          .filter(Boolean)
          .map((data) => Meeting.from(data))
          .filter((meeting) => meeting.isValid())
          .map((meeting) => meeting.data())
          .reduce((acc, meeting) => {
            acc[meeting.id] = meetingUtils.composeMeetingUIState(meeting);
            return acc;
          }, {}),
      };
    }
    case SET_MEETING_STATES: {
      return {
        ...state,
        ...action.payload,
      };
    }
    default:
      return state;
  }
}
