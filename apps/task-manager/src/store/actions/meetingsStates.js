export const SET_MEETING_STATES = 'SET_MEETING_STATES';

export const setMeetingsStates = payload => ({
  type: SET_MEETING_STATES,
  payload
});
