import api from '../../services/api';

export const STATUS_ACTIONS = {
  GET: 'STATUS_GET'
};

export const getStatuses = () => ({
  type: STATUS_ACTIONS.GET,
  payload: api.taskStatuses.list()
});
