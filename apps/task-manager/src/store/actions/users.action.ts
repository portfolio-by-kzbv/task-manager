import api from '../../services/api';

export const USERS_ACTIONS = {
  LIST: 'USERS_LIST',
  GET: 'USERS_GET',
};

export const getUsers = () => ({
  type: USERS_ACTIONS.LIST,
  payload: api.users.list(),
});

export const getUser = (user_id: number) => ({
  type: USERS_ACTIONS.GET,
  payload: api.users.getUser(user_id),
});
