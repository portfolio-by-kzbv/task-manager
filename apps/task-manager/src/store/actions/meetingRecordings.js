import api from '../../services/api';

export const MEETING_RECORDINGS_ACTIONS = {
  GET: 'MEETING_RECORDINGS_GET',
  DELETE: 'MEETING_RECORDINGS_DELETE'
};

export const listRecordings = meetingId => ({
  type: MEETING_RECORDINGS_ACTIONS.GET,
  payload: api.meetings.getRecordings(meetingId),
  meta: {
    meetingId
  }
});

export const deleteRecording = (meetingId, id) => ({
  type: MEETING_RECORDINGS_ACTIONS.DELETE,
  payload: api.meetings.removeRecording(meetingId, id),
  meta: {
    meetingId,
    id
  }
});
