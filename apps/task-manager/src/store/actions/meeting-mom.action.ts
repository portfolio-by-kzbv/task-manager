import { MoMDTO, MoMQueryParams } from '@dar/api-interfaces';
import api from '../../services/api';

export const MEETING_MINUTES_ACTIONS = {
  GET: 'MEETING_MINUTES_GET',
  CREATE: 'MEETING_MINUTES_CREATE',
  EDIT: 'MEETING_MINUTES_EDIT',
  DELETE: 'MEETING_MINUTES_DELETE',
  SET_DELETE_MOM_ID: 'MEETING_MINUTES_SET_DELETE_MOM_ID',
};

export const getMoM = (params: MoMQueryParams) => ({
  type: MEETING_MINUTES_ACTIONS.GET,
  payload: api.meetings.getMoM(params),
});

export const createMoM = (payload: MoMDTO) => ({
  type: MEETING_MINUTES_ACTIONS.CREATE,
  payload: api.meetings.createMoM(payload),
});

export const editMoM = (momId: number, payload: MoMDTO) => ({
  type: MEETING_MINUTES_ACTIONS.EDIT,
  payload: api.meetings.updateMoM(momId, payload),
});

export const deleteMoM = (momId: number) => ({
  type: MEETING_MINUTES_ACTIONS.DELETE,
  payload: api.meetings.removeMoM(momId),
});

export const setDeleteMomId = (momId: number) => ({
  type: MEETING_MINUTES_ACTIONS.SET_DELETE_MOM_ID,
  payload: momId,
});
