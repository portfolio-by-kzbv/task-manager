import api from '../../services/api';

export enum ATTACHMENTS_ACTIONS {
  GET = 'ATTACHMENTS_GET',
  LIST = 'ATTACHMENTS_LIST',
  LIST_COMMENTS_ATTACHMENTS = 'COMMENTS_ATTACHMENTS_LIST',
  CREATE = 'ATTACHMENTS_UPLOAD',
  DELETE = 'ATTACHMENTS_DELETE',
  EDIT = 'ATTACHMENTS_EDIT',
}

export const getAttachments = (params) => ({
  type: ATTACHMENTS_ACTIONS.LIST,
  payload: api.attachments.getAttachments(params),
});

export const getCommentsAttachments = (params) => ({
  type: ATTACHMENTS_ACTIONS.LIST_COMMENTS_ATTACHMENTS,
  payload: api.attachments.getAttachments(params),
});

export const getAttachment = (id: number) => ({
  type: ATTACHMENTS_ACTIONS.LIST,
  payload: api.attachments.getAttachment(id),
});

export const uploadCommentAttachment = (task_id: number, comment_id: number, data) => ({
  type: ATTACHMENTS_ACTIONS.CREATE,
  payload: api.attachments.uploadCommentAttachment(task_id, comment_id, data),
});

export const uploadAttachment = (task_id: number, data) => ({
  type: ATTACHMENTS_ACTIONS.CREATE,
  payload: api.attachments.uploadAttachment(task_id, data),
});

export const deleteAttachment = (attachment_id: number) => ({
  type: ATTACHMENTS_ACTIONS.DELETE,
  payload: api.attachments.deleteAttachment(attachment_id),
  meta: { id: attachment_id },
});
