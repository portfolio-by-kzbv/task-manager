import api from '../../services/api';
import { TaskDTO, StatusDTO } from '@dar/api-interfaces';

export const TASKS_ACTIONS = {
  CREATE: 'TASKS_CREATE',
  EDIT: 'TASKS_EDIT',
  EDIT_STATUS: 'EDIT_STATUS',
  DELETE: 'TASKS_DELETE',
  GET: 'TASKS_GET',
  LIST: 'TASKS_LIST',
  FILTER_TASKS: 'FILTER_TASKS',
};

export const getAllTasks = (params, hideSelfAssigned?: boolean) => ({
  type: TASKS_ACTIONS.LIST,
  payload: api.tasks.getAllTasks(params).then(tasks => hideSelfAssigned ? tasks.filter(task => task.assignee_id !== params.q.creator_id) : tasks),
});

export const filterTasks = (params) => ({
  type: TASKS_ACTIONS.FILTER_TASKS,
  payload: api.tasks.searchTask(params),
});

export const createTask = (payload: TaskDTO) => ({
  type: TASKS_ACTIONS.CREATE,
  payload: api.tasks.createTask(payload),
});

export const editTask = (id: number, payload: TaskDTO) => ({
  type: TASKS_ACTIONS.EDIT,
  payload: api.tasks.editTask(id, payload),
  meta: {
    id: id,
    params: payload,
  },
});

export const getTask = (id: number) => ({
  type: TASKS_ACTIONS.GET,
  payload: api.tasks.getTask(id),
});

export const deleteTask = (id: number) => ({
  type: TASKS_ACTIONS.DELETE,
  payload: api.tasks.deleteTask(id),
  meta: { id },
});

export const updateTaskStatus = (id: number, payload: StatusDTO) => ({
  type: TASKS_ACTIONS.EDIT_STATUS,
  payload: api.tasks.updateTaskStatus(id, payload),
  meta: {
    id: id,
  },
});
