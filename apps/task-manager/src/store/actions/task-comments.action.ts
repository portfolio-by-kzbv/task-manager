import { Comment } from '@dar/api-interfaces';
import api from '../../services/api';

export enum COMMENTS_ACTIONS {
  GET = 'COMMENTS_GET',
  CREATE = 'COMMENTS_CREATE',
  DELETE = 'COMMENTS_DELETE',
  EDIT = 'COMMENTS_EDIT',
  APPEND = 'COMMENTS_APPEND',
  UPDATE = 'COMMENTS_UPDATE',
}

export const appendComment = (payload: Comment) => ({
  type: COMMENTS_ACTIONS.APPEND,
  payload,
});

export const updateComment = (payload: Comment) => ({
  type: COMMENTS_ACTIONS.UPDATE,
  payload,
});

export const getComments = (params) => ({
  type: COMMENTS_ACTIONS.GET,
  payload: api.comments.getComments(params),
});

export const createComment = (data: Comment) => ({
  type: COMMENTS_ACTIONS.CREATE,
  payload: api.comments.createComment(data),
});

export const deleteComment = (task_id: number) => ({
  type: COMMENTS_ACTIONS.DELETE,
  payload: api.comments.deleteComment(task_id),
  meta: { id: task_id },
});

export const editComment = (id: number, payload: Comment) => ({
  type: COMMENTS_ACTIONS.EDIT,
  payload: api.comments.editComment(id, payload),
  meta: {
    id: id,
    params: payload,
  },
});
