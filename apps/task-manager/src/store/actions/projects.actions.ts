import api from '../../services/api';

export enum PROJECTS_ACTIONS {
  LIST = 'PROJECTS_LIST',
  GET = 'PROJECTS_GET',
}

export const getProjects = () => ({
  type: PROJECTS_ACTIONS.LIST,
  payload: api.projects.getAllProjects(),
});

export const getProject = (project_id: string) => ({
  type: PROJECTS_ACTIONS.GET,
  payload: api.projects.getProject(project_id),
});
