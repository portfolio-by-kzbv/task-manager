import { LinksRequest } from '@dar/api-interfaces';
import api from '../../services/api';

export enum LINKS_ACTIONS {
  GET = 'LINKS_GET',
  CREATE = 'LINKS_CREATE',
  DELETE = 'LINKS_DELETE',
  AFFECTS_GET = 'AFFECTS_GET',
  DEPENDS_GET = 'DEPENDS_GET',
  TASKS_GET = 'LINKS_TASKS_GET',
}

export const getLinks = (task_id: number) => ({
  type: LINKS_ACTIONS.GET,
  payload: api.links.getLinks(task_id),
});

export const getAffects = (data: number[]) => ({
  type: LINKS_ACTIONS.AFFECTS_GET,
  payload: api.tasks.getTasksBatch(data),
});

export const getDepends = (data: number[]) => ({
  type: LINKS_ACTIONS.DEPENDS_GET,
  payload: api.tasks.getTasksBatch(data),
});

export const getLinksTasks = (params) => ({
  type: LINKS_ACTIONS.TASKS_GET,
  payload: api.tasks.getAllTasks(params),
});

export const createLink = (data: LinksRequest) => ({
  type: LINKS_ACTIONS.CREATE,
  payload: api.links.createLink(data),
});

export const deleteLink = (task_id: number, link_id: number) => ({
  type: LINKS_ACTIONS.DELETE,
  payload: api.links.deleteLink(task_id, link_id),
});

export const deleteAndUpdateLink = (task_id: number, link_id: number, update_task_id: number) => {
  return (dispatch) => {
    return dispatch(deleteLink(task_id, link_id)).then(() => {
      dispatch(getLinks(update_task_id));
    });
  };
};
