import { NotesDTO, NotesQueryParams } from '@dar/api-interfaces';
import api from '../../services/api';

export const MEETING_NOTES_ACTIONS = {
  CREATE: 'MEETING_NOTES_CREATE',
  EDIT: 'MEETING_NOTES_EDIT',
  REMOVE: 'MEETING_NOTES_REMOVE',
  LIST: 'MEETING_NOTES_LIST',
  SET_DELETE_ID: 'MEETING_NOTES_SET_DELETE_ID',
};

export const listNotes = (params: NotesQueryParams) => ({
  type: MEETING_NOTES_ACTIONS.LIST,
  payload: api.meetings.getNoteList(params),
});

export const createNote = (payload: NotesDTO) => ({
  type: MEETING_NOTES_ACTIONS.CREATE,
  payload: api.meetings.createNote(payload),
});

export const editNote = (noteId: number, payload: NotesDTO) => ({
  type: MEETING_NOTES_ACTIONS.EDIT,
  payload: api.meetings.updateNote(noteId, payload),
});

export const removeNote = (noteId: number) => ({
  type: MEETING_NOTES_ACTIONS.REMOVE,
  payload: api.meetings.removeNote(noteId),
  meta: { noteId },
});

export const setDeleteNoteId = (noteId: number) => ({
  type: MEETING_NOTES_ACTIONS.SET_DELETE_ID,
  payload: noteId,
});
