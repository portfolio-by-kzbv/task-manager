import { Meeting, MeetingDTO } from '@dar/api-interfaces';
import api from '../../services/api';

export const MEETING_ACTIONS = {
  CREATE: 'MEETINGS_CREATE',
  EDIT: 'MEETINGS_EDIT',
  DELETE: 'MEETINGS_DELETE',
  GET: 'MEETINGS_GET',
  LIST: 'MEETINGS_LIST',
  SET_PAGE: 'MEETINGS_SET_PAGE',
  SET_QUERY: 'MEETINGS_SET_QUERY',
  SET_LIMIT: 'MEETINGS_SET_LIMIT',
  ADD_COUNT: 'ADD_COUNT',
  RESET_COUNT: 'RESET_COUNT',
  SET_COUNT: 'SET_COUNT',
  GET_SETTINGS: 'GET_SETTINGS',
  UPDATE_SETTINGS: 'UPDATE_SETTINGS',
};

export const createMeeting = (payload: MeetingDTO) => ({
  type: MEETING_ACTIONS.CREATE,
  payload: api.meetings.create(payload),
});

export const editMeeting = (payload: Meeting) => ({
  type: MEETING_ACTIONS.EDIT,
  payload: api.meetings.update(payload),
  meta: {
    id: payload.id,
    params: payload,
  },
});

export const deleteMeeting = (id: string) => ({
  type: MEETING_ACTIONS.DELETE,
  payload: api.meetings.remove(id),
  meta: { id },
});

export const getMeeting = (id: string) => ({
  type: MEETING_ACTIONS.GET,
  payload: api.meetings.getMeeting(id),
});

export const listMeetings = () => ({
  type: MEETING_ACTIONS.LIST,
  payload: api.meetings.list(),
});

export const setMeetingSearchQuery = (q: string) => ({
  type: MEETING_ACTIONS.SET_QUERY,
  payload: q,
});
