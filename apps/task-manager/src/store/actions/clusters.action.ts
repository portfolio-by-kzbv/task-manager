import api from '../../services/api';

export enum CLUSTERS_ACTIONS {
  LIST = 'CLUSTERS_LIST',
  GET = 'CLUSTERS_GET',
}

export const getClusters = () => ({
  type: CLUSTERS_ACTIONS.LIST,
  payload: api.clusters.getAllClusters(),
});

export const getCluster = (cluster_id: string) => ({
  type: CLUSTERS_ACTIONS.GET,
  payload: api.clusters.getCluster(cluster_id),
});
