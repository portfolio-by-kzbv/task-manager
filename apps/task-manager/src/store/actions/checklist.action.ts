import api from '../../services/api';
import { Todo } from '@dar/api-interfaces';

export const CHECKLIST_ACTIONS = {
  GET: 'CHECKLIST_GET',
  CREATE: 'CHECKLIST_CREATE',
  EDIT: 'CHECKLIST_EDIT',
  DELETE: 'CHECKLIST_DELETE',
  REORDER: 'CHECKLIST_REORDER'
};

export const getChecklist = (params) => ({
  type: CHECKLIST_ACTIONS.GET,
  payload: api.checklist.getAllTodos(params)
});

export const createTodo = (data: Todo) => ({
  type: CHECKLIST_ACTIONS.CREATE,
  payload: api.checklist.createTodo(data)
});

export const editTodo = (data: Todo) => ({
  type: CHECKLIST_ACTIONS.EDIT,
  payload: api.checklist.editTodo(data)
});

export const deleteTodo = (id: number) => ({
  type: CHECKLIST_ACTIONS.DELETE,
  payload: api.checklist.deleteTodo(id),
  meta: { id }
});

export const reorderTodos = (reorderedTodos: Todo[]) => ({
  type: CHECKLIST_ACTIONS.REORDER,
  payload: reorderedTodos
});
