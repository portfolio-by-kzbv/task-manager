import React from 'react';
import { BrowserRouter as Router } from 'react-router-dom';
import { Provider } from 'react-redux';
import theme from './theme';
import { createGenerateClassName, StylesProvider, ThemeProvider } from '@material-ui/core';
import store from './store/store';
import MainLayout from './layouts/MainLayout';
import { Toaster } from '@dartech/dms-ui';

const generateClassName = createGenerateClassName({
  productionPrefix: 'comms-styles',
  seed: 'comms',
});

const Root = () => {
  return (
    <Provider store={store}>
      <StylesProvider generateClassName={generateClassName}>
        <Router>
          <ThemeProvider theme={theme}>
            <Toaster />
            <MainLayout />
          </ThemeProvider>
        </Router>
      </StylesProvider>
    </Provider>
  );
};

export default Root;
