import React, { useEffect, useState } from 'react';
import { Switch, Route, Redirect, useLocation } from 'react-router-dom';
import { LastLocationProvider } from 'react-router-last-location';
import { makeStyles } from '@material-ui/core/styles';

import SideMenu from '../components/SideMenu/SideMenu';
import DateFnsUtils from '@date-io/date-fns';
import TaskManagerPage from '../pages/TaskManagerPage';

import { TaskManagerFiltersProvider } from '../contexts/TaskManagerFiltersContext/TaskManagerFiltersProvider';
import { TaskManagerModalsProvider } from '../contexts/TaskManagerModalsContext/TaskManagerModalsProvider';
import { getUsers } from '../store/actions/users.action';
import { useDispatch } from 'react-redux';
import { Dispatch } from '@dar/api-interfaces';
import MeetingsPage from '../pages/MeetingsPage';
import MeetingPage from '../pages/MeetingPage';
import { TaskManagerContextProvider } from '@dar/contexts/TaskManagerContext/TaskManagerTaskProvider';
import { MuiPickersUtilsProvider } from '@material-ui/pickers';
import TaskDetails from '@dar/components/TaskManager/TaskDetails/TaskDetails';

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
    width: '100vw',
    height: 'calc(100vh - 54px)',
  },
  container: {
    padding: theme.spacing(5, 3, 3, 3),
    flexGrow: 1,
    width: 'calc(100% - 300px)',
    overflow: 'scroll',
  },
}));

export default () => {
  const classes = useStyles();
  const [loading, setLoading] = useState<boolean>(false);
  const dispatch: Dispatch = useDispatch();

  useEffect(() => {
    setLoading(true);
    dispatch(getUsers()).then((res) => {
      setLoading(false);
    });
  }, [dispatch]);

  return (
    <div className={classes.root}>
      <SideMenu />
      <div className={classes.container}>
        <TaskManagerModalsProvider>
          <TaskManagerFiltersProvider>
            <TaskManagerContextProvider>
              <MuiPickersUtilsProvider utils={DateFnsUtils}>
                <Switch>
                  <Route exact path="/productivity">
                    <Redirect to="/productivity/tasks" />
                  </Route>
                  <Route path="/productivity/tasks" component={TaskManagerPage} />
                  <Route exact path="/productivity/meetings" component={MeetingsPage} />
                  <Route path="/productivity/meetings/:type/:id" component={MeetingPage} />
                </Switch>
                <Route exact path="/productivity/tasks/:id" component={TaskDetails} />
              </MuiPickersUtilsProvider>
            </TaskManagerContextProvider>
          </TaskManagerFiltersProvider>
        </TaskManagerModalsProvider>
      </div>
    </div>
  );
};
