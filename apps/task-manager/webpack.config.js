/* eslint-disable @typescript-eslint/no-var-requires */
const merge = require('webpack-merge');
const singleSpaDefaults = require('./webpack-config-single-spa-react-ts');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const path = require('path');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = (webpackConfigEnv, opts) => {
  // console.log(webpackConfigEnv)
  // const nxConfig = nrwlConfig(webpackConfigEnv);

  const singleSpaConfig = singleSpaDefaults({
    orgName: 'dar-dms',
    projectName: 'comms',
    webpackConfigEnv,
    options: opts,
  });
  const options = opts.options ? opts.options : opts.buildOptions;
  const extraPlugins = [];
  if (Array.isArray(options.assets) && options.assets.length > 0) {
    extraPlugins.push(createCopyPlugin(options.assets));
  }
  const config = merge.strategy({
    'module.rules': 'append',
    'resolve.alias': 'append',
    plugins: 'append',
  })(singleSpaConfig, {
    // modify the webpack config however you'd like to by adding to this object
    entry: path.resolve(process.cwd(), `apps/dar-front/src/dar-comms.tsx`),
    output: {
      path: path.resolve(process.cwd(), `dist/apps/dar-front`),
      pathinfo: false,
    },
    devtool: 'eval-source-map',
    mode: webpackConfigEnv.mode,
    devServer: {
      ...webpackConfigEnv.devServer,
      watchOptions: { aggregateTimeout: 600, poll: false, ignored: /node_modules/, followSymlinks: false },
    },
    plugins: [
      new MiniCssExtractPlugin({
        filename: `[name].css`,
      }),
      ...extraPlugins,
    ],
    module: {
      rules: [
        {
          test: /\.(png|jpe?g|gif|webp)$/,
          include: path.resolve(__dirname, 'src/assets'),
          loader: require.resolve('url-loader'),
          options: {
            limit: 10000,
            name: '[name].[hash:7].[ext]',
          },
        },
        {
          test: /\.svg$/,
          oneOf: [
            // If coming from JS/TS file, then transform into React component using SVGR.
            {
              issuer: {
                test: /\.[jt]sx?$/,
              },
              use: [
                '@svgr/webpack?-svgo,+titleProp,+ref![path]',
                {
                  loader: require.resolve('url-loader'),
                  options: {
                    limit: 10000,
                    name: '[name].[hash:7].[ext]',
                    esModule: false,
                  },
                },
              ],
            },
            // Fallback to plain URL loader.
            {
              use: [
                {
                  loader: require.resolve('url-loader'),
                  options: {
                    limit: 10000,
                    name: '[name].[hash:7].[ext]',
                  },
                },
              ],
            },
          ],
        },
        ...webpackConfigEnv.module.rules,
      ],
    },
    resolve: {
      symlinks: false,
      alias: webpackConfigEnv.resolve.alias,
    },
    stats: {
      hash: true,
      timings: false,
      cached: false,
      cachedAssets: false,
      modules: false,
      warnings: true,
      errors: true,
      colors: true,
      chunks: true,
      assets: false,
      chunkOrigins: false,
      chunkModules: false,
      children: false,
      reasons: false,
      version: false,
      errorDetails: false,
      moduleTrace: false,
      usedExports: false,
      warningsFilter: [/The comment file/i, /could not find any license/i],
    },
  });
  const tsPaths = new TsconfigPathsPlugin({
    configFile: './apps/dar-front/tsconfig.app.json',
  });

  config.resolve.plugins ? config.resolve.plugins.push(tsPaths) : (config.resolve.plugins = [tsPaths]);
  return config;
};

function createCopyPlugin(assets) {
  return new CopyWebpackPlugin({
    patterns: assets.map((asset) => {
      return {
        context: asset.input,
        // Now we remove starting slash to make Webpack place it from the output root.
        to: asset.output,
        from: asset.glob,
        globOptions: {
          ignore: ['.gitkeep', '**/.DS_Store', '**/Thumbs.db', ...(asset.ignore ? asset.ignore : [])],
          dot: true,
        },
      };
    }),
  });
}
