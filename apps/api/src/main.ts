/**
 * This is not a production server yet!
 * This is only a minimal backend to get started.
 */

import { Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { WsAdapter } from '@nestjs/platform-ws';
import { AppModule } from './app/app.module';
import { HttpExceptionFilter } from './app/http-exception-filter';

function healthcheck(req, res, next) {
  if (req.url === '/healthcheck') {
    return res.status(200).json({ status: 'OK' });
  }
  next();
}

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const globalPrefix = 'api';

  app.useGlobalFilters(new HttpExceptionFilter());

  app.setGlobalPrefix(globalPrefix);

  app.use(healthcheck);

  app.enableCors();

  app.useWebSocketAdapter(new WsAdapter(app));

  const port = process.env.PORT || 3333;
  await app.listen(port, () => {
    Logger.log('Listening at http://localhost:' + port + '/' + globalPrefix);
  });
}

bootstrap();
