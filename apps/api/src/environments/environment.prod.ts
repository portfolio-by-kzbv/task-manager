export const environment = {
  production: true,
  meetingToken: '8nxtrqhqqtbx9qc3go4dz7qepy',
  meetingsRootApi: 'https://dms.dar.io/api/v2/meeting-service',
  baseRootApi: 'https://productivity.dms.dar.io/api/v1',
  oktaUsersApi: 'https://api.bamboohr.com/api/gateway.php/darglobal/v1/employees/directory',
  oktaToken: 'MDk5YjMzOWQ3NGQwOTE5NDk0NTgxMmVkNTlhYTFiZjIxYTdjZjA0Mjou',
  hcmsApi: 'https://dms.dar.io/api/v1/hcms',
  hcmsAWSBase: 'https://s3-eu-west-1.amazonaws.com/dev-dms-hcms-bucket',
  meetingsNotesApi: 'https://comms.dar.io/meetings/api',
  httpModuleOptions: {},
};
