export const environment = {
  production: false,
  meetingsRootApi: 'https://dms.dar-dev.zone/api/v2/meeting-service',
  baseRootApi: 'https://dms-communications.dar-dev.zone/api/v1',
  oktaUsersApi: 'https://api.bamboohr.com/api/gateway.php/darglobal/v1/employees/directory',
  oktaToken: 'MDk5YjMzOWQ3NGQwOTE5NDk0NTgxMmVkNTlhYTFiZjIxYTdjZjA0Mjou',
  httpModuleOptions: {},
  hcmsApi: 'https://dms.dar-dev.zone/api/v1/hcms',
  meetingsNotesApi: 'https://dms-meetings.dar-dev.zone/api',
  hcmsAWSBase: 'https://s3-eu-west-1.amazonaws.com/dev-dms-hcms-bucket',
};
