export const environment = {
  production: true,
  meetingToken: '8nxtrqhqqtbx9qc3go4dz7qepy',
  meetingsRootApi: 'https://dms.dar-dev.zone/api/v2/meeting-service',
  baseRootApi: 'https://dms-communications.dar-dev.zone/api/v1',
  oktaUsersApi: 'https://api.bamboohr.com/api/gateway.php/darglobal/v1/employees/directory',
  oktaToken: 'MDk5YjMzOWQ3NGQwOTE5NDk0NTgxMmVkNTlhYTFiZjIxYTdjZjA0Mjou',
  hcmsApi: 'https://dms.dar-dev.zone/api/v1/hcms',
  hcmsAWSBase: 'https://s3-eu-west-1.amazonaws.com/dev-dms-hcms-bucket',
  httpModuleOptions: {},
  meetingsNotesApi: 'https://dms-meetings.dar-dev.zone/api',
};
