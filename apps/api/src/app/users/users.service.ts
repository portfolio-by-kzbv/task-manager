import { Injectable } from '@nestjs/common';
import { LoggedHttpService } from '../logged-http-client/logged-http.service';
import { environment } from '../../environments/environment';
import { map } from 'rxjs/operators';
import { User, UserResponse } from '@dar/api-interfaces';

@Injectable()
export class UsersService {
  constructor(private httpService: LoggedHttpService) {}

  getAll(id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .get<UserResponse>(`${environment.hcmsApi}/shared/employees/shortinfo?offset=0&limit=300`, { headers })
      .pipe(
        map((response) => {
          const employees = this.employeesConverter(response.data ? response.data.entities : []);
          return employees;
        })
      );
  }

  getUser(user_id: number, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .get<Response>(`${environment.hcmsApi}/shared/employees/${user_id}/shortinfo`, { headers })
      .pipe(
        map((response) => {
          const employees = this.employeeConverter(response.data ? response.data : []);
          return employees;
        })
      );
  }

  private employeesConverter(employees: any): User[] {
    return employees.map((user) => {
      return this.employeeConverter(user);
    });
  }

  private employeeConverter(user: any): User {
    return {
      id: user.id,
      displayName: user.name.first + ' ' + user.name.last,
      firstName: user.name.first,
      lastName: user.name.last,
      jobTitle: user.position,
      workEmail: user.email,
      photoUrl: `${environment.hcmsAWSBase}/${user.photo}` || null,
    };
  }
}
