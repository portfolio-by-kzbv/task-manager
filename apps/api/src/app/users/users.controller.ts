import { Controller, Headers, Get, Param } from '@nestjs/common';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Get()
  findAll(@Headers('authorization') id_token: string) {
    return this.usersService.getAll(id_token);
  }

  @Get('/:id')
  findOne(@Param('id') user_id: number, @Headers('authorization') id_token: string) {
    return this.usersService.getUser(user_id, id_token);
  }
}
