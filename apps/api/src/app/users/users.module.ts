import { Module } from "@nestjs/common";
import { UsersService } from "./users.service";
import { UsersController } from "./users.controller";
import { LoggedHttpClientModule } from "../logged-http-client/logged-http-client.module";

@Module({
  imports: [
    LoggedHttpClientModule
  ],
  controllers: [UsersController],
  providers: [
    UsersService
  ],
})
export class UsersModule {}
