import { Injectable } from '@nestjs/common';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
import { LoggedHttpService } from '../../logged-http-client/logged-http.service';

@Injectable()
export class ClustersService {
  constructor(private httpService: LoggedHttpService) {}

  getAllClusters(id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .get(`${environment.hcmsApi}/directories/types/CLUSTER`, { headers })
      .pipe(map((response) => response.data.entities));
  }

  getCluster(cluster_id: string, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .get(`${environment.hcmsApi}/directories/${cluster_id}`, { headers })
      .pipe(map((response) => response.data));
  }
}
