import { Module } from "@nestjs/common";
import { LoggedHttpClientModule } from "../../logged-http-client/logged-http-client.module";
import { ClustersController } from "./clusters.controller";
import { ClustersService } from "./clusters.service";

@Module({
  imports: [
    LoggedHttpClientModule
  ],
  controllers: [ClustersController],
  providers: [ClustersService],
})
export class ClustersModule { }
