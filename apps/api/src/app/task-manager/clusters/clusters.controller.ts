import { Controller, Get, Headers, Param } from '@nestjs/common';
import { ClustersService } from './clusters.service';

@Controller('task-manager/clusters')
export class ClustersController {
  constructor(private clustersService: ClustersService) {}

  @Get()
  getClusters(@Headers('authorization') id_token: string) {
    return this.clustersService.getAllClusters(id_token);
  }

  @Get('/:id')
  getCluster(@Param('id') cluster_id: string, @Headers('authorization') id_token: string) {
    return this.clustersService.getCluster(cluster_id, id_token);
  }
}
