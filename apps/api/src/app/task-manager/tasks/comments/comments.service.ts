import { Comment, CommentsQueryParams } from '@dar/api-interfaces';
import { Injectable } from '@nestjs/common';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { LoggedHttpService } from '../../../logged-http-client/logged-http.service';

@Injectable()
export class CommentsService {
  constructor(private httpService: LoggedHttpService) {}

  createComment(data: Comment, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .post<Comment>(`${environment.baseRootApi}/comments`, data, { headers })
      .pipe(map((res) => res.data));
  }

  getComments(query: CommentsQueryParams, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .get<Comment[]>(`${environment.baseRootApi}/comments`, {
        headers,
        params: {
          ...query,
        },
      })
      .pipe(map((res) => res.data));
  }

  deleteComment(comment_id: number, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .delete(`${environment.baseRootApi}/comments/${comment_id}`, { headers })
      .pipe(map((res) => res.data));
  }

  updateComment(id: number, data: Comment, id_token: string){
    const headers = {
      Authorization: id_token,
    };
    return this.httpService.put(`${environment.baseRootApi}/comments/${id}`, data, { headers })
    .pipe(map((response) => response.data));
  }
}
