import { Module } from "@nestjs/common";
import { TaskManagerService } from "./tasks.service";
import { TaskManagerController } from "./tasks.controller";
import { LoggedHttpClientModule } from "../../logged-http-client/logged-http-client.module";
import { LinksService } from './links/links.service';
import { CommentsService } from './comments/comments.service';
import { AttachmentsService } from "./attachments/attachments.service";

@Module({
  imports: [LoggedHttpClientModule],
  controllers: [TaskManagerController],
  providers: [TaskManagerService, LinksService, CommentsService, AttachmentsService],
})
export class TaskManagerModule {}
