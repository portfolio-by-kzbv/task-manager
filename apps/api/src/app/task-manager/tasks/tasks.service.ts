import {Injectable} from "@nestjs/common";
import {LoggedHttpService} from "../../logged-http-client/logged-http.service";
import {environment} from "../../../environments/environment";
import {map} from "rxjs/operators";
import { Task, TasksQueryParams, TaskDTO, StatusDTO } from '@dar/api-interfaces';

@Injectable()
export class TaskManagerService {
  constructor(private httpService: LoggedHttpService) {}

  getAll(query: TasksQueryParams, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .get(`${environment.baseRootApi}/tasks`, {
        headers,
        params: {
          ...query,
        },
      })
      .pipe(map((response) => response.data));
  }

  getTask(id: number, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService.get(`${environment.baseRootApi}/tasks/${id}`, { headers })
      .pipe(map((response) => response.data));
  }

  getTasksBatch(data: number[], id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .post<Task[]>(`${environment.baseRootApi}/tasks/batch`, data, { headers })
      .pipe(map((response) => response.data));
  }

  createTask(data: TaskDTO, id_token: string){
    const headers = {
      Authorization: id_token,
    };
    return this.httpService.post(`${environment.baseRootApi}/tasks`, data, { headers })
    .pipe(map((response) => response.data));
  }

  updateTask(id: number, data: Task, id_token: string){
    const headers = {
      Authorization: id_token,
    };
    return this.httpService.put(`${environment.baseRootApi}/tasks/${id}`, data, { headers })
    .pipe(map((response) => response.data));
  }

  updateTaskStatus(id: number, data: StatusDTO, id_token: string){
    const headers = {
      Authorization: id_token,
    };
    return this.httpService.put(`${environment.baseRootApi}/tasks/${id}/status`, data, { headers })
    .pipe(map((response) => response.data));
  }

  deleteTask(id: number, id_token: string){
    const headers = {
      Authorization: id_token,
    };
    return this.httpService.delete(`${environment.baseRootApi}/tasks/${id}`, { headers })
    .pipe(map((response) => response.data));
  }

  searchTask(query: TasksQueryParams, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .get(`${environment.baseRootApi}/search/tasks`, {
        headers,
        params: {
          ...query,
        },
      })
      .pipe(map((response) => response.data));
  }
}
