import { ApiConsumes } from '@nestjs/swagger';
import { Express } from 'express';
import { Multer } from 'multer';
import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
  UploadedFile,
  Headers,
  UseInterceptors,
} from '@nestjs/common';
import { FileInterceptor } from '@nestjs/platform-express';
import { TaskManagerService } from './tasks.service';
import {
  Task,
  TasksQueryParams,
  TaskDTO,
  LinksRequest,
  StatusDTO,
  Comment,
  CommentsQueryParams,
  AttachmentsQueryParams,
  Attachment,
  AttachmentDTO,
} from '@dar/api-interfaces';
import { LinksService } from './links/links.service';
import { CommentsService } from './comments/comments.service';
import { AttachmentsService } from './attachments/attachments.service';

@Controller('task-manager/tasks')
export class TaskManagerController {
  constructor(
    private taskManagerService: TaskManagerService,
    private linksService: LinksService,
    private commentsService: CommentsService,
    private attachmentsService: AttachmentsService
  ) {}

  @Post()
  addTask(@Headers('authorization') id_token: string, @Body() data: TaskDTO) {
    return this.taskManagerService.createTask(data, id_token);
  }

  @Get()
  findAll(@Headers('authorization') id_token: string, @Query() query: TasksQueryParams) {
    return this.taskManagerService.getAll(query, id_token);
  }

  @Get('/comments')
  getComments(@Headers('authorization') id_token: string, @Query() query: CommentsQueryParams) {
    return this.commentsService.getComments(query, id_token);
  }

  @Get('/attachments')
  getAttachments(@Headers('authorization') id_token: string, @Query() query: AttachmentsQueryParams) {
    return this.attachmentsService.getAttachments(query, id_token);
  }

  @Get('/attachments/:id')
  getAttachment(@Headers('authorization') id_token: string, @Param('id') id: number) {
    return this.attachmentsService.getAttachment(id, id_token);
  }

  @Post('/batch')
  getTasksBatch(@Headers('authorization') id_token: string, @Body() data: number[]) {
    if (data.length === 0) {
      return [];
    }
    return this.taskManagerService.getTasksBatch(data, id_token);
  }

  @Post('/links')
  createLink(@Headers('authorization') id_token: string, @Body() data: LinksRequest) {
    return this.linksService.createLink(data, id_token);
  }

  @Post('/comments')
  createComment(@Headers('authorization') id_token: string, @Body() data: Comment) {
    return this.commentsService.createComment(data, id_token);
  }

  @Post('/:task_id/attachments')
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('attachment'))
  uploadAttachment(
    @Headers('authorization') id_token: string,
    @Param('task_id') task_id: number,
    @UploadedFile() file: Express.Multer.File
  ) {
    return this.attachmentsService.uploadAttachment(task_id, file, id_token);
  }

  @Post('/:task_id/attachments/:comment_id')
  @ApiConsumes('multipart/form-data')
  @UseInterceptors(FileInterceptor('attachment'))
  uploadCommentAttachment(
    @Headers('authorization') id_token: string,
    @Param('task_id') task_id: number,
    @Param('comment_id') comment_id: number,
    @UploadedFile() file: Express.Multer.File
  ) {
    return this.attachmentsService.uploadCommentAttachment(task_id, comment_id, file, id_token);
  }

  @Get('/search')
  searchTask(@Headers('authorization') id_token: string, @Query() query: TasksQueryParams) {
    return this.taskManagerService.searchTask(query, id_token);
  }

  @Get('/:id')
  findOne(@Headers('authorization') id_token: string, @Param('id') id: number) {
    return this.taskManagerService.getTask(id, id_token);
  }

  @Get('/:id/links')
  getLinks(@Headers('authorization') id_token: string, @Param('id') task_id: number) {
    return this.linksService.getLinks(task_id, id_token);
  }

  @Put('/:id')
  updateTask(@Headers('authorization') id_token: string, @Param('id') id: number, @Body() data: Task) {
    console.log(data);
    return this.taskManagerService.updateTask(id, data, id_token);
  }

  @Delete('/:id')
  deleteTask(@Headers('authorization') id_token: string, @Param('id') id: number) {
    return this.taskManagerService.deleteTask(id, id_token);
  }

  @Delete('/:id/links/:link_id')
  deleteLink(@Headers('authorization') id_token: string, @Param('id') task_id: number, @Param('link_id') link_id) {
    return this.linksService.deleteLink(task_id, link_id, id_token);
  }

  @Delete('/:id/comments')
  deleteComment(@Headers('authorization') id_token: string, @Param('id') task_id: number) {
    return this.commentsService.deleteComment(task_id, id_token);
  }

  @Delete('/attachments/:id')
  deleteAttachment(@Headers('authorization') id_token: string, @Param('id') id: number) {
    return this.attachmentsService.deleteAttachment(id, id_token);
  }

  @Put('/comments/:id')
  updateComment(@Headers('authorization') id_token: string, @Param('id') comment_id: number, @Body() comment: Comment) {
    return this.commentsService.updateComment(comment_id, comment, id_token);
  }

  @Put('/status/:id')
  updateTaskStatus(@Headers('authorization') id_token: string, @Param('id') id: number, @Body() status: StatusDTO) {
    return this.taskManagerService.updateTaskStatus(id, status, id_token);
  }
}
