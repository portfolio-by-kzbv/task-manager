import { Attachment, AttachmentsQueryParams } from '@dar/api-interfaces';
import { Injectable } from '@nestjs/common';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { LoggedHttpService } from '../../../logged-http-client/logged-http.service';

// eslint-disable-next-line @typescript-eslint/no-var-requires
const FormData = require('form-data');
@Injectable()
export class AttachmentsService {
  private uploadingTimeout = 100000;

  constructor(private httpService: LoggedHttpService) {}

  uploadCommentAttachment(task_id: number, comment_id: number, file, id_token: string) {
    const formData = new FormData();
    formData.append('attachment', file.buffer, { filename: file.originalname });
    formData.append('comment_id', comment_id);
    const headers = {
      ...formData.getHeaders(),
      Authorization: id_token,
      'Content-Length': formData.getLengthSync(),
    };
    return this.httpService
      .post(`${environment.baseRootApi}/tasks/${task_id}/upload/attachments`, formData, {
        headers,
        timeout: this.uploadingTimeout,
      })
      .pipe(map((res) => res.data));
  }

  getAttachment(attachment_id: number, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .get<Attachment>(`${environment.baseRootApi}/attachments/${attachment_id}`, { headers })
      .pipe(map((res) => res.data));
  }

  getAttachments(query: AttachmentsQueryParams, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .get<Attachment[]>(`${environment.baseRootApi}/attachments`, {
        headers,
        params: {
          ...query,
        },
      })
      .pipe(map((res) => res.data));
  }

  deleteAttachment(attachment_id: number, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .delete(`${environment.baseRootApi}/attachments/${attachment_id}`, { headers })
      .pipe(map((res) => res.data));
  }

  uploadAttachment(task_id: number, file, id_token: string) {
    const formData = new FormData();
    formData.append('attachment', file.buffer, { filename: file.originalname });
    const headers = {
      ...formData.getHeaders(),
      Authorization: id_token,
      'Content-Length': formData.getLengthSync(),
    };
    return this.httpService
      .post(`${environment.baseRootApi}/tasks/${task_id}/upload/attachments`, formData, {
        headers,
        timeout: this.uploadingTimeout,
      })
      .pipe(map((res) => res.data));
  }
}
