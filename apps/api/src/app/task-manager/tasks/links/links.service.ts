import { LinksRequest, LinksResponse } from '@dar/api-interfaces';
import { Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from '../../../../environments/environment';
import { LoggedHttpService } from '../../../logged-http-client/logged-http.service';

@Injectable()
export class LinksService {
  constructor(private httpService: LoggedHttpService) {}

  createLink(data: LinksRequest, id_token: string): Observable<LinksRequest> {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .post<LinksRequest>(`${environment.baseRootApi}/tasks/links`, data, { headers })
      .pipe(map((res) => res.data));
  }

  getLinks(task_id: number, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .get<LinksResponse>(`${environment.baseRootApi}/tasks/${task_id}/links`, { headers })
      .pipe(map((res) => res.data));
  }

  deleteLink(task_id: number, link_id: number, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .delete(`${environment.baseRootApi}/tasks/${task_id}/links/dependency/${link_id}`, { headers })
      .pipe(map((res) => res.data));
  }
}
