import { Injectable } from '@nestjs/common';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
import { LoggedHttpService } from '../../logged-http-client/logged-http.service';

@Injectable()
export class ProjectsService {
  constructor(private httpService: LoggedHttpService) {}

  getAllProjects(id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .get(`${environment.hcmsApi}/directories/types/PROJECT`, { headers })
      .pipe(map((response) => response.data.entities));
  }

  getProject(project_id: string, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .get(`${environment.hcmsApi}/directories/${project_id}`, { headers })
      .pipe(map((response) => response.data));
  }
}
