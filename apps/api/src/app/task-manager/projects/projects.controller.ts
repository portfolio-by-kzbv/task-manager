import { Controller, Get, Headers, Param } from '@nestjs/common';
import { ProjectsService } from './projects.service';

@Controller('task-manager/projects')
export class ProjectsController {
  constructor(private projectsService: ProjectsService) {}

  @Get()
  getProjects(@Headers('authorization') id_token: string) {
    return this.projectsService.getAllProjects(id_token);
  }

  @Get('/:id')
  getProject(@Param('id') project_id, @Headers('authorization') id_token: string) {
    return this.projectsService.getProject(project_id, id_token);
  }
}
