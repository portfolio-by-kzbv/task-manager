import { Module } from "@nestjs/common";
import { LoggedHttpClientModule } from "../../logged-http-client/logged-http-client.module";
import { ProjectsController } from "./projects.controller";
import { ProjectsService } from "./projects.service";


@Module({
  imports: [
    LoggedHttpClientModule
  ],
  controllers: [ProjectsController],
  providers: [ProjectsService],
})
export class ProjectsModule { }
