import { Module } from "@nestjs/common";
import { ChecklistService } from "./checklist.service";
import { ChecklistController } from "./checklist.controller";
import { LoggedHttpClientModule } from "../../logged-http-client/logged-http-client.module";

@Module({
  imports: [
    LoggedHttpClientModule
  ],
  controllers: [ChecklistController],
  providers: [
    ChecklistService
  ],
})
export class ChecklistModule {}
