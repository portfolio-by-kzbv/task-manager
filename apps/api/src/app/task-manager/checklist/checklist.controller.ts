import {Body, Controller, Delete, Get, Param, Post, Put, Query, Headers} from "@nestjs/common";
import { ChecklistService } from "./checklist.service";
import { Todo, TodoQueryParams } from '@dar/api-interfaces';

@Controller('task-manager/checklist')
export class ChecklistController {
  constructor(private taskManagerService: ChecklistService) {
  }

  @Post()
  addTodo(@Body() data: Todo, @Headers('authorization') id_token: string) {
    return this.taskManagerService.createTodo(data, id_token);
  }
  
  @Get()
  findAll(@Query() query: TodoQueryParams, @Headers('authorization') id_token: string) {
    return this.taskManagerService.getAll(query, id_token);
  }
  
  @Put('/:id')
  updateTodo(@Param('id') id: number,
  @Body() data: Todo, 
  @Headers('authorization') id_token: string) {
    return this.taskManagerService.updateTodo(data, id_token);
  }

  @Delete('/:id')
  deleteTodo(@Param('id') id: number, @Headers('authorization') id_token: string) {
    return this.taskManagerService.deleteTodo(id, id_token);
  }
  
}
