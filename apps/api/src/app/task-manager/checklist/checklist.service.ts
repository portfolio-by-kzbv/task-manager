import {Injectable} from "@nestjs/common";
import {LoggedHttpService} from "../../logged-http-client/logged-http.service";
import {environment} from "../../../environments/environment";
import {map} from "rxjs/operators";
import { Todo, TodoQueryParams } from '@dar/api-interfaces';

@Injectable()
export class ChecklistService {
  constructor(
    private httpService: LoggedHttpService,
  ) {
  }

  getAll(query: TodoQueryParams, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService.get(`${environment.baseRootApi}/checklists`, {
      headers,
      params: {
        ...query
      }})
      .pipe(map((response) => response.data));
  }

  createTodo(data: Todo, id_token: string){
    const headers = {
      Authorization: id_token,
    };
    return this.httpService.post(`${environment.baseRootApi}/checklists`, data, { headers }).pipe(map((response) => response.data));
  }

  updateTodo(data: Todo, id_token: string){
    const headers = {
      Authorization: id_token,
    };
    return this.httpService.put(`${environment.baseRootApi}/checklists/${data.id}`, data, { headers }).pipe(map((response) => response.data));
  }

  deleteTodo(id: number, id_token: string){
    const headers = {
      Authorization: id_token,
    };
    return this.httpService.delete(`${environment.baseRootApi}/checklists/${id}`, { headers }).pipe(map((response) => response.data));
  }
}
