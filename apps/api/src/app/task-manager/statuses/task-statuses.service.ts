import {Injectable} from "@nestjs/common";
import {LoggedHttpService} from "../../logged-http-client/logged-http.service";
import {environment} from "../../../environments/environment";
import {map} from "rxjs/operators";

@Injectable()
export class TaskStatusesService {
  constructor(
    private httpService: LoggedHttpService,
  ) {
  }

  getAll(id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService.get(`${environment.baseRootApi}/statuses`, { headers })
      .pipe(map((response) => response.data));
  }
}
