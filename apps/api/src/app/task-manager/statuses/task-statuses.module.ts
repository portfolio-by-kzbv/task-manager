import { Module } from "@nestjs/common";
import { TaskStatusesService } from "./task-statuses.service";
import { TaskStatusesController } from "./task-statuses.controller";
import { LoggedHttpClientModule } from "../../logged-http-client/logged-http-client.module";

@Module({
  imports: [
    LoggedHttpClientModule
  ],
  controllers: [TaskStatusesController],
  providers: [
    TaskStatusesService
  ],
})
export class TaskStatusesModule {}
