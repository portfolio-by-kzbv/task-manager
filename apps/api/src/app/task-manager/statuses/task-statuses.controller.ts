import { Controller, Get, Headers } from "@nestjs/common";
import { TaskStatusesService } from "./task-statuses.service";

@Controller('task-manager/statuses')
export class TaskStatusesController {
  constructor(private taskStatusesService: TaskStatusesService) {
  }

  @Get()
  findAll(@Headers('authorization') id_token: string) {
    return this.taskStatusesService.getAll(id_token);
  }
}
