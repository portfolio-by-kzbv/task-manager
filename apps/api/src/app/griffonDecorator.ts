import {
  createParamDecorator,
  ExecutionContext,
  UnauthorizedException,
} from '@nestjs/common';

export const GriffonAuth = createParamDecorator(
  (_data: unknown, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest();
    const authHeader = request.header('Authorization');
    if (!authHeader) {
      throw new UnauthorizedException({ status: 401 });
    }
    const id_token = authHeader.replace('Bearer ');
    const payload = id_token.split('.')[1];
    const decoded = JSON.parse(Buffer.from(payload, 'base64').toString());
    return decoded;
  }
);
