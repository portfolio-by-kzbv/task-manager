import {
  BadRequestException,
  Body,
  Controller,
  ForbiddenException,
  Headers,
  HttpCode,
  Logger,
  Post,
} from '@nestjs/common';

import { ChatRequest, ChatResponse } from '@dar/api-interfaces';
import { environment } from '../../environments/environment.dev';
import { ChatService } from './chat.service';
import { Observable, of } from 'rxjs';

@Controller('chat')
export class ChatController {
  constructor(private readonly chatService: ChatService) {}

  @Post('/create-meeting')
  @HttpCode(200)
  createMeeting(@Body() data: ChatRequest): ChatResponse {
    if (data.token !== environment.meetingToken) {
      Logger.error(`Token is invalid: ${data.token}`);
      throw new ForbiddenException();
    }

    if (data.user_mentions === undefined || data.user_mentions === null) {
      Logger.error(`There is no user`);
      throw new BadRequestException('No user has been provided');
    }

    this.chatService.createMeeting(data);

    const response: ChatResponse = {
      response_type: 'ephemeral',
      text: `Wait a minute, I'll do my best to create a room with users: ${data.user_mentions}!`,
    };

    return response;
  }
}
