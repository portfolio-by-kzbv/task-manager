import { ChatRequest, ChatResponse } from '@dar/api-interfaces';
import { HttpService, Injectable, Logger } from '@nestjs/common';

let i = 0;

@Injectable()
export class ChatService {
  constructor(private readonly httpService: HttpService) {}

  createMeeting(data: ChatRequest) {
    const response: ChatResponse = {
      response_type: 'in_channel',
      text: `https://dms-besedka.dar-dev.zone/web/${i++}`,
    };

    Logger.log(`ChatResponse is ${JSON.stringify(response)}`);

    try {
      this.httpService
        .post(data.response_url, response)
        .subscribe((res) => console.log(res));

      Logger.log(
        `ChatResponse has been successfully sent to ${data.response_url}`
      );
    } catch (error) {
      Logger.error(error.message);
    }
  }
}
