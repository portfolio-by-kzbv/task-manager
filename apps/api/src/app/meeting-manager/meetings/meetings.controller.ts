import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Res,
  Headers,
} from '@nestjs/common';
import { MeetingService } from './meetings.service';
import { Response } from 'express';
import { GriffonAuth } from '../../griffonDecorator';

@Controller('meetings')
export class MeetingController {
  constructor(private readonly meetingService: MeetingService) {}

  @Get()
  findAll(@Res() res: Response, @GriffonAuth() auth) {
    const { email } = auth;
    return this.meetingService.getAll(res, email);
  }

  @Post()
  createEvent(@Res() res: Response, @Body() data, @GriffonAuth() auth, @Headers('authorization') id_token: string) {
    const { email } = auth;
    return this.meetingService.createEvent(res, data, email, id_token);
  }

  @Delete(':id')
  deleteEvent(@Res() res: Response, @Param('id') id, @GriffonAuth() auth) {
    const { email } = auth;
    return this.meetingService.deleteMeeting(res, id, email);
  }

  @Put()
  updateEvent(@Res() res: Response, @Body() data, @GriffonAuth() auth) {
    const { email } = auth;
    return this.meetingService.updateEvent(res, data, email);
  }

  @Get(':id')
  findOne(@Res() res: Response, @Param('id') id: string, @GriffonAuth() auth, @Headers('authorization') id_token: string) {
    const { email } = auth;
    return this.meetingService.getMeeting(res, id, email, id_token);
  }

  @Get(':id/user-recordings')
  getUserREcordings(@Headers('authorization') id_token: string, @Param('id') id: string) {
    return this.meetingService.getRecordings(id, id_token);
  }
}
