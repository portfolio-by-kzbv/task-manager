import { Injectable } from '@nestjs/common';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
import { LoggedHttpService } from '../../logged-http-client/logged-http.service';
import { endOfWeek, subWeeks } from 'date-fns';
import { join } from 'path';
import { Response } from 'express';
import { Meeting } from '@dar/api-interfaces';
import { calendar_v3 } from 'googleapis/build/src/apis/calendar/v3';
import { JWT } from 'google-auth-library';

const SCOPES = ['https://www.googleapis.com/auth/calendar'];

const MAX_MEETINGS_PER_REQUEST = 200;

const TIMEZONE = 'GMT+06:00';

@Injectable()
export class MeetingService {
  constructor(private httpService: LoggedHttpService) {}

  getAll(response: Response, email: string) {
    const calendar = this.getClient(email);
    calendar.events.list(
      {
        calendarId: 'primary',
        maxResults: MAX_MEETINGS_PER_REQUEST,
        timeMin: subWeeks(new Date(), 2).toISOString(),
        timeMax: endOfWeek(new Date()).toISOString(),
        singleEvents: true,
        orderBy: 'startTime',
      },
      (err, res) => {
        if (err) {
          console.log(`The API returned an error: ${err}`);
        }

        const appointments = this.eventsToMeeting(res.data ? res.data.items : []);
        response.status(200).json({
          meetings: appointments,
          meta: {
            total: appointments.length,
          },
        });
      }
    );
  }

  createEvent(response: Response, data: Meeting, email: string, id_token) {
    const calendar = this.getClient(email);
    const externalParticipants = data.externalParticipants ? data.externalParticipants : [];

    const event: calendar_v3.Schema$Event = {
      summary: data.title,
      location: data.location,
      description: data.description,
      start: {
        dateTime: data.startTime,
        timeZone: TIMEZONE,
      },
      end: {
        dateTime: data.endTime,
        timeZone: TIMEZONE,
      },
      organizer: {
        email: email,
        self: false,
      },
      creator: {
        email,
      },
      recurrence: data.recurrence ? [data.recurrence] : [],
      attendees: [...data.participants, ...externalParticipants].map((attendee) => ({ email: attendee })),
      reminders: {
        useDefault: true,
      },
    };
    calendar.events.insert(
      {
        calendarId: 'primary',
        requestBody: event,
      },
      (err, event) => {
        if (err) {
          console.log('There was an error contacting the Calendar service: ' + err);
          response.status(500).json(err);
          return;
        }
        const headers = {
          Authorization: id_token,
        };
        const secretaryData = {
          meeting_id: event.data.id,
          secretary_id: data.secretary,
        };
        this.httpService
          .post(`${environment.meetingsNotesApi}/meeting-settings`, secretaryData, { headers })
          .toPromise()
          .then(() => response.status(201).json(this.eventToMeeting(event.data)));
      }
    );
  }

  updateEvent(response: Response, data: Meeting, email: string) {
    const calendar = this.getClient(email);
    const externalParticipants = data.externalParticipants ? data.externalParticipants : [];

    const event: calendar_v3.Schema$Event = {
      summary: data.title,
      location: data.location,
      description: data.description,
      start: {
        dateTime: data.startTime,
        timeZone: TIMEZONE,
      },
      end: {
        dateTime: data.endTime,
        timeZone: TIMEZONE,
      },
      organizer: {
        email: email,
      },
      creator: {
        email,
      },
      recurrence: data.recurrence ? [data.recurrence] : [],
      attendees: [...data.participants, ...externalParticipants].map((attendee) => ({ email: attendee })),
      reminders: {
        useDefault: true,
      },
    };
    calendar.events.update(
      {
        eventId: data.id,
        calendarId: 'primary',
        requestBody: event,
      },
      (err, event) => {
        if (err) {
          console.log('There was an error contacting the Calendar service: ' + err);
          response.status(500).json(err);
          return;
        }
        response.status(201).json(this.eventToMeeting(event.data));
      }
    );
  }

  deleteMeeting(response: Response, id: string, email: string) {
    const calendar = this.getClient(email);
    calendar.events.delete(
      {
        calendarId: 'primary',
        eventId: id,
      },
      (err, event) => {
        if (err) {
          console.log('There was an error contacting the Calendar service: ' + err);
          response.status(500).json(err);
          return;
        }
        response.status(201).json({});
      }
    );
  }

  getMeeting(response: Response, id: string, email: string, id_token) {
    const calendar = this.getClient(email);
    calendar.events.get(
      {
        calendarId: 'primary',
        eventId: id,
      },
      (err, res) => {
        if (err) {
          console.log(`The API returned an error: ${err}`);
          response.status(404).json('nothing found');
        }
        response.status(200).json(this.eventToMeeting(res.data));
      }
    );
  }

  getRecordings(meetingId: string, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .get(`${environment.meetingsRootApi}/meetings/${meetingId}/user-recordings`, { headers })
      .pipe(map((response) => response.data));
  }

  private getClient(email: string) {
    const client = new JWT({
      keyFile: join(__dirname, './assets/credentials.json'),
      scopes: SCOPES,
      subject: email,
    });

    return new calendar_v3.Calendar({ auth: client });
  }

  private eventsToMeeting(items: calendar_v3.Schema$Event[]): Meeting[] {
    return items.map((appointment) => {
      return this.eventToMeeting(appointment);
    });
  }

  private eventToMeeting(appointment: calendar_v3.Schema$Event): Meeting {
    return {
      createdAt: appointment.created,
      description: appointment.description,
      endTime: appointment.end.dateTime || appointment.end.date,
      externalParticipants: [],
      id: appointment.id,
      location: appointment.location,
      owner: null,
      ownerEmail: appointment.creator.email,
      participants: appointment.attendees ? appointment.attendees.map((attendee) => attendee.email) : [],
      recurrence: Array.isArray(appointment.recurrence) ? appointment.recurrence[0] : null,
      recurringEventId: appointment.recurringEventId,
      startTime: appointment.start.dateTime || appointment.start.date,
      status: appointment.status,
      title: appointment.summary,
      updatedAt: appointment.created,
      videoConfUrl: '',
    };
  }
}
