import { Module } from "@nestjs/common";
import { LoggedHttpClientModule } from "../../logged-http-client/logged-http-client.module";
import { MeetingController } from "./meetings.controller";
import { MeetingService } from "./meetings.service";

@Module({
  imports: [LoggedHttpClientModule],
  controllers: [MeetingController],
  providers: [MeetingService],
})
export class MeetingModule { }
