import { MeetingSettings } from '@dar/api-interfaces';
import { Injectable } from '@nestjs/common';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
import { LoggedHttpService } from '../../logged-http-client/logged-http.service';

@Injectable()
export class SettingsService {
  constructor(private httpService: LoggedHttpService) {}

  getMeetingSettings(meeting_id: string, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .get(`${environment.meetingsNotesApi}/meeting-settings/${meeting_id}`, { headers })
      .pipe(map((response) => response.data));
  }

  createMeetingSettings(data: MeetingSettings, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .post(`${environment.meetingsNotesApi}/meeting-settings`, data, { headers })
      .pipe(map((response) => response.data));
  }
}
