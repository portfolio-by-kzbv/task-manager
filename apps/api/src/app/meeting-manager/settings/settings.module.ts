import { Module } from '@nestjs/common';
import { LoggedHttpClientModule } from '../../logged-http-client/logged-http-client.module';
import { SettingsController } from './settings.controller';
import { SettingsService } from './settings.service';

@Module({
  imports: [LoggedHttpClientModule],
  controllers: [SettingsController],
  providers: [SettingsService],
})
export class SettingsModule {}
