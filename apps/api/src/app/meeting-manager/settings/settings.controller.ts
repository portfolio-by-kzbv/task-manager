import { MeetingSettingsDTO} from '@dar/api-interfaces';
import { Body, Controller, Get, Param, Post, Headers } from '@nestjs/common';
import { SettingsService } from './settings.service';


@Controller('meeting-manager/settings')
export class SettingsController {
  constructor(private settingsService: SettingsService) {}

  @Get('/:id')
  getSettings(@Headers('authorization') id_token: string, @Param('id') meeting_id: string,) {
    return this.settingsService.getMeetingSettings(meeting_id, id_token);
  }

  @Post()
  createSettings(@Headers('authorization') id_token: string, @Body() data: MeetingSettingsDTO) {
    return this.settingsService.createMeetingSettings(data, id_token);
  }

}
