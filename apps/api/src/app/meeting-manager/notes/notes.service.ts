import { NotesDTO, NotesQueryParams } from '@dar/api-interfaces';
import { Injectable } from '@nestjs/common';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
import { LoggedHttpService } from '../../logged-http-client/logged-http.service';

@Injectable()
export class NotesService {
  constructor(private httpService: LoggedHttpService) {}

  getNotes(query: NotesQueryParams, id_token: string) {
    const headers = {
      Authorization: id_token,
    };

    return this.httpService
      .get(`${environment.meetingsNotesApi}/notes`, {
        headers,
        params: {
          ...query,
        },
      })
      .pipe(map((response) => response.data.rows));
  }

  createNote(data: NotesDTO, id_token: string) {
    const headers = {
      Authorization: id_token,
    };

    return this.httpService
      .post(`${environment.meetingsNotesApi}/notes`, data, { headers })
      .pipe(map((response) => response.data));
  }

  updateNote(id: number, data: NotesDTO, id_token: string) {
    const headers = {
      Authorization: id_token,
    };

    return this.httpService
      .put(`${environment.meetingsNotesApi}/notes/${id}`, data, { headers })
      .pipe(map((response) => response.data));
  }

  deleteNote(id: number, id_token: string) {
    const headers = {
      Authorization: id_token,
    };

    return this.httpService
      .delete(`${environment.meetingsNotesApi}/notes/${id}`, { headers })
      .pipe(map((response) => response.data));
  }
}
