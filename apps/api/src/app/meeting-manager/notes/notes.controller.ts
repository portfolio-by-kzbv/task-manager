import { NotesDTO, NotesQueryParams } from '@dar/api-interfaces';
import { Body, Controller, Delete, Get, Headers, Param, Post, Put, Query } from '@nestjs/common';
import { NotesService } from './notes.service';

@Controller('meeting-manager/notes')
export class NotesController {
  constructor(private notesService: NotesService) {}

  @Get()
  getNotes(@Headers('authorization') id_token: string, @Query() query: NotesQueryParams) {
    return this.notesService.getNotes(query, id_token);
  }

  @Post()
  createNote(@Headers('authorization') id_token: string, @Body() data: NotesDTO) {
    return this.notesService.createNote(data, id_token);
  }

  @Put('/:id')
  updateNote(@Headers('authorization') id_token: string, @Param('id') note_id, @Body() data: NotesDTO) {
    return this.notesService.updateNote(note_id, data, id_token);
  }

  @Delete('/:id')
  deleteNote(@Headers('authorization') id_token: string, @Param('id') note_id) {
    return this.notesService.deleteNote(note_id, id_token);
  }
}
