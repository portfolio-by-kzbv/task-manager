import { Module } from '@nestjs/common';
import { LoggedHttpClientModule } from '../../logged-http-client/logged-http-client.module';
import { NotesController } from './notes.controller';
import { NotesService } from './notes.service';

@Module({
  imports: [LoggedHttpClientModule],
  controllers: [NotesController],
  providers: [NotesService],
})
export class NotesModule {}
