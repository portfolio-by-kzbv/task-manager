import { MoMDTO, MoMQueryParams } from '@dar/api-interfaces';
import { Body, Controller, Get, Header, Param, Post, Put, Query, Delete, Headers } from '@nestjs/common';
import { MoMService } from './mom.service';

@Controller('meeting-manager/mom')
export class MoMController {
  constructor(private momService: MoMService) {}

  @Get()
  getMoM(@Headers('authorization') id_token: string, @Query() query: MoMQueryParams) {
    return this.momService.getMoM(query, id_token);
  }

  @Post()
  createMoM(@Headers('authorization') id_token: string, @Body() data: MoMDTO) {
    return this.momService.createMoM(data, id_token);
  }

  @Put('/:id')
  updateMoM(@Headers('authorization') id_token: string, @Param('id') mom_id: number, @Body() data: MoMDTO) {
    return this.momService.updateMoM(mom_id, data, id_token);
  }

  @Delete('/:id')
  deleteMoM(@Headers('authorization') id_token: string, @Param('id') mom_id: number) {
    return this.momService.deleteMoM(mom_id, id_token);
  }
}
