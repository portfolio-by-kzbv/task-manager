import { Module } from '@nestjs/common';
import { LoggedHttpClientModule } from '../../logged-http-client/logged-http-client.module';
import { MoMController } from './mom.controller';
import { MoMService } from './mom.service';

@Module({
  imports: [LoggedHttpClientModule],
  controllers: [MoMController],
  providers: [MoMService],
})
export class MoMModule {}
