import { MoMDTO, MoMQueryParams } from '@dar/api-interfaces';
import { Injectable } from '@nestjs/common';
import { environment } from '../../../environments/environment';
import { map } from 'rxjs/operators';
import { LoggedHttpService } from '../../logged-http-client/logged-http.service';

@Injectable()
export class MoMService {
  constructor(private httpService: LoggedHttpService) {}

  getMoM(query: MoMQueryParams, id_token: string) {
    const headers = {
      Authorization: id_token,
    };

    return this.httpService
      .get(`${environment.meetingsNotesApi}/mom`, {
        headers,
        params: {
          ...query,
        },
      })
      .pipe(map((response) => response.data.rows[0]));
  }

  createMoM(data: MoMDTO, id_token: string) {
    const headers = {
      Authorization: id_token,
    };

    return this.httpService
      .post(`${environment.meetingsNotesApi}/mom`, data, { headers })
      .pipe(map((response) => response.data));
  }

  updateMoM(id: number, data: MoMDTO, id_token: string) {
    const headers = {
      Authorization: id_token,
    };

    return this.httpService
      .put(`${environment.meetingsNotesApi}/mom/${id}`, data, { headers })
      .pipe(map((response) => response.data));
  }

  deleteMoM(id: number, id_token: string) {
    const headers = {
      Authorization: id_token,
    };
    return this.httpService
      .delete(`${environment.meetingsNotesApi}/mom/${id}`, { headers })
      .pipe(map((response) => response.data));
  }
}
