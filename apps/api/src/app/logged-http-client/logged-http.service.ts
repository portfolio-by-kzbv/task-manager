import { HttpService, Logger, Injectable, HttpException } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
import { catchError, tap } from 'rxjs/operators';

@Injectable()
export class LoggedHttpService {
  constructor(private httpService: HttpService) {}

  get<T = any>(url: string, config?: AxiosRequestConfig): Observable<AxiosResponse<T>> {
    return this.logResponse(this.httpService.get<T>(url, config), url, config);
  }

  post<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Observable<AxiosResponse<T>> {
    return this.logResponse(this.httpService.post<T>(url, data, config), url, config, data);
  }

  put<T = any>(url: string, data?: any, config?: AxiosRequestConfig): Observable<AxiosResponse<T>> {
    return this.logResponse(this.httpService.put<T>(url, data, config), url, config, data);
  }

  delete<T = any>(url: string, config?: AxiosRequestConfig): Observable<AxiosResponse<T>> {
    return this.logResponse(this.httpService.delete<T>(url, config), url, config);
  }

  request<T = any>(config?: AxiosRequestConfig): Observable<AxiosResponse<T>> {
    return this.logResponse(this.httpService.request<T>(config), config ? config.url : '', config);
  }

  logResponse(request: Observable<AxiosResponse>, url: string, config?: AxiosRequestConfig, data?: any) {
    Logger.log(
      `Request data: [${url}] [${JSON.stringify(config ? config : {})}] ${JSON.stringify(data ? data : {})}`,
      'ExternalHttpRequest'
    );
    return request.pipe(
      catchError((error: AxiosError) => {
        Logger.error(
          `[${url}] [${error.message} - ${
            error.response && error.response.data ? JSON.stringify(error.response.data) : ''
          }]`,
          '',
          'ExternalHttpRequest'
        );
        throw new HttpException(error.response?.data, error.response?.status);
      }),
      tap((resp) => {
        Logger.log(`Response data: [${url}] ${JSON.stringify(resp.data)}`, 'ExternalHttpRequest');
      })
    );
  }
}
