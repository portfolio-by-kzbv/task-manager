import { Module, HttpModule } from '@nestjs/common';
import { environment } from '../../environments/environment';
import { LoggedHttpService } from './logged-http.service';

@Module({
  imports: [
    HttpModule.register(environment.httpModuleOptions)
  ],
  exports: [
    HttpModule,
    LoggedHttpService
  ],
  providers: [
    LoggedHttpService
  ]
})
export class LoggedHttpClientModule { }
