import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { join } from 'path';
import { ChatModule } from './chat/chat.module';
import { LoggedHttpClientModule } from './logged-http-client/logged-http-client.module';
import { TaskManagerModule } from './task-manager/tasks/tasks.module';
import { TaskStatusesModule } from './task-manager/statuses/task-statuses.module';
import { ChecklistModule } from './task-manager/checklist/checklist.module';
import { UsersModule } from './users/users.module';
import { ClustersModule } from './task-manager/clusters/clusters.module';
import { ProjectsModule } from './task-manager/projects/projects.module';
import { MeetingModule } from './meeting-manager/meetings/meetings.module';
import { MoMModule } from './meeting-manager/mom/mom.module';
import { NotesModule } from './meeting-manager/notes/notes.module';
import { SettingsModule } from './meeting-manager/settings/settings.module';
import { MeetingGateway } from './websocket/meeting.gateway';

@Module({
  imports: [
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '..', 'dar-front'),
    }),
    ChatModule,
    LoggedHttpClientModule,
    TaskManagerModule,
    TaskStatusesModule,
    ChecklistModule,
    UsersModule,
    ClustersModule,
    ProjectsModule,
    MeetingModule,
    MoMModule,
    NotesModule,
    SettingsModule,
  ],
  controllers: [AppController],
  providers: [AppService, MeetingGateway],
})
export class AppModule {}
