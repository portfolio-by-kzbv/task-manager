import {
  WebSocketGateway,
  SubscribeMessage,
  MessageBody,
  ConnectedSocket,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';
import { Logger } from '@nestjs/common';
import WebSocket, { Server } from 'ws';

const rooms: {
  [roomId: string]: {
    [userId: string]: WebSocket[];
  };
} = {};
@WebSocketGateway()
export class MeetingGateway implements OnGatewayConnection, OnGatewayDisconnect {
  @WebSocketServer() server: Server;

  private logger: Logger = new Logger('MeetingGateway');

  @SubscribeMessage('message')
  handleMessage(@MessageBody() data: unknown, @ConnectedSocket() client: WebSocket) {
    const event = 'message';
    const roomId = client['id'].split('##')[0];
    const authorId = client['id'].split('##')[1];
    if (rooms[roomId]) {
      const now = new Date();
      Object.keys(rooms[roomId]).forEach((userId) => {
        rooms[roomId][userId].forEach((socket) => {
          socket.send(JSON.stringify({ type: event, data: { text: data, userId: authorId, roomId, time: now } }));
        });
      });
    }
  }

  @SubscribeMessage('heartbeat')
  handleHeartbeat(@MessageBody() data: any, @ConnectedSocket() client: WebSocket) {
    client.send(JSON.stringify({ type: 'heartbeat' }));
  }

  handleDisconnect(client: WebSocket) {
    this.logger.log(`Client disconnected: ${client}`);
  }

  handleConnection(client: WebSocket, req) {
    this.logger.log(`Client connected: ${client}`);
    const data = req.url.replace('/?').split('&');
    if (data.length === 2) {
      const roomId = data[0].split('=')[1];
      const userId = data[1].split('=')[1];
      if (roomId && userId) {
        if (!rooms[roomId]) {
          rooms[roomId] = {};
        }
        if (!rooms[roomId][userId]) {
          rooms[roomId][userId] = [];
        }
        rooms[roomId][userId].push(client);
        client['id'] = `${roomId}##${userId}`;
        client.send(JSON.stringify({ type: 'event', data: 'Connected' }));
        return;
      }
    }
    client.close();
  }
}
